#----------------------------------------------------------------------------
# If you're Melissa running on Fifteen, run with *this* command from within build/:
# cmake -DCMAKE_C_COMPILER=gcc-5.1.0 -DCMAKE_CXX_COMPILER=g++-5.1.0 ..
# If this is on ada (TAMU's computing cluster)
# cmake .. -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc
#----------------------------------------------------------------------------
# Setup the project
CMAKE_MINIMUM_REQUIRED(VERSION 2.7)
PROJECT(K37 CXX)

OPTION(GDML "USE GEANT4 GDML LIBRARIES" OFF)
OPTION(PROVENANCE "Create provenance files for reproducability" OFF)

FIND_PACKAGE(Geant4 REQUIRED ui_all vis_all)
INCLUDE(FindROOT.cmake)
INCLUDE(${Geant4_USE_FILE})

INCLUDE_DIRECTORIES(include include/external)
INCLUDE_DIRECTORIES(SYSTEM ${ROOT_INCLUDE_DIR})

LINK_DIRECTORIES(${ROOT_LIBRARY_DIR})

ADD_DEFINITIONS(-g -O3 -std=c++11 -W -Wall -Wpointer-arith)

#SET(CMAKE_BUILD_TYPE ReLWithDebInfo)
SET(MY_MULTITHREADED_FLAGS "-DK37_MULTITHREADING")  # change this flag if you don't want to use multithreading.  Might break it.
SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${MY_MULTITHREADED_FLAGS}")



SET (CONFIGFILES_ "\"${CMAKE_BINARY_DIR}/ConfigurationFiles/\"")  # not needed now?
SET (CONFIGURATION_DIRECTORY "${PROJECT_BINARY_DIR}/ConfigurationFiles/")  
SET (OUTDIR "\"${PROJECT_BINARY_DIR}/Output\"")  # no trailing slash?

configure_file (
	"${PROJECT_SOURCE_DIR}/include/K37Config.hh.in"  ##  MELISSA LOOK HERE!  add metadatafilename to configuration, maybe...
	"${PROJECT_SOURCE_DIR}/include/K37Config.hh"
	)

configure_file (
	"${PROJECT_SOURCE_DIR}/SetupScripts/IOconfiguration.txt.in"
	"${PROJECT_BINARY_DIR}/SetupScripts/IOconfiguration.txt"
	)

ADD_SUBDIRECTORY(src)
ADD_EXECUTABLE(K37 K37.cc)
TARGET_LINK_LIBRARIES(K37 K37_lib ${Geant4_LIBRARIES} Core RIO Tree)


#------------------------------------
set(K37_SCRIPTS
  vis_PreInit.mac
  vis_PostInit.mac
  Setup_PreInit.mac
  Setup_PostInit.mac
  )

foreach(_script ${K37_SCRIPTS})
  configure_file(
    ${PROJECT_SOURCE_DIR}/SetupScripts/${_script}
    ${PROJECT_BINARY_DIR}/SetupScripts/${_script}
    COPYONLY
    )
endforeach()

#------------------------------------
set(CONFIGFILES
  K_37_INPUT.txt
  upper_strip_detector_x.res
  upper_strip_detector_y.res
  lower_strip_detector_x.res
  lower_strip_detector_y.res
  charge_state_dist.res
  )

foreach(_file ${CONFIGFILES})
  configure_file(
    ${CMAKE_SOURCE_DIR}/ConfigurationFiles/${_file}
    ${CMAKE_BINARY_DIR}/ConfigurationFiles/${_file}
    COPYONLY
    )
endforeach()

# Make a new directory inside build/ to save runs:
file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/Output)

#----------------------------------------------------------------------------


