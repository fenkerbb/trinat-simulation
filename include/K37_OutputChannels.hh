#include "TDC_Channel.hh"
#include "Generic_Channel.hh"
#include "K37_Data.hh"

// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 
class set_of_channels
{
public:
	set_of_channels();
	~set_of_channels();
	void initialize_the_channels();
	
public:
	Generic_Channel *qdc_upper_pmt;
	Generic_Channel *qdc_lower_pmt;

	// Do I need this stuff??
	Generic_Channel *qdc_upper_pmt_ae;
	Generic_Channel *qdc_lower_pmt_ae;
	Generic_Channel *qdc_upper_pmt_p;
	Generic_Channel *qdc_lower_pmt_p;
	Generic_Channel *qdc_upper_pmt_e;
	Generic_Channel *qdc_lower_pmt_e;
	Generic_Channel *qdc_upper_pmt_g;
	Generic_Channel *qdc_lower_pmt_g;
	
//	Generic_Channel *upper_scint_E;
//	Generic_Channel *lower_scint_E;
	
	//-------------------------------------------------------------------
	Generic_Channel *qdc_upper_dssd;
	Generic_Channel *qdc_lower_dssd;
	Generic_Channel *qdc_upper_dssd_ae;
	Generic_Channel *qdc_lower_dssd_ae;
	Generic_Channel *qdc_upper_dssd_p;
	Generic_Channel *qdc_lower_dssd_p;
	Generic_Channel *qdc_upper_dssd_e;
	Generic_Channel *qdc_lower_dssd_e;
	Generic_Channel *qdc_upper_dssd_g;
	Generic_Channel *qdc_lower_dssd_g;
	Generic_Channel *qdc_upper_mirror;
	Generic_Channel *qdc_lower_mirror;
	Generic_Channel *qdc_upper_BeWindow;
	Generic_Channel *qdc_lower_BeWindow;
	//-------------------------------------------------------------------
	Generic_Channel *vnim_event_type_;
	//-------------------------------------------------------------------
	TDC_Channel *dl_x_pos, *dl_z_pos;
	TDC_Channel *hex_x_pos, *hex_z_pos;
	
	TDC_Channel *tdc_scint_top_, *tdc_scint_bottom_;
	TDC_Channel *tdc_ion_mcp_, *tdc_electron_mcp_;
	TDC_Channel *tdc_photo_diode_;
//	TDC_Channel *volumeList;
	TDC_Channel *strip_detector_upper_x_, *strip_detector_upper_y_;
	TDC_Channel *strip_detector_lower_x_, *strip_detector_lower_y_;
	TDC_Channel *strip_detector_upper_x_T, *strip_detector_upper_y_T;
	TDC_Channel *strip_detector_lower_x_T, *strip_detector_lower_y_T;

	Generic_Channel *electron_kinetic_energy_generated_, *electron_mu_generated_;
	Generic_Channel *recoil_mu_generated_, /* *run_action_, */ *recoil_mcp_particle_;
	Generic_Channel *recoil_charge_state_;
	Generic_Channel *two_percent_branch_;
	Generic_Channel *ttlbit_op_beam_, *upper_pmt_particle_, *lower_pmt_particle_;
//	Generic_Channel *ttlbit_sigmaplus_;
	
	Generic_Channel *num_hits_r_mcp_;
	Generic_Channel *num_hits_e_mcp_, *electron_mcp_particle_;
	
	// Here, have some more channels.
	Generic_Channel *gen_Ebeta_, *gen_Tbeta_, *gen_costheta_;
	Generic_Channel *naive_hit_t_, *naive_hit_b_;
	Generic_Channel *gen_xhit_t_, *gen_yhit_t_, *gen_rhit_t_;
	Generic_Channel *gen_xhit_b_, *gen_yhit_b_, *gen_rhit_b_;
};
