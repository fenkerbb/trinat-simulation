// ... 
// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 2013
// 
// The K37Run class is specifically here for collecting data from events in multithreaded mode.
// ......also, maybe this is never even used????  
// Also, it probably *shouldn't* be used.  ...no wait, that's actually fine, and expected??

#ifndef K37Run_h
#define K37Run_h 1

//
#include <map>
#include <string>

//
#include <TBranch.h> // because otherwise it doesn't know what a Double_t is...

//
#include "G4Run.hh"
#include "G4SDManager.hh"
#include "G4DigiManager.hh"
#include "G4THitsMap.hh"
#include "globals.hh"

//
#include "K37RunManagerMT.hh"
#include "K37RunAction.hh"
#include "K37PrimaryGeneratorAction.hh"
#include "K37ScintillatorDigitizer.hh"
#include "K37StripDetectorDigitizer.hh"

#include "K37ElectronMCPHit.hh"
#include "K37RecoilMCPHit.hh"
#include "K37ScintillatorHit.hh" 
#include "K37EventAction.hh"

#include "K37_Data.hh"

using std::map;
using std::string;
using K37_ABC::K37_Data;


class K37Run : public G4Run
{
public:
	K37Run();
	virtual ~K37Run();
//	~K37Run();
	
	void Merge(const G4Run*);
	
	void RecordEvent(const G4Event*);
	int EventTypeSorter(const G4Event* evt);
	
	void IncrementGenerated()     { N_generated_total++;    };
	void IncrementAccepted()      { N_accepted_total++;     };
//	void IncrementNaiveAccepted() { naive_accepted++; }  // not used yet?

	int Get_Nevents() const       { return N_generated_total; };
	int Get_Naccepted() const     { return N_accepted_total;  };
	

//	void PrintResultsToScreen() const // must be a 'const' otherwise we can't call it from K37RunAction.  Because reasons, I guess.
//	{
//		G4cout << "------------------------------------------------------" << G4endl;
//		G4cout << "    All Threads" << G4endl;
//		G4cout << "  # Events ___________: " << N_generated_total << G4endl;
//		G4cout << "  # Accepted _________: " << N_accepted_total  << G4endl;
//		G4cout << "------------------------------------------------------" << G4endl;
//	};
	
	
private:
//-------------------------------------------------------------------
// These are actually threadlocal, but they'll get summed up before we print them out.
	int N_generated_total;
	int N_accepted_total;
	

	// ******VERY IMPORTANT**********
	// When using this function, always pass it the x-start
	// for the SD you wnat to fill!!!
//	void fillSDNtuples(vector<G4double> EDep_strip, G4int ntuple_number_start);  // not used??
	void fillSDNtuples(vector<G4double> energy_strip, G4String name);

	// V1190 TDC Clock beats every 97.65625 ns; output from analyzer is in
	// number of ticks.  See manual pg 13 for details.  I want to output the
	// number of ticks to the ntuple to match the analyzer.  To convert from ns
	// to number of ticks, divide by this number.
	const G4double v1190_factor_ns;
	
	
//-------------------------------------------------------------------
// for the specific event:
	Double_t decay_pos_x, decay_pos_y, decay_pos_z;                 // queried by Aggregator in K37RunAction::BeginOfRunAction(), which sets the branch addresses.
	Double_t decay_velocity_x, decay_velocity_y, decay_velocity_z;  // same.
	
	G4ThreeVector naive_hitposition;  // from K37EventAction
	bool naive_hit_top, naive_hit_bottom;  // from K37EventAction
//-------------------------------------------------------------------
	G4int recoil_mcp_collection_id;  // from K37EventAction
	G4int electron_mcp_collection_id;  // from K37EventAction
	
};



/*
class K37Run : public G4Run
{
public:
public:
    G4Run();
    virtual ~G4Run();

private:
    // These copy constructor and = operator must not be used.
    G4Run(const G4Run &) {;}
    G4Run& operator=(const G4Run &) { return *this; }

protected:
    G4int runID;
    G4int numberOfEvent;
    G4int numberOfEventToBeProcessed;
    G4HCtable* HCtable;
    G4DCtable* DCtable;
    G4String randomNumberStatus;
    std::vector<const G4Event*>* eventVector;

public: // with description
    virtual void RecordEvent(const G4Event*);
    //  Method to be overwritten by the user for recording events in this run.
    //  In such a case, it is the user's responsibility to increment numberOfEvent.
    //  Also, user's run class object must be instantiated in user's runAction.
    virtual void Merge(const G4Run*);
    //  Method to be overwritten by the user for merging local G4Run object to 
    //  the global G4Run object.

public: // with description
    inline G4int GetRunID() const
    { return runID; }
    //  Returns the run ID. Run ID is set by G4RunManager.
    inline G4int GetNumberOfEvent() const
    { return numberOfEvent; }
    //  Returns number of events processed in this run. The number is
    // incremented at the end of each event processing.
    inline G4int GetNumberOfEventToBeProcessed() const
    { return numberOfEventToBeProcessed; }
    inline const G4HCtable* GetHCtable() const
    { return HCtable; }
    //  List of names of hits collection
    inline const G4DCtable* GetDCtable() const
    { return DCtable; }
    //  List of names of digi collection
    inline const G4String& GetRandomNumberStatus() const
    { return randomNumberStatus; }
    // Return random number status at the beginning of this run
public:
    inline void SetRunID(G4int id)
    { runID = id; }
    inline void SetNumberOfEventToBeProcessed(G4int n_ev)
    { numberOfEventToBeProcessed = n_ev; }
    inline void SetHCtable(G4HCtable* HCtbl)
    { HCtable = HCtbl; }
    inline void SetDCtable(G4DCtable* DCtbl)
    { DCtable = DCtbl; }
    inline void SetRandomNumberStatus(G4String& st)
    { randomNumberStatus = st; }

public: // with description
    void StoreEvent(G4Event* evt);
    // Store a G4Event object until this run object is deleted.
    // Given the potential large memory size of G4Event and its datamember
    // objects stored in G4Event, the user must be careful and responsible for
    // not to store too many G4Event objects. This method is invoked by G4RunManager
    // if the user invokes G4EventManager::KeepTheCurrentEvent() or 
    // /event/keepCurrentEvent UI command while the particular event is in process
    // (typically in EndOfEventAction).
    inline const std::vector<const G4Event*>* GetEventVector() const
    { return eventVector; }
    // Return the event vector
};
*/


#endif
