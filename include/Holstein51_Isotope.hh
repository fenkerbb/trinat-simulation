// Authors:  
//     Spencer Behling, Benjamin Fenker, Melissa Anholm.
//     2013 - 

#ifndef Isotope_h
#define Isotope_h 1

#include <string>

class IsotopeMessenger;

class Holstein51_Isotope
{
public:
	Holstein51_Isotope(const std::string &filename_);
	~Holstein51_Isotope();
	//Get the beta assymetry as a function of kinetic energy of e+
	double A_beta   (const double &T_);
	//Get the neutrino assymetry as a function of kinetic energy of e+ 
	double B_nu     (const double &T_);
	//Get the beta neutrino correlation as a function of kinetic energy of e+ 
	double a_beta_nu(const double &T_);
	//Get the alignment as a function of kinetic energy of e+ 
	double c_align  (const double &T_);
	//Get zhi as a function of kinetic energy of e+ 
	double zhi      (const double &T_);
	//
	double b_Fierz  (const double &T_);
	
	// For debugging
	double f_1(double T);
	double f_4(double T);
	double f_7(double T);
	
	void ChangeRho (const double &NewRhoValue_);
	void Print(std::string ofname);
	void SetAxialFormFactor_gA(const double g_A);
	//This function can retrieve the value of a const stored in the 
	//isotope database. Really this should not be used in simulations
	//but is useful for debugging.
	double FindValue(const std::string &key_) const;
	
	//To make Holstein51_Isotope non-copyable
	Holstein51_Isotope(const Holstein51_Isotope &) = delete;
	const Holstein51_Isotope & operator=(const Holstein51_Isotope &) = delete;
	
	//stands for my Implement Holstein51_Isotope
	class ImplementI;
	//my Holstein51_Isotope
	ImplementI *mI;

	IsotopeMessenger* isotopeMessenger;   // messenger of this class
};
	

#endif
