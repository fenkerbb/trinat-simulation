// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 
// 2019 - 
// 
// The K37ActionInitialization class is specifically for use with multithreading in Geant4.10.x.
#ifndef K37ActionInitialization_h
#define K37ActionInitialization_h 1

//
#include "G4VUserActionInitialization.hh"

//
#include "K37CloudSetup.hh"
#include "K37_GlobalAggregator.hh"

//
class K37CloudSetup;
class GlobalAggregator;

// ----- *  ----- *  ----- *  ----- *  ----- *  ----- *  ----- *  ----- *  ----- *  ----- //
class K37ActionInitialization : public G4VUserActionInitialization
{
public:
	K37ActionInitialization();
	~K37ActionInitialization();
	
	void Build() const;
	void BuildForMaster() const;
	
	K37CloudSetup    * GetCloud()      { return the_cloud;      }
	GlobalAggregator * GetAggregator() { return the_aggregator; }
	
private:
	K37CloudSetup    * the_cloud;
	GlobalAggregator * the_aggregator;
	
/*
protected:
  void SetUserAction(G4VUserPrimaryGeneratorAction*) const;
  void SetUserAction(G4UserRunAction*) const;
  void SetUserAction(G4UserEventAction*) const;
  void SetUserAction(G4UserStackingAction*) const;
  void SetUserAction(G4UserTrackingAction*) const;
  void SetUserAction(G4UserSteppingAction*) const;
  // These methods should be used to define user's action classes.
*/
};


#endif
