// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 2013

#ifndef K37EventAction_h
#define K37EventAction_h 1

#include <map>  // not needed?
#include <string>  // not needed?
#include <vector>

#include "G4UserEventAction.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4Electron.hh"

#include "K37DetectorConstruction.hh"
#include "K37EventMessenger.hh"
#include "K37ContainerForStripInformation.hh"

#include "TBranch.h"  // Double_t.

using std::map;  // not needed?
using std::string;  // not needed?
using std::vector;

//class K37ContainerForStripInformation;  // where is this defined, anyway?
class K37EventMessenger;

class K37EventAction : public G4UserEventAction 
{
public:
	explicit K37EventAction();  
	~K37EventAction();
	
	void BeginOfEventAction(const G4Event*);
	void EndOfEventAction(const G4Event*);

	void SetElectronMCPthreshold(double t) {electron_mcp_threshold_ = t;}
	G4double GetElectronMCPthreshold() {return electron_mcp_threshold_;}
	
	UInt_t TTTL_OP_Beam;//, TTLBit_SigmaPlus;  // not needed anymore?
	UInt_t tdc_scint_bottom_count, tdc_scint_top_count;  // not needed anymore?
	
	
	// below:  things that have been transported to K37Run.xx.  but not well enough:
	Double_t decay_pos_x, decay_pos_y, decay_pos_z;
	Double_t decay_velocity_x, decay_velocity_y, decay_velocity_z;
	
	
//	G4ThreeVector naive_hitposition;  // to K37EventAction
//	bool naive_hit_top, naive_hit_bottom;  // to K37EventAction
	
//public:
//	G4double ReturnEventType() { return event_type; }
	
private:
	K37ContainerForStripInformation* stripHandler;  // do we really still want this here??

	G4int fullenergy1CollID;
	G4int fullenergy2CollID;
	G4int dedx1CollID;
	G4int dedx2CollID;

	G4int recoil_mcp_collection_id;  // not needed anymore? -- to K37Run, but maybe still needed here.
	G4int electron_mcp_collection_id;  // not needed anymore?  -- to K37Run, but maybe still needed here.
	
	G4Electron *theElectron;
	G4double emass;

	double electron_mcp_threshold_;  // do I need this still?  maybe not.
	
	K37EventMessenger *event_messenger_;
//-------------------------------------------------------------------
//-------------------------------------------------------------------
};

#endif


