// Authors: Spencer Behling 2014
//          Ben Fenker, probably.
//          Melissa Anholm 2018

#ifndef IsotopeMessenger_h
#define IsotopeMessenger_h 1

#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIdirectory.hh"
#include "G4UImessenger.hh"
#include "globals.hh"

#include "Holstein51_Isotope.hh"

//----------------------------------

class IsotopeMessenger: public G4UImessenger 
{
public:
	explicit IsotopeMessenger(/*ISO::*/Holstein51_Isotope* iso_);
	virtual  ~IsotopeMessenger();

	void SetNewValue(G4UIcommand*, G4String);

private:
	/*ISO::*/Holstein51_Isotope             *isotope;
	G4UIdirectory            *isotope_dir;
	G4UIcmdWithADouble       *set_rho;
	G4UIcmdWithoutParameter  *get_rho;
	G4UIcmdWithADouble       *set_axial_vector_form_factor;
};

//----------------------------------

#endif

