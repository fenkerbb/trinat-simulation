// Authors: Spencer Behling
// The point of this class is to hack in a quick 2% branch. It is 
// following the venerable software practice of cut and paste programming.

#ifndef K37EventGeneratorNoRecoilOrderEffects_H
#define K37EventGeneratorNoRecoilOrderEffects_H 1

#include <map>
#include <string>

#include "globals.hh"

#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"

// #include "Aggregator.hh"  // still included via K37EventGenerator.hh
#include "K37_Data.hh"
#include "K37EventGenerator.hh"  // Do we need you??  ... yes, yes we do.

//using AGG::Aggregator;
using K37_ABC::K37_Data;
using std::map;
using std::string;

class K37FermiFunction;
class particle;

class K37EventGeneratorNoRecoilOrderEffects 
{
protected:
	particle electron, parent, daughter, neutrino;
	G4double QValue, cosBetaNeutrinoAngle, eDotJ, nDotJ, eDotn;
	G4double Omega, testOmega, Zhi, BigA, BigB, LittleC, LittleA;
	K37FermiFunction* FF;
	// Used to only simulate events in a cone around the detectors.
	// Angular distributions are accurate within this cone
	G4double min_cos_theta_;

public:
	K37EventGeneratorNoRecoilOrderEffects();
	virtual ~K37EventGeneratorNoRecoilOrderEffects();
//	virtual void MakeEvent()=0;  // MJA:  never called.
//	virtual void MakeEvent(G4double polarization, G4double alignment,
// 	                       G4double recoil_charge)  = 0;  // MJA:  never called.
	
	G4double eMomentumX()     { return electron.X;     }
	G4double eMomentumY()     { return electron.Y;     }
	G4double eMomentumZ()     { return electron.Z;     }
	G4double eMomentumTheta() { return electron.Theta; }
	G4double eMomentumPhi()   { return electron.Phi;   }
	G4double eMomentumPmag()  { return electron.Pmag;  }
//	G4double eKineticEnergy() { return electron.T;     }
	G4double dMomentumX()     { return daughter.X;     }
	G4double dMomentumY()     { return daughter.Y;     }
	G4double dMomentumZ()     { return daughter.Z;     }
	G4double dMomentumTheta() { return daughter.Theta; }
	G4double dMomentumPhi()   { return daughter.Phi;   }
	G4double dMomentumPmag()  { return daughter.Pmag;  }
	G4double dKineticEnergy() { return daughter.T;     }
		
	void SetBigA(G4double BigA_);
	void SetMinCosTheta(G4double mct);
	void SetConeHalfAngle(G4double angle);
	
//	void MakeEvent();
	void MakeEvent(G4double polarization, G4double alignment /* , G4double recoil_charge*/ );
	void ResetGeneratedCounters();
	
	G4int GetNumMins();
	G4int GetNumPlus();

private:
	G4int numMins, numPlus;
};

/*
class JTW_EventNoRecoilOrderEffects: public K37EventGeneratorNoRecoilOrderEffects 
{
public:
	void MakeEvent();
	void MakeEvent(G4double polarization, G4double alignment,
	               G4double recoil_charge);
	void ResetGeneratedCounters();
	G4int GetNumMins();
	G4int GetNumPlus();
};
*/

#endif
