// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013
#ifndef LocalAggregator_h
#define LocalAggregator_h 1

// C/C++ headers
#include <map>
#include <string>
#include <fstream>

// ROOT headers
#include <TBranch.h>
#include <TFile.h>
#include <TTree.h>

// G4 includes:
#include "globals.hh"
#include "G4Types.hh"

//
#include "K37_OutputChannels.hh"

using std::map;
using std::string;
using K37_ABC::K37_Data;

// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 
// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 
class LocalAggregator 
{
public:
	LocalAggregator();
	~LocalAggregator();
//	void RegisterData(K37_ABC::K37_Data *dataPointer_);
	void RegisterIOMethod();
	void EndEvent(const bool &recordEvent_);
	TBranch* Branch(const char *name, void *address,
	                const char *leaflist, Int_t bufsize = 32000);
private:
	// to make Aggregator non-copyable
	LocalAggregator(const LocalAggregator &);
	// to make Aggregator non-copyable
	const LocalAggregator & operator=(const LocalAggregator &);
	
	
// ----- // ----- // ----- // ----- // ----- // ----- // 	
public:
	void InitilzeOutput();    // 
	void ClearData();         // 
	
	map<string, K37_Data*> * GetActiveChannels() { return active_channels_; }  // I really don't want this to be a thing..
	TFile                  * GetRootFile()       { return rootFile; }  // still needed?
	
public:
	void SetFileName();      // sets full_local_outputfilename, and also returns it.	
	G4String GetFileName();  // returns full_local_outputfilename.
	
public:
	void EndRun();
	void BeginRun();
	void SaveParamString();
	
	void RegisterChannels();
	void RegisterChannel(K37_Data* channel);
	void RegisterExcessBranches();  //

public:  
	double tell_it_to_print()
	{ 
		G4cout << "Hello, world!" << G4endl;
		return 2.0;
	}
	
private:
	G4String full_local_outputfilename;
	
// ----- // ----- // ----- // ----- // ----- // ----- // 		
private:
	map<string, K37_Data*> * active_channels_;  // this needs to be more thread-local than it is. ... does this work??
	set_of_channels        * channelset;
	TFile                  * rootFile;          //  does this need to actively be threadlocal?  No, it's fine, the whole LocalAggregator is threadlocal.
	TTree                  * theNTuple; // ...?
	
	map<string, K37_Data*>::iterator it_data;
	std::ofstream outputFile;  
};


// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- //
#endif