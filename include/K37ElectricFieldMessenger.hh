// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 
// 2013 -

#ifndef K37ElectricFieldMessenger_h
#define K37ElectricFieldMessenger_h 1

#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "globals.hh"

//#include "K37ElectricFieldSetup2.hh"
#include "K37ElectricFieldSetup.hh"
#include "G4UImessenger.hh"
#include "G4UIdirectory.hh"

#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAnInteger.hh"
//#include "G4UIcmdWith3VectorAndUnit.hh"

//class K37ElectricFieldSetup2;  // because the include just isn't enough.
//class K37ElectricField;
class K37ElectricFieldSetup;

class K37ElectricFieldMessenger: public G4UImessenger 
{
public:
	explicit K37ElectricFieldMessenger(K37ElectricFieldSetup* efs);
//	explicit K37ElectricFieldMessenger(K37ElectricFieldSetup2* efs);
//	explicit K37ElectricFieldMessenger(K37ElectricField* ef);
	~K37ElectricFieldMessenger();
	void SetNewValue(G4UIcommand*, G4String);
	//
private:
	K37ElectricFieldSetup*     fEFieldSetup;
//	K37ElectricFieldSetup2*    fEFieldSetup;
//	K37ElectricField*          the_field;
	G4UIdirectory*             fieldDir;
	//
private:
	G4UIcmdWithAnInteger*      StepperCmd;
	G4UIcmdWithADoubleAndUnit* SetEfield_cmd;
	G4UIcmdWithADoubleAndUnit* MinStepCmd;
	G4UIcmdWithoutParameter*   print_field_cmd;
};

#endif

