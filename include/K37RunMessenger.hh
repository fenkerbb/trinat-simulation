// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 2013

#ifndef K37RunMessenger_h
#define K37RunMessenger_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UImessenger.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"

#include "K37_GlobalAggregator.hh"

//---------------------------------

class K37RunAction;
class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithAString;
class G4UIcmdWithABool;
//class G4UIcmdWithoutParameter;

//---------------------------------

class K37RunMessenger: public G4UImessenger 
{
public:
	explicit K37RunMessenger(K37RunAction* ra, GlobalAggregator * ga);
	~K37RunMessenger();
	void SetNewValue(G4UIcommand*, G4String);

private:
	K37RunAction     * runAction;
	GlobalAggregator * the_aggregator;
	
private:
	G4UIcmdWithAString* printEnergyLossTable;
	G4UIcmdWithoutParameter* setFillAllSDData;  // this one is obsolete.  new one is in K37DetectorMessenger.
	G4UIdirectory* RunDirectory;
	
	
	G4UIcmdWithAString* setFileName;
	G4UIcmdWithAString* set_configuration_file_;
	G4UIcmdWithAString* set_output_directory_;
	G4UIcmdWithADoubleAndUnit *set_default_cut_cmd_;  // obsolete.
	G4UIcmdWithABool* auto_increment_cmd_;
	
	G4UIcmdWithoutParameter* print_metadata_header;
	G4UIcmdWithABool* setsave_metadata;
	G4UIcmdWithAString* set_metadatafile;
	//
	G4UIcmdWithAString * set_event_acceptance_types_;
	G4UIcmdWithABool   * get_event_acceptance_types_;
};

#endif

