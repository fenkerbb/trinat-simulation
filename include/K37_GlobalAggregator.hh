// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013
#ifndef GlobalAggregator_h
#define GlobalAggregator_h 1

// C/C++ headers
#include <map>
#include <string>
#include <fstream>

// ROOT headers
#include <TFile.h>
#include <TTree.h>

#include "globals.hh"

// Project-specific includes:
#include "K37CloudSetup.hh" // eventually includes Geant4/CLHEP/Units/SystemOfUnits.h
//#include "K37ElectricFieldSetup2.hh"
#include "K37DetectorConstruction.hh"  // namespace patch.


using std::map;
using std::string;
// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 

class GlobalAggregator 
{
public:
	GlobalAggregator();
	~GlobalAggregator();
private:
	// to make GlobalAggregator non-copyable
	GlobalAggregator(const GlobalAggregator &);
	// to make GlobalAggregator non-copyable
	const GlobalAggregator & operator=(const GlobalAggregator &);
public:
	TFile* GetRootFile() { return rootFile; }  // could make rootFile a private member?
	
// ----- // ----- // ----- // ----- // ----- // ----- // 	
public:
	void SaveMetaData();
	void MergeRootFiles();  // not from the run action.
	void PrintMetaDataHeader();
	void SetupIO();
	void EndRun();
	
	// Setters, from K37RunAction:
	void Set_SaveMetaData(bool setval) 
		{ setsave_metadata=setval; }
	void Set_MetaDataFilename(G4String fname) 
		{ metadata_filename = fname; }
	void SetConfigurationFileName(const char *filename) 
		{ snprintf(configuration_filename, sizeof(configuration_filename), "%s", filename); }
	void SetOutputDirectory(const char *directory_name) 
		{ snprintf(output_directory_, sizeof(output_directory_), "%s", directory_name); }
	void setFileName(G4String newName)                          
		{ namestub = newName; }  // don't append ".root" to the file name.  the code will do it for you.
	
	void SetAutoIncrement(G4bool ai) 
	{
		autoincrement = ai;
		G4cout << "Setting autoIncrement to:  " << ai << G4endl;
	};
	
	// Getters, from K37RunAction:
	char* GetOutputDirectory()          { return output_directory_;      }
	char* GetConfigurationFileName()    { return configuration_filename; }
	
	
	// Functions that may or may not be used, but also, I really don't want to
	// store *all* of these things as their own strings.
	G4String GetGlobalNameStub()
	{
		G4String the_result;
		if(autoincrement) { the_result = namestub + G4String("_") + G4String(patch::to_string(filenumber)); }
		else              { the_result = namestub; }
		return the_result;
	}
	G4String GetGlobalMiniName()
	{
		G4String the_result;
		if(autoincrement) { the_result = namestub + G4String("_") + G4String(patch::to_string(filenumber)) + G4String(".root"); }
		else              { the_result = namestub + G4String(".root"); }
		return the_result;
	}
	G4String GetGlobalFullName()
	{
		G4String the_result;
		if(autoincrement) { the_result = out_dir + namestub + G4String("_") + G4String(patch::to_string(filenumber)) + G4String(".root"); }
		else              { the_result = out_dir + namestub + G4String(".root"); }
		return the_result;
	}
	G4String GetGlobalOutputPath()
	{
		return out_dir;
	}
	G4String Make_LocalFileName(int threadnumber)  // 
	{
		G4String full_local_outputfilename = GetGlobalOutputPath() + GetGlobalNameStub() + G4String("_tmp") + G4String(patch::to_string(threadnumber)) + G4String(".root");
		return full_local_outputfilename;
	}
	
private:
	G4bool   autoincrement;
	G4bool   setsave_metadata;
	
	// baked in stuff:
	G4String metadata_filename;
	G4String config_filename;
	
	G4int    filenumber;
	G4String namestub;
	G4String out_dir;
	
	//
	char     configuration_filename[400];
	char     output_directory_[400];
private:	
	TFile * rootFile;     // 
	TTree * theNTuple;    // 
};


// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- //
#endif