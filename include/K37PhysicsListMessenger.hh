// Code originated as a Geant4 example, written by the Geant4 collaboration.
// Subsequently modified by some combination of:  
//   Spencer Behling
//   Ben Fenker
//   Melissa Anholm
// 
// Original file from:  
// \file electromagnetic/TestEm7/src/PhysicsListMessenger.hh
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef K37PhysicsListMessenger_h
#define K37PhysicsListMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class K37PhysicsList;
class G4UIdirectory;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAString;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
class K37PhysicsListMessenger: public G4UImessenger
{
  public:
    K37PhysicsListMessenger(K37PhysicsList * );
   ~K37PhysicsListMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  
    K37PhysicsList * pPhysicsList;
    
    G4UIdirectory*             physDir;        
    G4UIcmdWithADoubleAndUnit* gammaCutCmd;
    G4UIcmdWithADoubleAndUnit* electCutCmd;
    G4UIcmdWithADoubleAndUnit* protoCutCmd;    
    G4UIcmdWithADoubleAndUnit* allCutCmd;    
    G4UIcmdWithAString*        pListCmd;
    
};


#endif
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

