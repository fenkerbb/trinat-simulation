// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2012

#ifndef K37CloudSize_h
#define K37CloudSize_h 1

#include "G4ParticleDefinition.hh" // calls:  #include <CLHEP/Units/SystemOfUnits.h>
#include "G4SystemOfUnits.hh" // calls:  #include <CLHEP/Units/SystemOfUnits.h> and then lets us use unit names for free.
#include "G4PhysicalConstants.hh"
#include "G4IonTable.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

#include "K37CloudMessenger.hh"

class K37CloudMessenger;

class K37CloudSetup 
{
///
/// \class K37CloudSetup  K37CloudSetup.hh K37CloudSetup
/// \brief This is a class to change the cloud size based on temperature
/// \author Spencer Behling
/// \date Jan. 5 2012
/// \since version 1.0
///
public:
//	K37CloudSetup(G4ThreeVector cloud_center, G4ThreeVector temperature,
//	             G4ThreeVector  initial_size, G4ThreeVector sail_velocity);
	K37CloudSetup();
	~K37CloudSetup();
	//
//	void makeEvent();
	
	// Event-specific:
	G4ThreeVector GetFinalPosition() { return final_position_; }  // specific to each event.
	G4ThreeVector GetFinalVelocity() { return final_velocity_; }  // specific to each event.
	G4double GetDecayTime()          { return decay_time_; }
	
	// For the whole cloud:
	G4ThreeVector GetCloudCenter()      { return cloud_center_; }
	G4ThreeVector GetInitialCloudSize() { return initial_cloud_size_; }
	G4ThreeVector GetTemperature()      { return temperature_; }
	G4ThreeVector GetSailVelocity()     { return sail_velocity_; }
	G4ThreeVector GetVelocitySigma()    { return velocity_sigma_; }  // must be already set up with temperature?
	G4double GetFreeExpansionTime()     { return expansion_before_polarized_; }
	G4double GetOP_CycleTime()          { return cycleTime; }
	
	// new from PGA:
	double GetPolarization()            { return polarization_; }
	double GetAlignment()               { return alignment_; }
	void SetPolarization(double);
	void SetAlignment(double);
	void SetPolarizationAlignment(double, double);
	
	
	void SetCloudCenter(G4ThreeVector center);
	void SetTemperature(G4ThreeVector temp);
	void SetTemperature(G4double temp);
	void SetInitialCloudSize(G4ThreeVector size);
	void SetInitialCloudSize(G4double size);
	void SetSailVelocity(G4ThreeVector vel);

	void SetFreeExpansionTime(G4double time);
	void SetOP_CycleTime(G4double time);

	void SetMatchedRunsetLetter(G4String newRunsetLetter);
	G4String GetMatchedRunsetLetter();

private:
	K37CloudMessenger*            CloudMessenger;   

	// Event-specific:
	G4ThreeVector final_position_;  // individual event.
	G4ThreeVector final_velocity_;  // individual event.
	G4double decay_time_;           // individual event.
	
	// For the whole cloud:
	G4ThreeVector cloud_center_;
	G4ThreeVector initial_cloud_size_;
	G4ThreeVector velocity_sigma_;
	G4ThreeVector temperature_;
	G4ThreeVector sail_velocity_;
	G4double cycleTime;
	G4double expansion_before_polarized_;
	G4String MatchedRunsetLetter;
	
	// new from PGA:
	double polarization_;
	double alignment_;
	
	void SetupVelocitySigma(G4ThreeVector temperature);
	G4double CalcSigma(G4double temperature);
	G4bool initialize_complete_;
	void Initialize();
};

#endif
