// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm 2013

#ifndef K37CloudMessenger_h
#define K37CloudMessenger_h 1

#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIdirectory.hh"
#include "G4UImessenger.hh"
#include "G4UnitsTable.hh"
#include "globals.hh"

#include "K37CloudSetup.hh"

class K37CloudSetup;

//----------------------------------

class K37CloudMessenger: public G4UImessenger 
{
public:
	explicit  K37CloudMessenger(K37CloudSetup * cloud);
	virtual ~K37CloudMessenger();

	void SetNewValue(G4UIcommand*, G4String);

private:
	K37CloudSetup             *the_cloud;
	G4UIdirectory             *cloudDir;

	G4UIcmdWithoutParameter   *cloud_print_cmd_;
	G4UIcmdWithADouble        *set_pol_cmd_;
	G4UIcmdWithADouble        *set_ali_cmd_;
	G4UIcmdWith3VectorAndUnit *set_cloud_center_;
	G4UIcmdWith3VectorAndUnit *set_cloud_temperature_v_;
	G4UIcmdWithADoubleAndUnit *set_cloud_temperature_d_;
	G4UIcmdWith3VectorAndUnit *set_initial_cloud_size_v_;
	G4UIcmdWithADoubleAndUnit *set_initial_cloud_size_d_;
	G4UIcmdWith3VectorAndUnit *set_cloud_sail_velocity_v_;

	G4UIcmdWithADoubleAndUnit *set_cloud_expansiontime_;
	G4UIcmdWithADoubleAndUnit *set_cloud_opcycle_time_;
	
	G4UIcmdWithAString        *set_cloud_matched_runset_cmd_;
};

//----------------------------------

#endif

