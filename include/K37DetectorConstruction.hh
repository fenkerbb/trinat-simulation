// Authors: Spencer Behling and Benjamin Fenker 2013

#ifndef K37DetectorConstruction_h
#define K37DetectorConstruction_h 1

#include <CLHEP/Vector/Rotation.h>
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "globals.hh"

#include "G4Box.hh"
#include "G4SubtractionSolid.hh"
#include "G4Tubs.hh"
#include "G4UnitsTable.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"

//
#include "G4MultiFunctionalDetector.hh"
#include "G4Cache.hh"

//
#include "K37ElectronMCPSD.hh"
#include "K37RecoilMCPSD.hh"
#include "K37ScintillatorSD.hh"
#include "K37StripDetectorSD.hh"
//#include "K37ElectricFieldSetup2.hh"
#include "K37ElectricFieldSetup.hh"

class G4VPhysicalVolume;
class G4VisAttributes;
class G4LogicalVolume;
class K37DetectorMessenger;
//class K37ElectricFieldSetup;  // needed for anything??
class HepRotation;

namespace patch
{
	template < typename T > std::string to_string( const T& n )
	{
		std::ostringstream stm ;
		stm << n ;
		return stm.str() ;
	}
}  

struct GeometryElement 
{
	G4String nameBase;
	G4double inner_radius;
	G4double outer_radius;
	G4double inner_radius2;
	G4double outer_radius2;
	G4double sweep_radius;
	
	G4double start_phi;
	G4double delta_phi;
	G4double rotation_angle;
	G4ThreeVector center_position;
	
	// Three cartesian dimensions
	G4double length;
	G4double width;
	G4double depth;
	
	G4double cutout_side_length;
	G4double cutout_radius;
	G4double cutout_depth;
};

struct set_of_K37_detectors
{
public:
	K37ScintillatorSD         * upper_scintillator_sens_;      // pointer to the sensitive scin  // ok
	K37ScintillatorSD         * lower_scintillator_sens_;      // pointer to the sensitive scin  // ok
	K37StripDetectorSD        * upper_strip_detector_sens_;    // ptr to the sensitive DSSSD     // ok
	K37StripDetectorSD        * lower_strip_detector_sens_;    // ptr to the sensitive DSSSD     // ok
	K37ElectronMCPSD          * electron_mcp_sens_;            // pointer to the sensitive eMCP
	K37RecoilMCPSD            * recoil_mcp_sens_;              // pointer to the sensitive rMCP
	//
	G4MultiFunctionalDetector * be_scorer_t;    // pointer to the sensitive Be
	G4MultiFunctionalDetector * be_scorer_b;    // pointer to the sensitive Be	
	// there are SDs in the two mirrors too, but they never had a pointer to their SDs held within the class.
	G4MultiFunctionalDetector * mirror_scorer_t;
	G4MultiFunctionalDetector * mirror_scorer_b;
	// not sure if I need to keep the scorers in here or not?
	G4VPrimitiveScorer * mirror_edep_sens_t; // ok
	G4VPrimitiveScorer * mirror_edep_sens_b; // ok
	G4VPrimitiveScorer * Be_edep_sens_t; 
	G4VPrimitiveScorer * Be_edep_sens_b; 
};

class K37DetectorConstruction : public G4VUserDetectorConstruction 
{
public:
	K37DetectorConstruction();
	~K37DetectorConstruction();
	
	// as of 4.10ish, there exists a virtual method, ConstructSDandField().  this is called for *every* thread.  sensitive detectors are thread-local.
	// SetSensitiveDetector("LVName",pSD), that's a new thing...
	// alternately, SetSensitiveDetector(G4logicalVolume*, G4VSensitiveDetector*)
	// the K37DetectorConstruction object is instantiated by the run manager.  So Which parts are global and which are thread local??
	// ConstructSDandField() method is used to initialize the thread-local things.  sensitive detectors *should* be thread-local, as should the field.
	// Construct() should just make the global things.
	
	// constructs materials, volumes and visualization attributes
	G4VPhysicalVolume* Construct()  
	{
		G4cout << "Called [shared] K37DetectorConstruction::Construct()." << G4endl;
		return this -> ConstructK37Experiment_shared();
	}
	
	//This method is used in multi-threaded applications to build
	//per-worker non-shared objects: SensitiveDetectors and Field managers
	void ConstructSDandField()
	{
		G4cout << "Called [local] K37DetectorConstruction::ConstructSDandField()." << G4endl;
		this -> ConstructK37Experiment_local();
	}
	
	//
	const G4VPhysicalVolume* GetPhysicalWorld() { return world_phys_; }
	G4Material*  GetMirrorMaterial() { return MirrorMaterial; }
		
	G4double GetVDistance_dssd_to_center( ) { return (strip_detector.center_position.z() - 0.5*strip_detector.depth); }
	G4double Get_dssd_width() { return strip_detector.width; }
	
//	void UpdateGeometry();
	void SetMirrorMaterial(G4String);
	void PrintMaterialList();
	
	void SetMakeChamber(G4bool arg) 
	{
		if( (makeSimpleChamber == true) && (arg == true) )
		{
			G4cerr<<"Cannot make chamber and simple chamber simultanously."<<G4endl;
			G4cerr<<"Turn off simple chamber and try again."<<G4endl;
		}
		else
		{
			makeChamber = arg;
		}
	}
	void SetMakeSimpleChamber(G4bool arg) 
	{
		if( ( makeChamber == true) && (arg == true) )
		{
			G4cerr<<"Cannot make chamber and simple chamber simultanously."<<G4endl;
			G4cerr<<"Turn off chamber and try again."<<G4endl;
		}
		else
		{
			makeSimpleChamber = arg;
		}
	}

	void SetMakeMirrors(G4bool f)         { makeMirrors = f;         }
	void SetMakeHoops(G4bool f)           { makeHoops = f;           }
	void SetMakeCoils(G4bool f)           { makeCoils = f;           }
	void SetMakeSDholders(G4bool f)       { make_sd_holders_ = f;    }
	
	void SetMakeElectronMCP(G4bool f)     { makeElectronMCP = f;     }
	void SetMakeRecoilMCP(G4bool f)       { makeRecoilMCP = f;       }
	void SetElectronMCPradius(G4double d) { electron_mcp_radius_= d; }
	void SetRecoilMCPradius(G4double d)   { recoil_mcp_radius_= d;   }
	
	G4bool   GetMakeElectronMCP()   { return makeElectronMCP;      }  // unused?
	G4bool   GetMakeRecoilMCP()     { return makeRecoilMCP;        }      // unused?
	G4double GetElectronMCPradius() { return electron_mcp_radius_; }
	G4double GetRecoilMCPradius()   { return recoil_mcp_radius_;   }
	
	void SetTeflonTapeThickness(G4double t)     { teflon_front_face.length = t;      }
	void SetMountingScrewHeadLength(G4double l) { sd_mounting_screw_head.length = l; }
	void SetMirrorThickness(G4double t)         { mirror.length = t;                 }
	void SetMirrorBias(G4double b)              { mirror_length_bias = b;            }
	void SetBe_WindowBias(G4double w)           { be_window_bias = w;                }
	void SetBB1Bias(G4double b)                 { bb1_bias = b;                      }
	void SetBeWindowThickness(G4double t)       { beryllium_window.length = t;       }
	void SetBB1Thickness(G4double t) 
	{
		strip_detector.depth = t;
		sd_inactive.depth = t;
	}

	void SetRecordStripDetectorData(G4bool record)  { record_strip_detector_data_ = record; }   // moved from K37RunAction
	G4bool GetRecordStripDetectorData()             { return record_strip_detector_data_;   }   // moved from K37RunAction
	
	
//	static G4ThreadLocal K37ElectricFieldSetup* GetField() { return the_Efield; }
	K37ElectricFieldSetup * GetField() { return the_Efield.Get(); }
	
private:
	K37DetectorMessenger* detectorMessenger;  // pointer to the Messenger
	
	
	G4VPhysicalVolume* ConstructK37Experiment_shared();  // materials, volumes and visualization attributes
	void ConstructK37Experiment_local();                 // sensitive detectors.
	
	void DefineMaterials();
	void CalculateDimensions();
	
	void ConstructScintillators();
	void ConstructStripDetectors();
	void ConstructElectronMCP();
	void ConstructRecoilMCP();
	
	void ConstructMirrors();      // Not a sensitive detector ... yes it is.
	void ConstructBerylliumFoils();
	
	void ConstructChamber();      // Not a sensitive detector
	void ConstructSimpleChamber();// Not a sensitive detector
	void ConstructHoops();        // Not a sensitive detector
	void ConstructSupportRods();
	void ConstructCoils();        // Not a sensitive detector
	void ConstructAir();          // Not a sensitive detector
	void PlaceSpacerWithBounds(G4double ymin, G4double ymax);
	
	// sensitive detectors.
	void ConstructScintillatorsSD();
	void ConstructStripDetectorsSD();
	void ConstructElectronMCPSD();
	void ConstructRecoilMCPSD();
	void ConstructMirrorsSD();    
	void ConstructBerylliumFoilsSD();
	
	// World:
	G4Material        * world_material_;  // default is vacuum
	G4double            world_size_;      // default is 2.0 x 2.0 x 2.0 m
	G4Box             * world_box_;       // pointer to the solid world
	G4LogicalVolume   * world_log_;       // pointer to the logical world
	G4VPhysicalVolume * world_phys_;      // pointer to the physical World
	
	// Chamber:
	G4SubtractionSolid * chamber_box_;        // pointer to the solid chamber
	G4LogicalVolume    * chamber_log_;        // pointer to the logical chamber
	G4VPhysicalVolume  * chamber_phys_;       // pointer to the physical chamber
	
	// Scintillators:
	G4Tubs            * scintillator_tubs_;        // pointer to the solid scintillator
	G4LogicalVolume   * upper_scintillator_log_;   // pointer to the logical scinti
	G4VPhysicalVolume * upper_scintillator_phys_;  // pointer to the physical scint
	G4LogicalVolume   * lower_scintillator_log_;   // pointer to the logical scinti
	G4VPhysicalVolume * lower_scintillator_phys_;  // pointer to the physical scint
	
	G4Box *strip_detector_box_[2];                   // pointer to the solid DSSSD
	G4LogicalVolume    *upper_strip_detector_log_;   // ptr to the logical DSSSD
	G4VPhysicalVolume  *upper_strip_detector_phys_;  // ptr to the physical DSSSD
	G4LogicalVolume    *lower_strip_detector_log_;   // ptr to the logical DSSSD
	G4VPhysicalVolume  *lower_strip_detector_phys_;  // ptr to the physical DSSSD
	
	G4Tubs *electron_mcp_tub_;                   // pointer to the solid eMCP
	G4LogicalVolume *electron_mcp_log_;          // pointer to the logical eMCP
	G4LogicalVolume *electron_mcp_log_noSense;   // pointer to the logical eMCP  .........what.
	G4VPhysicalVolume *electron_mcp_phys_;       // pointer to the physical eMCP
	
	G4Tubs *recoil_mcp_tub_;                  // pointer to the solid rMCP
	G4LogicalVolume *recoil_mcp_log_;         // pointer to the logical rMCP
	G4VPhysicalVolume *recoil_mcp_phys_;      // pointer to the physcial rMCP
	
	G4LogicalVolume * mirror_log[2];
	G4LogicalVolume* beryllium_log[2]; // !!!!
	G4Material* MirrorMaterial;
	
	G4LogicalVolume *air_log_plus_;
	G4LogicalVolume *air_log_mins_;
	
	G4Material* FullEnergyDetectorMaterial;
	G4Material* DeDxDetectorMaterial;
	G4Material* SiliconDetectorFrameMaterial;
	G4Material* ChamberMaterial;
	G4Material* FoilMaterial;
	G4Material* HoopMaterial;
	G4Material* MirrorMountMaterial;
	G4Material* CoilsMetal;
	G4Material* CoilsLiquid;
	
	G4double electron_mcp_radius_;
	G4double recoil_mcp_radius_;
	
	//Numbers for the coil construction see /Drawings/cros-coil.png
	G4double distanceFromCloseEdgeToPolarization;
	G4double distanceFromBottomToChamberCenter;
	G4double tubeOuterDiameter;
	G4double tubeInnerDiameter;
	G4double tubeSpacing;
	
	G4bool check_all_for_overlaps_;
	
	// = true or straight cut = false
	CLHEP::HepRotation* changeZtoX;
	CLHEP::HepRotation* changeZto45;
	CLHEP::HepRotation* changeZtoNeg45;
	CLHEP::HepRotation* changeZto35;
	CLHEP::HepRotation* changeZtoNeg35;
	CLHEP::HepRotation* changeZtoNeg62;
	CLHEP::HepRotation* changeZtoNeg118;
	CLHEP::HepRotation* changeYtoBeamAxis;
	CLHEP::HepRotation* changeYtoBeamAxisForLPP1;
	CLHEP::HepRotation* changeYtoBeamAxisForLPP2;
	CLHEP::HepRotation* changeYtoBeamAxisForLPP3;
	CLHEP::HepRotation* changeYtoBeamAxisForLPP4;
	CLHEP::HepRotation* rotationForOpticalPumpingBeams1;
	CLHEP::HepRotation* rotationForOpticalPumpingBeams2;
	CLHEP::HepRotation* RFDRotation;
	CLHEP::HepRotation* FFRFRotation;
	CLHEP::HepRotation* MirrorCutRotation;
	CLHEP::HepRotation* hoopRotation;
	CLHEP::HepRotation* zRot;
	CLHEP::HepRotation* y180;
	CLHEP::HepRotation* mirror_rotation;
	CLHEP::HepRotation* MMRotation;
		
	// Bools to turn off or on various aspects of the apparatus to simulate their individual effects
	G4bool makeScintillators, makeStripDetectors, make_beryllium_foils_, makeMirrors;
	G4bool makeHoops, makeElectronMCP, makeCoils, makeRecoilMCP, make_sd_holders_;
	G4bool makeChamber, makeSimpleChamber;
	//
	G4bool record_strip_detector_data_;   // from K37RunAction
	
	GeometryElement simpleChamberTub;
	GeometryElement simpleChamberRentrantFlangeCut;
	GeometryElement mirror;
	G4double mirror_length_bias, be_window_bias, bb1_bias;
	
	GeometryElement mirror_mount;
	GeometryElement sd_frame;
	GeometryElement sd_inactive;
	GeometryElement sd_mounting_screw_head;
	GeometryElement strip_detector;
	GeometryElement teflon_front_face;
	GeometryElement scintillator;
	GeometryElement beryllium_window;
	GeometryElement reentrant_flange_front_face;
	GeometryElement reentrant_flange_descender;
	GeometryElement reentrant_flange_pipe;
	GeometryElement coil_inner_torus_PZ[4][4];
	GeometryElement coil_outer_tubs_PZ[4][4];
	GeometryElement coil_inner_torus_MZ[4][4];
	GeometryElement coil_outer_tubs_MZ[4][4];
	
	std::map<G4String, G4Material*> trinat_materials_;
	
// Any logical volume element that will be assigned visual attributes will go here:	
	G4LogicalVolume * coil_log_t[4][4];
	G4LogicalVolume * coil_log_b[4][4];
	G4LogicalVolume * coilMetal_log_t[4][4];
	G4LogicalVolume * coilMetal_log_b[4][4];
	
	G4LogicalVolume * dedxFrame_log;
	G4LogicalVolume * sd_inactive_log;
	G4LogicalVolume * mounting_screw_head_log;
	G4LogicalVolume * FFRF_log;
	G4LogicalVolume * temp_log;  // simple chamber thing.
	G4LogicalVolume * OPRF_log;
	G4LogicalVolume * RFD_log;
	
	G4LogicalVolume * mirror_mount_log;
	
	G4LogicalVolume * hoop_3through6_box_log;
	G4LogicalVolume * hoop_1_log;
	G4LogicalVolume * hoop_7_log;
	G4LogicalVolume * hoop_2_log;
	
	G4LogicalVolume * mcp_plate_rear_log;
	G4LogicalVolume * teflon_log;
	
public:
	void SetupVisAttributes();
	bool do_the_visualizing;
private:
// Vis Attributes.  Visualization doesn't actually work since the upgrade to G4.10.x.
// I don't know why it's broken, and I may not get around to fixing it, but I'll 
// preserve the attributes in case someone ever does fix it.
	G4VisAttributes * world_logVisAttributes;      // within ConstructK37Experiment_shared().
	G4VisAttributes * air_vis;                     // within ConstructAir().
	G4VisAttributes * chamber_logVisAttributes;    // within ConstructSimpleChamber() and ConstructChamber().
	G4VisAttributes * OPRF_logVisAttributes;       // within ConstructSimpleChamber() and ConstructChamber().
	G4VisAttributes * RFD_logVisAttributes;        // within ConstructSimpleChamber() and ConstructChamber().
	G4VisAttributes * beryllium_logVisAttributes;  // within ConstructBerylliumFoils() 
	G4VisAttributes * FFRF_logVisAttributes;       // within ConstructBerylliumFoils() 
	G4VisAttributes * mirror_logVisAttributes[2];  // within ConstructMirrors().
	G4VisAttributes * MM_logVisAttributes;         // within ConstructMirrors().
	G4VisAttributes * cut_ESP_logVisAttributes;    // within ConstructHoops().
	G4VisAttributes * hoop7_logVisAttributes;      // within ConstructHoops().
	G4VisAttributes * coils_logVisAttributes;      // within ConstructCoils().
	G4VisAttributes * coilLiquid_logVisAttributes; // within ConstructCoils().
	G4VisAttributes * scintillator_vis;            // within ConstructScintillators().
	G4VisAttributes * teflon_vis;                  // within ConstructScintillators().
	G4VisAttributes * strip_detector_vis;          // within ConstructStripDetectors(). no extra requirements.
	G4VisAttributes * dedxFrame_logVisAttributes;  // within ConstructStripDetectors(), requires if (make_sd_holders_) 
	G4VisAttributes * sd_inactive_vis;             // within ConstructStripDetectors(), requires if (make_sd_holders_) 
	G4VisAttributes * mounting_screw_head_vis;     // within ConstructStripDetectors(), requires if (make_sd_holders_) 
	G4VisAttributes * SOED_logVisAttributes;       // within ConstructElectronMCP().
	G4VisAttributes * rmcp_logVisAttributes_;      // within ConstructRecoilMCP().
	
private:
// Sensitive Detectors:  they used to just be regular members of the K37DetectorConstruction class, 
// but now with multithreading, they probably need to be G4Cache objects or something.
	
	/*
//	K37ScintillatorSD *upper_scintillator_sens_;     // pointer to the sensitive scin
//	K37ScintillatorSD *lower_scintillator_sens_;     // pointer to the sensitive scin
//	K37StripDetectorSD *upper_strip_detector_sens_;  // ptr to the sensitive DSSSD
//	K37StripDetectorSD *lower_strip_detector_sens_;  // ptr to the sensitive DSSSD
//	K37ElectronMCPSD *electron_mcp_sens_;            // pointer to the sensitive eMCP
//	K37RecoilMCPSD *recoil_mcp_sens_;                // pointer to the sensitive rMCP
//	G4MultiFunctionalDetector *be_scorer_t;    // pointer to the sensitive Be
//	G4MultiFunctionalDetector *be_scorer_b;    // pointer to the sensitive Be
//  there are SDs in the two mirrors too, but they never had a pointer to their SDs held within the class.
	*/

//	G4Cache<LocalAggregator*> aggregator_cache;
//	LocalAggregator * GetAggregator()   { return aggregator_cache.Get(); }
	// G4Cache has .Get() and .Put(...) methods.
	/*
	G4Cache<K37ScintillatorSD*>         upper_scintillator_sens_cache;
	G4Cache<K37ScintillatorSD*>         lower_scintillator_sens_cache;
	G4Cache<K37StripDetectorSD*>        upper_strip_detector_sens_cache;
	G4Cache<K37StripDetectorSD*>        lower_strip_detector_sens_cache;
	G4Cache<K37ElectronMCPSD*>          electron_mcp_sens_cache;
	G4Cache<K37RecoilMCPSD*>            recoil_mcp_sens_cache;
	G4Cache<G4MultiFunctionalDetector*> be_scorer_t_cache;
	G4Cache<G4MultiFunctionalDetector*> be_scorer_b_cache;
	G4Cache<G4MultiFunctionalDetector*> mirror_scorer_t_cache;
	G4Cache<G4MultiFunctionalDetector*> mirror_scorer_b_cache;
	*/
	
	G4Cache<set_of_K37_detectors> the_detectors;
	G4Cache<K37ElectricFieldSetup*> the_Efield;
//	static G4ThreadLocal K37ElectricFieldSetup* the_Efield;

};

#endif
