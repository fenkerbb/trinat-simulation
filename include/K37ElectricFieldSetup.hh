// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 2013

#ifndef K37ElectricFieldSetup_H
#define K37ElectricFieldSetup_H

#include <stdio.h>

#include "K37ElectricFieldMessenger.hh"

//
#include "G4TransportationManager.hh"
#include "G4ElectricField.hh"
//#include "G4UniformElectricField.hh"

#include "G4ChordFinder.hh"
#include "G4FieldManager.hh"

#include "G4MagIntegratorDriver.hh"  // G4MagInt_Driver ?
#include "G4MagIntegratorStepper.hh"
#include "G4EqMagElectricField.hh"

class K37ElectricFieldMessenger;  // for when includes just aren't going to cut it.

class K37ElectricFieldSetup 
{
public:
	explicit operator bool() const { return true; }  // this is so we can call if(field)
	K37ElectricFieldSetup();  //  A zero field
	~K37ElectricFieldSetup();
	

	// Setters
	void SetStepperType(G4int i);
	void SetMinStep(G4double s); 
	void SetFieldValue(G4double);
	
	// Getters
	G4int    GetStepperTypeIndex()   { return fStepperType;     }
	G4double GetMinStep()            { return fMinStep;         }
	G4double GetConstantFieldValue() { return fEfieldMagnitude; }  
	G4String GetStepperName();

	void UpdateIntegrator();
	// Prepare all the classes required for tracking - from stepper 
	//    to Chord-Finder
	//   NOTE:  field and equation must have been created before calling this.

protected:
	// Utility method -- Find the global Field Manager
	G4FieldManager* GetGlobalFieldManager()
	{
		return G4TransportationManager::GetTransportationManager()->GetFieldManager();
	}
	void CreateStepper();

private:
	G4double                    fMinStep;
	//
	G4ChordFinder*              fChordFinder;
	G4EqMagElectricField*       fEquation;
	G4ElectricField*            fEMfield;
	G4ThreeVector               fElFieldValue;       // Only ever use this one internally.  
	G4double                    fEfieldMagnitude;
	G4MagIntegratorStepper*     fStepper;
	G4MagInt_Driver*            fIntgrDriver;
	G4int                       fStepperType;
	
	K37ElectricFieldMessenger*  fFieldMessenger;

private:
	void SetFieldValueV(G4ThreeVector fieldVector);  // nobody else should get to call this function, ever.
};

#endif
