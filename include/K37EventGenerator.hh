// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013

#ifndef K37EventGenerator_H
#define K37EventGenerator_H 1

#include <map>
#include <string>

#include "globals.hh"

#include "G4SystemOfUnits.hh" // calls:  #include <CLHEP/Units/SystemOfUnits.h> and then lets us use unit names for free.
#include "G4PhysicalConstants.hh"

#include "K37_Data.hh"
#include "Holstein51_Isotope.hh"

using K37_ABC::K37_Data;
using std::map;
using std::string;

class K37FermiFunction;

struct particle 
{
	G4double MaxE;          // Max Total Energy
	G4double MaxT;          // Max Kinetic Energy
	G4double Mass;          // Mass of particle
	G4double E;             // Total Energy
	G4double T;             // Kinetic Energy
	G4double Pmag;          // Magnitude of Momentum
	G4double PmagSquared;   // Magnitude of Momentum Squared
	G4double Theta;         // Theta of particle with respect to the polarization axis
	G4double Phi;           // 0 to 2pi around the polarization axis
	G4double unitX;         // unit vector of momentum in X
	G4double unitY;         // unit vector of momentum in Y
	G4double unitZ;         // unit vector of momentum in Z coordinate
	G4double X;             // X component of momentum
	G4double Y;             // Y component of momentum
	G4double Z;             // Z component of momentum
	
	double get_v_from_p() // [v] = unitless m/s, pbeta in G4units of MeV.  particle must be an electron.
	{
		double beta = (Pmag/MeV)* sqrt( (1.0)/((Mass/MeV)*(Mass/MeV) + (Pmag/MeV)*(Pmag/MeV)) );
		double v = beta*c_light/(meter/second);  // meters/second 
		return v;  // should be unitless.
	}
};
	
	

class K37EventGenerator 
{
protected:
	particle electron, parent, daughter, neutrino;
	G4double QValue, cosBetaNeutrinoAngle, eDotJ, nDotJ, eDotn;

	K37FermiFunction* FF;
	Holstein51_Isotope *the_StandardModel_37K;
	// of event generator for testing purposes
	// Used to only simulate events in a cone around the detectors.
	// Angular distributions are accurate within this cone
	G4double min_cos_theta_;

public:
	G4double Omega, testOmega, Zhi, BigA, BigB, LittleC, LittleA;
	G4double maxOmega;
	K37EventGenerator();
	virtual ~K37EventGenerator();
	Holstein51_Isotope* GetSMphysics() {return the_StandardModel_37K;}

	G4double eMomentumX()     {return electron.X;}
	G4double eMomentumY()     {return electron.Y;}
	G4double eMomentumZ()     {return electron.Z;}
	G4double eMomentumTheta() {return electron.Theta;} // is this really the *momentum* theta?  I suspect it isn't....
	G4double eMomentumPhi()   {return electron.Phi;}
	G4double eMomentumPmag()  {return electron.Pmag;}
//	G4double eKineticEnergy() {return electron.T;}  // ever used?  ... yeah, I don't think so.
	G4double get_electron_T() {return electron.T;}
	G4double get_electron_E() {return electron.E;}
	double get_electron_costheta() { return cos(electron.Theta); }
	double get_electron_voverc() { return electron.get_v_from_p(); }
	double get_e_vX() { return electron.unitX*electron.get_v_from_p(); }
	double get_e_vY() { return electron.unitY*electron.get_v_from_p(); }
	double get_e_vZ() { return electron.unitZ*electron.get_v_from_p(); }
	
	G4double dMomentumX()     {return daughter.X;}
	G4double dMomentumY()     {return daughter.Y;}
	G4double dMomentumZ()     {return daughter.Z;}
	G4double dMomentumTheta() {return daughter.Theta;}
	G4double dMomentumPhi()   {return daughter.Phi;}
	G4double dMomentumPmag()  {return daughter.Pmag;}
	G4double dKineticEnergy() {return daughter.T;}
	
	void SetMinCosTheta(G4double mct);
	void SetConeHalfAngle(G4double angle);  // wrapper
	G4double GetMinCosTheta() {return min_cos_theta_;}
	
	void MakeEvent();
	void MakeEvent(G4double polarization, G4double alignment);
//	void MakeEvent_UniformE(G4double polarization, G4double alignment);
	void ResetGeneratedCounters();
	G4int GetNumMins();
	G4int GetNumPlus();	
private:
	G4int numMins, numPlus;
};



#endif
