// Authors: Spencer Behling and Benjamin Fenker 2013

#ifndef K37PhysicsList_hh
#define K37PhysicsList_hh 1

#include "G4VModularPhysicsList.hh"
#include "G4VUserPhysicsList.hh"  // needed?

#include "G4SystemOfUnits.hh" // this one is needed.
#include "G4UnitsTable.hh"    // not needed?

#include "globals.hh"

//----------------------------------------------------------------------------
class G4Decay;
class G4StepLimiter;

class G4VPhysicsConstructor;
class StepMax;
class K37PhysicsListMessenger;

/*
//----------------------------------------------------------------------------
class K37PhysicsList_: public G4VModularPhysicsList 
{
public:
	K37PhysicsList_();
	~K37PhysicsList_();
	void SetCuts();      // Overloaded in PHysicsList.
protected:
//	void AddStepMax();   // overloaded in PhysicsList

private:
//	G4Decay*       theDecayProcess;
//	G4StepLimiter* stepLimiter;  // no longer used??
};
*/

//----------------------------------------------------------------------------

// PhysicsList class:  
// \file electromagnetic/TestEm7/include/PhysicsList.hh
// Code originally from Geant4 9.x, 
// Pasted here by Ben Fenker and modified slightly.
// Replaced by the equivalent code in Geant 4.10.5, 
// Modified by Melissa Anholm to match Ben Fenker's previous modifications.
// Further modified by Melissa Anholm to incorporate a whole 'nother related
//   class, which was previously coded by some combination of 
//   Spencer/Ben/Melissa.  
// 
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


// class PhysicsList: public G4VModularPhysicsList
//class K37PhysicsList: public K37PhysicsList_   // bf, copied by mja.
class K37PhysicsList: public G4VModularPhysicsList
{
public:
	K37PhysicsList();
   ~K37PhysicsList();

	virtual void ConstructParticle();

	void SetCuts();                         // old version, re-added by mja.
	void SetCutForGamma(G4double);          // old version, re-added by mja.
	void SetCutForElectron(G4double);       // old version, re-added by mja.
	void SetCutForPositron(G4double);       // old version, re-added by mja.

	void AddPhysicsList(const G4String& name);
	virtual void ConstructProcess();  // overloads K37PhysicsList_ definition, which is obsolete.
	void AddStepMax();       
	StepMax* GetStepMaxProcess() {return fStepMaxProcess;};
	G4String GetEmName() {return fEmName;}  // bf, probably.  copied by mja.

	private:
	// to make K37PhysicsList non-copyable
	K37PhysicsList(const K37PhysicsList &);
	// to make K37PhysicsList non-copyable
	const K37PhysicsList & operator=(const K37PhysicsList &);

	void AddIonGasModels();

	G4double cutForGamma;     // old version, re-added by mja.
	G4double cutForElectron;  // old version, re-added by mja.
	G4double cutForPositron;  // old version, re-added by mja.

	G4bool   fHelIsRegisted;
	G4bool   fBicIsRegisted;
	G4bool   fBiciIsRegisted;

	G4String                             fEmName;
	G4VPhysicsConstructor*               fEmPhysicsList;
	G4VPhysicsConstructor*               fDecPhysicsList;
	std::vector<G4VPhysicsConstructor*>  fHadronPhys;    
	
	StepMax*                             fStepMaxProcess;
	G4StepLimiter* stepLimiter;          // no longer used??  from original K37PhysicsList class.

	K37PhysicsListMessenger*  fMessenger;

//	G4Decay*       theDecayProcess;      // no longer used??  from original K37PhysicsList class.
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#endif

