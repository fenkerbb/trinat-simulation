// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 2013

#ifndef K37RunAction_h
#define K37RunAction_h 1

// G4:  
#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4Cache.hh"

// Project:
#include "K37_LocalAggregator.hh"
#include "Generic_Channel.hh"
#include "K37_Data.hh"
//#include "K37ElectricFieldSetup.hh"
#include "K37PhysicsList.hh"
#include "K37RunMessenger.hh"
#include "TDC_Channel.hh"
#include "K37Run.hh"

//
using K37_ABC::K37_Data;

//----------------------------------
class LocalAggregator;
class K37Run;

class K37RunAction : public G4UserRunAction 
{
public:
	K37RunAction();
	~K37RunAction();
	
	void BeginOfRunAction(const G4Run*);  // oh my.... not a K37Run ??
	void EndOfRunAction(const G4Run*);
//	void BeginOfRunAction(const K37Run*);
//	void EndOfRunAction(const K37Run*);

	G4Run* GenerateRun();
//	K37Run * GenerateRun();
	
	void IncrementGenerated()     { N_generated_thread++;    }  
	void IncrementAccepted()      { N_accepted_thread++;     }  
	void IncrementNaiveAccepted() { N_naive_thread++; }  // not used yet?
	
	void PrintEnergyLossTable(G4String);

	void PrintResultsToScreen(/*const K37Run* aRun*/);
	
	// event type stuff from pga.
	void SetEventTypes(G4String EventTypesString);
	void GetEventTypes();
	bool EventAcceptanceChecker(G4int this_event);
	G4String Get_AcceptanceTypesString() { return EventAcceptanceTypesString; }
	
public:
// From G4UserRunAction.hh:  
//    virtual G4Run* GenerateRun();

	LocalAggregator * GetAggregator() { return aggregator_cache.Get(); }
	
	// For the Aggregator:
	G4int Get_Nevents_thread()              { return N_generated_thread; }
	G4int Get_Naccepted_thread()            { return N_accepted_thread;  }
	G4int Get_Nnaive_thread()               { return N_naive_thread;     }  // not used yet.  is it even the right number?
	G4int Get_Nevents_total()               { return N_generated_total; }
	G4int Get_Naccepted_total()             { return N_accepted_total;  }
	G4int Get_Nnaive_total()                { return N_naive_total;     }  // not used yet.  is it even the right number?

private:
	void RegisterChannel(K37_Data* channel);  // K37RunAction::RegisterChannel(...) crashes it now, in multithreaded mode.  ... no, I think it's fine again.
	
//	G4int    NbofEvents;
	G4int    N_generated_thread;
	G4int    N_accepted_thread;
	G4int    N_naive_thread;
	
	G4int    N_generated_total;
	G4int    N_accepted_total;
	G4int    N_naive_total;
	
	
private:
	K37RunMessenger * runMessenger;
	
//	LocalAggregator * the_aggregator;
	G4Cache<LocalAggregator*> aggregator_cache;
	// G4Cache has .Get() and .Put(...) methods.
	
private:
	// Accepted event types (migrated from pga):
	G4int       EventAcceptanceTypesInt;    // 
	G4String    EventAcceptanceTypesString; //
};

//----------------------------------

#endif





