#ifndef K37RunManagerMT_h
#define K37RunManagerMT_h 1


#include "G4MTRunManager.hh"
#include "K37ActionInitialization.hh"
#include "K37DetectorConstruction.hh"
#include "K37PhysicsList.hh"

class K37ActionInitialization;

class K37RunManagerMT : public G4MTRunManager 
{
public:
	K37RunManagerMT() {}
//	virtual ~K37RunManagerMT() {}
//	~K37RunManagerMT() { G4MTRunManager::~G4MTRunManager(); }


//	static G4RunManager* GetRunManager();
//	static G4MTRunManager* GetMasterRunManager();
	static K37RunManagerMT* GetMasterRunManager() { return (K37RunManagerMT*)(G4MTRunManager::GetMasterRunManager()); }
	
//	static K37RunManagerMT* GetRunManager() { return (K37RunManagerMT*)(G4RunManager::GetRunManager()); }  // *should* return the threadlocal object...
	static G4RunManager* GetRunManager() { return G4RunManager::GetRunManager(); }  // *should* return the threadlocal object...  and really, this thing is a duplicate of a function that's already there.
	
//	from G4RunManager:  
//    static G4RunManager* GetRunManager();
//    //  Static method which returns the singleton pointer of G4RunManager or
//    // its derived class.
//    // Note this returns the per-thread singleton in case of multi-threaded
//    // build

// from G4RunManager:
//    inline const G4UserRunAction* GetUserRunAction() const  // apparently it can still be typecast as a K37RunAction*.

// from G4RunManager:
//    inline const G4UserEventAction* GetUserEventAction() const
//    { return userEventAction; }

	
	K37ActionInitialization* GetUserActionInitialization()
		{ return (K37ActionInitialization*)(G4MTRunManager::GetUserActionInitialization()); }
	
//	cloud = ( (K37ActionInitialization*)(G4MTRunManager::GetMasterRunManager()->GetUserActionInitialization()) )->GetCloud();
// From G4RunManager.hh (single-threaded...)
//    inline const G4VUserActionInitialization* GetUserActionInitialization() const
//    { return userActionInitialization; }
		
	K37DetectorConstruction* GetUserDetectorConstruction()
		{ return (K37DetectorConstruction*)(G4MTRunManager::GetUserDetectorConstruction()); }
	
	// from RunManager:   G4VUserPhysicsList * GetUserPhysicsList () const 
//	K37PhysicsList * GetPhysicsList() 
//		{ return (K37PhysicsList*)(G4MTRunManager::GetMasterRunManager()->GetUserPhysicsList() ); }
	K37PhysicsList * GetPhysicsList() { return the_physics_list; }
		
// Maybe do something about this too?
// 	if( ((K37RunAction*)(G4MTRunManager::GetMasterRunManager()->GetUserRunAction())) -> GetRecordStripDetectorData() )

//	this doesn't seem to do what I want.
//	K37RunAction* GetMasterRunAction()
//		{ return ( (K37RunAction*)(GetMasterRunManager()->GetUserRunAction()) ); }

// from G4RunManager.hh:  
//    inline const G4VUserDetectorConstruction* GetUserDetectorConstruction() const
//    { return userDetector; }

// from G4RunManager.hh:  
//    inline const G4VUserPhysicsList* GetUserPhysicsList() const
//    { return physicsList; }
//	K37PhysicsList* GetUserPhysicsList()
//		{ return (K37PhysicsList*)(G4MTRunManager::GetUserPhysicsList() ); }

// for the worker threads:
//	inline const G4UserEventAction* GetUserEventAction() const
//	{ return userEventAction; }
	
	// G4Run* G4RunManager::GetNonConstCurrentRun 	( 		) 	const	
	// const G4Run* G4RunManager::GetCurrentRun 	( 		) 	const
	
public:
	// runManager -> SetUserInitialization(physics);
	void SetUserInitialization( K37PhysicsList * thephysics ) // this apparently overloads *all* of the K37RunManagerMT::SetUserInitialization(...) methods.
	{ 
		the_physics_list=thephysics; 
		G4MTRunManager::SetUserInitialization( (G4VUserPhysicsList*)thephysics );
	}
	void SetUserInitialization( K37DetectorConstruction * detector ) 
	{ 
		the_detector=detector; 
		G4MTRunManager::SetUserInitialization( (G4VUserDetectorConstruction*)detector );
	}
	void SetUserInitialization( K37ActionInitialization * kai )
	{
		action_initialization=kai;
		G4MTRunManager::SetUserInitialization( (G4VUserActionInitialization*)kai );
	}
private:
	K37PhysicsList          * the_physics_list;
	K37DetectorConstruction * the_detector;
	K37ActionInitialization * action_initialization;
};

// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- //
#endif