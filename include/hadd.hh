// Modified by Melissa Anholm in order to make it compile into the K37 G4 code - 2019.
// The hadd.C source available in text format with the root install, from which this is 
// taken and modified, is *not* the same as whatever went into the "hadd" shell script 
// macro.  In particular, the hadd.C function will choke on a TArrayD object, while the 
// "hadd" shell script doesn't seem to mind them.
//
// Original attributions are as below.
//
/// \file
/// \ingroup tutorial_io
/// \notebook -nodraw
/// Macro to add histogram files
/// This macro is kept for didactical purposes only: use instead the executable $ROOTSYS/bin/hadd !
/// 
/// This macro will add histograms from a list of root files and write them
/// to a target root file. The target file is newly created and must not be
/// identical to one of the source files.
/// This code is based on the hadd.C example by Rene Brun and Dirk Geppert,
/// which had a problem with directories more than one level deep.
/// The macro from Sven has been enhanced by Anne-Sylvie Nicollerat <Anne-Sylvie.Nicollerat@cern.ch>
/// to automatically add Trees (via a chain of trees).
///
/// \macro_code
///
/// \author Sven A. Schmidt, sven.schmidt@cern.ch, 13.2.2001

#ifndef havehadd_h
#define havehadd_h 1


#include <string.h>
#include <cstdio>

#include "TChain.h"
#include "TFile.h"
#include "TH1.h"
#include "TTree.h"
#include "TKey.h"
#include "Riostream.h"
//#include "TString.h"

#include "TSystem.h"  // gSystem
using std::cout;  //
using std::endl;  // 

void MergeRootfile( TDirectory *target, TList *sourcelist );

void hadd2(string the_outfilename, vector<string> the_namevector, bool deleteafterwards=false)
{
	if( !gSystem->AccessPathName(the_outfilename.c_str()) )
	{
	//	cout << "File found (BAD):  " << the_outfilename << endl;
		return;
	}
	else
	{
	//	cout << "Can't find file (GOOD):  " << the_outfilename << endl;
	}
	TFile * Target = TFile::Open(the_outfilename.c_str(), "RECREATE");
	
	TList * FileList = new TList();
	int n_files = the_namevector.size();
	
	cout << "Expect to hadd " << n_files << " different root files.  tmp files will ";
	if( !deleteafterwards ) { cout << "NOT "; }
	cout << "be deleted." << endl;
	
	//
	for(int i=0; i<n_files; i++)
	{
	//	cout << "Trying to add " << the_namevector.at(i) << endl;
		if( gSystem->AccessPathName(the_outfilename.c_str()) )
		{
			cout << "\tcouldn't find file.  :(  " << endl;
			return;
		}
		else
		{
			FileList->Add( TFile::Open( the_namevector.at(i).c_str() ) );
		}
	}
	
	MergeRootfile( Target, FileList );
	
	// just delete the extra files.
	if(deleteafterwards)
	{
		int isitdeleted = -1;
		for(int i=0; i<n_files; i++)
		{
			isitdeleted = remove( the_namevector.at(i).c_str() );
		//	if(isitdeleted==0) // if it's zero, it was deleted.  stupid convention.
		//	{
		//		cout << "File " << the_namevector.at(i) << " has been deleted." << endl;
		//	}
		//	else
		//	{
		//		cout << "File " << the_namevector.at(i) << " has NOT been deleted, even though we tried." << endl;
		//	}
		}
	}
}

void MergeRootfile( TDirectory *target, TList *sourcelist ) 
{
	cout << "Target path: " << target->GetPath() << endl;
	TString path( (char*)strstr( target->GetPath(), ":" ) );
	path.Remove( 0, 2 );
	
//	cout << "Creating file: " << path.ReadString() << endl;

	TFile *first_source = (TFile*)sourcelist->First();
	first_source->cd( path );
	TDirectory *current_sourcedir = gDirectory;
	//gain time, do not add the objects in the list in memory
	Bool_t status = TH1::AddDirectoryStatus();
	TH1::AddDirectory(kFALSE);

	// loop over all keys in this directory
	TChain *globChain = 0;
	TIter nextkey( current_sourcedir->GetListOfKeys() );
	TKey *key, *oldkey=0;
	while ( (key = (TKey*)nextkey()) ) 
	{
		//keep only the highest cycle number for each key
		if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) { continue; }
		
		// read object from first source file
		first_source->cd( path );
		TObject *obj = key->ReadObj();

		if ( obj->IsA()->InheritsFrom( TTree::Class() ) ) 
		{
			// loop over all source files create a chain of Trees "globChain"
			const char* obj_name= obj->GetName();

			globChain = new TChain(obj_name);
			globChain->Add(first_source->GetName());
			
			TFile *nextsource = (TFile*)sourcelist->After( first_source );
			while ( nextsource ) 
			{
				globChain->Add(nextsource->GetName());
				nextsource = (TFile*)sourcelist->After( nextsource );
			}
		} 
		else // object is of no type that we know or can handle
		{
			// I'm not sure this actually gets called in the event that it's not a TTree...
			cout << "Unknown object type, name: " << obj->GetName() << " title: " << obj->GetTitle() << endl;
		}
		
		// now write the merged histogram (which is "in" obj) to the target file
		// note that this will just store obj in the current directory level,
		// which is not persistent until the complete directory itself is stored
		// by "target->Write()" below
		if ( obj ) 
		{
			target->cd();
			
			//!!if the object is a tree, it is stored in globChain...
			if(obj->IsA()->InheritsFrom( TTree::Class() ))  { globChain->Merge(target->GetFile(),0,"keep"); }
			else                                            { obj->Write( key->GetName() ); }
		}
	}
	// save modifications to target file
	target->SaveSelf(kTRUE);
	TH1::AddDirectory(status);
}


#endif