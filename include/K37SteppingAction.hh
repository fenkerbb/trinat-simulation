// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013

#ifndef K37SteppingAction_H
#define K37SteppingAction_H 1

#include <map>
#include <string>
#include "globals.hh"
#include "G4UserSteppingAction.hh"
//#include "K37_Data.hh"

class K37SteppingActionMessenger;
//class K37_ABC::K37_Data;
//class K37_Data;

using std::map;
using std::string;

class K37SteppingAction : public G4UserSteppingAction 
{
public:
	K37SteppingAction();
	virtual ~K37SteppingAction();
	virtual void UserSteppingAction(const G4Step*);

//	void SetActiveChannels(map<string, K37_ABC::K37_Data*> *active_channels_)  // not used.
//		{ active_channels = active_channels_; }
	
	void TrackNames(const bool &ToTrack_)
		{ ToTrack = ToTrack_; }
	
private:
//	map<string, K37_ABC::K37_Data*> *active_channels;
	K37SteppingActionMessenger *stepping_messenger;
	bool ToTrack;
};

#endif

