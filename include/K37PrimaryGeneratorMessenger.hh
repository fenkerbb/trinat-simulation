// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm 2013

#ifndef K37PrimaryGeneratorMessenger_h
#define K37PrimaryGeneratorMessenger_h 1

//#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIdirectory.hh"
#include "G4UImessenger.hh"
#include "G4UnitsTable.hh"
#include "globals.hh"

#include "K37PrimaryGeneratorAction.hh"

class K37PrimaryGeneratorAction;
//----------------------------------

class K37PrimaryGeneratorMessenger: public G4UImessenger 
{
public:
	explicit  K37PrimaryGeneratorMessenger(K37PrimaryGeneratorAction* pga);
	virtual ~K37PrimaryGeneratorMessenger();

	void SetNewValue(G4UIcommand*, G4String);

private:
	K37PrimaryGeneratorAction* action_;
	G4UIdirectory*             gunDir;

	G4UIcmdWithAnInteger       *set_recoil_charge_cmd_;
	G4UIcmdWithAString         *set_chargestate_file_;
	G4UIcmdWithABool           *get_chargestate_;

	G4UIcmdWithABool           *set_make_beta_;
	G4UIcmdWithABool           *set_make_recoil_;
	G4UIcmdWithABool           *set_make_shakeoff_electrons_;
	// For creating maps...
	G4UIcmdWithABool           *set_make_monoenergetic_;
	G4UIcmdWithABool           *set_make_uniform_energy_;
	G4UIcmdWithADoubleAndUnit  *set_monoenergetic_energy_;
	
	G4UIcmdWithADouble         *set_minimum_cos_theta_cmd_;
	G4UIcmdWithADoubleAndUnit  *set_cone_half_angle_cmd_;
	G4UIcmdWithoutParameter    *toggle_gps_;
	G4UIcmdWithABool           *set_gps_;
	
//	G4UIcmdWithAString * set_event_acceptance_types_;
//	G4UIcmdWithABool   * get_event_acceptance_types_;
};

//----------------------------------

#endif

