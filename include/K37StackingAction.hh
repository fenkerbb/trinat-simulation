// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013

#ifndef K37StackingAction_h
#define K37StackingAction_h 1

#include "G4UserStackingAction.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"

#include "K37ScintillatorHit.hh"
#include "K37StackingActionMessenger.hh"
#include "K37StripDetectorHit.hh"

class G4Track;
class K37StackingActionMessenger;

// -----------------------------------------------

class K37StackingAction : public G4UserStackingAction 
{
public:
	K37StackingAction();
	~K37StackingAction();
	
	G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);
	void NewStage();
	void PrepareNewEvent();
	void SetTrackSecondaries(G4bool track) {track_secondaries_ = track;}
	
private:
	K37ScintillatorHitsCollection* scintillatorPlusZHits;   // surely I shouldn't be storing this here, right? 
	K37ScintillatorHitsCollection* scintillatorMinusZHits;  // surely I shouldn't be storing this here, right? 
	K37StripDetectorHitsCollection* stripDetectorPlusZHits; // surely I shouldn't be storing this here, right? 
	K37StripDetectorHitsCollection* stripDetectorMinusZHits;// surely I shouldn't be storing this here, right? 
	// K37ElectronMCPHit things crash it, so maybe this part is actually fine...
	
	G4VHitsCollection* GetCollection(G4String colName);  // do I really want to be able to do this?
	G4bool GetPointersToHitCollections(); // do I really want to be able to do this?
	
	G4ParticleDefinition *electron;
	G4ParticleDefinition *positron;
	G4ParticleDefinition *gamma;
	K37StackingActionMessenger *messenger_;
	G4bool track_secondaries_;
};

#endif
