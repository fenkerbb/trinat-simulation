// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 
// 2013 - 

#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sstream>
#include <iomanip>
#include <unistd.h>

#include <TROOT.h>

#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4SystemOfUnits.hh"  // is this actually needed here?
#include "G4UnitsTable.hh"

#include "K37RunManagerMT.hh"
#include "K37ActionInitialization.hh"   // multi-threaded
#include "K37DetectorConstruction.hh"
#include "K37PhysicsList.hh"
#include "K37Config.hh"


#undef G4VIS_USE
//#ifdef G4VIS_USE  // Where is this even defined?  ... it's in Geant4Config.cmake, which somehow gets included through cmake.
//#include "G4VisExecutive.hh"
//#endif

#ifdef G4UI_USE // defined in Geant4Config.cmake, which somehow gets included through cmake.
#include "G4UIExecutive.hh"  // arguably, this is only needed for #ifdef G4UI_USE [defined in Geant4Config.cmake].  But I'm going to include it either way.  ...or am I?
#endif

#ifdef K37_MULTITHREADING
	#ifndef G4MULTITHREADED
		assert(0);
	#endif
#else
#endif

int main(int argc, char** argv) 
{
//	G4cout << "=================================" << G4endl;
//	G4cout << "This is K37 revision " << K37_VERSION << G4endl;
//	G4cout << "=================================" << G4endl;
//	
//	#ifdef K37_MULTITHREADING
//		G4cout << "OK but really the multithreading is happening." << G4endl;
//	#else
//		G4cout << "OK but really, there's not going to be any multi-threading." << G4endl;
//	#endif
	
//-------------------------------------------------------------
	CLHEP::Ranlux64Engine randomEngine;
	CLHEP::HepRandom::setTheEngine(&randomEngine);
	timeval t1;
	gettimeofday(&t1, NULL);
	
	int pid = ::getpid();
	G4cout << "pid: " << pid << G4endl;
	
	G4long randseed = (t1.tv_usec * t1.tv_sec) + pid;
	G4cout << "randseed: " << randseed << G4endl;
	CLHEP::HepRandom::setTheSeed(randseed);
	G4Random::showEngineStatus();
//------------------------------------------------------------- //
	ROOT::EnableImplicitMT();
	ROOT::EnableThreadSafety();
//------------------------------------------------------------- //
	// Run manager.
	// multi/single threading for the run manager must be controlled with a compiler flag.
	G4cout << "Creating the runManager... ";
	#ifdef K37_MULTITHREADING
		G4cout << "It's multi-threaded!" << G4endl;
	//	G4MTRunManager  * runManager = new G4MTRunManager();
		K37RunManagerMT * runManager = new K37RunManagerMT();
		runManager -> SetNumberOfThreads(8);
	#else
		G4cout << "It's single-threaded." << G4endl;
		G4RunManager* runManager = new G4RunManager();
	#endif	
	
	K37PhysicsList          * physics  = new K37PhysicsList();  //
	K37DetectorConstruction * detector = new K37DetectorConstruction();  // this thing is responsible for the electric field now too.
	
	// User Initialization classes (mandatory)
	runManager -> SetUserInitialization(detector);   // detector.  global, so put it directly into the run manager.  good for single- and multi-.
	runManager -> SetUserInitialization(physics);    // the physics list is global, so it can go right into the run manager.  this works for single- and multi-.
	
	#ifdef K37_MULTITHREADING
		G4cout << "Now calling SetUserInitialization [multithreaded] ..." << G4endl;
	#else
		G4cout << "Now calling SetUserInitialization [sequential], and it might not work ..." << G4endl;
	#endif
		
	runManager -> SetUserInitialization( new K37ActionInitialization() );  // makes a new cloud.  just the one.
	// makes a new K37RunAction too.  Probably several of them!
	// at first, it's just the one.  the others aren't created until runManager->Initialize();  
	
	/*
	#ifndef K37_MULTITHREADING
		map<string, K37_Data*> active_channels;
		
		// User Action classes
		// The main 98% branch 
		K37EventGenerator * evGen = new K37EventGenerator();
		// The 2% branch 
		K37EventGeneratorNoRecoilOrderEffects *twoPercent = new K37EventGeneratorNoRecoilOrderEffects();  
		//
		K37PrimaryGeneratorAction* gen_action = new K37PrimaryGeneratorAction(evGen, twoPercent);
		the_aggregator -> SetPrimaryGeneratorAction(gen_action);  
		
		// originally, 'the_aggregator -> SetPrimaryGeneratorAction(gen_action);' came after 'runManager -> SetUserAction(gen_action);'.  
		// Can we move it to before?  ...yes, yes we can.
		// this is needed *only* to collect and record cloud parameters and recoil charge *to the root file* at the end of the run.  
		// the K37RunAction is what records to the metadata file.
		
		G4cout << "Creating a new K37RunAction* ..." << G4endl;
		K37RunAction* run_action = new K37RunAction();           // run action is thread-local.
		run_action -> SetActiveChannels(&active_channels);  // registers all the channels, by telling the aggregator to do it.
		run_action -> SetAggregator(the_aggregator);
		run_action -> SetPhysicsList(physics);   // aaaaand, we put the physics list into the run action, too.
		run_action -> SetDetectorConstruction(detector);
		
		G4cout << "Single-threaded.  nothing funny happening here." << G4endl;
		runManager -> SetUserAction(gen_action);  // MyActionInitialization.
		runManager -> SetUserAction(run_action);  // this has to go here.  or at least somewhere.  it has to exist.  at least for single-threaded.  is it multithreaded also?
		
		K37EventAction* event_action = new K37EventAction(run_action);
		runManager   -> SetUserAction(event_action);
		
		K37SteppingAction *stepping_action = new K37SteppingAction();  // this is a whole different class from the stepping verbose.
		stepping_action -> SetActiveChannels(&active_channels);
	
		runManager->SetUserAction(stepping_action);
		runManager->SetUserAction(new K37StackingAction);
	#endif
	*/
	
//-------------------------------------------------------------
	G4UImanager * UImanager = G4UImanager::GetUIpointer();
	G4UIExecutive * ui = new G4UIExecutive(argc, argv);   
	UImanager -> ApplyCommand("/control/execute SetupScripts/Setup_PreInit.mac");
	
	#ifdef G4VIS_USE  // I don't know what fucking any of this stuff does.
		G4VisManager * visManager = new G4VisExecutive("quiet");
		visManager->Initialize();
	
		G4VisTrajContext * theContext = new G4VisTrajContext("ParticleTypeContext");
		theContext->SetDrawAuxPts(true);
		theContext->SetDrawStepPts(true);
		G4TrajectoryDrawByParticleID * model =  new G4TrajectoryDrawByParticleID("ParticleType", theContext);
		visManager->RegisterModel(model);
		visManager->SelectTrajectoryModel(model->Name());
		
		UImanager -> ApplyCommand("/control/execute SetupScripts/vis_PreInit.mac");
	#endif
	
	//
	G4cout << "Initializing..." << G4endl;
	runManager->Initialize();  // equivalent to:  UImanager->ApplyCommand("/run/initialize");  This also starts all the worker threads.
	G4cout << "And so it begins." << G4endl;
	//
	UImanager -> ApplyCommand( (string("/K37/RunControls/setOutputDirectory")+string(OUTDIR)).c_str() );  // OUTDIR is set by cmake via K37Config.hh.in
	
	UImanager -> ApplyCommand("/control/execute SetupScripts/Setup_PostInit.mac");
	UImanager -> ApplyCommand("/tracking/verbose 0");  // not sure if I need this for non-visualizer things too?
	#ifdef G4VIS_USE
		UImanager -> ApplyCommand("/control/execute SetupScripts/vis_PostInit.mac");
	#endif
	
	
	G4cout << "starting the ui session ..." << G4endl;
	ui->SessionStart();  // Begin interactive terminal session.
	// Here's where all the content happens.
	
	
	
	// Job termination
	// Free the store: user actions, physics_list and detector_description are
	//                 owned and deleted by the run manager, so they should not
	//                 be deleted in the main() program !
	delete ui;
	G4cout << "ui session is deleted." << G4endl;
	
	//
	#ifdef G4VIS_USE
		if (visManager) 
		{
			G4cout << "We're deleting the visManager now..." << G4endl;
			delete visManager;
			G4cout << "Done." << G4endl;
		}
		else
		{
			G4cout << "No visManager, so we won't try to delete it." << G4endl;
		}
	#endif
		
	//
//	if (runManager)  { delete runManager; }
//	G4cout << "Deleted the run manager." << G4endl;
	//
	if (physics)     { delete physics;    }
	G4cout << "Deleted physics." << G4endl;
	if (detector)    { delete detector;   }
	G4cout << "Deleted detector." << G4endl;
	// srsly?  it gives me warnings ***now*** ??
	
	return 0;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
