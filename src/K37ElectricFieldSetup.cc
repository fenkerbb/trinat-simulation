// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 2013

#include "K37ElectricFieldSetup.hh"
//#include "K37ElectricFieldMessenger.hh"

#include "G4UniformElectricField.hh"

#include "G4ExplicitEuler.hh"
#include "G4ImplicitEuler.hh"
#include "G4SimpleRunge.hh"
#include "G4SimpleHeum.hh"
#include "G4ClassicalRK4.hh"
#include "G4HelixExplicitEuler.hh"
#include "G4HelixImplicitEuler.hh"
#include "G4HelixSimpleRunge.hh"
#include "G4CashKarpRKF45.hh"
#include "G4RKG3_Stepper.hh"

//--------------------------------------------------------------------
//  Constructor:
K37ElectricFieldSetup::K37ElectricFieldSetup() : 
	fFieldMessenger(0),
	fChordFinder(0), 
	fStepper(0), 
	fIntgrDriver(0), 
	fEfieldMagnitude(350.0*volt/cm)
{
	G4cout << "* Created a new K37ElectricFieldSetup()." << G4endl;
	
	// Put unit definitions here, rather than in class?  
	// something needs to have been declared already, otherwise it breaks.
	// I think it's the run manager.  
	// exampleB3 puts the new units into the run action.  That doesn't work for us.
	// They're going in K37ElectricFieldSetup() instead.
	new G4UnitDefinition("kilovolt/cm", "kV/cm", "Electric field", kilovolt/cm);
	new G4UnitDefinition("volt/cm", "V/cm", "Electric field", volt/cm);
	// ....wait, how does this not break with the default value given before the units are defined?
	
	fElFieldValue   = G4ThreeVector(0.0, fEfieldMagnitude, 0.0);
	fMinStep        = 0.010*mm;  // minimal step of 10 microns
	fStepperType    = 5;         // 4 = ClassicalRK4 -- the default stepper.   Why is it 5 ??
	
	//
	fEMfield        = new G4UniformElectricField(fElFieldValue);
	fEquation       = new G4EqMagElectricField(fEMfield);
	
	fFieldMessenger = new K37ElectricFieldMessenger(this);
	
	UpdateIntegrator();
	/*
	CreateStepper();   // called by UpdateIntegrator().
	
	fIntgrDriver = new G4MagInt_Driver(fMinStep, fStepper, fStepper->GetNumberOfVariables() );  // called by UpdateIntegrator()
	fChordFinder = new G4ChordFinder(fIntgrDriver);  // called by UpdateIntegrator().

	G4FieldManager* fieldManager = G4TransportationManager::GetTransportationManager()->GetFieldManager();
	fieldManager->SetDetectorField(fEMfield);    // called by UpdateIntegrator().
	fieldManager->SetChordFinder(fChordFinder);  // called by UpdateIntegrator().
	*/
}

//--------------------------------------------------------------------
// Destructor: 
K37ElectricFieldSetup::~K37ElectricFieldSetup() 
{
//	G4cout << "* Deleting the K37ElectricFieldSetup." << G4endl;
	
	// Delete the messenger first, to avoid messages to deleted classes!
	if (fFieldMessenger) { delete fFieldMessenger; }
	else 
	{
		G4cout << "Couldn't find fFieldMessenger, so we won't try to delete it." << G4endl;
	} 
	
	
	// what order do these even go in??
	if (fChordFinder) { delete fChordFinder; fChordFinder= nullptr; }
	else 
	{
		G4cout << "Couldn't find fChordFinder, so we won't try to delete it." << G4endl;
	}
	if (fStepper) { delete fStepper; fStepper = nullptr; }
	else 
	{
		G4cout << "Couldn't find fStepper, so we won't try to delete it." << G4endl;
	}
	if (fEquation) { delete fEquation; fEquation = nullptr; }
	else 
	{
		G4cout << "Couldn't find fEquation, so we won't try to delete it." << G4endl;
	}
	if (fEMfield) { delete fEMfield; fEMfield = nullptr; }
	else 
	{
		G4cout << "Couldn't find fEMfield, so we won't try to delete it." << G4endl;
	}
}


// -------------------------------------------------------------------- //
// -------------------------------------------------------------------- //
void K37ElectricFieldSetup::SetMinStep(G4double s)  // this is never going to work without more things.
{
	G4cout << "* Called K37ElectricFieldSetup::SetMinStep(...)" << G4endl;
	fMinStep = s;
	//
	if(!fIntgrDriver)
	{
		fIntgrDriver = new G4MagInt_Driver(fMinStep, fStepper, fStepper->GetNumberOfVariables() );
	}
	else
	{
		fIntgrDriver-> SetHmin(fMinStep);
		fIntgrDriver-> RenewStepperAndAdjust(fStepper);  // this probably accounts for fMinStep as well...
	}
	//
	if(!fChordFinder)
	{
		fChordFinder = new G4ChordFinder(fIntgrDriver);
	}
	else
	{
		fChordFinder->SetIntegrationDriver(fIntgrDriver);
	}
	GetGlobalFieldManager()->SetChordFinder(fChordFinder);
	return;
}  
	

//--------------------------------------------------------------------
// Set stepper according to the stepper type
void K37ElectricFieldSetup::SetStepperType(G4int i) 
{
	fStepperType = i;
	CreateStepper();   // sets fStepper, and makes a new equation.
	
//	GetGlobalFieldManager()->SetDetectorField(fEMfield);  // we have an equation, but do we want to make a new fEMfield now??
	// fEquation takes fEMfield as an argument..
	// fEMfield just needs the field strength as an argument.  
	// so, we don't do anything to fEMfield when we set the stepper. 
	
	if(!fIntgrDriver)
	{
		G4cout << "No fIntgrDriver !  Making one now." << G4endl;
		fIntgrDriver = new G4MagInt_Driver(fMinStep, fStepper, fStepper->GetNumberOfVariables() );
	}
	else
	{
		fIntgrDriver-> RenewStepperAndAdjust(fStepper);
	}

	if(!fChordFinder)
	{
		G4cout << "There's no fChordFinder !  :(  Creating one now." << G4endl;
		fChordFinder = new G4ChordFinder(fIntgrDriver);
	}
	else
	{
		fChordFinder->SetIntegrationDriver(fIntgrDriver);
	}
	GetGlobalFieldManager()->SetChordFinder(fChordFinder);
	
	return;
}

G4String K37ElectricFieldSetup::GetStepperName()
{
	G4String StepperName = "Unknown";
	if(fStepperType==0) { StepperName = "G4ExplicitEuler";      }
	if(fStepperType==1) { StepperName = "G4ImplicitEuler";      }
	if(fStepperType==2) { StepperName = "G4SimpleRunge";        }
	if(fStepperType==3) { StepperName = "G4SimpleHeum";         }
	if(fStepperType==4) { StepperName = "G4ClassicalRK4";       }
	if(fStepperType==5) { StepperName = "G4CashKarpRKF45";      }
	if(fStepperType==6) { StepperName = "G4RKG3_Stepper";       }  // broken?
	if(fStepperType==7) { StepperName = "G4HelixExplicitEuler"; }  // broken?
	if(fStepperType==8) { StepperName = "G4HelixImplicitEuler"; }  // broken?
	if(fStepperType==9) { StepperName = "G4HelixSimpleRunge";   }  // broken?
	
	return StepperName;
}

void K37ElectricFieldSetup::CreateStepper() // Formerly SetStepper().
{
	G4cout << "Called K37ElectricFieldSetup::CreateStepper()." << G4endl;
	
	G4int nvar = 8;
	if (fStepper) { delete fStepper; }
	//
	G4cout << "Set fStepperType = " << fStepperType << G4endl;
	G4cout << "For electric field tracking... ";
	switch (fStepperType) 
	{
		case 0:
			fStepper = new G4ExplicitEuler(fEquation, nvar);
			G4cout << "G4ExplicitEuler is calledS" << G4endl;
			break;
		case 1:
			fStepper = new G4ImplicitEuler(fEquation, nvar);
			G4cout << "G4ImplicitEuler is called" << G4endl;
			break;
		case 2:
			fStepper = new G4SimpleRunge(fEquation, nvar);
			G4cout << "G4SimpleRunge is called" << G4endl;
			break;
		case 3:
			fStepper = new G4SimpleHeum(fEquation, nvar);
			G4cout << "G4SimpleHeum is called" << G4endl;
			break;
		case 4:
			fStepper = new G4ClassicalRK4(fEquation, nvar);
			G4cout << "G4ClassicalRK4 (default) is called" << G4endl;
			break;
		case 5:
			fStepper = new G4CashKarpRKF45(fEquation, nvar);
			G4cout << "G4CashKarpRKF45 is called" << G4endl;
			break;
		case 6:
			fStepper = 0;  // new G4RKG3_Stepper( fEquation, nvar );
			G4cout << "G4RKG3_Stepper is not currently working for Electric Field"
				   << G4endl;
			break;
		case 7:
			fStepper = 0;  // new G4HelixExplicitEuler( fEquation );
			G4cout << "G4HelixExplicitEuler is not valid for Electric Field"
				   << G4endl;
			break;
		case 8:
			fStepper = 0;  // new G4HelixImplicitEuler( fEquation );
			G4cout << "G4HelixImplicitEuler is not valid for Electric Field"
				   << G4endl;
			break;
		case 9:
			fStepper = 0;  // new G4HelixSimpleRunge( fEquation );
			G4cout << "G4HelixSimpleRunge is not valid for Electric Field"
				   << G4endl;
			break;
		default:
			fStepper = 0;
			G4cout << "That stepper type is not recognized." << G4endl;
			break;
	} // end of switch statement.
	G4cout << "end of K37ElectricFieldSetup::CreateStepper()." << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// UpdateIntegrator() is taken from example F02 and modified to suit.
void K37ElectricFieldSetup::UpdateIntegrator()
{
	// Register this field to 'global' Field Manager and
	// Create Stepper and Chord Finder with predefined type, minstep (resp.)

	G4FieldManager* fFieldManager = G4TransportationManager::GetTransportationManager()->GetFieldManager();

	// It must be possible to call 'again' after an alternative stepper
	//   has been chosen, or other changes have been made
	assert(fEquation!=nullptr);
	G4cout<< " K37ElectricFieldSetup: The minimal step is equal to " << fMinStep/mm << " mm" << G4endl;
	
	if (fChordFinder) 
	{
		delete fChordFinder;
		fChordFinder= nullptr;
		// The chord-finder's destructor deletes the driver
		fIntgrDriver= nullptr;
	}
	
	// Currently driver does not 'own' stepper      ( 17.05.2017 J.A. )
	//   -- so this stepper is still a valid object after this
	if( fStepper ) 
	{
		delete fStepper;
		fStepper = nullptr;
	}
	
	// Create the new objects, in turn for all relevant classes
	//  -- Careful to call this after all old objects are destroyed, and
	//      pointers nullified.
	CreateStepper();  // Note that this method deleted the existing Stepper!
	// G4cout << "F02ElectricFieldSetup::UpdateIntegrator> "
	//        << "New value of stepper ptr= " << fStepper << G4endl;
	assert(fStepper != nullptr);

	if( fStepper ) 
	{
		fIntgrDriver = new G4MagInt_Driver(fMinStep,
		                                   fStepper,
		                                   fStepper->GetNumberOfVariables());
		if( fIntgrDriver )
		{ 
			fChordFinder = new G4ChordFinder(fIntgrDriver);
		}
	}
	
	fFieldManager->SetChordFinder(fChordFinder);
	fFieldManager->SetDetectorField(fEMfield);
}


//--------------------------------------------------------------------
// Set the value of the Global Field value to fieldVector
void K37ElectricFieldSetup::SetFieldValueV(G4ThreeVector fieldVector) 
{
	if (fEMfield) { delete fEMfield; }

	fElFieldValue = fieldVector;
	if (fieldVector != G4ThreeVector(0., 0., 0.)) 
	{
		fEMfield = new G4UniformElectricField(fieldVector);
	//	if (fEquation) { delete fEquation; } // ... no?
	//	fEquation = new G4EqMagElectricField(fEMfield);  // ...no?
	//	GetGlobalFieldManager()->SetDetectorField(fEMfield);
	} 
	else 
	{
		// If the new field's value is Zero, then it is best to
		// ensure that it is not used for propagation.
		fEMfield = 0;
	//	if (fEquation) { delete fEquation; }  // ... no?
	//	fEquation = 0;  // ...no?
	}
	GetGlobalFieldManager()->SetDetectorField(fEMfield);
	fEquation->SetFieldObj(fEMfield);  // must now point to the new field
	
	fEfieldMagnitude = fElFieldValue.mag();  // update the value of the field magnitude.
}


//--------------------------------------------------------------------
// Set the value of the Global Field to fieldValue along Y
void K37ElectricFieldSetup::SetFieldValue(G4double fieldValue) 
{
	G4ThreeVector fieldVector(0.0, fieldValue, 0.0);
	SetFieldValueV(fieldVector);
	G4cout << "Set a new electric field value:  " << G4BestUnit(fieldValue, "Electric field") << G4endl;
//	G4cout << "Set a new electric field value:  " << fieldValue/(kilovolts/cm) << " kV/cm" << G4endl;  // these aren't real units right now, I guess...
}
//--------------------------------------------------------------------



//--------------------------------------------------------------------
//  Utility method
//G4FieldManager*  K37ElectricFieldSetup::GetGlobalFieldManager() 
// Do I really want this to be a method of the K37ElectricFieldSetup class?  .... meh, it's fine.
//{
//	return G4TransportationManager::GetTransportationManager()->GetFieldManager();
//}
