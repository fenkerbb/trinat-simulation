// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm 2013

#include "K37CloudMessenger.hh"
#include "K37CloudSetup.hh"
#include "K37Config.hh"

// ----------------------------------
K37CloudMessenger::K37CloudMessenger(K37CloudSetup * cloud)
   :the_cloud(cloud)
{
	
	new G4UnitDefinition("millimeter/nanosecond", "mm/ns", "Velocity", mm/ns);
	new G4UnitDefinition("millimeter/microsecond", "mm/us", "Velocity", mm/microsecond);
	new G4UnitDefinition("meter/second", "m/s", "Velocity", m/s);
	new G4UnitDefinition("mKelvin", "mK", "Temperature", kelvin/1000.0);
	
	
	cloudDir = new G4UIdirectory("/K37/Cloud/");
	cloudDir->SetGuidance("Cloud control");

	// Commands that go here:
	cloud_print_cmd_ = new G4UIcmdWithoutParameter("/K37/Cloud/Print", this);
	cloud_print_cmd_ -> SetGuidance("Print out the cloud settings.");
	
	set_cloud_matched_runset_cmd_ = new G4UIcmdWithAString("/K37/Cloud/SetMatchedRunset", this);
	set_cloud_matched_runset_cmd_ -> SetGuidance("Select a specific 'runset' letter to be saved as metadata for this run.");
	set_cloud_matched_runset_cmd_ -> SetParameterName("MatchedRunset", true);
	set_cloud_matched_runset_cmd_ -> SetDefaultValue("0");  //

	
	set_pol_cmd_ = new G4UIcmdWithADouble("/K37/Cloud/SetPolarization", this);
	set_pol_cmd_ -> SetGuidance("Enter a new polarization.  (-1 -> 1)");
	set_pol_cmd_ -> SetParameterName("Polarization", false);
	set_pol_cmd_ -> SetDefaultValue(1.0);

	set_ali_cmd_ = new G4UIcmdWithADouble("/K37/Cloud/SetAlignment", this);
	set_ali_cmd_ -> SetGuidance("Enter a new alignment.  (-1 -> 1)");
	set_ali_cmd_ -> SetParameterName("Alignment", false);
	set_ali_cmd_ -> SetDefaultValue(1.0);

	set_cloud_center_ = new G4UIcmdWith3VectorAndUnit("/K37/Cloud/SetCloudCenter", this);
	set_cloud_center_ -> SetGuidance("Enter the coordinates of the center of ");
	set_cloud_center_ -> SetGuidance("the cloud at the start of the optical ");
	set_cloud_center_ -> SetGuidance("pumping cycle.");
	set_cloud_center_ -> SetParameterName("x", "y", "z", true);
	set_cloud_center_ -> SetDefaultValue(G4ThreeVector(0.0, 0.0, 0.0));
	set_cloud_center_ -> SetUnitCategory("Length");
	
	set_initial_cloud_size_v_ = new G4UIcmdWith3VectorAndUnit("/K37/Cloud/SetCloudSizeV", this);
	set_initial_cloud_size_v_ -> SetGuidance("Enter cloud size (sigma) at the start of the OP cycle.");
	set_initial_cloud_size_v_ -> SetParameterName("wx", "wy", "wz", true);
	set_initial_cloud_size_v_ -> SetDefaultValue(G4ThreeVector(0.0, 0.0, 0.0));
	set_initial_cloud_size_v_ -> SetUnitCategory("Length");

	set_initial_cloud_size_d_ = new G4UIcmdWithADoubleAndUnit("/K37/Cloud/SetCloudSize", this);
	set_initial_cloud_size_d_ -> SetGuidance("Enter spherical cloud size");
	set_initial_cloud_size_d_ -> SetGuidance(" (sigma) at the start of the");
	set_initial_cloud_size_d_ -> SetGuidance(" optical pumping cycle.");
	set_initial_cloud_size_d_ -> SetParameterName("width (sigma)", true);
	set_initial_cloud_size_d_ -> SetDefaultValue(0.0);
	set_initial_cloud_size_d_ -> SetUnitCategory("Length");

	set_cloud_sail_velocity_v_ = new G4UIcmdWith3VectorAndUnit("/K37/Cloud/SetSailVelocity", this);
	set_cloud_sail_velocity_v_ -> SetGuidance("Enter the cloud's sail velocity as a vector");
	set_cloud_sail_velocity_v_ -> SetParameterName("vx", "vy", "vz", false);
	set_cloud_sail_velocity_v_ -> SetUnitCategory("Velocity");

	set_cloud_temperature_v_ = new G4UIcmdWith3VectorAndUnit("/K37/Cloud/SetTemperatureV", this);
	set_cloud_temperature_v_ -> SetGuidance("Enter temperature for the atom");
	set_cloud_temperature_v_ -> SetGuidance("as a vector");
	set_cloud_temperature_v_ -> SetParameterName("T_x", "T_y", "T_z", true);
	set_cloud_temperature_v_ -> SetDefaultValue(G4ThreeVector(0.0, 0.0, 0.0));
	set_cloud_temperature_v_ -> SetUnitCategory("Temperature");

	set_cloud_temperature_d_ = new G4UIcmdWithADoubleAndUnit("/K37/Cloud/SetTemperature", this);
	set_cloud_temperature_d_ -> SetGuidance("Enter temperature for the atoms.");
	set_cloud_temperature_d_ -> SetGuidance("Note that this will be applied to");
	set_cloud_temperature_d_ -> SetGuidance(" every direction!");
	set_cloud_temperature_d_ -> SetParameterName("Temperature", true);
	set_cloud_temperature_d_ -> SetDefaultValue(0.0);
	set_cloud_temperature_d_ -> SetUnitCategory("Temperature");

	set_cloud_expansiontime_ = new G4UIcmdWithADoubleAndUnit("/K37/Cloud/SetExpansionTime", this);
	set_cloud_expansiontime_ -> SetGuidance("Enter the free expansion time after shutting off MOT and before full polarization.");
	set_cloud_expansiontime_ -> SetUnitCategory("Time");

	set_cloud_opcycle_time_ = new G4UIcmdWithADoubleAndUnit("/K37/Cloud/SetOPcycleTime", this);
	set_cloud_opcycle_time_ -> SetGuidance("Enter the length of time between shutting off the MOT and ending OP.");
	set_cloud_opcycle_time_ -> SetUnitCategory("Time");

}

// ----------------------------------

K37CloudMessenger::~K37CloudMessenger() 
{
	delete cloudDir;
	
	delete cloud_print_cmd_;
	delete set_pol_cmd_;
	delete set_ali_cmd_;

	delete set_cloud_temperature_v_;
	delete set_cloud_temperature_d_;
	delete set_initial_cloud_size_v_;
	delete set_initial_cloud_size_d_;
	delete set_cloud_expansiontime_;
	delete set_cloud_opcycle_time_;
	delete set_cloud_center_;
	delete set_cloud_sail_velocity_v_;
	delete set_cloud_matched_runset_cmd_;
}

// ----------------------------------

void K37CloudMessenger::SetNewValue(G4UIcommand* command, G4String newValue) 
{
	if (command == cloud_print_cmd_) 
	{ //   /K37/gun/Cloud/Print
		G4cout << "*** *** *** *** *** *** *** *** *** *** *** *** *** *** " << G4endl;
		G4cout << "*  Cloud position:        ";
		G4cout << G4BestUnit(the_cloud->GetCloudCenter(), "Length") << G4endl;
		
		G4cout << "*  Cloud size (sigma):    "; 
		G4cout << G4BestUnit(the_cloud->GetInitialCloudSize(), "Length") << G4endl;
		
		G4cout << "*  Cloud temperature:     ";
		G4cout << G4BestUnit(the_cloud->GetTemperature(), "Temperature") << G4endl;

		G4cout << "*  Cloud sail velocity:   ";
		G4cout << G4BestUnit(the_cloud->GetSailVelocity(), "Velocity") << G4endl;

		G4cout << "*  Cloud polarization:    ";
		G4cout << the_cloud -> GetPolarization() << G4endl;
		G4cout << "*  Cloud alignment:       ";
		G4cout << the_cloud -> GetAlignment() << G4endl;
		G4cout << "*  OP cycle time (no MOT):                ";
		G4cout << G4BestUnit(the_cloud->GetOP_CycleTime(), "Time") << G4endl;
		
		G4cout << "*  Time before polarization is complete:  ";
		G4cout << G4BestUnit(the_cloud->GetFreeExpansionTime(), "Time") << G4endl;
		G4cout << "*  Matched Runset Letter:  ";
		G4cout << the_cloud -> GetMatchedRunsetLetter() << G4endl;
		
		G4cout << "*** *** *** *** *** *** *** *** *** *** *** *** *** *** " << G4endl;
	}
	if (command == set_cloud_matched_runset_cmd_)
	{
		the_cloud -> SetMatchedRunsetLetter(newValue);
	}
	if (command == set_pol_cmd_) 
	{
		the_cloud -> SetPolarization(set_pol_cmd_->GetNewDoubleValue(newValue));
	}
	if (command == set_ali_cmd_) 
	{
		the_cloud -> SetAlignment(set_ali_cmd_->GetNewDoubleValue(newValue));
	}
	if (command == set_cloud_temperature_v_) 
	{
		the_cloud -> SetTemperature(set_cloud_temperature_v_ -> GetNew3VectorValue(newValue));
	}
	if (command == set_cloud_temperature_d_) 
	{
		the_cloud -> SetTemperature(set_cloud_temperature_d_ -> GetNewDoubleValue(newValue));
	}
	if (command == set_initial_cloud_size_v_) 
	{
		the_cloud ->  SetInitialCloudSize(set_initial_cloud_size_v_ -> GetNew3VectorValue(newValue));
	}
	if (command == set_initial_cloud_size_d_) 
	{
		the_cloud -> SetInitialCloudSize(set_initial_cloud_size_d_ -> GetNewDoubleValue(newValue));
	}
	if (command == set_cloud_center_) 
	{
		the_cloud -> SetCloudCenter(set_cloud_center_ -> GetNew3VectorValue(newValue));
	}
	if (command == set_cloud_sail_velocity_v_) 
	{
		the_cloud -> SetSailVelocity(set_cloud_sail_velocity_v_ -> GetNew3VectorValue(newValue));
	}
	if (command == set_cloud_expansiontime_) 
	{
		the_cloud -> SetFreeExpansionTime(set_cloud_expansiontime_ -> GetNewDoubleValue(newValue));
	}
	if (command == set_cloud_opcycle_time_) 
	{
		the_cloud -> SetOP_CycleTime(set_cloud_opcycle_time_ -> GetNewDoubleValue(newValue));
	}
}

// ----------------------------------

