// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 2013
// 
// When a run is started, K37RunAction::GenerateRun() is called, 
// followed by BeginOfRunAction(...), by the master. Then the worker
// threads call all the commands in Setup_PostInit.mac, followed by 
// any commands that have been entered manually.  And *then* the 
// workers call GenerateRun() and BeginOfRunAction(...).

#include <stdlib.h>
#include <stdio.h>

#include "G4RunManager.hh"
#include "G4Version.hh"
#include "G4EmCalculator.hh"
#include "Randomize.hh"

#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

#include "K37RunManagerMT.hh"
#include "K37RunAction.hh"
#include "K37PrimaryGeneratorAction.hh"
#include "K37DetectorConstruction.hh"  // needed originally, and now for namespace patch as well.


//----------------------------------
K37RunAction::K37RunAction():
	N_generated_thread(0),
	N_accepted_thread(0),
//	naive_accepted(0),
	N_generated_total(0),
	N_accepted_total(0),
//	N_naive_total(0),
	runMessenger(0),
	EventAcceptanceTypesInt(261), 
	EventAcceptanceTypesString("CIA")
{
	G4cout << "Creating a new K37RunAction." << G4endl;
	
//	NbofEvents = 0;
//	accepted = 0;      // K37RunAction::accepted
	
	aggregator_cache.Put( new LocalAggregator() );
	
//	use G4Threading::IsWorkerThread(), or G4Threading::G4GetThreadId(), rather than G4RunAction::IsMaster().
	if( G4Threading::IsWorkerThread() )
	{
		G4cout << "Calling K37RunAction::K37RunAction()->GetAggregator-> RegisterChannels();  " << G4endl;
		
		this->GetAggregator() -> RegisterChannels();
	//	G4cout << "(Worker thread, so the local aggregator has registered channels.)" << G4endl;
	//	G4cout << "G4Threading::G4GetThreadId() = " << G4Threading::G4GetThreadId() << G4endl;
	}
	else
	{
	//	G4cout << "Master thread, so we'll create a new run messenger." << G4endl;
		runMessenger = new K37RunMessenger(this, K37RunManagerMT::GetMasterRunManager()->GetUserActionInitialization()->GetAggregator() );
	}

	/*
	the_aggregator = new LocalAggregator();
	G4cout << "LocalAggregator is created." << G4endl;
	
//	use G4Threading::IsWorkerThread(), or G4Threading::G4GetThreadId(), rather than G4RunAction::IsMaster().
	if( G4Threading::IsWorkerThread() )
	{
		the_aggregator -> RegisterChannels();  // 
		G4cout << "(Worker thread, so the local aggregator has registered channels.)" << G4endl;
		G4cout << "G4Threading::G4GetThreadId() = " << G4Threading::G4GetThreadId() << G4endl;
	}
	else // if ( IsMaster() )
	{
	//	// Don't bother registering channels if it's the master local aggregator.
		G4cout << "Master thread, so we'll create a new run messenger." << G4endl;
		runMessenger = new K37RunMessenger(this, K37RunManagerMT::GetMasterRunManager()->GetUserActionInitialization()->GetAggregator() );
	}
	*/
}

//G4ThreadLocal LocalAggregator * K37RunAction::LocalAggregator = 0;  // is this what I want?

//----------------------------------
K37RunAction::~K37RunAction() 
{
	G4cout << "Deleting the K37RunAction." << G4endl;
	delete runMessenger;
}

G4Run* K37RunAction::GenerateRun()
{
	G4cout << "Called K37RunAction::GenerateRun()." << G4endl;
	return new K37Run();
}

//----------------------------------
void K37RunAction::BeginOfRunAction(const G4Run* the_G4Run) 
{
	// Finish setup of electric field
	G4cout << "K37RunAction::BeginOfRunAction(...):  First random number:  \t" << G4UniformRand() << G4endl;
	G4cout << "K37RunAction::BeginOfRunAction(...):  Seed:  " << G4Random::getTheSeed() << G4endl;
//	G4Random::showEngineStatus();
	
	N_generated_thread = 0;
	N_accepted_thread  = 0;    // K37RunAction::accepted
	// -- -
	
//	stripDetectorMinusZHits = static_cast<K37StripDetectorHitsCollection*> (GetCollection("dsssdMinusZHC"));
//	const K37StripDetectorHitsCollection *hits_collection( static_cast<const K37StripDetectorHitsCollection*> ( digitizer_manager -> GetHitsCollection(hit_collection_id)));

	const K37Run* aRun = static_cast<const K37Run*>(the_G4Run);
	
//	if(the_aggregator) delete the_aggregator;  // don't do it.
//	the_aggregator = new LocalAggregator();  // not if it's a G4Cache, maybe?
//	G4cout << "LocalAggregator is created." << G4endl;
	
	/*
	if( G4Threading::IsWorkerThread() )
	{
		this->GetAggregator() -> RegisterChannels();
	//	G4cout << "(Worker thread, so the local aggregator has registered channels.)" << G4endl;
	//	G4cout << "G4Threading::G4GetThreadId() = " << G4Threading::G4GetThreadId() << G4endl;
	}
	else
	{
	//	G4cout << "Master thread, so we'll create a new run messenger." << G4endl;
		runMessenger = new K37RunMessenger(this, K37RunManagerMT::GetMasterRunManager()->GetUserActionInitialization()->GetAggregator() );
	}
	*/
	// -- -
	

//	// Instead of G4RunAction::IsMaster(), use G4Threading::IsWorkerThread(), 
//	// or G4Threading::G4GetThreadId().  Otherwise, bad things happen.
//	if( IsMaster() )
//		{ G4cout << "Master Thread, according to K37RunAction::IsMaster()." << G4endl; }
//	else
//		{ G4cout << "Worker Thread, according to K37RunAction::IsMaster()." << G4endl; }
//	if ( G4Threading::IsWorkerThread() )
//		{ G4cout << "Worker Thread, according to G4Threading::IsWorkerThread()." << G4endl; }
//	else 
//		{ G4cout << "Master Thread, according to G4Threading::IsWorkerThread()." << G4endl; }
//	G4cout << "Thread ID:  " << G4Threading::G4GetThreadId() << G4endl;
	
	//
	/*
	if( !IsMaster() )
	{
		this->GetAggregator() -> RegisterIOMethod();
		this->GetAggregator() -> RegisterExcessBranches();
	}
	else
	{
	//	G4cout << "Thread ID that's calling to get the master run manager and update config file:  " << G4Threading::G4GetThreadId() << G4endl;
		K37RunManagerMT::GetMasterRunManager()->GetUserActionInitialization()->GetAggregator()->SetupIO();
	}
	*/
	
	G4cout << "We're about to have the local aggregator register some things (if it's a worker)..." << G4endl;
	if( G4Threading::IsWorkerThread() )
	{
		this->GetAggregator() -> RegisterIOMethod();
		this->GetAggregator() -> RegisterExcessBranches();
	}
	else
	{
		G4cout << "Current G4 Version: " << G4Version << "\t" << G4Date << G4endl;
		G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
		K37RunManagerMT::GetMasterRunManager()->GetUserActionInitialization()->GetAggregator()->SetupIO();
	}
	
	
	G4cout << "End of K37RunAction::BeginOfRunAction(...)" << G4endl;
}


void K37RunAction::EndOfRunAction(const G4Run* the_G4Run) 
{
//	G4cout << "Called EndOfRunAction(...)" << G4endl;
	const K37Run* aRun = static_cast<const K37Run*>(the_G4Run);
	
//	NbofEvents = aRun->GetNumberOfEvent();
//	this->PrintResultsToScreen();  //
//	this->PrintResultsToScreen2();  //
	
	// copy these values into the run action so that we can output them in the metadata.
	N_generated_total = aRun->Get_Nevents();
	N_accepted_total  = aRun->Get_Naccepted();

	this->PrintResultsToScreen(/*aRun*/);
	
	/*
	if( G4Threading::IsWorkerThread() )
	{
		this->PrintResultsToScreen();
	}
	else //if( !G4Threading::IsWorkerThread() )
	{
	//	aRun->PrintResultsToScreen();
		this->PrintResultsToScreen();
	}
	*/
	
	/*
	G4cout << "------------------------------------------------------" << G4endl;
	G4cout << "  # Events ___________: " << aRun->Get_Nevents()   << G4endl;
	G4cout << "  # Accepted _________: " << aRun->Get_Naccepted() << G4endl;
	G4cout << "------------------------------------------------------" << G4endl;
	*/
	
//	In general, we have a PGA if this is a worker thread, and none if it's a master thread.
//	G4cout << "In K37RunAction::EndOfRunAction(...), do we have a pga still?" << G4endl;
//	K37PrimaryGeneratorAction * pga      = (K37PrimaryGeneratorAction*)G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction();  // why no pga??
//	if(pga) { G4cout << "pga exists!" << G4endl; }
//	else { G4cout << "no pga.  :(" << G4endl; }
	

	// Call Aggregator::SaveMetaData() here no matter what the settings are.  
	// The aggregator will check whether we want to save metadata, and also whether 
	// we're even on a master thread right now.
	G4cout << "Thread ID that's calling to get the master run manager and save metadata:  " << G4Threading::G4GetThreadId() << G4endl;
	K37RunManagerMT::GetMasterRunManager()->GetUserActionInitialization()->GetAggregator()->SaveMetaData();  // global aggregator, I think.
	this->GetAggregator() -> EndRun();
	
	
	
//	G4String rand_dir = G4RunManager::GetRunManager() -> GetRandomNumberStoreDir();  // this does nothing?
//	delete the_aggregator;  // with G4Cache, we don't delete this here I guess.
//	if( G4Threading::IsWorkerThread() )
//	{
//		
//	}
	if( !G4Threading::IsWorkerThread() )
	{
		GlobalAggregator * the_globalaggregator = ( K37RunManagerMT::GetMasterRunManager()->GetUserActionInitialization() ) -> GetAggregator();
		the_globalaggregator -> MergeRootFiles();
		
		G4cout << "This runID = " << aRun -> GetRunID() << G4endl;
		G4cout << "Last line of EORA" << G4endl;
	}
}

void K37RunAction::PrintEnergyLossTable(G4String materialChoice) 
{
	G4cout << "Called K37RunAction::PrintEnergyLossTable.  This probably only happens when K37RunMessenger calls it?" << G4endl;
	// get the pointer to the material table
	const G4MaterialTable* theMaterialTable = G4Material::GetMaterialTable();
	G4bool materialIsOnTheList = false;
	// search the material by its name
	G4Material*materialSelectedFromTable;
	for (size_t J = 0 ; J < theMaterialTable -> size(); J++) 
	{
		materialSelectedFromTable = (*theMaterialTable)[J];
		if (materialSelectedFromTable ->GetName() == materialChoice) 
		{
			G4cout << materialSelectedFromTable -> GetName()
			       << " is on the list of availble materials" << G4endl;
			G4cout << "The energy loss table of e+ in "
			       << materialSelectedFromTable -> GetName()
			       << " will be calculated" << G4endl;
			materialIsOnTheList = true;
			break;
		}
	}
	
	if (materialIsOnTheList == false) 
	{
		G4cerr << "Material was not on list" << G4endl;
		return;
	}
	G4EmCalculator emCalculator;
	G4double test_energies[800];
	for (G4int i = 0; i < 800; i++) 
	{
		test_energies[i]=G4double(i)*0.005;
	}
	std::ofstream energyLossTableFile;
	energyLossTableFile.open("energyLossTableFile.txt",
	                         std::ofstream::out | std::ofstream::trunc);
	
	energyLossTableFile << "Energy(MeV)" << "    " << "DEDX(MeV/cm)" << G4endl;
	for (G4int i = 0; i < 800; i++) 
	{
		energyLossTableFile << std::setw(15) << std::left << (test_energies[i])/MeV
		  << std::setw(15)
		  << (emCalculator.GetDEDX((test_energies[i])*MeV,"e+",
		      materialSelectedFromTable -> GetName(),
		      "DefaultRegionForTheWorld"))/ (MeV/cm) << G4endl;
	}
	energyLossTableFile.close();
	G4cout<< "energyLossTableFile.txt written." << G4endl;
}

/*
void K37RunAction::PrintResultsToScreen() 
{
	G4cout << "------------------------------------------------------" << G4endl;
	G4cout << "  K37RunAction::PrintResultsToScreen() says:  " << G4endl;
	G4cout << "  # Events ___________: " << N_generated << G4endl;
	G4cout << "  # Accepted _________: " << N_accepted << G4endl;
	G4cout << "------------------------------------------------------" << G4endl;
}
*/
void K37RunAction::PrintResultsToScreen(/*const K37Run* aRun*/) // keep this function in K37RunAction.  Don't move to K37Run. ....no; move it!!  .... no really, keep it.
{
	int the_threadid = G4Threading::G4GetThreadId();
	if(the_threadid >= 0)
	{
		G4cout << "------------------------------------------------------" << G4endl;
		G4cout << "    Worker Thread # " << the_threadid << G4endl;
		G4cout << "  # Events ___________: " << N_generated_thread << G4endl;
		G4cout << "  # Accepted _________: " << N_accepted_thread << G4endl;
		G4cout << "------------------------------------------------------" << G4endl;
	}
	else
	{
		G4cout << "------------------------------------------------------" << G4endl;
		G4cout << "    All Threads" << G4endl;
		G4cout << "  # Events ___________: " << N_generated_total << G4endl;
		G4cout << "  # Accepted _________: " << N_accepted_total  << G4endl;
		G4cout << "------------------------------------------------------" << G4endl;
	}
};


//
//void K37PrimaryGeneratorAction::SetEventTypes(G4String EventTypesString) 
void K37RunAction::SetEventTypes(G4String EventTypesString) 
// later, generalize this function so that it accepts an int as input too.
{
	EventAcceptanceTypesString  = "";  // only reset this if a string is actually entered!!  Fix this later.
	EventAcceptanceTypesInt     = 0;

	if( EventTypesString.contains("A") || EventTypesString.contains("a") ) 
	{
		EventAcceptanceTypesString.append("A");
		EventAcceptanceTypesInt += 1;
	}
	if( EventTypesString.contains("B") || EventTypesString.contains("b") ) 
	{
		EventAcceptanceTypesString.append("B");
		EventAcceptanceTypesInt += 2;
	}
	if( EventTypesString.contains("C") || EventTypesString.contains("c") ) 
	{
		EventAcceptanceTypesString.append("C");
		EventAcceptanceTypesInt += 4;
	}
	if( EventTypesString.contains("D") || EventTypesString.contains("d") ) 
	{
		EventAcceptanceTypesString.append("D");
		EventAcceptanceTypesInt += 8;
	}
	if( EventTypesString.contains("E") || EventTypesString.contains("e") ) 
	{
		EventAcceptanceTypesString.append("E");
		EventAcceptanceTypesInt += 16;
	}
	if( EventTypesString.contains("F") || EventTypesString.contains("f") ) 
	{
		EventAcceptanceTypesString.append("F");
		EventAcceptanceTypesInt += 32;
	}
	if( EventTypesString.contains("G") || EventTypesString.contains("g") ) 
	{
		EventAcceptanceTypesString.append("G");
		EventAcceptanceTypesInt += 64;
	}
	if( EventTypesString.contains("H") || EventTypesString.contains("h") ) 
	{
		EventAcceptanceTypesString.append("H");
		EventAcceptanceTypesInt += 128;
	}
	if( EventTypesString.contains("I") || EventTypesString.contains("i") ) 
	{
		EventAcceptanceTypesString.append("I");
		EventAcceptanceTypesInt += 256;
	}
	if( EventTypesString.contains("J") || EventTypesString.contains("j") ) 
	{
		EventAcceptanceTypesString.append("J");
		EventAcceptanceTypesInt += 512;
	}
	if( EventTypesString.contains("K") || EventTypesString.contains("k") ) 
	{
		EventAcceptanceTypesString.append("K");
		EventAcceptanceTypesInt += 1024;
	}

	G4cout << "Now using event types " << EventAcceptanceTypesString << G4endl;
	G4cout << "Event types code:     " << EventAcceptanceTypesInt << G4endl;
}

//void K37PrimaryGeneratorAction::GetEventTypes()
void K37RunAction::GetEventTypes()
{
	G4cout << "Accepting event types:  " << EventAcceptanceTypesString << G4endl;
	G4cout << "Numerical event code:   " << EventAcceptanceTypesInt << G4endl;
	return;
}

//bool K37PrimaryGeneratorAction::EventAcceptanceChecker(G4int this_event) 
bool K37RunAction::EventAcceptanceChecker(G4int this_event) 
// This function should only ever be called in EndOfEventAction.  ...right?
// Surely there must be a more computationally efficient way to do this.
{
	int tmp_event_type = this_event;
	int tmp_allowed_events = EventAcceptanceTypesInt;
	bool accept_event = false;
	
	if(tmp_event_type==0)  // toss out all gratuitously useless events first.
	{
		return accept_event;
	}
	//
	
	// .......what.
	
	if (tmp_allowed_events >= 1024)  // 
	{
		if(tmp_event_type >= 1024)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 1024;
	}
	if (tmp_allowed_events >= 512)  //  J = 512:  (rMCP && (scint_t || scint_b) && eMCP)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 512;
	}
	//
	if (tmp_allowed_events >= 256)  //  I = 256:  (rMCP && (scint_t || scint_b))
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 256;
	}
	//
	if (tmp_allowed_events >= 128)  //  H = 128:  (rMCP)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		
		if(tmp_event_type >= 128)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 128;
	}
	//
	if (tmp_allowed_events >= 64)  //  G = 64:   (eMCP)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		
		if(tmp_event_type >= 64)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 64;
	}
	//
	if (tmp_allowed_events >= 32)  //  F = 32:   (eMCP && rMCP)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		
		if(tmp_event_type >= 32)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 32;
	}
	//
	if (tmp_allowed_events >= 16)  //  E = 16:    [not implemented] -- (rMCP && UV)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }

		if(tmp_event_type >= 16)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 16;
	}
	//
	if (tmp_allowed_events >= 8)   //  D = 8:     [not implemented] -- (eMCP && UV)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }
		if(tmp_event_type >= 16)  { tmp_event_type -= 16; }
		if(tmp_event_type >= 8)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 8;
	}
	//
	if (tmp_allowed_events >= 4)  //  C = 4:    (eMCP && (scint_t || scint_b))
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }
		if(tmp_event_type >= 16)  { tmp_event_type -= 16; }
		if(tmp_event_type >= 8)   { tmp_event_type -= 8; }
		if(tmp_event_type >= 4)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 4;
	}
	/* 
	// Event type B is not implemented.  
	if (tmp_allowed_events >= 2)  //  B = 2:     [not implemented] -- (scint && LED)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }
		if(tmp_event_type >= 16)  { tmp_event_type -= 16; }
		if(tmp_event_type >= 8)   { tmp_event_type -= 8; }
		if(tmp_event_type >= 4)   { tmp_event_type -= 4; }
		if(tmp_event_type >= 2)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 2;
	}
	*/
	if (tmp_allowed_events >= 1)  // A = 1:    (scint_t || scint_b)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }
		if(tmp_event_type >= 16)  { tmp_event_type -= 16; }
		if(tmp_event_type >= 8)   { tmp_event_type -= 8; }
		if(tmp_event_type >= 4)   { tmp_event_type -= 4; }
		if(tmp_event_type >= 2)   { tmp_event_type -= 2; }
		if(tmp_event_type >= 1)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 1;
	}
	
	return accept_event;
}
