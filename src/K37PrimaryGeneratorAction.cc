// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm 2013

#include <iomanip>
#include <cmath>
#include <map>
#include <string>


#include "G4IonTable.hh"
#include "G4SingleParticleSource.hh"
#include "G4GeneralParticleSource.hh"
#include "G4RandomDirection.hh"  //

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "Randomize.hh"

#include "K37PrimaryGeneratorAction.hh"
#include "K37Config.hh"
#include "K37RunManagerMT.hh"
#include "K37RunAction.hh"
#include "K37_Data.hh"

using std::string;
using std::ifstream;
using std::map;
using K37_ABC::K37_Data;

//
K37PrimaryGeneratorAction::K37PrimaryGeneratorAction(
      K37EventGenerator* evGen,
      K37EventGeneratorNoRecoilOrderEffects* twoP)
   : //Initalizer List
      use_gps(false),
//      polarization_(1.0),
//      alignment_(1.0),
      charge_state_ratio_(8, 1.0/8.0),
      branching_ratio(2, 1.0/2.0),
      v(),
      vertex(NULL),
      EventVertex(),
      K37Neutral(NULL),
      K37Minus(NULL),
      decayTableAr37Minus(NULL),
      K37MinusDecayMode(NULL),
      evGenerator(evGen),
      twoPercent(twoP),
      make_beta_(true),
      make_recoil_(true),
      make_shakeoff_electrons_(true),
      make_uniform_E_(false),
      makeTwoPercent(true),
      thisEventIsATwoPercent(false),
      recoil_charge_file_("charge_state_dist.res")//, 
//      EventAcceptanceTypesInt(261), 
//      EventAcceptanceTypesString("CIA")
{
	G4cout << "Created a new PGA." << G4endl;
	
//	new  G4UnitDefinition("eV/c",   "eV/c_light",   "Momentum", eV/c_light);
	new  G4UnitDefinition("kg*m/s", "kg*m/s",       "Momentum", kg*m/s);
	//

	gunMessenger = new K37PrimaryGeneratorMessenger(this);

	branching_ratio[0] = 0.9789; //Main branch we care about
	branching_ratio[1] = 0.0207; //Two percent branch.
	NormalizeBranchingRatio();
	
	
	if (use_gps) { particleGun = new G4GeneralParticleSource(); } 
	else         { particleGun = new G4SingleParticleSource();  }
	
	//
	G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
	G4String particleName;
	// Ar37_ion = particleTable->GetIon(18, 37, 0);
	positron = particleTable->FindParticle(particleName="e+");
	electron = particleTable->FindParticle(particleName="e-");
	gamma = particleTable->FindParticle(particleName="gamma");
//	Ar37MinusParticle = particleTable->FindParticle(particleName="Ar37Minus");
	
	
	/*
//	charge_state_ratio_[0] = 1.0;
//	charge_state_ratio_[1] = 0.69699;
//	charge_state_ratio_[2] = 0.19483;
//	charge_state_ratio_[3] = 0.08173;
//	charge_state_ratio_[4] = 0.01953;
//	charge_state_ratio_[5] = 0.00581;
//	charge_state_ratio_[6] = 0.00110;
//	charge_state_ratio_[7] = 0.0;
	*/
	recoil_charge_ =  1.0;  // +1 state by default. 
}

K37PrimaryGeneratorAction::~K37PrimaryGeneratorAction() 
{
	delete particleGun;
	delete gunMessenger;
	delete K37Neutral;
	delete K37Minus;
	delete decayTableAr37Minus;
	delete K37MinusDecayMode;
	delete evGenerator;
	delete twoPercent; 
}


G4ThreeVector K37PrimaryGeneratorAction::GetEventPositionFromCloud(K37CloudSetup * the_cloud) // replaces void K37CloudSetup::makeEvent() 
{
	G4ThreeVector initial_position =
	    G4ThreeVector(G4RandGauss::shoot(the_cloud->GetCloudCenter().x(), the_cloud->GetInitialCloudSize().x()),
	                  G4RandGauss::shoot(the_cloud->GetCloudCenter().y(), the_cloud->GetInitialCloudSize().y()),
	                  G4RandGauss::shoot(the_cloud->GetCloudCenter().z(), the_cloud->GetInitialCloudSize().z()));

	// The trapping laser is off 200 us before the OP laser comes on and it takes
	// ~ 100 us for the atoms to polarize.  Therefore the atoms will have been
	// expanidng for at least 300 us (expansion_before_polarized_) before they
	// will be classified as "polarized" and counted.
	G4double decay_time_ = -10;
	while (decay_time_ < the_cloud->GetFreeExpansionTime() ) 
	{ // pick a new thing for "decay_time_" until you get something longer than "expansion_before_polarized_".
		decay_time_ = (the_cloud->GetOP_CycleTime())*G4UniformRand();  // G4UniformRand() ranges from 0 to 1.
	}

	// Thermal velocity of individual atom (random)
	G4ThreeVector final_velocity_ =
	    G4ThreeVector(G4RandGauss::shoot(0, the_cloud->GetVelocitySigma().x()),
	                  G4RandGauss::shoot(0, the_cloud->GetVelocitySigma().y()),
	                  G4RandGauss::shoot(0, the_cloud->GetVelocitySigma().z()));
	// Overall (bulk) movement of the cloud

	final_velocity_ += the_cloud->GetSailVelocity();
	G4ThreeVector final_position = initial_position + final_velocity_*decay_time_;  
//	}
	return final_position;
}


void K37PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) 
{
//	G4cout << "Called K37PrimaryGeneratorAction::GeneratePrimaries(...)" << G4endl;
	
	if (use_gps) 
	{
		particleGun -> GeneratePrimaryVertex(anEvent);
		return; 
	}
	if(makeTwoPercent && recoil_charge_ != -3) 
	{ // 2% branch is allowed && not photoions.
		thisEventIsATwoPercent = TwoPercentEvent();
	}
	recoil_charge_this_event = GetChargeStateThisEvent();
	is_2p                    = GetIs2p();
	
	K37CloudSetup * cloud = K37RunManagerMT::GetMasterRunManager()->GetUserActionInitialization()->GetCloud();

//	G4cout << "K37PrimaryGeneratorAction::GeneratePrimaries(...) is NOT inserting data into the two percent branch right now." << G4endl;
//	// Below:  These fuck shit up.  Moved to K37EventAction, which may or may not also break it.  ...Subsequently moved to K37Run, where it seems fine?
//	(*active_channels_)["TWO_PERCENT_BRANCH"] -> InsertData(is_2p);
//	(*active_channels_)["ION_CHARGE"]         -> InsertData(recoil_charge_this_event);
	
	
//	// Get cloud position
//	cloud -> makeEvent();
	EventVertex = GetEventPositionFromCloud(cloud);
	
//	G4cout << "K37PrimaryGeneratorAction:  cloud->GetPolarization() = " << cloud->GetPolarization() << G4endl;
//	G4cout << "K37PrimaryGeneratorAction:  cloud->GetAlignment()    = " << cloud->GetAlignment() << G4endl;
	
	// Set initial position of all particles where the cloud tells you
	particleGun->SetParticlePosition(EventVertex);
	
	// Setup initial momenta of B-decay particles
	if(thisEventIsATwoPercent)
	{
		twoPercent -> MakeEvent(cloud -> GetPolarization(), cloud -> GetAlignment() );
	}
	else
	{
		evGenerator -> MakeEvent(cloud -> GetPolarization(), cloud -> GetAlignment() );
	}
	if (make_beta_) 
	{
		this->setBetaVertex();  //
		anEvent->AddPrimaryVertex(vertex);
	}
	if (make_recoil_) 
	{
		this->setDaughterVertex(recoil_charge_this_event);
		anEvent->AddPrimaryVertex(vertex);
	}
	if (make_shakeoff_electrons_) 
	{
		SetSOelectronVertices(anEvent, recoil_charge_this_event + 1);
	}
	if (thisEventIsATwoPercent) 
	{
		SetGammaVertex(anEvent);
	}
//	G4cout << "Finished K37PrimaryGeneratorAction::GeneratePrimaries(...)" << G4endl;
}

G4double K37PrimaryGeneratorAction::getVelocity(G4double kineticEnergy, G4double massOfParticle) 
{
	G4double velocityOverC = sqrt(1.0 - (1.0/((kineticEnergy/massOfParticle + 1.0)*
	                                    (kineticEnergy/massOfParticle + 1.0))));
	return velocityOverC;
}

void K37PrimaryGeneratorAction::setBetaVertex() 
{
	vertex = new G4PrimaryVertex(EventVertex, 0);
	G4PrimaryParticle* particle = 0;
	if(thisEventIsATwoPercent)
	{
		particle = new G4PrimaryParticle(positron, twoPercent->eMomentumX(),
			twoPercent->eMomentumY(), twoPercent->eMomentumZ());
	}
	else
	{
		particle = new G4PrimaryParticle(positron, evGenerator->eMomentumX(),
			evGenerator->eMomentumY(), evGenerator->eMomentumZ());
	}
	vertex->SetPrimary(particle);
}

void K37PrimaryGeneratorAction::SetGammaVertex(G4Event *ev)
{
	vertex = new G4PrimaryVertex(EventVertex, 0);
	G4double gamma_energy = 2.796 * MeV;
	G4ThreeVector gamma_direction = G4RandomDirection(); //isotropic
	G4PrimaryParticle *twoPercentGamma = new G4PrimaryParticle(gamma);
	twoPercentGamma -> SetMomentumDirection(gamma_direction);
	twoPercentGamma -> SetKineticEnergy(gamma_energy);

	vertex -> SetPrimary(twoPercentGamma);
	ev -> AddPrimaryVertex(vertex);
}

void K37PrimaryGeneratorAction::setDaughterVertex(G4double recoil_charge) 
{
	G4bool debug = false;
	G4ThreeVector momentum(0, 0, 0);
	vertex = new G4PrimaryVertex(EventVertex, 0);
	
	G4IonTable * ionTable = G4IonTable::GetIonTable();
	G4ParticleDefinition *Ar37_ion = ionTable -> GetIon(18, 37, 0);  // 37Ar	
	G4PrimaryParticle* particle = 0;
	
	if(thisEventIsATwoPercent)
	{
		particle = new G4PrimaryParticle(Ar37_ion, twoPercent->dMomentumX(),
			twoPercent->dMomentumY(), twoPercent->dMomentumZ());
	}
	else
	{
		particle = new G4PrimaryParticle(Ar37_ion, evGenerator->dMomentumX(),
			evGenerator->dMomentumY(), evGenerator->dMomentumZ());
	}
	// Simulate any charge state >= +1
	particle -> SetCharge(recoil_charge * eplus);
	if (debug) 
	{
		G4cout << "Ion mass: " << Ar37_ion -> GetPDGMass()/c_squared/kg << " kg"
		       << G4endl;
		G4cout << "Ion velocity: " << G4endl << "v0_x = "
		       << (particle -> GetPx()/(particle -> GetMass())*c_light)/(mm/ns)
		       << " mm/ns" << G4endl << "v0_y = "
		       << (particle -> GetPy()/(particle -> GetMass())*c_light)/(mm/ns)
		       << " mm/ns" << G4endl << "v0_z = "
		       << (particle -> GetPz()/(particle -> GetMass())*c_light)/(mm/ns)
		       << " mm/ns" << G4endl;
		G4cout << "Ion momentum: "
		       << particle -> GetTotalMomentum()/(c_light*kg*m/s) << " kg*m/s"
		       << G4endl;
		G4cout << "Ion kinetic energy: " << particle -> GetKineticEnergy()/joule
		       << " J " << G4endl;
	}
	vertex->SetPrimary(particle);
}

G4ThreeVector K37PrimaryGeneratorAction::GetMomentumIsotropic( G4double kinetic_energy, G4double mass) 
{
	G4bool debug = false;
	G4double total_energy = kinetic_energy + mass;
	G4double momentum = sqrt((total_energy*total_energy) - (mass * mass));
	G4double mu = 1.0 - 2.0*G4UniformRand();
	G4double theta = acos(mu);
	G4double phi = 2.0*M_PI*G4UniformRand();
	G4double px, py, pz;
	px = momentum * sin(theta) * cos(phi);
	py = momentum * sin(theta) * sin(phi);
	pz = momentum * cos(theta);
	if (debug) 
	{
		G4cout << "\tT = " << G4BestUnit(kinetic_energy, "Energy") << "\tE = "
		       << G4BestUnit(total_energy, "Energy") << "\tP = "
		       << momentum << G4endl;
		G4cout << "\ttheta = " << theta << "\tphi = " << phi << G4endl;
		G4cout << "\tpx = " << px << "\tpy = " << py << "\tpz = " << pz << G4endl;
	}
	return G4ThreeVector(px, py, pz);
}

void K37PrimaryGeneratorAction::SetSOelectronVertices(G4Event *ev, G4int num_so_electrons) 
{
	for (G4int i = 0; i < num_so_electrons; i++) 
	{
		G4double kinetic_energy = -1.0;
		// Ar binding energy is 15.7 eV and SOE have around this energy
		// The 5.0 eV width is a total guess
		while (kinetic_energy < 0.0) 
		{
			kinetic_energy = CLHEP::RandGauss::shoot(100.0*eV, 5.0*eV);
		}
		G4ThreeVector momentum = 
		GetMomentumIsotropic(kinetic_energy, electron_mass_c2);
		
		G4PrimaryParticle *particle = new G4PrimaryParticle(electron,
		                                               momentum.getX(),
		                                               momentum.getY(),
		                                               momentum.getZ());
		vertex = new G4PrimaryVertex(EventVertex, 0);  // 0 means occurs at t = 0
		vertex -> SetPrimary(particle);
		ev -> AddPrimaryVertex(vertex);
	}
}

/*
void K37PrimaryGeneratorAction::SetPolarization(G4double pol) 
{
	if (fabs(pol) <= 1.0) 
	{
		polarization_ = pol;
		G4cout << "Polarization set to " << pol << G4endl;
	} 
	else 
	{
		G4cout << "WARNING: Polarization " << pol
			   << " not in allowed range.  No changes made." << G4endl;
	}
}
void K37PrimaryGeneratorAction::SetAlignment(G4double ali) 
{
	if (ali <= 1.0 && ali >= 0.0) 
	{
		alignment_ = ali;
		G4cout << "Alignment set to " << ali << G4endl;
	} 
	else if (ali < 0.0 && ali >= -1.0) 
	{
		ali = fabs(ali);
		G4cout << "WARNING: Alignment must be positive (for both polarization states)";
		G4cout << G4endl;
		G4cout << "Note that this sign convention is opposite that in JTW 1957 and that";
		G4cout << G4endl;
		G4cout << "this change is fully documented in the \"JTWEvent.cc\" source file";
		G4cout << G4endl;
		G4cout << "*********************************************************" << G4endl;
		G4cout << "   Setting alignment to T = +" << ali << G4endl;
		G4cout << "*********************************************************" << G4endl;
		alignment_ = ali;
	} 
	else 
	{
		G4cout << "WARNING: Alignment " << ali
			   << " not in allowed range.  No changes made." << G4endl;
	}
}
*/

void K37PrimaryGeneratorAction::SetRecoilCharge(G4int charge) 
{
	if (charge < 0 && charge != -2 && charge != -3) 
	{
		G4cout << "Negative ions not supported as primary particles.  ";
		G4cout << "No change.  Recoil charge = " << recoil_charge_ << G4endl;
	} 
	else if (charge == -2) 
	{
		recoil_charge_ = charge;
		G4cout << "Set charge state = -2"  << G4endl;
		G4cout << "Charge state will be randomized based on file:  " << recoil_charge_file_ << G4endl;
		LoadChargeStates();         
		NormalizeChargeStateRatio();
	} 
	else if (charge == -3) 
	{
		G4cout << "Set charge state = -3:  ";
		G4cout << "Photoions."  << G4endl;  // How implemented is this??
		recoil_charge_ = charge;
	} 
	else 
	{ // Positive ions action
		recoil_charge_ = charge;
		G4cout << "Set recoil charge = " << recoil_charge_ << G4endl;
	}
}

void K37PrimaryGeneratorAction::NormalizeChargeStateRatio() 
{
	G4double sum = 0.0;
	for (unsigned int i = 0; i < charge_state_ratio_.size(); i++) 
	{
		sum += charge_state_ratio_[i];
	}
	for (unsigned int i = 0; i < charge_state_ratio_.size(); i++) 
	{
		charge_state_ratio_[i] = charge_state_ratio_[i] / sum;
	}
}

void K37PrimaryGeneratorAction::NormalizeBranchingRatio()
{
	G4double sum = 0.0;
	for (unsigned int i = 0; i < branching_ratio.size(); i++)
	{
		sum += branching_ratio[i];
	}
	for (unsigned int i = 0; i < branching_ratio.size(); i++)
	{
		branching_ratio[i] = branching_ratio[i] / sum;
	}
}

G4double K37PrimaryGeneratorAction::GetChargeStateThisEvent() // does this still work?
{
	G4double this_charge_state = -5.0;
	if (recoil_charge_ == -2) // randomized charge state based on distribution.
	{
		G4double sum = 0.0;
		unsigned an_index = 0;
		G4double guess = CLHEP::RandFlat::shoot(0.0, 1.0);
		while (this_charge_state < 0 && an_index < charge_state_ratio_.size()) 
		{
			sum += charge_state_ratio_[an_index];
			if (guess < sum) 
			{
				this_charge_state = an_index * eplus;
			}
			an_index++;
		}
		G4cout << "this_charge_state = " << this_charge_state << G4endl;
	}
	else if(recoil_charge_ == -3) // photoions.
	{
		this_charge_state = 1.0 * eplus;
	}
	else // recoil_charge_ != -2, so the charge state is given and not randomized.
	{
		this_charge_state = recoil_charge_ * eplus;
	}
	return this_charge_state;
}
G4double K37PrimaryGeneratorAction::GetIs2p()
{
	is_2p = -1;
	if(thisEventIsATwoPercent) { is_2p = 1.0; }
	else                       { is_2p = 0.0; }
	return is_2p;
} 

G4bool K37PrimaryGeneratorAction::TwoPercentEvent()  
{ // Decide whether or not this event is a 2% branch event.
	G4double guess = CLHEP::RandFlat::shoot(0.0, 1.0);
	if (guess < branching_ratio[1])
	{
		return true;
	}
	else
	{
		return false;
	}
}

void K37PrimaryGeneratorAction::ToggleUseGPS() 
{
	use_gps = !(use_gps);
	delete particleGun;
	if (use_gps) 
	{
		particleGun = new G4GeneralParticleSource();
	} 
	else 
	{
		particleGun = new G4SingleParticleSource();
	}
}

void K37PrimaryGeneratorAction::SetUseGPS(bool g) 
{
	if (g == use_gps) { } 
	else 
	{
		ToggleUseGPS();
	}
	return;  
}

/*
void K37PrimaryGeneratorAction::SetEventTypes(G4String EventTypesString) 
// later, generalize this function so that it accepts an int as input too.
{
	EventAcceptanceTypesString  = "";  // only reset this if a string is actually entered!!  Fix this later.
	EventAcceptanceTypesInt     = 0;

	if( EventTypesString.contains("A") || EventTypesString.contains("a") ) 
	{
		EventAcceptanceTypesString.append("A");
		EventAcceptanceTypesInt += 1;
	}
	if( EventTypesString.contains("B") || EventTypesString.contains("b") ) 
	{
		EventAcceptanceTypesString.append("B");
		EventAcceptanceTypesInt += 2;
	}
	if( EventTypesString.contains("C") || EventTypesString.contains("c") ) 
	{
		EventAcceptanceTypesString.append("C");
		EventAcceptanceTypesInt += 4;
	}
	if( EventTypesString.contains("D") || EventTypesString.contains("d") ) 
	{
		EventAcceptanceTypesString.append("D");
		EventAcceptanceTypesInt += 8;
	}
	if( EventTypesString.contains("E") || EventTypesString.contains("e") ) 
	{
		EventAcceptanceTypesString.append("E");
		EventAcceptanceTypesInt += 16;
	}
	if( EventTypesString.contains("F") || EventTypesString.contains("f") ) 
	{
		EventAcceptanceTypesString.append("F");
		EventAcceptanceTypesInt += 32;
	}
	if( EventTypesString.contains("G") || EventTypesString.contains("g") ) 
	{
		EventAcceptanceTypesString.append("G");
		EventAcceptanceTypesInt += 64;
	}
	if( EventTypesString.contains("H") || EventTypesString.contains("h") ) 
	{
		EventAcceptanceTypesString.append("H");
		EventAcceptanceTypesInt += 128;
	}
	if( EventTypesString.contains("I") || EventTypesString.contains("i") ) 
	{
		EventAcceptanceTypesString.append("I");
		EventAcceptanceTypesInt += 256;
	}
	if( EventTypesString.contains("J") || EventTypesString.contains("j") ) 
	{
		EventAcceptanceTypesString.append("J");
		EventAcceptanceTypesInt += 512;
	}
	if( EventTypesString.contains("K") || EventTypesString.contains("k") ) 
	{
		EventAcceptanceTypesString.append("K");
		EventAcceptanceTypesInt += 1024;
	}

	G4cout << "Now using event types " << EventAcceptanceTypesString << G4endl;
	G4cout << "Event types code:     " << EventAcceptanceTypesInt << G4endl;
}

void K37PrimaryGeneratorAction::GetEventTypes()
{
	G4cout << "Accepting event types:  " << EventAcceptanceTypesString << G4endl;
	G4cout << "Numerical event code:   " << EventAcceptanceTypesInt << G4endl;
	return;
}
*/
/*
bool K37PrimaryGeneratorAction::EventAcceptanceChecker(G4int this_event) 
// This function should only ever be called in EndOfEventAction.  ...right?
// Surely there must be a more computationally efficient way to do this.
{
	int tmp_event_type = this_event;
	int tmp_allowed_events = EventAcceptanceTypesInt;
	bool accept_event = false;
	
	if(tmp_event_type==0)  // toss out all gratuitously useless events first.
	{
		return accept_event;
	}
	//
	
	// .......what.
	
	if (tmp_allowed_events >= 1024)  // 
	{
		if(tmp_event_type >= 1024)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 1024;
	}
	if (tmp_allowed_events >= 512)  //  J = 512:  (rMCP && (scint_t || scint_b) && eMCP)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 512;
	}
	//
	if (tmp_allowed_events >= 256)  //  I = 256:  (rMCP && (scint_t || scint_b))
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 256;
	}
	//
	if (tmp_allowed_events >= 128)  //  H = 128:  (rMCP)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		
		if(tmp_event_type >= 128)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 128;
	}
	//
	if (tmp_allowed_events >= 64)  //  G = 64:   (eMCP)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		
		if(tmp_event_type >= 64)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 64;
	}
	//
	if (tmp_allowed_events >= 32)  //  F = 32:   (eMCP && rMCP)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		
		if(tmp_event_type >= 32)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 32;
	}
	//
	if (tmp_allowed_events >= 16)  //  E = 16:    [not implemented] -- (rMCP && UV)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }

		if(tmp_event_type >= 16)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 16;
	}
	//
	if (tmp_allowed_events >= 8)   //  D = 8:     [not implemented] -- (eMCP && UV)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }
		if(tmp_event_type >= 16)  { tmp_event_type -= 16; }
		if(tmp_event_type >= 8)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 8;
	}
	//
	if (tmp_allowed_events >= 4)  //  C = 4:    (eMCP && (scint_t || scint_b))
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }
		if(tmp_event_type >= 16)  { tmp_event_type -= 16; }
		if(tmp_event_type >= 8)   { tmp_event_type -= 8; }
		if(tmp_event_type >= 4)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 4;
	}
//	
//	// Event type B is not implemented.  
//	if (tmp_allowed_events >= 2)  //  B = 2:     [not implemented] -- (scint && LED)
//	{
//		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
//		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
//		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
//		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
//		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
//		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }
//		if(tmp_event_type >= 16)  { tmp_event_type -= 16; }
//		if(tmp_event_type >= 8)   { tmp_event_type -= 8; }
//		if(tmp_event_type >= 4)   { tmp_event_type -= 4; }
//		if(tmp_event_type >= 2)
//		{
//			accept_event=true;
//			return accept_event;
//		}
//		tmp_allowed_events -= 2;
//	}
//	
	if (tmp_allowed_events >= 1)  // A = 1:    (scint_t || scint_b)
	{
		if(tmp_event_type >= 1024){ tmp_event_type -= 1024; }
		if(tmp_event_type >= 512) { tmp_event_type -= 512; }
		if(tmp_event_type >= 256) { tmp_event_type -= 256; }
		if(tmp_event_type >= 128) { tmp_event_type -= 128; }
		if(tmp_event_type >= 64)  { tmp_event_type -= 64; }
		if(tmp_event_type >= 32)  { tmp_event_type -= 32; }
		if(tmp_event_type >= 16)  { tmp_event_type -= 16; }
		if(tmp_event_type >= 8)   { tmp_event_type -= 8; }
		if(tmp_event_type >= 4)   { tmp_event_type -= 4; }
		if(tmp_event_type >= 2)   { tmp_event_type -= 2; }
		if(tmp_event_type >= 1)
		{
			accept_event=true;
			return accept_event;
		}
		tmp_allowed_events -= 1;
	}
	
	return accept_event;
}
*/


void K37PrimaryGeneratorAction::SetChargeStatesFile(G4String filename) 
{
	// Set the parameter...
	recoil_charge_file_ = filename;
	G4cout << "Set the recoil charge states file to " << filename << G4endl;
	// Load up the charge states from the file...
	LoadChargeStates();
	NormalizeChargeStateRatio();
	// Automatically set the recoil charge to -2 so the file distribution is used.
	recoil_charge_ = -2.0; 
	return;  
}

void K37PrimaryGeneratorAction::LoadChargeStates()
{
	G4String fname_string = G4String(CONFIGURATION_DIRECTORY)+recoil_charge_file_;
	
	G4cout << "* Loading charge states from file:  " << recoil_charge_file_ << G4endl;
	int nstates = 8;
	// Make sure charge_state_ratio_ is the right size...?

	int dummyline = 1;

	G4int cs;
	G4double value;
	G4String nl;

	ifstream file;
	file.open(fname_string.c_str(), std::ifstream::in);
	if (file.is_open()) 
	{
		for(int i=0; i<dummyline; i++)
		{
			getline(file, nl);
		}
		for(int i=0; i<nstates; i++)
		{
			// check if it's not EOF.
			file >> cs >> value;// >> nl;
			getline(file, nl);
			charge_state_ratio_[i] = value;
		}
	}
	else
	{
		G4cout << "* Could not open file " << fname_string << G4endl;
		G4cout << "* Charge state distribution was NOT loaded. " << G4endl;
		G4cout << "* Setting recoil_charge_ to +1." << G4endl;
		recoil_charge_ = 1.0;
	}
	return;
}

G4String K37PrimaryGeneratorAction::GetChargeStatesFile()
{
	return recoil_charge_file_;
} 


void K37PrimaryGeneratorAction::GetChargeStates()
{
	int nstates = 8;
	G4cout << "Charge state distribution is:  " << G4endl;
	for(int i=0; i<nstates; i++)
	{
		G4cout << "Charge +" << i << ":   " << charge_state_ratio_[i] << G4endl;
	}
	return;
} 

