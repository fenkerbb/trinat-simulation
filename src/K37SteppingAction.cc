// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013

#include <iomanip>
#include <string>

#include "G4SteppingManager.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4TrackStatus.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"

//#include "K37EventAction.hh"
#include "K37SteppingAction.hh"
#include "K37SteppingActionMessenger.hh"
#include "K37RunManagerMT.hh"
#include "K37_Data.hh"

#include "K37RunAction.hh"

using K37_ABC::K37_Data;


K37SteppingAction::K37SteppingAction()
  :stepping_messenger(new K37SteppingActionMessenger(this)),
   ToTrack(false)
{;}

K37SteppingAction::~K37SteppingAction()
{
	delete stepping_messenger;
}

void K37SteppingAction::UserSteppingAction(const G4Step * theStep)
{
	if (ToTrack)
	{
		G4Track * theTrack = theStep->GetTrack();
		const G4ParticleDefinition* theDefinition = theTrack->GetParticleDefinition();
		
		G4String theParticleName = theDefinition->GetParticleName();
		if ((theParticleName == "e+")  && (theTrack -> GetTrackStatus() != fStopAndKill))
		{
			G4StepPoint * thePostPoint = theStep->GetPostStepPoint();
			G4String thePostPVname = thePostPoint->GetPhysicalVolume()->GetName();
			
		//	if ((thePostPoint -> GetStepStatus() == fGeomBoundary))
		//	{
		//		// This breaks it now.  
		//		// I don't know what this output channel really even means, so for the time being I'll just disable it.
		//		map<string, K37_Data*> * active_channels = 
		//			((K37RunAction*)(K37RunManagerMT::GetRunManager()->GetUserRunAction())) -> GetAggregator()->GetActiveChannels();
		//		(*active_channels)["VOLUME_LIST"] -> InsertData(static_cast<std::string>(thePostPVname));
		//	}
		}
	}
}
