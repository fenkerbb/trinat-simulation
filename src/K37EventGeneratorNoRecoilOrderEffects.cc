// Authors: Spencer Behling
// The point of this class is to hack in a quick 2% branch. It is 
// following the venerable software practice of cut and paste programming.

#include "K37EventGeneratorNoRecoilOrderEffects.hh"
#include "K37FermiFunction.hh"
#include "G4ParticleDefinition.hh"
#include "G4Electron.hh"
#include "G4Proton.hh"
#include "G4Gamma.hh"
#include "G4UnitsTable.hh"
#include "K37Config.hh"

// below: from JTW_EventNoRecoilOrderEffects.cc
#include <cmath>
//#include "K37Analysis.hh"
//#include "JTW_EventNoRecoilOrderEffects.hh"
#include "K37FermiFunction.hh"
//#include "K37AnalysisNumbering.hh"
#include "globals.hh"
#include "Randomize.hh"

K37EventGeneratorNoRecoilOrderEffects::K37EventGeneratorNoRecoilOrderEffects()
  :FF(0)
{
	FF = new K37FermiFunction();
	electron.Mass = 0.510998;           // MeV/c^2
	parent.Mass = 36.97337589*931.46;     // 37K (MeV/c^2)
	// 37Ar 5/2^+ excited state (MeV/c^2)
	daughter.Mass = (36.96677632*931.46)+2.796;
	neutrino.Mass = 0.0;

	//These are the values for the transition to the 2% branch. 
	//In this class these values have meaning because they are not 
	//overridden at event time by energy dependent values from the 
	//recoil order effects. The values in
	//K37EventGeneratorNoRecoilOrderEffects.cc are overiden at event time. 
	Zhi     =  2.0;
	BigA    = -3.0/5.0;
	BigB    =  3.0/5.0;
	LittleC = -1.0/5.0;
	LittleA = -1.0/3.0;

	// All in MeV/c^2
	QValue = -1.0 * (daughter.Mass - parent.Mass  + 2. * electron.Mass);
	electron.MaxE = ( pow(parent.Mass , 2.0) + pow(electron.Mass, 2.0) -
	                  pow((neutrino.Mass + daughter.Mass + electron.Mass), 2.0) ) / (2.0 * parent.Mass);
	neutrino.MaxE = ( pow(parent.Mass, 2.0) + pow(neutrino.Mass, 2.0) -
	                  pow((electron.Mass + daughter.Mass + electron.Mass), 2.0) ) / (2.0 * parent.Mass);
	daughter.MaxE = ( pow(parent.Mass, 2.0) + pow((daughter.Mass + electron.Mass), 2.0) -
	                  pow((electron.Mass+neutrino.Mass), 2.0)) /(2.0 * parent.Mass);
	electron.MaxT = electron.MaxE - electron.Mass;
	neutrino.MaxT = neutrino.MaxE - neutrino.Mass;
	daughter.MaxT = daughter.MaxE - daughter.Mass - electron.Mass;

	min_cos_theta_ = 0.0;                 // Default is no bias
}

K37EventGeneratorNoRecoilOrderEffects::~K37EventGeneratorNoRecoilOrderEffects() 
{
	delete FF;
}

void K37EventGeneratorNoRecoilOrderEffects::SetBigA(G4double BigA_)
{
	if ((BigA_ > 1) && (BigA_ < -1))
	{
		G4cout << "Warning! A_{beta} should be in range [-1,1]. " 
			   << BigA_<<" is unacceptable!"<< G4endl;
		G4cout << "No action taken." << G4endl;
		return;
	}
	BigA = BigA_;
}

void K37EventGeneratorNoRecoilOrderEffects::SetMinCosTheta(G4double mct) 
{
	if (mct < 0) 
	{
		G4cout << "Warning! Minimum cos(theta) should be positive!" << G4endl;
		G4cout << "No action taken." << G4endl;
		return;
	}
	min_cos_theta_ = mct;
}

void K37EventGeneratorNoRecoilOrderEffects::SetConeHalfAngle(G4double angle) 
{
	SetMinCosTheta(cos(angle));
}

// ---------------------------------------------------------------------------- //
// below: from JTW_EventNoRecoilOrderEffects.cc
/*
void K37EventGeneratorNoRecoilOrderEffects::MakeEvent() 
{
	G4double this_polarization  = 1.0;
	G4double this_alignment     = 1.0;
//	G4double this_recoil_charge = 1.0;
	MakeEvent(this_polarization, this_alignment);
}
*/

void K37EventGeneratorNoRecoilOrderEffects::MakeEvent(G4double polarization, G4double alignment) 
{
	bool verbose=false;
	if(verbose)
	{
		G4cout << "Called K37EventGeneratorNoRecoilOrderEffects::MakeEvent(G4double polarization, G4double alignment)." << G4endl;
		G4cout << " -- First random number:  " << G4UniformRand() << G4endl;
	}
	
	// Set cuts on production for testing the EV Generator
	G4double minElectronT = 0.0*electron.MaxT;  // MeV --> beta_min = 0.995
	// radius 20 mm 98.5 mm from trap center
	// electron.Theta = M_PI/2.0;
	electron.T = -10.0;
	while (electron.T < minElectronT) 
	{
		while (true) 
		{
		testOmega = 3.*G4UniformRand();
			// testOmega = 30.0*G4UniformRand(); // What's the right value here!?
			while (true) 
			{
				while (true) 
				{
					// For faster testing:
					// electron.T = 0.1*electron.MaxT;
					// electron.T -=  (electron.MaxT - minElectronT) * G4UniformRand();
					electron.T = electron.MaxT*G4UniformRand();  // MeV
					electron.E = electron.T + electron.Mass;     // MeV
					electron.PmagSquared = (electron.T+electron.Mass)*
					  (electron.T+electron.Mass) -
					  electron.Mass*electron.Mass;  // MeV/c with c = 1
					electron.Pmag = sqrt(electron.PmagSquared);
					
					G4double mu;    // cos(electron.Theta)
					// Picks mu from -1 to 1 uniformly
					//          mu = 1.0 - 2.0*G4UniformRand();
					// Picks mu uniformly from abs(mu) = minCosTheta to 1
					mu = 1.0 - (1.0-min_cos_theta_)*G4UniformRand();
					if (G4UniformRand() > 0.5) mu *= -1.0;
					// mu = 1.0;
					
					electron.Theta = acos(mu);
					electron.Phi = 2.0*M_PI*G4UniformRand();
					
					// x = r*sin(Theta)*cos(Phi)
					// y = r*sin(Theta)*sin(Phi)
					// z = r*cos(Theta)
					
					electron.unitX = sin(electron.Theta)*cos(electron.Phi);
					electron.unitY = sin(electron.Theta)*sin(electron.Phi);
					electron.unitZ = cos(electron.Theta);
					
					electron.X = electron.Pmag*electron.unitX;
					electron.Y = electron.Pmag*electron.unitY;
					electron.Z = electron.Pmag*electron.unitZ;
					
					neutrino.Theta = acos(1.0 - 2.0*G4UniformRand());
					neutrino.Phi = 2.0*M_PI*G4UniformRand();
					neutrino.unitX = sin(neutrino.Theta)*cos(neutrino.Phi);
					neutrino.unitY = sin(neutrino.Theta)*sin(neutrino.Phi);
					neutrino.unitZ = cos(neutrino.Theta);
					
					// V dot W = mag (V)*mag(W) cos(alpha)
					// where alpha is the angle between them for unit vectors this reduces
					// to alpha = acos(V dot W) and cos(alpha) = V dot W
					
					cosBetaNeutrinoAngle =  electron.unitX*neutrino.unitX +
						electron.unitY*neutrino.unitY + electron.unitZ*neutrino.unitZ;
					
					neutrino.E =  parent.Mass*(electron.MaxE - electron.E)/
						(parent.Mass - electron.E + electron.Pmag * cosBetaNeutrinoAngle);
					
					if (neutrino.E < neutrino.MaxE) 
					{
						// G4cout << "Broke out neutrino" << G4endl;
						break;
					}
					continue;
				}  // Innermost while(true)
				neutrino.T = neutrino.E;        // Massless neutrino
				neutrino.Pmag = neutrino.E;
				
				neutrino.X = neutrino.Pmag*neutrino.unitX;
				neutrino.Y = neutrino.Pmag*neutrino.unitY;
				neutrino.Z = neutrino.Pmag*neutrino.unitZ;
				
				neutrino.PmagSquared = neutrino.Pmag*neutrino.Pmag;
				
				daughter.X = -1.0*(electron.X + neutrino.X);
				daughter.Y = -1.0*(electron.Y + neutrino.Y);
				daughter.Z = -1.0*(electron.Z + neutrino.Z);
				
				daughter.Pmag = sqrt(daughter.X*daughter.X + daughter.Y*daughter.Y +
				                     daughter.Z*daughter.Z);
				
				daughter.unitX = daughter.X/daughter.Pmag;
				daughter.unitY = daughter.Y/daughter.Pmag;
				daughter.unitZ = daughter.Z/daughter.Pmag;
				
				daughter.E = sqrt(daughter.Pmag*daughter.Pmag +
				                  daughter.Mass*daughter.Mass);
				// daughter.T =  daughter.E-daughter.Mass-electron.Mass;
				daughter.T =  daughter.E-daughter.Mass;
				
				daughter.Theta =  acos(daughter.Z/daughter.Pmag);
				daughter.Phi =  atan2(daughter.Y, daughter.X);
				if (daughter.Phi < 0.0) 
				{
					daughter.Phi = 2.0*M_PI + daughter.Phi;
				}
				if (daughter.E < daughter.MaxE) 
				{
					// G4cout << "broke out (daughter)" << G4endl;
					break;
				}
				continue;
			}  // Middle while(true)
			// since j is (0,0,1) eMomentum dot j is just eMomentumZ
			
			eDotJ = electron.Z;
			nDotJ = neutrino.Z;
			
			eDotn = electron.X*neutrino.X + electron.Y*neutrino.Y +
			  electron.Z*neutrino.Z;
			
			Omega = (1.0/(4.0*pow(M_PI, 3.0)))
			  *(FF->getVFF(electron.T))
			  *electron.Pmag
			  *electron.E
			  *pow((electron.MaxE - electron.E), 2.0)
			  *Zhi*(1.0
			        + LittleA*(eDotn)/(electron.E*neutrino.E)
			        - alignment*LittleC*(eDotn)/(3.0*electron.E*neutrino.E)
			        + alignment*LittleC*eDotJ*nDotJ/(electron.E*neutrino.E)
			        + polarization*BigA*eDotJ/electron.E
			        + polarization*BigB*nDotJ/neutrino.E);
			if (Omega >= testOmega) 
			{
				// G4cout << "Broke out (omega)! " << G4endl;
				break;
			}
			continue;
		}  // Outermost while(true)
	}
	//  G4cout << cos(electron.Theta) << "\t" << electron.T << G4endl;
	// Calculate the velocity of the electron in the lab from.
	// E = gamma*m*c^2; gamma = (1-v^2/c^2)^(-1/2) --> v = c*sqrt(1-(m^2c^4/E^2)
//	double gamma = electron.E/electron.Mass;
//	double v_over_c = sqrt(1.0 - pow(gamma, -2.0));

	// G4cout << "T = " << electron.T << "\t-->gamma = ";
	// G4cout << (electron.T/electron.Mass)+1.0 << "\t E = " << electron.E;
	// G4cout << "\t--> gamma = " << electron.E/electron.Mass << "\tbeta = ";
	// G4cout << v_over_c << G4endl;
	// G4cout << "Used in calculation: " << eDotJ/electron.E << G4endl;

	// Fill up histogram to check that event generator makes sense
//	running_denom += fabs(v_over_c*cos(electron.Theta));
	if (cos(electron.Theta) > 0.0) 
	{
		numPlus++;
	} 
	else 
	{
		numMins++;
	}
	/*
	if (false) 
	{
		(*active_channels_)["T_GEN_ELE"] -> InsertData(electron.T);  // kinetic energy.
		(*active_channels_)["MU_GEN_ELE"] -> InsertData(cos(electron.Theta));
		(*active_channels_)["MU_GEN_RECOIL"] -> InsertData(cos(daughter.Theta));
		(*active_channels_)["ION_CHARGE"] -> InsertData(recoil_charge);
		//    the_aggregator_ -> EndEvent()
	}
	*/
}     // End makeEvent

void K37EventGeneratorNoRecoilOrderEffects::ResetGeneratedCounters() 
{
	numPlus = 0.0;
	numMins = 0.0;
}

G4int K37EventGeneratorNoRecoilOrderEffects::GetNumMins() 
{ 
	return numMins; 
}

G4int K37EventGeneratorNoRecoilOrderEffects::GetNumPlus() 
{ 
	return numPlus; 
}
