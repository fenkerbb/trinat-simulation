// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 2013
// 
// Possibly this class is never even used.

#include "K37Run.hh"
#include "G4HCofThisEvent.hh"

//using std::string;

K37Run::K37Run():
	v1190_factor_ns(0.09765625)  // value copied from K37EventAction, because apparently it's needed here now.
/*
: N_eventsgenerated(0),
  N_eventsaccepted(0)
*/
{
	G4cout << "* Created a new K37Run::K37Run()." << G4endl;
	N_generated_total = 0;
	N_accepted_total = 0;
}


K37Run::~K37Run()
{ }

void K37Run::Merge(const G4Run* aRun)  // this one gets called correctly.  the argument literally changes whether it gets called.
//void K37Run::Merge(const K37Run* aRun)  // this never seems to get called .....
{
//	G4cout << "******* K37Run::Merge(...) has been called." << G4endl;
	const K37Run* localRun = static_cast<const K37Run*>(aRun);
	
//	fEDep += localRun->fEDep;
//	G4Run::Merge(aRun);
//	fGoodEvents += localRun->fGoodEvents;
//	fSumDose    += localRun->fSumDose;
//	fStatDose   += localRun->fStatDose;
	
	N_generated_total += localRun->N_generated_total;
	N_accepted_total  += localRun->N_accepted_total;
	
	G4Run::Merge(aRun); 
//	this->PrintResultsToScreen();
} 

void K37Run::RecordEvent(const G4Event* evt) // I think this gets called whether we actually end up recording it or not.
{
	// ok, all the event-recording stuff goes here.  thread-local.
//	G4cout << "K37Run::RecordEvent(const G4Event* evt) is called.  So that seems promising." << G4endl;

//	UInt_t TTTL_OP_Beam;//, TTLBit_SigmaPlus;  // not needed anymore?
	UInt_t tdc_scint_bottom_count, tdc_scint_top_count;  // never goes into the data?  I guess?
	
	map<string, K37_Data*>    * active_channels_       = ((K37RunAction*)(K37RunManagerMT::GetRunManager()->GetUserRunAction()))->GetAggregator()->GetActiveChannels();
	K37DetectorConstruction   * detector_construction_ = K37RunManagerMT::GetMasterRunManager()->GetUserDetectorConstruction();
	G4RunManager              * runMan                 = G4RunManager::GetRunManager();
	K37PrimaryGeneratorAction * kpga                   = (K37PrimaryGeneratorAction*)(runMan -> GetUserPrimaryGeneratorAction()); // how ... does this even work??
	K37RunAction              * the_runaction          =  (K37RunAction*)(G4RunManager::GetRunManager()->GetUserRunAction());
	G4SDManager               * SDman                  = G4SDManager::GetSDMpointer();
	LocalAggregator           * the_aggregator_        = ((K37RunAction*)(G4RunManager::GetRunManager()->GetUserRunAction()))->GetAggregator();
	
//	G4cout << "We've got pointers to many things." << G4endl;
	
	// ***************************************************************************
	// Determine what type of event this is.
	// ***************************************************************************
	G4double event_type = 0.0;
	// the thing below might have worked, but I think we need to move the event type sorter over to the K37Run class too.
//	K37EventAction            *ea                      = (K37EventAction*)G4RunManager::GetRunManager()->GetUserEventAction();
//	event_type = ea -> EventTypeSorter(evt);
	event_type = EventTypeSorter(evt);
	
	// Digitizers
	G4DigiManager *digitizer_manager(G4DigiManager::GetDMpointer());
	
	K37ScintillatorDigitizer *upper_scintillator_digitizer(
	    static_cast<K37ScintillatorDigitizer *>( digitizer_manager -> FindDigitizerModule("scintillatorPlusZ")) );
	K37ScintillatorDigitizer *lower_scintillator_digitizer(
	    static_cast<K37ScintillatorDigitizer *>( digitizer_manager -> FindDigitizerModule("scintillatorMinusZ")) );

	K37StripDetectorDigitizer *upper_strip_detector_digitizer(
	    static_cast<K37StripDetectorDigitizer *>( digitizer_manager -> FindDigitizerModule("dsssdPlusZ")) );
	K37StripDetectorDigitizer *lower_strip_detector_digitizer(
	    static_cast<K37StripDetectorDigitizer *>( digitizer_manager -> FindDigitizerModule("dsssdMinusZ")) );

	// Digitizers are mostly here to insert noise.  
	// Not useful actually, but it's extra work to take it out.  
	lower_scintillator_digitizer   -> Digitize();
	upper_scintillator_digitizer   -> Digitize();
	upper_strip_detector_digitizer -> Digitize();
	lower_strip_detector_digitizer -> Digitize();
	
	// ------------------------------------------------------------
	G4double energyUpperScint_Total    = 0;
	G4double energyUpperScint_AllElse  = 0;
	G4double energyUpperScint_Positron = 0;
	G4double energyUpperScint_Electron = 0;
	G4double energyUpperScint_Gamma    = 0;

	G4double energyLowerScint_Total    = 0;
	G4double energyLowerScint_AllElse  = 0;
	G4double energyLowerScint_Positron = 0;
	G4double energyLowerScint_Electron = 0;
	G4double energyLowerScint_Gamma    = 0;
	
	G4double energyUpperSilicon_Total    = 0;
	G4double energyUpperSilicon_AllElse  = 0;
	G4double energyUpperSilicon_Positron = 0;
	G4double energyUpperSilicon_Electron = 0;
	G4double energyUpperSilicon_Gamma    = 0;

	G4double energyLowerSilicon_Total    = 0;
	G4double energyLowerSilicon_AllElse  = 0;
	G4double energyLowerSilicon_Positron = 0;
	G4double energyLowerSilicon_Electron = 0;
	G4double energyLowerSilicon_Gamma    = 0;
	
	G4double time_upper_scintillator = 0.0;
	G4double time_lower_scintillator = 0.0;

	G4int upper_scintillator_pdg = 0;
	G4int lower_scintillator_pdg = 0;

	// Strip-by-strip information for each detector
	vector<G4double> sd_energy_plusZ_X(40, 0.0), sd_energy_plusZ_Y(40, 0.0);
	vector<G4double> sd_energy_minsZ_X(40, 0.0), sd_energy_minsZ_Y(40, 0.0);

	// rMCP info
	G4double recoil_mcp_x_pos  = 0.0;
	G4double recoil_mcp_z_pos  = 0.0;
	G4double recoil_mcp_time   = 0.0;
	G4double recoil_mcp_energy = 0.0;  // MJA.  Doesn't do anything yet.
	G4int recoil_pdg = 0;

	// eMCP info
	G4double electron_mcp_x_pos  = 0.0;  // MJA.  Doesn't do anything yet.
	G4double electron_mcp_z_pos  = 0.0;  // MJA.  Doesn't do anything yet.
	G4double electron_mcp_time   = 0.0;
	G4double electron_mcp_energy = 0.0;
	G4int electron_pdg = 0;
	
	G4HCofThisEvent              * hit_collection              = evt->GetHCofThisEvent();
	K37RecoilMCPHitsCollection   * recoil_mcp_hit_collection   = 0;
	K37ElectronMCPHitsCollection * electron_mcp_hit_collection = 0;
	
	if (hit_collection) 
	{
		recoil_mcp_hit_collection =
			static_cast<K37RecoilMCPHitsCollection*>(hit_collection -> GetHC(recoil_mcp_collection_id));
		electron_mcp_hit_collection =
			static_cast<K37ElectronMCPHitsCollection*>(hit_collection -> GetHC(electron_mcp_collection_id));
	}
	G4bool electron_mcp_constructed = detector_construction_ -> GetMakeElectronMCP();
	G4bool recoil_mcp_constructed   = detector_construction_ -> GetMakeRecoilMCP();

	G4double upper_mirror_E = 0.0;
	G4double lower_mirror_E = 0.0;
	G4double upper_beWindow_E = 0.0;
	G4double lower_beWindow_E = 0.0;
	G4double *pval;
	
	G4THitsMap<G4double> *scorer_map;
	scorer_map = (G4THitsMap<G4double>*) (hit_collection->GetHC(SDman->GetCollectionID("MirrorEnergyScorerT/MirrorEdepT")));
	pval = (*scorer_map)[0];
	if (pval) { upper_mirror_E = *pval; }
	scorer_map = (G4THitsMap<G4double>*) (hit_collection->GetHC(SDman->GetCollectionID("MirrorEnergyScorerB/MirrorEdepB")));
//	pval = (*scorer_map)[1];  // don't do it!  it looks like a good idea at first glance, but it's wrong!
	pval = (*scorer_map)[0];
	if (pval) { lower_mirror_E = *pval; }

	scorer_map = (G4THitsMap<G4double>*) (hit_collection->GetHC(SDman->GetCollectionID("BeWindowEnergyScorerT/BeEdepT")));
	pval = (*scorer_map)[0];
	if (pval) { upper_beWindow_E = *pval; }
	scorer_map = (G4THitsMap<G4double>*) (hit_collection->GetHC(SDman->GetCollectionID("BeWindowEnergyScorerB/BeEdepB")));
//	pval = (*scorer_map)[1];
	pval = (*scorer_map)[0];
	if (pval) { lower_beWindow_E = *pval; }
	
	
	// ************************************************************
	// -------------------- Query the upper scintillator
	// ************************************************************
	time_upper_scintillator = upper_scintillator_digitizer -> GetHitTime();
	tdc_scint_top_count = 0;
	if (time_upper_scintillator > 0.) tdc_scint_top_count = 1;

	energyUpperScint_Total    = upper_scintillator_digitizer -> GetEnergyTotal();
	energyUpperScint_Electron = upper_scintillator_digitizer -> GetEnergyElectron();
	energyUpperScint_Positron = upper_scintillator_digitizer -> GetEnergyPositron();
	energyUpperScint_Gamma    = upper_scintillator_digitizer -> GetEnergyGamma();
	energyUpperScint_AllElse  = upper_scintillator_digitizer -> GetEnergyOther();
	upper_scintillator_pdg    = upper_scintillator_digitizer -> GetParticleCode();
	
	// ************************************************************
	// -------------------- Query the lower scintillator
	// ************************************************************
	time_lower_scintillator = lower_scintillator_digitizer -> GetHitTime();
	tdc_scint_bottom_count = 0;
	if (time_lower_scintillator > 0.) tdc_scint_bottom_count = 1;

	energyLowerScint_Total    = lower_scintillator_digitizer -> GetEnergyTotal();
	energyLowerScint_Electron = lower_scintillator_digitizer -> GetEnergyElectron();
	energyLowerScint_Positron = lower_scintillator_digitizer -> GetEnergyPositron();
	energyLowerScint_Gamma    = lower_scintillator_digitizer -> GetEnergyGamma();
	energyLowerScint_AllElse  = lower_scintillator_digitizer -> GetEnergyOther();
	lower_scintillator_pdg    = lower_scintillator_digitizer -> GetParticleCode();
	// ------------------------------------------------------------

	// ************************************************************
	// -------------------- Query the upper strip detector
	// ************************************************************
	sd_energy_plusZ_X = upper_strip_detector_digitizer -> GetEnergyXstrip();
	sd_energy_plusZ_Y = upper_strip_detector_digitizer -> GetEnergyYstrip();
	
	energyUpperSilicon_Total    = upper_strip_detector_digitizer -> GetEnergyTotal();
	energyUpperSilicon_Electron = upper_strip_detector_digitizer -> GetEnergyElectron();
	energyUpperSilicon_Positron = upper_strip_detector_digitizer -> GetEnergyPositron();
	energyUpperSilicon_Gamma    = upper_strip_detector_digitizer -> GetEnergyGamma();
	energyUpperSilicon_AllElse  = upper_strip_detector_digitizer -> GetEnergyOther();
	// ------------------------------------------------------------

	// ************************************************************
	// -------------------- Query the lower strip detector
	// ************************************************************
	sd_energy_minsZ_X = lower_strip_detector_digitizer -> GetEnergyXstrip();
	sd_energy_minsZ_Y = lower_strip_detector_digitizer -> GetEnergyYstrip();
	
	energyLowerSilicon_Total    = lower_strip_detector_digitizer -> GetEnergyTotal();
	energyLowerSilicon_Electron = lower_strip_detector_digitizer -> GetEnergyElectron();
	energyLowerSilicon_Positron = lower_strip_detector_digitizer -> GetEnergyPositron();
	energyLowerSilicon_Gamma    = lower_strip_detector_digitizer -> GetEnergyGamma();
	energyLowerSilicon_AllElse  = lower_strip_detector_digitizer -> GetEnergyOther();
	// ------------------------------------------------------------
	
	// ***************************************************************************
	// Get all the info the the electron MCP detector
	// Use only the first hit
	// ***************************************************************************
	if (electron_mcp_hit_collection && electron_mcp_constructed) 
	{ // eMCP
		if (electron_mcp_hit_collection -> entries() > 0) 
		{
			int n_hit = electron_mcp_hit_collection -> entries();
			K37ElectronMCPHit *emcp_hit;
			
			electron_mcp_energy = 0;  // MJA:  added this.  do I need it?
			for (int i = 0; i < n_hit; i++) 
			{
				emcp_hit = (*electron_mcp_hit_collection)[i];
				if(i==0)
				{
					electron_mcp_time = emcp_hit -> GetTime();
					electron_pdg = emcp_hit -> GetParticlePDG();
					electron_mcp_x_pos = emcp_hit -> GetXPos();  // MJA.  Doesn't do anything yet.
					electron_mcp_z_pos = emcp_hit -> GetZPos();  // MJA.  Doesn't do anything yet.
				}
				electron_mcp_energy += emcp_hit -> GetEnergy();
			}
		}
	}
	
	
	// ***************************************************************************
	// Event type determined.  Now figure out what to do with it.
	// ***************************************************************************
	the_runaction -> IncrementGenerated();
	this -> IncrementGenerated();

// later I'll put it back to the old way of using control statements here, because it will run faster that way, but be more annoying to debug.	
//	if( !(kpga->EventAcceptanceChecker( G4int(event_type))) )  //Event did not pass trigger
//	{
//		the_aggregator_ -> EndEvent(false);
//	}
//	else  // event accepted.

//	if( (kpga->EventAcceptanceChecker( G4int(event_type))) )  // accepted.
//	if( (this->EventAcceptanceChecker( G4int(event_type))) )  // accepted.
	if( (the_runaction->EventAcceptanceChecker( G4int(event_type))) )  // accepted.
	{
		the_runaction -> IncrementAccepted();
		this -> IncrementAccepted();
		
	//	runAct = G4MTRunManager::GetMasterRunManager()->GetEventManager()->GetEventAction();
	//	runAct->IncrementAccepted();  // K37RunAction.hh:  increments "K37RunAction::accepted".
	//	cloud = ( (K37ActionInitialization*)(G4MTRunManager::GetMasterRunManager()->GetUserActionInitialization()) )->GetCloud();
		
//		G4cout << "* Event is accepted.  ... Yeah, this might crash here in the second run. ..." << G4endl;  
		// it still might crash here, but I want it to be less verbose.

		(*active_channels_)["QDC_UpperPMT"   ] -> InsertData(energyUpperScint_Total/keV   );
		(*active_channels_)["QDC_UpperPMT_AE"] -> InsertData(energyUpperScint_AllElse/keV );
		(*active_channels_)["QDC_UpperPMT_P" ] -> InsertData(energyUpperScint_Positron/keV);
		(*active_channels_)["QDC_UpperPMT_E" ] -> InsertData(energyUpperScint_Electron/keV);
		(*active_channels_)["QDC_UpperPMT_G" ] -> InsertData(energyUpperScint_Gamma/keV   );
		
		(*active_channels_)["QDC_LowerPMT"   ] -> InsertData(energyLowerScint_Total/keV   );
		(*active_channels_)["QDC_LowerPMT_AE"] -> InsertData(energyLowerScint_AllElse/keV );
		(*active_channels_)["QDC_LowerPMT_P" ] -> InsertData(energyLowerScint_Positron/keV);
		(*active_channels_)["QDC_LowerPMT_E" ] -> InsertData(energyLowerScint_Electron/keV);
		(*active_channels_)["QDC_LowerPMT_G" ] -> InsertData(energyLowerScint_Gamma/keV   );
		
		(*active_channels_)["QDC_UpperSilicon"   ] -> InsertData(energyUpperSilicon_Total/keV   );
		(*active_channels_)["QDC_UpperSilicon_AE"] -> InsertData(energyUpperSilicon_AllElse/keV );
		(*active_channels_)["QDC_UpperSilicon_P" ] -> InsertData(energyUpperSilicon_Positron/keV);
		(*active_channels_)["QDC_UpperSilicon_E" ] -> InsertData(energyUpperSilicon_Electron/keV);
		(*active_channels_)["QDC_UpperSilicon_G" ] -> InsertData(energyUpperSilicon_Gamma/keV   );
		
		(*active_channels_)["QDC_LowerSilicon"   ] -> InsertData(energyLowerSilicon_Total/keV   );
		(*active_channels_)["QDC_LowerSilicon_AE"] -> InsertData(energyLowerSilicon_AllElse/keV );
		(*active_channels_)["QDC_LowerSilicon_P" ] -> InsertData(energyLowerSilicon_Positron/keV);
		(*active_channels_)["QDC_LowerSilicon_E" ] -> InsertData(energyLowerSilicon_Electron/keV);
		(*active_channels_)["QDC_LowerSilicon_G" ] -> InsertData(energyLowerSilicon_Gamma/keV   );
		
		(*active_channels_)["Upper_Mirror_Edep"]   -> InsertData(upper_mirror_E/keV  );
		(*active_channels_)["Lower_Mirror_Edep"]   -> InsertData(lower_mirror_E/keV  );
		(*active_channels_)["Upper_BeWindow_Edep"] -> InsertData(upper_beWindow_E/keV);
		(*active_channels_)["Lower_BeWindow_Edep"] -> InsertData(lower_beWindow_E/keV);
		
		(*active_channels_)["TDC_SCINT_TOP"]    -> InsertData(time_upper_scintillator/ns/v1190_factor_ns);
		(*active_channels_)["TDC_SCINT_BOTTOM"] -> InsertData(time_lower_scintillator/ns/v1190_factor_ns);
				
		///// naive hit info goes here.
		(*active_channels_)["gen_Ebeta"]    -> InsertData( kpga->GetEventGenerator()->get_electron_E()/keV );
		(*active_channels_)["gen_Tbeta"]    -> InsertData( kpga->GetEventGenerator()->get_electron_T()/keV );
		(*active_channels_)["gen_costheta"] -> InsertData( kpga->GetEventGenerator()->get_electron_costheta() );
		if(naive_hit_top==true)
		{
			(*active_channels_)["naive_hit_t"]  -> InsertData( 1. );
			(*active_channels_)["naive_hit_b"]  -> InsertData( 0. );
			(*active_channels_)["gen_xhit_t"]   -> InsertData( naive_hitposition.x()/mm );
			(*active_channels_)["gen_yhit_t"]   -> InsertData( naive_hitposition.y()/mm );
			double tmp_x = floor( naive_hitposition.x()/mm ) + 0.5;
			double tmp_y = floor( naive_hitposition.y()/mm ) + 0.5;  // re-allocating every event is probably a slow way to run it.
			(*active_channels_)["gen_rhit_t"]   -> InsertData( sqrt( tmp_x*tmp_x + tmp_y*tmp_y ) );  // pixelate it.
			
			
			(*active_channels_)["gen_xhit_b"]   -> InsertData( -50. );
			(*active_channels_)["gen_yhit_b"]   -> InsertData( -50. );
			(*active_channels_)["gen_rhit_b"]   -> InsertData( -50. );
		}
		else if(naive_hit_bottom==true)
		{
			(*active_channels_)["naive_hit_t"]  -> InsertData( 0. );
			(*active_channels_)["naive_hit_b"]  -> InsertData( 1. );
			(*active_channels_)["gen_xhit_b"]   -> InsertData( naive_hitposition.x()/mm );
			(*active_channels_)["gen_yhit_b"]   -> InsertData( naive_hitposition.y()/mm );
			double tmp_x = floor( naive_hitposition.x()/mm ) + 0.5;
			double tmp_y = floor( naive_hitposition.y()/mm ) + 0.5;
			(*active_channels_)["gen_rhit_b"]   -> InsertData( sqrt( tmp_x*tmp_x + tmp_y*tmp_y ) );  // pixelate it.
			
			(*active_channels_)["gen_xhit_t"]   -> InsertData( -50. ); // figure out how to veto the event later.  I can't seem to make a vector of hits, so...
			(*active_channels_)["gen_yhit_t"]   -> InsertData( -50. );
			(*active_channels_)["gen_rhit_t"]   -> InsertData( -50. );
		}
		else // naively, didn't hit top or bottom detector.
		{
			(*active_channels_)["naive_hit_t"]  -> InsertData( 0. );
			(*active_channels_)["naive_hit_b"]  -> InsertData( 0. );
			
			(*active_channels_)["gen_xhit_t"]   -> InsertData( -50. );
			(*active_channels_)["gen_yhit_t"]   -> InsertData( -50. );
			(*active_channels_)["gen_rhit_t"]   -> InsertData( -50. );
			
			(*active_channels_)["gen_xhit_b"]   -> InsertData( -50. );
			(*active_channels_)["gen_yhit_b"]   -> InsertData( -50. );
			(*active_channels_)["gen_rhit_b"]   -> InsertData( -50. );
		}
		//////////////
		(*active_channels_)["TWO_PERCENT_BRANCH"] -> InsertData( kpga->ReturnIs2p() );
		(*active_channels_)["ION_CHARGE"]         -> InsertData( kpga->ReturnChargeStateThisEvent() );
		
		
		
		if (electron_mcp_constructed && electron_mcp_hit_collection) 
		{
			(*active_channels_)["ELECTRON_MCP_PARTICLE_PDG"] -> InsertData((G4double)electron_pdg);
			// Run through all the hits.  .... or not.
			if (electron_mcp_hit_collection -> entries() > 0) 
			{
				(*active_channels_)["TDC_ELECTRON_MCP"]  -> InsertData(electron_mcp_time/ns/v1190_factor_ns);
				(*active_channels_)["HEX75_XPos"]        -> InsertData(electron_mcp_x_pos/mm);
				(*active_channels_)["HEX75_ZPos"]        -> InsertData(electron_mcp_z_pos/mm);
			}
			(*active_channels_)["ELECTRON_MCP_N_HITS"] ->
				InsertData((G4double)electron_mcp_hit_collection -> entries());
		}
		if (recoil_mcp_constructed && recoil_mcp_hit_collection) 
		{
			(*active_channels_)["ION_MCP_PARTICLE_PDG"] -> InsertData((G4double)recoil_pdg);
			// Run through all the hits.
			if (recoil_mcp_hit_collection -> entries() > 0) 
			{
				K37RecoilMCPHit *rmcp_hit;
				int n_hit_recoil = recoil_mcp_hit_collection -> entries();  // this is always 2.  why??
				
				recoil_mcp_x_pos = 0.0;
				recoil_mcp_z_pos = 0.0;
				recoil_mcp_time  = 0.0;
				recoil_mcp_energy= 0.0; // MJA.  doesn't do anything yet.
				
				for (int i=0; i<n_hit_recoil; i++) 
				{
					rmcp_hit = (*recoil_mcp_hit_collection)[i];
					if(i==0)
					{ 
						recoil_pdg = rmcp_hit -> GetParticlePDG();
						(*active_channels_)["ION_MCP_PARTICLE_PDG"] -> InsertData((G4double)recoil_pdg);
					}
					recoil_mcp_x_pos   = rmcp_hit -> GetXPos();
					recoil_mcp_z_pos   = rmcp_hit -> GetZPos();
					recoil_mcp_time    = rmcp_hit -> GetTime();
					recoil_mcp_energy += rmcp_hit -> GetEnergy();  // MJA.  Doesn't do anything yet.
					
					(*active_channels_)["TDC_ION_MCP"] -> InsertData(recoil_mcp_time/ns/v1190_factor_ns);
					(*active_channels_)["DL_X_Pos"]    -> InsertData(recoil_mcp_x_pos/mm);
					(*active_channels_)["DL_Z_Pos"]    -> InsertData(recoil_mcp_z_pos/mm);
				}
			}
		}		
		(*active_channels_)["VNIM_event_type"] -> InsertData(event_type);
		(*active_channels_)["UPPER_SCINTILLATOR_PDG"] -> InsertData((G4double)upper_scintillator_pdg);
		(*active_channels_)["LOWER_SCINTILLATOR_PDG"] -> InsertData((G4double)lower_scintillator_pdg);

		// Fill all the ntuples with data from the vectors
	//	if (runAct -> GetRecordStripDetectorData() ) 
		if ( (K37RunManagerMT::GetMasterRunManager()->GetUserDetectorConstruction()) -> GetRecordStripDetectorData() ) 
		{
			// New (Aggregator) way
			fillSDNtuples(sd_energy_minsZ_X, "BB1_AMPLITUDE_LX");
			fillSDNtuples(sd_energy_minsZ_Y, "BB1_AMPLITUDE_LY");
			fillSDNtuples(sd_energy_plusZ_X, "BB1_AMPLITUDE_UX");
			fillSDNtuples(sd_energy_plusZ_Y, "BB1_AMPLITUDE_UY");
		}
		
		// Add a new row here to add a new row for only accpeted events where
		// either there was energy in the plus or minus z detector, but not both!
		// Fill Run_Number with negative version number to indicate simulated
		//    (*active_channels_)["Run_Number"] -> InsertData(-1.0*K37_VERSION);
		
		K37CloudSetup * cloud = ( (K37ActionInitialization*)(G4MTRunManager::GetMasterRunManager()->GetUserActionInitialization()) )->GetCloud();
		
		double polarization = cloud -> GetPolarization();
		// 1000000 ns = 1000 us is within the optical pumping on time
		// 10 ns is outside the optical pumping on time
		uint64_t  op_time;
		G4double op_bit_onoff;
		if (fabs(polarization) > 0) 
		{  // Polarized
			op_time = cloud -> GetDecayTime();
			op_bit_onoff = 1.0;
		} 
		else 
		{               // Not polarized
			op_time = 10;        // Will show up as unpolarized just like real data
			op_bit_onoff = 0.0;
		}
//		TTTL_OP_Beam = static_cast<UInt_t>(op_time/ns);
		(*active_channels_)["TTLBit_OPBeam"] -> InsertData(op_bit_onoff);	
		
		// Get cloud data and add to event file:
	//	K37CloudSetup *cloud = kpga -> GetCloud();
		
		G4ThreeVector pos = cloud -> GetFinalPosition();
		decay_pos_x = pos.x()/mm;
		decay_pos_y = pos.y()/mm;
		decay_pos_z = pos.z()/mm;
		
		G4ThreeVector vel = cloud -> GetFinalVelocity();
		decay_velocity_x = vel.x()/mm;
		decay_velocity_y = vel.y()/mm;
		decay_velocity_z = vel.z()/mm;
		
		
	//	the_aggregator_ -> EndEvent(true);
	} // end if check if event is accepted.
	
//	if( !(kpga->EventAcceptanceChecker( G4int(event_type))) )  //Event did not pass trigger
//	if( !(this->EventAcceptanceChecker( G4int(event_type))) )  //Event did not pass trigger
	if( !(the_runaction->EventAcceptanceChecker( G4int(event_type))) )  //Event did not pass trigger
	{
		the_aggregator_ -> EndEvent(false);
	}
	else
	{
		the_aggregator_ -> EndEvent(true);
	}
}  

//void K37EventAction::fillSDNtuples(vector<G4double> energy_strip, G4String name) 
void K37Run::fillSDNtuples(vector<G4double> energy_strip, G4String name) 
{
//	G4cout << "Called K37EventAction::fillSDNtuples(...)" << G4endl;
	G4bool debug = false;
	map<string, K37_Data*> * active_channels_ = ((K37RunAction*)(K37RunManagerMT::GetRunManager()->GetUserRunAction()))->GetAggregator()->GetActiveChannels();
	
	G4String temp = name.substr(name.size()-2, 2);
	for (G4int i = 0; i < 40; i++) 
	{
		(*active_channels_)[name] -> InsertData(energy_strip[i]/keV);
		if (energy_strip[i] > 0 && debug) 
		{
			G4cout << name << " strip " << i << " with " << energy_strip[i]/keV << G4endl;
		}
	}
}
//--------------------------------------------------------------------------
// Melissa!!!  Look here!
// EventTypeSorter(...) needs to sort events that "naively should have hit" a detector if we didn't care about scattering.
// Let's make it type 1024.
//int K37EventAction::EventTypeSorter(const G4Event* evt)  
int K37Run::EventTypeSorter(const G4Event* evt)  
{
//	G4cout << "Called K37Run::EventTypeSorter(...)." << G4endl;
	G4double event_type = 0.0;
	
	K37DetectorConstruction   * detector_construction_ = K37RunManagerMT::GetMasterRunManager()->GetUserDetectorConstruction();
	G4RunManager              * runMan                 = G4RunManager::GetRunManager();
	K37PrimaryGeneratorAction * kpga                   = (K37PrimaryGeneratorAction*)(runMan -> GetUserPrimaryGeneratorAction());  // does this even do what I want anymore??
	G4SDManager               * SDman                  = G4SDManager::GetSDMpointer();
	
	// below:  these things migrated from K37EventAction, but I didn't really remove references from there.  Is it OK, or does it crash?
	G4String colNam;
	recoil_mcp_collection_id   = SDman -> GetCollectionID(colNam="rMCP_HC");
	electron_mcp_collection_id = SDman -> GetCollectionID(colNam="eMCP_HC");
	
	bool is_photoionization_event = false;
	if( kpga->GetRecoilCharge() == -3) { is_photoionization_event = true; }
	
	//
	G4DigiManager *digitizer_manager(G4DigiManager::GetDMpointer());
	K37ScintillatorDigitizer *upper_scintillator_digitizer(
		static_cast<K37ScintillatorDigitizer *>( digitizer_manager -> FindDigitizerModule("scintillatorPlusZ")) );
	K37ScintillatorDigitizer *lower_scintillator_digitizer(
		static_cast<K37ScintillatorDigitizer *>( digitizer_manager -> FindDigitizerModule("scintillatorMinusZ")) );
			
	bool scint_t_hit = false;
	bool scint_b_hit = false;
	if( upper_scintillator_digitizer -> IsTriggered() ) { scint_t_hit = true; }
	if( lower_scintillator_digitizer -> IsTriggered() ) { scint_b_hit = true; }
	
	// check if naiive hit..  
	//	naive_hit_t, naive_hit_b.
	// get naive_xhit_t, naive_yhit_t, naive_xhit_b, naive_yhit_b.  somehow.
	// get distance and relative orientation from decay to the detector..
	// get vdistance_t, vdistance_b.
//	K37CloudSetup *cloud  = kpga -> GetCloud();
	//
	K37CloudSetup * cloud = ( (K37ActionInitialization*)(G4MTRunManager::GetMasterRunManager()->GetUserActionInitialization()) )->GetCloud();
//	G4ThreeVector pos = cloud -> GetFinalPosition();  // event-specific... // ok, this is the best we've got, but I don't think it's taking expansion into account...  
	G4ThreeVector pos = kpga -> GetEventPositionFromCloud(cloud);
	
	G4double dssd_to_center = detector_construction_->GetVDistance_dssd_to_center();
	// ok, so do we think it hit or what?? which way did it go?
	G4double vertical_distance_to_travel;
	G4double naive_time_to_travel;
//	G4ThreeVector naive_hitposition;
	bool naive_detectoracceptance = false;
	
	// what's the initial velocity after the decay?
	bool naive_going_up = false;  // initialize it to something, to stop the compiler warnings.  we set it to the real value immediately anyway.
	if(kpga->GetEventGenerator()->eMomentumZ() > 0)       { naive_going_up = true; }
	else if( kpga->GetEventGenerator()->eMomentumZ() < 0) { naive_going_up = false; }
	if(naive_going_up)
	{
		vertical_distance_to_travel = dssd_to_center - pos.z();
	}
	if(!naive_going_up)
	{
		vertical_distance_to_travel = pos.z() - dssd_to_center;  // negative.  
	}
	// get velocity from momentum. 
	if( kpga->GetEventGenerator()->get_e_vZ() != 0 )
	{
		naive_time_to_travel = vertical_distance_to_travel / kpga->GetEventGenerator()->get_e_vZ(); // ok, so this should always come out positive.
		naive_hitposition.set(
			( pos.x()/mm + naive_time_to_travel*kpga->GetEventGenerator()->get_e_vX() )*mm,
			( pos.y()/mm + naive_time_to_travel*kpga->GetEventGenerator()->get_e_vY() )*mm,
			( pos.z()/mm + naive_time_to_travel*kpga->GetEventGenerator()->get_e_vZ() )*mm ); 
		
		if(naive_going_up)
		{
			if( abs(naive_hitposition.x()) <= (detector_construction_->Get_dssd_width())/2.0 && abs(naive_hitposition.y()) <= (detector_construction_->Get_dssd_width())/2.0 )
			{
			//	G4cout << "going up!  naive_hitposition.x()/mm = " << naive_hitposition.x()/mm << G4endl;
				naive_detectoracceptance = true;
				naive_hit_top = true;
				naive_hit_bottom = false;
			}
			else
			{
		//		G4cout << "too far.  naive_hitposition.x()/mm = " << naive_hitposition.x()/mm << G4endl;
				naive_detectoracceptance = false;
				naive_hit_top = false;
				naive_hit_bottom = false;
			}
		}
		else if(!naive_going_up)
		{
			if( abs(naive_hitposition.x()) <= (detector_construction_->Get_dssd_width())/2.0 && abs(naive_hitposition.y()) <= (detector_construction_->Get_dssd_width())/2.0 )
			{
		//		G4cout << "Going down." << G4endl;
				naive_detectoracceptance = true;
				naive_hit_top = false;
				naive_hit_bottom = true;
			}
			else
			{
			//	G4cout << "Might have been going down, but there's no hit." << G4endl;
				naive_detectoracceptance = false;
				naive_hit_top = false;
				naive_hit_bottom = false;
			}
		}
	}
	else
	{
		// zero vertical velocity.
		naive_time_to_travel = -1.0;  
		naive_hitposition.set(-50.0, -50.0, 0);
	}
		
	//
	G4HCofThisEvent * hit_collection = evt->GetHCofThisEvent();
	K37RecoilMCPHitsCollection* recoil_mcp_hit_collection = 0;
	K37ElectronMCPHitsCollection* electron_mcp_hit_collection = 0;

	if (hit_collection) 
	{
		recoil_mcp_hit_collection =
			static_cast<K37RecoilMCPHitsCollection*> (hit_collection -> GetHC(recoil_mcp_collection_id));
		electron_mcp_hit_collection =
			static_cast<K37ElectronMCPHitsCollection*> (hit_collection -> GetHC(electron_mcp_collection_id));
	}
	G4bool electron_mcp_constructed = detector_construction_ -> GetMakeElectronMCP();
	G4bool recoil_mcp_constructed = detector_construction_ -> GetMakeRecoilMCP();

	bool emcp_hit = false;
	bool rmcp_hit = false;
	if(electron_mcp_hit_collection && electron_mcp_constructed) 
	{ 
		if (electron_mcp_hit_collection -> entries() > 0) 
			{ emcp_hit = true; }
	}
	if(recoil_mcp_hit_collection && recoil_mcp_constructed) 
	{ 
		if (recoil_mcp_hit_collection -> entries() > 0) 
			{ rmcp_hit = true; }
	}

	//
	if (scint_t_hit || scint_b_hit)
	{ // Event type A.  scint.
		event_type += 1;
	}
	// Event type B:  scint+LED.  Not implemented.
	if ( (scint_t_hit || scint_b_hit) && emcp_hit )
	{ // Event type C.  scint+emcp.
		event_type += 4;
	}
	if( is_photoionization_event && emcp_hit )
	{ // Event type D:  emcp+UV.  Not implemented?
		event_type += 8;
	}
	if( is_photoionization_event && rmcp_hit )
	{ // Event type E:  rmpc+UV.  Not implemented?
		event_type += 16;
	}
	if ( emcp_hit && rmcp_hit )
	{ // Event type F:  emcp+rmcp.
		event_type += 32;
	}
	if ( emcp_hit )
	{ // Event type G (NEW!):  emcp singles.
		event_type += 64;
	}
	if ( rmcp_hit )
	{ // Event type H (NEW!):  rmcp singles.
		event_type += 128;
	}
	if ( (scint_t_hit || scint_b_hit) && rmcp_hit )
	{ // Event type I (NEW!):  rmcp + scint.
		event_type += 256;
	}
	if ( (scint_t_hit || scint_b_hit) && rmcp_hit && emcp_hit )
	{ // Event type J (NEW!):  rmcp + emcp + scint.  triple coincidence.
		event_type += 512;
	}
	if( naive_detectoracceptance )
	{
		event_type += 1024;
	}
	//
	return event_type;
}

