// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 2013

#include "K37RunMessenger.hh"
#include "K37RunAction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"

#include "G4ios.hh"
#include "globals.hh"

//extern G4bool fillEvGenData;

//---------------------------------

K37RunMessenger::K37RunMessenger(K37RunAction* ra, GlobalAggregator * ga): 
	runAction(ra), 
	the_aggregator(ga),
	printEnergyLossTable(0),
	RunDirectory(0)
{
	// Make sure we're running this on the *master* run action, but ***don't*** use runAction->IsMaster() !!  
	// For some reason, it always returns true when called from in here, although other times it works fine.
	// I don't know why.  
	// Instead, use G4Threading::IsWorkerThread(), or G4Threading::G4GetThreadId().  
	
	if( G4Threading::G4GetThreadId()<0 ) // master thread has ID of -1.  Others are 0 or positive.
	{
	//	G4cout << "The K37RunMessenger is successfully created." << G4endl;
	//	G4cout << "G4Threading::IsWorkerThread() = " << G4Threading::IsWorkerThread() << G4endl;
	//	G4cout << "G4Threading::G4GetThreadId()  = " << G4Threading::G4GetThreadId() << G4endl;
	}
	else
	{
		G4cout << "*** Tried to create a K37RunMessenger on a worker K37RunAction.  Fail." << G4endl;
	//	G4cout << "G4Threading::IsWorkerThread() = " << G4Threading::IsWorkerThread() << G4endl;
	//	G4cout << "G4Threading::G4GetThreadId()  = " << G4Threading::G4GetThreadId() << G4endl;
		return;
	}
	
	RunDirectory = new G4UIdirectory("/K37/RunControls/");
	RunDirectory->SetGuidance("K37 Run Controls ");

	set_event_acceptance_types_ = new G4UIcmdWithAString("/K37/RunControls/SetEventTypes", this);
	set_event_acceptance_types_ -> SetParameterName("EventTypeString", true);
	set_event_acceptance_types_ -> SetDefaultValue("CIA");  //
	set_event_acceptance_types_ -> AvailableForStates(G4State_PreInit, G4State_Idle);

	set_event_acceptance_types_ -> SetGuidance("Allowed event types:  ABCDEFGHIJ (...)");
	set_event_acceptance_types_ -> SetGuidance("Event types are set to match the codes used in acquisition.");
	set_event_acceptance_types_ -> SetGuidance(" A = 1:    (scint_t || scint_b)");
	set_event_acceptance_types_ -> SetGuidance(" B = 2:     [not implemented] -- (scint && LED)");
	set_event_acceptance_types_ -> SetGuidance(" C = 4:    (eMCP && (scint_t || scint_b))");
	set_event_acceptance_types_ -> SetGuidance(" D = 8:     [not implemented] -- (eMCP && UV)");
	set_event_acceptance_types_ -> SetGuidance(" E = 16:    [not implemented] -- (rMCP && UV)");
	set_event_acceptance_types_ -> SetGuidance(" F = 32:   (eMCP && rMCP)");
	set_event_acceptance_types_ -> SetGuidance(" G = 64:   (eMCP)");
	set_event_acceptance_types_ -> SetGuidance(" H = 128:  (rMCP)");
	set_event_acceptance_types_ -> SetGuidance(" I = 256:  (rMCP && (scint_t || scint_b))");
	set_event_acceptance_types_ -> SetGuidance(" J = 512:  (rMCP && (scint_t || scint_b) && eMCP)");
	set_event_acceptance_types_ -> SetGuidance(" K = 1024: (naive hit to either beta detector)\n");
	
	get_event_acceptance_types_ =  new G4UIcmdWithABool("/K37/RunControls/GetEventTypes", this);
	get_event_acceptance_types_ -> SetGuidance("Printing accepted event types...\n");
	get_event_acceptance_types_ -> SetGuidance("Allowed event types:  ABCDEFGHIJ (...)");
	get_event_acceptance_types_ -> SetGuidance("Event types are set to match the codes used in acquisition.");
	get_event_acceptance_types_ -> SetGuidance(" A = 1:    (scint_t || scint_b)");
	get_event_acceptance_types_ -> SetGuidance(" B = 2:     [not implemented] -- (scint && LED)");
	get_event_acceptance_types_ -> SetGuidance(" C = 4:    (eMCP && (scint_t || scint_b))");
	get_event_acceptance_types_ -> SetGuidance(" D = 8:     [not implemented] -- (eMCP && UV)");
	get_event_acceptance_types_ -> SetGuidance(" E = 16:    [not implemented] -- (rMCP && UV)");
	get_event_acceptance_types_ -> SetGuidance(" F = 32:   (eMCP && rMCP)");
	get_event_acceptance_types_ -> SetGuidance(" G = 64:   (eMCP)");
	get_event_acceptance_types_ -> SetGuidance(" H = 128:  (rMCP)");
	get_event_acceptance_types_ -> SetGuidance(" I = 256:  (rMCP && (scint_t || scint_b))");
	get_event_acceptance_types_ -> SetGuidance(" J = 512:  (rMCP && (scint_t || scint_b) && eMCP)");
	get_event_acceptance_types_ -> SetGuidance(" K = 1024: (naive hit to either beta detector)\n");
	get_event_acceptance_types_ -> SetParameterName("dummy", true);
	
	//
	printEnergyLossTable = new G4UIcmdWithAString("/K37/RunControls/printEnergyLossTable", this);
	printEnergyLossTable -> SetGuidance("Print the energy Loss Table for e+ in Materials");
	printEnergyLossTable -> SetGuidance("Material choices listed by /K37/geometry/printMaterialList");
	printEnergyLossTable -> SetParameterName("material", true);
	printEnergyLossTable -> SetDefaultValue("SiliconCarbide");
	printEnergyLossTable -> AvailableForStates(G4State_Idle);
	
	setFillAllSDData = new G4UIcmdWithoutParameter("/K37/RunControls/fillAllSDData", this);
	setFillAllSDData -> SetGuidance("This command is obsolete.  Use /K37/geometry/fillAllSDData instead.");
	setFillAllSDData -> AvailableForStates(G4State_Idle);
	
	// setFileName sets the namestub.  
	setFileName = new G4UIcmdWithAString("/K37/RunControls/setFilename", this);  // probably test if this still works?
	setFileName -> SetGuidance("Enter output filename without extension");
	setFileName -> SetParameterName("filename", true);
	setFileName -> SetDefaultValue("outputfile");
	setFileName -> AvailableForStates(G4State_Idle);

	set_configuration_file_ = new G4UIcmdWithAString("/K37/RunControls/setConfigurationFile", this);  // test if this still works?
	set_configuration_file_ -> SetGuidance("Enter complete configuration file");
	set_configuration_file_ -> SetParameterName("configfilename", true);
	set_configuration_file_ -> SetDefaultValue("IOconfiguration.mac");
	set_configuration_file_ -> AvailableForStates(G4State_Idle);

	set_output_directory_ = new G4UIcmdWithAString("/K37/RunControls/setOutputDirectory", this);  // test if this still works?
	set_output_directory_ -> SetGuidance("Enter new output directory");
	set_output_directory_ -> SetParameterName("path", true);
	set_output_directory_ -> SetDefaultValue("Output/");
	set_output_directory_ -> AvailableForStates(G4State_Idle);

	set_default_cut_cmd_ = new G4UIcmdWithADoubleAndUnit("/K37/RunControls/setDefaultCut", this);  // Obsolete.
	set_default_cut_cmd_ -> SetGuidance("Set secondary production threshold");
	set_default_cut_cmd_ -> SetParameterName("Threshold", false);
	set_default_cut_cmd_ -> SetUnitCategory("Length");
	set_default_cut_cmd_ -> AvailableForStates(G4State_PreInit);

	auto_increment_cmd_ = new G4UIcmdWithABool("/K37/RunControls/setAutoIncrement", this);
	auto_increment_cmd_ -> SetGuidance("Toggle on/off to automatically increment run name");
	auto_increment_cmd_ -> SetParameterName("auto", true);
	auto_increment_cmd_ -> SetDefaultValue(true);
	auto_increment_cmd_ -> AvailableForStates(G4State_Idle);
	
	print_metadata_header = new G4UIcmdWithoutParameter("/K37/RunControls/PrintMetaDataHeader", this);
	print_metadata_header -> SetGuidance("Print the header to the metadata save file.");
	print_metadata_header -> AvailableForStates(G4State_Idle);
	
	setsave_metadata = new G4UIcmdWithABool("/K37/RunControls/SetSaveMetaData", this);
	setsave_metadata -> SetGuidance("Save the run-by-run metadata to a separate text file?");
	setsave_metadata -> SetParameterName("save_meta", true);
	setsave_metadata -> SetDefaultValue(true);
	setsave_metadata -> AvailableForStates(G4State_Idle);

	set_metadatafile = new G4UIcmdWithAString("/K37/RunControls/SetMetaDataFile", this);
	set_metadatafile -> SetGuidance("Set the filename to which run-by-run metadata will be saved.");
	set_metadatafile -> SetParameterName("meta_fname", true);
	set_metadatafile -> SetDefaultValue("/Output/MetaData.txt");
	set_metadatafile -> AvailableForStates(G4State_Idle);
	
}

//---------------------------------

K37RunMessenger::~K37RunMessenger() 
{
//	delete * runAction;    // ???  Do I *not* want to delete this?? - MJA  ... no, no I don't.
	delete printEnergyLossTable;
	delete setFillAllSDData;  // obsolete.
	delete RunDirectory;
	
	delete setFileName;
	delete set_configuration_file_;
	delete set_output_directory_;
	delete set_default_cut_cmd_;
	delete auto_increment_cmd_;
	delete print_metadata_header;
	delete setsave_metadata;
	delete set_metadatafile;
	
	delete set_event_acceptance_types_;
	delete get_event_acceptance_types_;
}

void K37RunMessenger::SetNewValue(G4UIcommand* command, G4String newValues) 
{
	if (command == printEnergyLossTable) 
	{
		runAction->PrintEnergyLossTable(newValues);
	}
	if (command == setFillAllSDData) // this one is migrating to K37StripDetectorMessenger.
	{
		G4cout << "This command is obsolete.  Don't use it." << G4endl;
		G4cout << "Instead, use:  /K37/geometry/fillAllSDData " << G4endl;
		int j;
		G4cout << "Enter any number to acknowledge " << G4endl;
		G4cin >> j;
	}
	
	if (command == setFileName) 
	{
		the_aggregator -> setFileName(newValues);  // it sets the namestub.
	}
	if (command == set_configuration_file_) 
	{
		the_aggregator -> SetConfigurationFileName(newValues.c_str());  // do I actually still need to do something to update the config file??
	//	the_aggregator -> UpdateConfigFile();
	}
	if (command == set_output_directory_) 
	{
		the_aggregator -> SetOutputDirectory(newValues.c_str());  // do I actually still need to do something to update the config file??
	//	the_aggregator -> UpdateConfigFile(); // this might increment a number when it shouldn't?  I should check.
	}
	if (command == set_default_cut_cmd_) 
	{
		G4cout << "WARNING: " << G4endl;
		G4cout << " \"/K37/RunControls/setDefaultCut\" is redundant and has been removed." << G4endl;
		G4cout << "Instead use the command:" << G4endl;
		G4cout << " *** /run/setCut {cut} {unit} *** " << G4endl;
		int j;
		G4cout << "Enter any number to acknowledge " << G4endl;
		G4cin >> j;
	}
	if (command == auto_increment_cmd_) 
	{
		the_aggregator -> SetAutoIncrement(auto_increment_cmd_ -> GetNewBoolValue(newValues));
	}
	if (command == print_metadata_header)
	{
		the_aggregator -> PrintMetaDataHeader();
	}
	if (command == setsave_metadata)
	{
		the_aggregator -> Set_SaveMetaData( setsave_metadata -> GetNewBoolValue(newValues) );
	}
	if (command == set_metadatafile)
	{
	//	runAction -> Set_MetaDataFilename( set_metadatafile -> GetNewStringValue(newValues) );
		the_aggregator -> Set_MetaDataFilename( newValues.c_str() );
	}
	//
	if (command == set_event_acceptance_types_)
	{
		runAction -> SetEventTypes(newValues);
	}
	if (command == get_event_acceptance_types_)
	{
		G4cout << "Allowed event types:  ABCDEFGHIJ (...)" << G4endl;
		G4cout << "Event types are set to match the codes used in acquisition." << G4endl;
		G4cout << " A = 1:    (scint_t || scint_b)" << G4endl;
		G4cout << " B = 2:     [not implemented] -- (scint && LED)" << G4endl;
		G4cout << " C = 4:    (eMCP && (scint_t || scint_b))" << G4endl;
		G4cout << " D = 8:     [not implemented] -- (eMCP && UV)" << G4endl;
		G4cout << " E = 16:    [not implemented] -- (rMCP && UV)" << G4endl;
		G4cout << " F = 32:   (eMCP && rMCP)" << G4endl;
		G4cout << " G = 64:   (eMCP)" << G4endl;
		G4cout << " H = 128:  (rMCP)" << G4endl;
		G4cout << " I = 256:  (rMCP && (scint_t || scint_b))" << G4endl;
		G4cout << " J = 512:  (rMCP && (scint_t || scint_b) && eMCP)" << G4endl;
		G4cout << " K = 1024: (naive hit to either beta detector)\n" << G4endl;
	//	G4cout << G4endl;
		runAction -> GetEventTypes();
	}

}

//---------------------------------
