// Authors: Spencer Behling 2014
//          Ben Fenker ?
//          Melissa Anholm 2017

#include "IsotopeMessenger.hh"

// ----------------------------------

IsotopeMessenger::IsotopeMessenger(/*ISO::*/Holstein51_Isotope *iso_)
:isotope(iso_)
{
	isotope_dir = new G4UIdirectory("/K37/isotope/");
	isotope_dir->SetGuidance("Control Isotope Properties That Affect The Decay");

	set_rho = new G4UIcmdWithADouble("/K37/isotope/SetRho", this);
	set_rho -> SetGuidance("Change the Rho Value for the Decay.");
	set_rho -> SetParameterName("rho", true);
	set_rho -> SetDefaultValue(0.576468);

	get_rho = new G4UIcmdWithoutParameter("/K37/isotope/GetRho", this);
	get_rho -> SetGuidance("Print out the value of rho.");

	set_axial_vector_form_factor =
		new G4UIcmdWithADouble("/K37/isotope/SetAxialVectorFormFactor", this);
	set_axial_vector_form_factor -> SetGuidance("Set g_A");
	set_axial_vector_form_factor -> SetParameterName("g_A", false);
}

// ----------------------------------

IsotopeMessenger::~IsotopeMessenger()
{
	delete isotope_dir;
	delete set_rho;
	delete get_rho;
	delete set_axial_vector_form_factor;
}

// ----------------------------------

void IsotopeMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
	if (command == set_rho)
	{
		isotope -> ChangeRho(set_rho->GetNewDoubleValue(newValue));
	}
	else if (command == get_rho)
	{
		G4cout << "RHO = " << isotope -> FindValue("RHO") << G4endl;
	}
	else if (command == set_axial_vector_form_factor) 
	{
		isotope -> SetAxialFormFactor_gA(set_axial_vector_form_factor ->
			GetNewDoubleValue(newValue));
	}
}

// ----------------------------------

