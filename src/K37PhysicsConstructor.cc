// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013


#include "G4EmPenelopePhysics.hh"
#include "G4ParticleDefinition.hh"

// *** Processes and models

// gamma
#include "G4PhotoElectricEffect.hh"
#include "G4PenelopePhotoElectricModel.hh"

#include "G4ComptonScattering.hh"
#include "G4PenelopeComptonModel.hh"

#include "G4GammaConversion.hh"
#include "G4PenelopeGammaConversionModel.hh"

#include "G4RayleighScattering.hh" 
#include "G4PenelopeRayleighModel.hh"

// e- and e+
#include "G4eMultipleScattering.hh"
#include "G4UniversalFluctuation.hh"

#include "G4eIonisation.hh"
#include "G4PenelopeIonisationModel.hh"

#include "G4eBremsstrahlung.hh"
#include "G4PenelopeBremsstrahlungModel.hh"

// e+ only
#include "G4eplusAnnihilation.hh"
#include "G4PenelopeAnnihilationModel.hh"

// mu
#include "G4MuMultipleScattering.hh"
#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

#include "G4MuBremsstrahlungModel.hh"
#include "G4MuPairProductionModel.hh"
#include "G4hBremsstrahlungModel.hh"
#include "G4hPairProductionModel.hh"

// hadrons
#include "G4hMultipleScattering.hh"
#include "G4MscStepLimitType.hh"

#include "G4hBremsstrahlung.hh"
#include "G4hPairProduction.hh"

#include "G4hIonisation.hh"
#include "G4ionIonisation.hh"
#include "G4alphaIonisation.hh"
#include "G4IonParametrisedLossModel.hh"
#include "G4NuclearStopping.hh"

// msc models
//#include "G4UrbanMscModel93.hh"  // can't find this in 4.10.5.  
//#include "G4UrbanMscModel95.hh"  // can't find this in 4.10.5
#include "G4UrbanMscModel.hh"      // new filename in 4.10.5 ?
#include "G4GoudsmitSaundersonMscModel.hh"
#include "G4WentzelVIModel.hh"
#include "G4CoulombScattering.hh"

//
#include "G4LossTableManager.hh"
#include "G4VAtomDeexcitation.hh"
#include "G4UAtomicDeexcitation.hh"
#include "G4EmProcessOptions.hh"
 
// particles
#include "G4Gamma.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4MuonPlus.hh"
#include "G4MuonMinus.hh"
#include "G4PionPlus.hh"
#include "G4PionMinus.hh"
#include "G4KaonPlus.hh"
#include "G4KaonMinus.hh"
#include "G4Proton.hh"
#include "G4AntiProton.hh"
#include "G4Deuteron.hh"
#include "G4Triton.hh"
#include "G4He3.hh"
#include "G4Alpha.hh"
#include "G4GenericIon.hh"
 
//
#include "G4PhysicsListHelper.hh"
#include "G4BuilderType.hh"

#include "K37PhysicsConstructor.hh"

K37PhysicsConstructor::K37PhysicsConstructor(const G4String& name, G4int ver)
  :  G4VPhysicsConstructor(name), verbose(ver) 
{ }

K37PhysicsConstructor::~K37PhysicsConstructor() 
{ }

void K37PhysicsConstructor::ConstructProcess() 
{
	G4PhysicsListHelper *ph = G4PhysicsListHelper::GetPhysicsListHelper();
	//  theParticleIterator->reset();
	//  
	//  With Geant4.10.5, theParticleIterator is no longer a member of the G4VPhysicsConstructor class.
	//  (it's K37PhysicsConstructor:public G4VPhysicsConstructor, see.)
	//  supposedly the change is to support multi-threading.  one proposed workaround is to just declare
	//  a local variable if you need a particle iterator.  So that's what I'll (attempt to) do.

	G4ParticleTable::G4PTblDicIterator * a_new_particle_iterator = theParticleTable->GetIterator();
	a_new_particle_iterator -> reset();

	//  while ((*theParticleIterator)()) 
	while ((*a_new_particle_iterator)()) 
	{
		//  G4ParticleDefinition* particle = theParticleIterator->value();
		G4ParticleDefinition* particle = a_new_particle_iterator->value();
		//    G4ProcessManager* pmanager = particle->GetProcessManager();
		G4String particleName = particle->GetParticleName();
		G4double PenelopeHighEnergyLimit = 1.0*GeV;
		if (particleName == "gamma") 
		{
			// gamma
			//       pmanager->AddDiscreteProcess(new G4PhotoElectricEffect);
			//       pmanager->AddDiscreteProcess(new G4ComptonScattering);
			//       pmanager->AddDiscreteProcess(new G4GammaConversion);

			// Photo-electric effect
			G4PhotoElectricEffect *thePhotoElectricEffect = new G4PhotoElectricEffect();
			G4PenelopePhotoElectricModel *thePEPenelopeModel = new G4PenelopePhotoElectricModel();
			thePEPenelopeModel -> SetHighEnergyLimit(PenelopeHighEnergyLimit);
			thePhotoElectricEffect -> AddEmModel(0, thePEPenelopeModel);
			ph -> RegisterProcess(thePhotoElectricEffect, particle);

			// Compton scattering
			G4ComptonScattering *theComptonScattering = new G4ComptonScattering();
			G4PenelopeComptonModel *theComptonPenelopeModel = new G4PenelopeComptonModel();
			theComptonPenelopeModel -> SetHighEnergyLimit(PenelopeHighEnergyLimit);
			theComptonScattering -> AddEmModel(0, theComptonPenelopeModel);
			ph -> RegisterProcess(theComptonScattering, particle);

			// Gamma conversion
			G4GammaConversion *theGammaConversion = new G4GammaConversion();
			G4PenelopeGammaConversionModel *theGCPenelopeModel = new G4PenelopeGammaConversionModel();
			theGammaConversion -> AddEmModel(0, theGCPenelopeModel);
			ph -> RegisterProcess(theGammaConversion, particle);

			// Rayleigh scattering
			G4RayleighScattering *theRayleigh = new G4RayleighScattering();
			G4PenelopeRayleighModel *theRayleighPenelopeModel = new G4PenelopeRayleighModel();
			theRayleighPenelopeModel -> SetHighEnergyLimit(PenelopeHighEnergyLimit);
			theRayleigh -> AddEmModel(0, theRayleighPenelopeModel);
			ph -> RegisterProcess(theRayleigh, particle);
		} 
		else if (particleName == "e-") 
		{
			// electron
			//       pmanager->AddProcess(new G4eMultipleScattering, -1, 1, 1);
			//       pmanager->AddProcess(new G4eIonisation,         -1, 2, 2);
			//       pmanager->AddProcess(new G4eBremsstrahlung,     -1, 3, 3);
			
			// Multiple (Coulomb) scattering
			G4eMultipleScattering *msc = new G4eMultipleScattering();
			msc -> AddEmModel(0, new G4GoudsmitSaundersonMscModel());
			msc -> SetStepLimitType(fUseDistanceToBoundary); // ???
			ph -> RegisterProcess(msc, particle);

			// Ionisation
			G4eIonisation *eIoni = new G4eIonisation();
			G4PenelopeIonisationModel *theIoniPenelope = new G4PenelopeIonisationModel();
			theIoniPenelope -> SetHighEnergyLimit(PenelopeHighEnergyLimit);
			eIoni -> AddEmModel(0, theIoniPenelope, new G4UniversalFluctuation());
			eIoni -> SetStepFunction(0.2, 100*um);
			ph -> RegisterProcess(eIoni, particle);

			// Bremsstrahlung
			G4eBremsstrahlung *eBrem = new G4eBremsstrahlung();
			G4PenelopeBremsstrahlungModel *theBremPenelope = new G4PenelopeBremsstrahlungModel();
			theBremPenelope -> SetHighEnergyLimit(PenelopeHighEnergyLimit);
			eBrem -> AddEmModel(0, theBremPenelope);
			ph -> RegisterProcess(eBrem, particle);
		} 
		else if (particleName == "e+") 
		{
			// positron
			//       pmanager->AddProcess(new G4eMultipleScattering, -1, 1, 1);
			//       pmanager->AddProcess(new G4eIonisation,         -1, 2, 2);
			//       pmanager->AddProcess(new G4eBremsstrahlung,     -1, 3, 3);
			//       pmanager->AddProcess(new G4eplusAnnihilation,    0, -1, 4);
			// Multiple scattering
			G4eMultipleScattering *msc = new G4eMultipleScattering();
			msc -> AddEmModel(0, new G4GoudsmitSaundersonMscModel());
			msc -> SetStepLimitType(fUseDistanceToBoundary);
			ph -> RegisterProcess(msc, particle);

			// Ionisation
			G4eIonisation *eIoni = new G4eIonisation();
			G4PenelopeIonisationModel *theIoniPenelope = new G4PenelopeIonisationModel();
			theIoniPenelope -> SetHighEnergyLimit(PenelopeHighEnergyLimit);
			eIoni -> AddEmModel(0, theIoniPenelope, new G4UniversalFluctuation());
			eIoni -> SetStepFunction(0.2, 100*um);
			ph -> RegisterProcess(eIoni, particle);

			// Bremsstrahlung
			G4eBremsstrahlung *eBrem = new G4eBremsstrahlung();
			G4PenelopeBremsstrahlungModel *theBremPenelope = new G4PenelopeBremsstrahlungModel();
			theBremPenelope -> SetHighEnergyLimit(PenelopeHighEnergyLimit);
			eBrem -> AddEmModel(0, theBremPenelope);
			ph -> RegisterProcess(eBrem, particle);

			// Annihilation
			G4eplusAnnihilation *eAnni = new G4eplusAnnihilation();
			G4PenelopeAnnihilationModel *theAnnPenelope = new G4PenelopeAnnihilationModel();
			theAnnPenelope -> SetHighEnergyLimit(PenelopeHighEnergyLimit);
			eAnni -> AddEmModel(0, theAnnPenelope);
			ph -> RegisterProcess(eAnni, particle);
		} 
		else if (particleName == "mu+" || particleName == "mu-" ) 
		{
			// Identical to G4EmStandardPhysics_option3
			G4MuMultipleScattering* msc = new G4MuMultipleScattering();
			msc->AddEmModel(0, new G4WentzelVIModel());

			G4MuIonisation* muIoni = new G4MuIonisation();
			muIoni->SetStepFunction(0.2, 50*um);          

			G4MuBremsstrahlung* mub = new G4MuBremsstrahlung();
			G4MuPairProduction* mup = new G4MuPairProduction();

			ph->RegisterProcess(msc, particle);
			ph->RegisterProcess(muIoni, particle);
			ph->RegisterProcess(mub, particle);
			ph->RegisterProcess(mup, particle);
			ph->RegisterProcess(new G4CoulombScattering(), particle);
		} 
		else if (particleName == "alpha" || particleName == "He3") 
		{
			// Identical to G4EmStandardPhysics_option3
			G4ionIonisation* ionIoni = new G4ionIonisation();
			ionIoni->SetStepFunction(0.1, 10*um);

			ph->RegisterProcess(new G4hMultipleScattering(), particle);
			ph->RegisterProcess(ionIoni, particle);
			ph->RegisterProcess(new G4NuclearStopping(), particle);
		} 
		else if (particleName == "GenericIon"  ||
				 particleName == "Ar37Neutral" ||
				 particleName == "Ar37PlusOne" ||
				 particleName == "Ar37Minus") 
		{
			// OBJECT may be dynamically created as either a GenericIon or nucleus
			// G4Nucleus exists and therefore has particle type nucleus
			// genericIon:
			// multiple scattering

			// G4cout << "Does this ever even happen?!?" << G4endl;
			// It does happen, but only at program start-up.

			G4ionIonisation* ionIoni = new G4ionIonisation();
			ionIoni->SetEmModel(new G4IonParametrisedLossModel());
			ionIoni->SetStepFunction(0.1, 1*um);

			ph -> RegisterProcess(new G4hMultipleScattering(), particle);
			ph -> RegisterProcess(ionIoni, particle);
			ph -> RegisterProcess(new G4NuclearStopping(), particle);
		} 
		else if(particleName == "K37[0.0]" || particleName == "Ar37[0.0]")
		{
			G4cout << "* " <<  particleName << G4endl;
			// This *literally* never happens.  why?
		}
		else if (particleName == "pi+" ||
		         particleName == "pi-" ||
		         particleName == "kaon+" ||
		         particleName == "kaon-" ||
		         particleName == "proton") 
		{
			G4hIonisation* hIoni = new G4hIonisation();
			hIoni->SetStepFunction(0.2, 50*um);

			G4hBremsstrahlung* pib = new G4hBremsstrahlung();
			G4hPairProduction* pip = new G4hPairProduction();

			ph->RegisterProcess(new G4hMultipleScattering(), particle);
			ph->RegisterProcess(hIoni, particle);
			ph->RegisterProcess(pib, particle);
			ph->RegisterProcess(pip, particle);      
		}
		else if (particleName == "B+" ||
		         particleName == "B-" ||
		         particleName == "D+" ||
		         particleName == "D-" ||
		         particleName == "Ds+" ||
		         particleName == "Ds-" ||
		         particleName == "anti_He3" ||
		         particleName == "anti_alpha" ||
		         particleName == "anti_deuteron" ||
		         particleName == "anti_lambda_c+" ||
		         particleName == "anti_omega-" ||
		         particleName == "anti_proton" ||
		         particleName == "anti_sigma_c+" ||
		         particleName == "anti_sigma_c++" ||
		         particleName == "anti_sigma+" ||
		         particleName == "anti_sigma-" ||
		         particleName == "anti_triton" ||
		         particleName == "anti_xi_c+" ||
		         particleName == "anti_xi-" ||
		         particleName == "deuteron" ||
		         particleName == "lambda_c+" ||
		         particleName == "omega-" ||
		         particleName == "sigma_c+" ||
		         particleName == "sigma_c++" ||
		         particleName == "sigma+" ||
		         particleName == "sigma-" ||
		         particleName == "tau+" ||
		         particleName == "tau-" ||
		         particleName == "triton" ||
		         particleName == "xi_c+" ||
		         particleName == "xi-") 
		{
			ph -> RegisterProcess(new G4hMultipleScattering(), particle);
			ph -> RegisterProcess(new G4hIonisation(), particle);
		}
	}
	//
	G4Decay *theDecayProcess = new G4Decay();
	//  theParticleIterator->reset();
	//  while ((*theParticleIterator)()) 
	a_new_particle_iterator->reset();
	
	while ((*a_new_particle_iterator)()) 
	{
		//    G4ParticleDefinition* particle = theParticleIterator->value();
		G4ParticleDefinition* particle = a_new_particle_iterator->value();
		G4ProcessManager* pmanager = particle->GetProcessManager();
		if (theDecayProcess->IsApplicable(*particle)) 
		{
			// G4cout << particle -> GetParticleName() << G4endl;
			// G4DecayTable *table = particle -> GetDecayTable();
			// if (table) table -> DumpInfo();
			pmanager ->AddProcess(theDecayProcess);
			// set ordering for PostStepDoIt and AtRestDoIt
			pmanager ->SetProcessOrdering(theDecayProcess, idxPostStep);
			pmanager ->SetProcessOrdering(theDecayProcess, idxAtRest);
		}
	}
	G4EmProcessOptions opt;
	opt.SetVerbose(verbose);
	// Physics Tables
	opt.SetMinEnergy(100*eV);
	opt.SetMaxEnergy(10*TeV);
	opt.SetDEDXBinning(220);
	opt.SetLambdaBinning(220);
	opt.SetPolarAngleLimit(CLHEP::pi);

	G4VAtomDeexcitation *deexcitation = new G4UAtomicDeexcitation();
	G4LossTableManager::Instance() -> SetAtomDeexcitation(deexcitation);
	deexcitation -> SetFluo(true);
}
