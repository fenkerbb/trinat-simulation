// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013

// C / C++
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <cassert>

// ROOT headers
//#include <TTree.h>
#include <TDirectory.h>

// G4:
#include "G4Threading.hh"
#include "G4DigiManager.hh"
#include "globals.hh"
#include "G4Types.hh"

// Project Specific
#include "K37_LocalAggregator.hh"
#include "K37RunManagerMT.hh"
#include "K37PrimaryGeneratorAction.hh"
#include "K37EventGenerator.hh"
#include "K37EventAction.hh"
#include "K37ActionInitialization.hh"
#include "K37RunAction.hh"
#include "K37ScintillatorDigitizer.hh"  // 
#include "K37CloudSetup.hh" // 

using std::abs;
//using std::cerr;
using std::cout;
using std::endl;
//using std::left;
//using std::setw;
//using std::sqrt;
using std::ifstream;

// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 
LocalAggregator::LocalAggregator() : 
	rootFile(0),
	theNTuple(0)
{ 
	G4cout << "Creating a new LocalAggregator::LocalAggregator()." << G4endl;
//	G4cout << "About to clear the channels... " << G4endl;	
	active_channels_ = new map<string, K37_Data*>;
	active_channels_->clear();
	channelset = new set_of_channels();  // do I even want this to happen if this is on the master thread?
}
LocalAggregator::~LocalAggregator() 
{
	G4cout << "Deleting the LocalAggregator." << G4endl;
	
	if(channelset) delete channelset;
	active_channels_->clear();
	
	if(rootFile)         delete rootFile;
	if(theNTuple)        delete theNTuple;
	if(active_channels_) delete active_channels_;
}

// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 
void LocalAggregator::SetFileName()  // sets full_local_outputfilename. no longer returns it.
{
	GlobalAggregator * the_global_aggregator = K37RunManagerMT::GetMasterRunManager()->GetUserActionInitialization()->GetAggregator();
	full_local_outputfilename = the_global_aggregator->GetGlobalOutputPath() + the_global_aggregator->GetGlobalNameStub() + G4String("_tmp") + G4String(patch::to_string(G4Threading::G4GetThreadId())) + G4String(".root");
	
//	return full_local_outputfilename;
	return;
}
G4String LocalAggregator::GetFileName()  // returns full_local_outputfilename.
{
	return full_local_outputfilename;
}


/*
void Aggregator::InitilzeOutput() 
{
//	if (textFileOpen == true) 
//		{ PrintHeader(outputFile); }
//	if (writeToScreen == true) 
//		{ PrintHeader(std::cout); }
}
*/

void LocalAggregator::ClearData() 
{
	for (it_data = active_channels_->begin(); it_data != active_channels_->end(); ++it_data) 
	{
		((*it_data).second)->Clear();
	}
}
/*
void LocalAggregator::RegisterData(K37_ABC::K37_Data *dataPointer_) 
{
	this->active_channels_->insert(std::make_pair(dataPointer_->GetName(), dataPointer_));
}
*/
void LocalAggregator::EndEvent(const bool &recordEvent_)
{
//	G4cout << "LocalAggregator:  recordEvent_ = " << recordEvent_ << G4endl;
	if(recordEvent_) 
	{ 
	//	if(theNTuple) { G4cout << "theNTuple exists!" << G4endl; }
	//	else { G4cout << "theNTuple doesn't exist.  :(" << G4endl; }
		
		// sanity check;  probably slows it down.
		if(!theNTuple) { G4cout << "theNTuple is broken!  :(" << G4endl; return; }
		
		theNTuple->Fill();
		ClearData();
	}
	else             
	{ 
		this->ClearData(); 
	}
}

void LocalAggregator::RegisterIOMethod( ) // root file name outputFileName
{
	// RegisterIOMethod(...) used to call Aggregator::ImplementAGG::InitializeIO(...), 
	// and later on it was just Aggregator::InitializeIO(...), however I've just imported 
	// the complete contents of that function to this one here.
	G4cout << "Called LocalAggregator::RegisterIOMethod()" << G4endl;
	SetFileName();
	
	
	// you have to use a pointer to a TFile and then use new to create
	// an instance. Using any other design pattern screws up
	// roots "supposedly easy" automatic association of object with
	// files.
	if (rootFile != 0) 
	{
		G4cout << "rootFile exists, and we're preemptively deleting it." << G4endl;
		rootFile -> cd();
		rootFile -> Close();
		delete rootFile;
		rootFile = 0;
		theNTuple = 0;
	}
//	else
//	{
//		G4cout << "no rootFile, so I guess it's cool." << G4endl;
//	}
	if (theNTuple != 0) 
	{
		G4cout << "theNTuple exists, and we're preemptively deleting it." << G4endl;
		delete theNTuple;
		theNTuple = 0;
	}
//	else
//	{
//		G4cout << "no theNTuple, so I guess it's cool." << G4endl;
//	}
	//
	/*
	std::string variableNames;
	
	if( active_channels_ )
	{
		G4cout << "active_channels_ exists!" << G4endl;
	}
	else
	{
		G4cout << "active_channels_ does not exist." << G4endl;
	}
	
	for (it_data = active_channels_->begin(); it_data != active_channels_->end(); ++it_data) 
	{
		if ( it_data == active_channels_->begin() ) 
		{
		//	G4cout << "it_data == active_channels_->begin() is true." << G4endl;
			variableNames = (*it_data).first;
		//	G4cout << "variableNames = " << variableNames << G4endl;
		} 
		else 
		{
		//	G4cout << "it_data == active_channels_->begin() is false." << G4endl;
			variableNames += (":"+ (*it_data).first);
		//	G4cout << "variableNames = " << variableNames << G4endl;
		}
	}
	G4cout << "I guess we've looked through the active_channels_ now.." << G4endl;
	*/
	
//	string tttitle = string("TheTuple_")+patch::to_string(G4Threading::G4GetThreadId());
//	string ttname = string("ntuple_")+patch::to_string(G4Threading::G4GetThreadId());
	string tttitle = string("TheTuple");
	string ttname = string("ntuple");
	
	G4cout << "Local TFile name (we're about to create this):  " << full_local_outputfilename << G4endl;
//	rootFile = new TFile(fileName_.c_str(), "RECREATE");//, "", 0);
	rootFile = new TFile(full_local_outputfilename.c_str(), "RECREATE", "", 0);  // rootFile seems fine?
	
	G4cout << "Creating the new ntuple." << G4endl;
	theNTuple = new TTree(ttname.c_str(), tttitle.c_str());
	
	if( !theNTuple ) { G4cout << "We have no theNTuple!" << G4endl; }
	
	std::string columnName;
	std::string typeBit;
	std::string typeName;
	
//	G4cout << "Going through the channels and shit now." << G4endl;
//	int i=0;
	for (it_data = active_channels_->begin(); it_data != active_channels_->end(); ++it_data) 
	{
		columnName = (*it_data).first;
		if ((*it_data).second) 
		{
			typeBit = ((*it_data).second)->GetType();
			typeName = columnName + typeBit;
		//	G4cout << "typeName = " << typeName << G4endl;
			//
			if (typeBit == "/D") 
			{
			//	G4cout << "it's a D !" << G4endl;
			//	G4cout << "the ReferenceD is:  " << ((*it_data).second)->GetReferenceD() << G4endl;
				//
				theNTuple->Branch(columnName.c_str(), ((*it_data).second)->GetReferenceD(), typeName.c_str());
			}
			if (typeBit == "/l") // does this even work?!?
			{
				G4cout << "It's an l !  (srsly?)" << G4endl;
				G4cout << "the GetReferenceI is:  " << ((*it_data).second)->GetReferenceI() << G4endl;
				//
				theNTuple->Branch(columnName.c_str(), ((*it_data).second)->GetReferenceI(), typeName.c_str());
				//
			}
			if (typeBit == "/v") 
			{
			//	G4cout << "It's a v ! "<< G4endl;
			//	G4cout << "the GetReferenceV is:  " << ((*it_data).second)->GetReferenceV() << G4endl;
				//
				theNTuple->Branch(columnName.c_str(), "std::vector<double>", ((*it_data).second)->GetReferenceV());
			}
			if (typeBit == "/vs") 
			{
				G4cout << "It's a vs !  this totes might crash." << G4endl;
				G4cout << "the GetReferenceVS is:  " << ((*it_data).second)->GetReferenceVS() << G4endl;
				//
				theNTuple->Branch(columnName.c_str(), "std::vector<std::string>", ((*it_data).second)->GetReferenceVS() );
			}
		}
	//	G4cout << "Made it to the end of a loop!" << G4endl;
	//	i++;
	}
	//
//	G4cout << "Done with LocalAggregator::RegisterIOMethod(...)" << G4endl;
}



// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 
void LocalAggregator::RegisterChannel(K37_Data* channel)  // never used???  ... oh yes it is.
{
//	G4cout << "Registering channel:  " << channel->GetName() << ";\tlist size = " << active_channels_->size() << G4endl;
	(*active_channels_)[channel->GetName()] = channel;
	
//	this -> RegisterData(channel);
//	void LocalAggregator::RegisterData(K37_ABC::K37_Data *dataPointer_) 
//	this->active_channels_->insert(std::make_pair(dataPointer_->GetName(), dataPointer_));
	
	this->active_channels_->insert(std::make_pair(channel->GetName(), channel));
}
void LocalAggregator::RegisterChannels()
{
	G4cout << "LocalAggregator::RegisterChannels() is registering channels..." << G4endl;
	
	///////////////////////// My new stuff //////////////////
	// Create analysis manager
	// The choice of analysis technology is done via selectin of a namespace
	// in K37Analysis.hh

	// Analysis Version 3! Spencer's Aggregator---should be IDENTICAL to what we
	// get from the analyzer.
	// the_aggregator_ = new Aggregator();
	// Actually, at some point, they diverged slightly...
	
	RegisterChannel(channelset->qdc_upper_pmt);
	RegisterChannel(channelset->qdc_lower_pmt);
//	G4cout << "Some channels registered..." << G4endl;
	
	RegisterChannel(channelset->qdc_upper_pmt_ae);
	RegisterChannel(channelset->qdc_lower_pmt_ae);
	RegisterChannel(channelset->qdc_upper_pmt_p);
	RegisterChannel(channelset->qdc_lower_pmt_p);
	RegisterChannel(channelset->qdc_upper_pmt_e);
	RegisterChannel(channelset->qdc_lower_pmt_e);
	RegisterChannel(channelset->qdc_upper_pmt_g);
	RegisterChannel(channelset->qdc_lower_pmt_g);
	RegisterChannel(channelset->qdc_upper_dssd);
	RegisterChannel(channelset->qdc_lower_dssd);
	RegisterChannel(channelset->qdc_upper_dssd_ae);
	RegisterChannel(channelset->qdc_lower_dssd_ae);
	RegisterChannel(channelset->qdc_upper_dssd_p);
	RegisterChannel(channelset->qdc_lower_dssd_p);
	RegisterChannel(channelset->qdc_upper_dssd_e);
	RegisterChannel(channelset->qdc_lower_dssd_e);
	RegisterChannel(channelset->qdc_upper_dssd_g);
	RegisterChannel(channelset->qdc_lower_dssd_g);
	RegisterChannel(channelset->qdc_upper_mirror);
	RegisterChannel(channelset->qdc_lower_mirror);
	RegisterChannel(channelset->qdc_upper_BeWindow);
	RegisterChannel(channelset->qdc_lower_BeWindow);

//	G4cout << "More channels registered." << G4endl;
//	if ( detector_construction_->GetMakeRecoilMCP() ) 
	if ( K37RunManagerMT::GetMasterRunManager()->GetUserDetectorConstruction()->GetMakeRecoilMCP() ) 
	{
		RegisterChannel(channelset->dl_x_pos);
		RegisterChannel(channelset->dl_z_pos);
		RegisterChannel(channelset->tdc_ion_mcp_);
		RegisterChannel(channelset->recoil_mcp_particle_);
		RegisterChannel(channelset->num_hits_r_mcp_);  // tghis one fucks it up??
	}
	if ( (K37RunManagerMT::GetMasterRunManager()->GetUserDetectorConstruction()) -> GetRecordStripDetectorData() ) 
	{
		RegisterChannel(channelset->strip_detector_upper_x_);
		RegisterChannel(channelset->strip_detector_upper_y_);
		RegisterChannel(channelset->strip_detector_lower_x_);
		RegisterChannel(channelset->strip_detector_lower_y_);
		// RegisterChannel(strip_detector_upper_x_T);
		// RegisterChannel(strip_detector_upper_y_T);
		// RegisterChannel(strip_detector_lower_x_T);
		// RegisterChannel(strip_detector_lower_y_T);
	}
	
	RegisterChannel(channelset->tdc_scint_top_);
	RegisterChannel(channelset->tdc_scint_bottom_);
//	if (detector_construction_ -> GetMakeElectronMCP()) 
	if ( (K37RunManagerMT::GetMasterRunManager()->GetUserDetectorConstruction()) -> GetMakeElectronMCP() ) 
	{
		RegisterChannel(channelset->tdc_electron_mcp_);
		RegisterChannel(channelset->num_hits_e_mcp_);
		RegisterChannel(channelset->electron_mcp_particle_);
		RegisterChannel(channelset->hex_x_pos);
		RegisterChannel(channelset->hex_z_pos);
	}
	RegisterChannel(channelset->tdc_photo_diode_);
	RegisterChannel(channelset->electron_kinetic_energy_generated_);
	RegisterChannel(channelset->electron_mu_generated_);
	RegisterChannel(channelset->recoil_mu_generated_);
//	RegisterChannel(run_action_);
//	RegisterChannel(channelset->volumeList);  // onoz!  we disabled this!

	RegisterChannel(channelset->recoil_charge_state_);
	RegisterChannel(channelset->two_percent_branch_);
	RegisterChannel(channelset->ttlbit_op_beam_);
//	RegisterChannel(ttlbit_sigmaplus_);
	
	RegisterChannel(channelset->vnim_event_type_);  
	RegisterChannel(channelset->upper_pmt_particle_);
	RegisterChannel(channelset->lower_pmt_particle_);
	
	RegisterChannel(channelset->gen_Ebeta_);
	RegisterChannel(channelset->gen_Tbeta_);
	RegisterChannel(channelset->gen_costheta_);
	RegisterChannel(channelset->naive_hit_t_);
	RegisterChannel(channelset->gen_xhit_t_);
	RegisterChannel(channelset->gen_yhit_t_);
	RegisterChannel(channelset->gen_rhit_t_);
	RegisterChannel(channelset->naive_hit_b_);
	RegisterChannel(channelset->gen_xhit_b_); 
	RegisterChannel(channelset->gen_yhit_b_); 
	RegisterChannel(channelset->gen_rhit_b_);

//	G4cout << "Done registering channels" << G4endl;
}

TBranch* LocalAggregator::Branch(const char *name, void *address, const char *leaflist, Int_t bufsize) 
{
	/*
	//       (const char *name, void *address, const char *leaflist, Int_t bufsize) 
	// Branch(            name,       address,             leaflist,       bufsize) );
	G4cout << "Called TBranch* LocalAggregator::Branch(...):  " << G4endl;
	G4cout << "\tname     = "  << name << G4endl;
	G4cout << "\taddress  = "  << address << G4endl;
	G4cout << "\tleaflist = " << leaflist << G4endl;
	G4cout << "\tbufsize  = " << bufsize << G4endl;
	//
	*/
	return (this -> theNTuple -> Branch(name, address, leaflist, bufsize) );
}
void LocalAggregator::RegisterExcessBranches()
{
//	G4cout << "Called LocalAggregator::RegisterExcessBranches()." << G4endl;
	//
	K37EventAction            *ea     = (K37EventAction*)G4RunManager::GetRunManager() -> GetUserEventAction();
	G4RunManager              *runMan = G4RunManager::GetRunManager();
//	K37PrimaryGeneratorAction *kpga   = (K37PrimaryGeneratorAction*)runMan -> GetUserPrimaryGeneratorAction();  // this works, but it's not needed?
	K37EventGenerator         *eGen   = (K37EventGenerator*)(runMan -> GetUserEventAction()); 


	this -> Branch("TTTL_OP_Beam", &(ea -> TTTL_OP_Beam), "TTTL_OP_Beam/i");
//	the_aggregator -> Branch("TTLBit_SigmaPlus", &(ea -> TTLBit_SigmaPlus), "TTLBit_SigmaPlus/i");

	this -> Branch("TDC_SCINT_TOP_Count", &(ea -> tdc_scint_top_count), "TDC_SCINT_TOP_Count/i");
	this -> Branch("TDC_SCINT_BOTTOM_Count", &(ea -> tdc_scint_bottom_count), "TDC_SCINT_BOTTOM_Count/i");
	this -> Branch("DECAY_POS_X", &(ea -> decay_pos_x), "DECAY_POS_X/D");
	this -> Branch("DECAY_POS_Y", &(ea -> decay_pos_y), "DECAY_POS_Y/D");
	this -> Branch("DECAY_POS_Z", &(ea -> decay_pos_z), "DECAY_POS_Z/D");

	this -> Branch("DECAY_V_X", &(ea -> decay_velocity_x), "DECAY_V_X/D");
	this -> Branch("DECAY_V_Y", &(ea -> decay_velocity_y), "DECAY_V_Y/D");
	this -> Branch("DECAY_V_Z", &(ea -> decay_velocity_z), "DECAY_V_Z/D");

	this -> Branch("CORRELATION_Abeta",  &(eGen -> BigA),    "CORRELATION_Abeta/D");
	this -> Branch("CORRELATION_Bnu",    &(eGen -> BigB),    "CORRELATION_Bnu/D");
	this -> Branch("CORRELATION_abetanu",&(eGen -> LittleA), "CORRELATION_abetanu/D");
	this -> Branch("CORRELATION_calign", &(eGen -> LittleC), "CORRELATION_calign/D");
}

void LocalAggregator::EndRun() 
{
//	G4cout << "Called LocalAggregator::EndRun()" << G4endl;
//	G4RunManager * RunMan        = G4RunManager::GetRunManager();  
//	G4RunManager * MasterRunMan  = G4MTRunManager::GetMasterRunManager();  
//	G4cout << "From within LocalAggregator::EndRun():   MasterRunMan->GetUserRunAction()->IsMaster() = " << MasterRunMan->GetUserRunAction()->IsMaster() << G4endl;
//	G4cout << "From within LocalAggregator::EndRun():   RunMan->GetUserRunAction()->IsMaster()       = " << RunMan->GetUserRunAction()->IsMaster()       << G4endl;
//	G4cout << "From within LocalAggregator::EndRun():   G4Threading::G4GetThreadId()                 = " << G4Threading::G4GetThreadId()                 << G4endl;
//	G4cout << "From within LocalAggregator::EndRun():   G4Threading::IsWorkerThread()                = " << G4Threading::IsWorkerThread()                << G4endl;
	
	G4cout << "From within LocalAggregator::EndRun():  G4Threading::G4GetThreadId() = " << G4Threading::G4GetThreadId() << G4endl;
	if( G4Threading::IsWorkerThread() )
	{
		this->SaveParamString();  // no...... just don't.  
	//	active_channels_->clear();  // maybe ... don't?
		
	//	G4cout << "Local aggregator on worker thread.  We've saved the parameter string." << G4endl;
	}
//	else
//	{
//	//	G4cout << "Local aggregator on master thread.  We won't save parameter string." << G4endl;
//	}
//	G4cout << "We've finished LocalAggregator::EndRun()." << G4endl;
	
	/*
	// we do this when the aggregator is created:
	active_channels_ = new map<string, K37_Data*>;
	active_channels_->clear();
	channelset = new set_of_channels();  // do I even want this to happen if this is on the master thread?
	*/
	
//	G4cout << "NOT deleting the various LocalAggregator-based channels..." << G4endl;
	// don't delete things.  I'll need them for the next run, and I can't *really* reallocate them....
//	delete active_channels_;
//	delete channelset;
}

/*
void LocalAggregator::BeginRun()
{
	active_channels_ = new map<string, K37_Data*>;
	active_channels_->clear();
	channelset = new set_of_channels();  // do I even want this to happen if this is on the master thread?
}
*/

void LocalAggregator::SaveParamString()
{
	/*
	G4RunManager              * runMan        = G4RunManager::GetRunManager();  // ....really??
	K37EventAction            * event_action_ = (K37EventAction*)(runMan->GetUserEventAction());
	K37PhysicsList            * physics_list_ = K37RunManagerMT::GetMasterRunManager()->GetPhysicsList();
	K37ElectricFieldSetup     * Efield        = K37RunManagerMT::GetMasterRunManager()->GetUserDetectorConstruction()->GetField();  // hopefully this doesn't kill it...
	K37CloudSetup             * cloud         = ( (K37ActionInitialization*)(G4MTRunManager::GetMasterRunManager()->GetUserActionInitialization()) )->GetCloud();
//	G4cout << "* LocalAggregator::SaveParamString() says:  the matched runset letter is:  " << cloud->GetMatchedRunsetLetter() << G4endl;
	
	K37PrimaryGeneratorAction * pga           = (K37PrimaryGeneratorAction*)G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction();  // pga exists iff it's a worker thread.
//	if(pga) { G4cout << "pga exists!" << G4endl; }
//	else { G4cout << "no pga.  :(" << G4endl; }

	// Get digitizer modules to look up thresholds
	G4DigiManager *digitizer_manager( G4DigiManager::GetDMpointer() );
	K37ScintillatorDigitizer *upper_scintillator_digitizer(
	    static_cast<K37ScintillatorDigitizer *> ( digitizer_manager -> FindDigitizerModule("scintillatorPlusZ") ));
	K37ScintillatorDigitizer *lower_scintillator_digitizer(
	    static_cast<K37ScintillatorDigitizer *>( digitizer_manager -> FindDigitizerModule("scintillatorMinusZ") ));
	
//	G4cout << "LocalAggregator::SaveParamString():  Aggregator pointers have been pointed." << G4endl;
	//
	TArrayD *parameters = new TArrayD(23);
	
	parameters -> AddAt(cloud -> GetPolarization()       , 0);
	parameters -> AddAt(cloud -> GetAlignment()          , 1);

	parameters -> AddAt(0.0, 2);  // Electric Field "x"
	parameters -> AddAt(Efield -> GetConstantFieldValue()/(volt/cm) , 3);
	parameters -> AddAt(0.0, 4);   // Electric Field "z"
	
	parameters -> AddAt(cloud -> GetCloudCenter().x()/mm, 5);
	parameters -> AddAt(cloud -> GetCloudCenter().y()/mm, 6);
	parameters -> AddAt(cloud -> GetCloudCenter().z()/mm, 7);
	
	parameters -> AddAt(cloud -> GetInitialCloudSize().x()/mm, 8);
	parameters -> AddAt(cloud -> GetInitialCloudSize().y()/mm, 9);
	parameters -> AddAt(cloud -> GetInitialCloudSize().z()/mm, 10);
	parameters -> AddAt(cloud -> GetTemperature().x()/kelvin, 11);
	parameters -> AddAt(cloud -> GetTemperature().y()/kelvin, 12);
	parameters -> AddAt(cloud -> GetTemperature().z()/kelvin, 13);
//	G4cout << "Cloud things have been output, I guess." << G4endl;
	
	parameters -> AddAt(pga -> GetRecoilCharge(), 14);
//	G4cout << "We did a thing with the pga." << G4endl;
	
	parameters -> AddAt(event_action_ -> GetElectronMCPthreshold(), 15);    
	parameters -> AddAt(upper_scintillator_digitizer -> GetThreshold(), 16);
	parameters -> AddAt(lower_scintillator_digitizer -> GetThreshold(), 17);
	
	parameters -> AddAt(physics_list_ -> GetDefaultCutValue()/micrometer, 18);
	G4String list_str = physics_list_ -> GetEmName();  
	
//	G4cout << "I guess we're about to assign list_code a value..." << G4endl;
	double list_code = -1;
	if (list_str == "emstandard_opt0") { list_code = 0;  }
	if (list_str == "emstandard_opt1") { list_code = 1;  }
	if (list_str == "emstandard_opt2") { list_code = 2;  }
	if (list_str == "emstandard_opt3") { list_code = 3;  }
	if (list_str == "local")           { list_code = 4;  }
	if (list_str == "standardSS")      { list_code = 5;  }
	if (list_str == "standardNR")      { list_code = 6;  }
	if (list_str == "empenelope")      { list_code = 7;  }
	if (list_str == "emlivermore")     { list_code = 8;  }
	if (list_str == "K37")             { list_code = 9;  }
	if (list_str == "emstandard_opt4") { list_code = 10; }
	parameters -> AddAt(list_code, 19);
	
	parameters -> AddAt(cloud -> GetSailVelocity().x()/(mm/ns), 20);
	parameters -> AddAt(cloud -> GetSailVelocity().y()/(mm/ns), 21);
	parameters -> AddAt(cloud -> GetSailVelocity().z()/(mm/ns), 22);
	
	//
	G4cout << "Saving parameter string to the root file..." << G4endl;
	*/
	
//	TFile * file = LocalAggregator::GetRootFile();
	
//	string rpname = string("RunParameters")+patch::to_string(G4Threading::G4GetThreadId());
//	file -> WriteObject(parameters, rpname.c_str() );
	
	rootFile -> cd();
	theNTuple -> Write(0, TObject::kOverwrite); 
	rootFile -> Close();
	delete rootFile;
	rootFile = 0;
	theNTuple = 0;
}
