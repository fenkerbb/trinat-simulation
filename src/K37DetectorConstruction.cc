// Authors: Spencer Behling and Benjamin Fenker 2013

#include <string>
#include <iostream>
#include <sstream>

// CLHEP files
#include <CLHEP/Vector/Rotation.h>

// Geant Files
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Element.hh"
#include "G4ElementTable.hh"
#include "G4EllipticalTube.hh"
#include "G4FieldManager.hh"
#include "G4GeometryManager.hh"
#include "G4LogicalVolume.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4NistManager.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PVParameterised.hh"  // ??
#include "G4PVPlacement.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4RotationMatrix.hh"
#include "G4SDParticleFilter.hh" // ??
#include "G4SolidStore.hh"
#include "G4ThreeVector.hh"
#include "G4Torus.hh"
#include "G4Transform3D.hh"
#include "G4TransportationManager.hh" // ??
#include "G4UnionSolid.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4VSDFilter.hh"  // ??
#include "G4IntersectionSolid.hh"

#include "G4AutoDelete.hh"

#include "G4Colour.hh"
#include "G4VisAttributes.hh"

//#include "G4MultiFunctionalDetector.hh"
#include "G4SDManager.hh"

using namespace std;

// Our Files
#include "K37DetectorConstruction.hh"
#include "K37DetectorMessenger.hh"
//#include "K37ElectricFieldSetup.hh"


K37DetectorConstruction::K37DetectorConstruction(): 
	detectorMessenger(0),
	
	world_material_(0), world_box_(0), world_log_(0), world_phys_(0),
	chamber_box_(0), chamber_log_(0), chamber_phys_(0), 
	
	scintillator_tubs_(0), 
	upper_scintillator_log_(0), upper_scintillator_phys_(0), 
	lower_scintillator_log_(0), lower_scintillator_phys_(0),
	
	upper_strip_detector_log_(0), upper_strip_detector_phys_(0),
	lower_strip_detector_log_(0), lower_strip_detector_phys_(0), 
	
	electron_mcp_tub_(0), electron_mcp_log_(0), electron_mcp_log_noSense(0), electron_mcp_phys_(0), 
	recoil_mcp_tub_(0), recoil_mcp_log_(0), recoil_mcp_phys_(0),
	
	MirrorMaterial(0), FullEnergyDetectorMaterial(0), DeDxDetectorMaterial(0),
	SiliconDetectorFrameMaterial(0), ChamberMaterial(0), FoilMaterial(0),
	HoopMaterial(0), MirrorMountMaterial(0), CoilsMetal(0),CoilsLiquid(0),
	electron_mcp_radius_(40.0*mm),
//	shouldTheMirrorBeWFEDMCut(false), 
	check_all_for_overlaps_(false),
	changeZtoX(0),
	changeZto45(0), changeZtoNeg45(0), changeZto35(0), changeZtoNeg35(0),
	changeZtoNeg62(0), changeZtoNeg118(0), changeYtoBeamAxis(0),
	changeYtoBeamAxisForLPP1(0), changeYtoBeamAxisForLPP2(0),
	changeYtoBeamAxisForLPP3(0), changeYtoBeamAxisForLPP4(0),
	rotationForOpticalPumpingBeams1(0), rotationForOpticalPumpingBeams2(0),
	RFDRotation(0), FFRFRotation(0), MirrorCutRotation(0),
	hoopRotation(0), zRot(0), y180(0), mirror_rotation(0), MMRotation(0),
	
	// visualizer attributes:
	do_the_visualizing(true),
	world_logVisAttributes(0), 
	air_vis(0), 
	chamber_logVisAttributes(0), 
	OPRF_logVisAttributes(0),
	RFD_logVisAttributes(0), 
	beryllium_logVisAttributes(0),
	FFRF_logVisAttributes(0), 
	MM_logVisAttributes(0), 
	cut_ESP_logVisAttributes(0),
	hoop7_logVisAttributes(0), 
	coils_logVisAttributes(0), 
	coilLiquid_logVisAttributes(0),
	scintillator_vis(0),
	teflon_vis(0), 
	strip_detector_vis(0), 
	dedxFrame_logVisAttributes(0),
	sd_inactive_vis(0),
	mounting_screw_head_vis(0),
	SOED_logVisAttributes(0),
	rmcp_logVisAttributes_(0) 
{
	// Default values
	world_size_ = 2.0 * m;

	// Define the inch.  Inelegant, but it's fine...
	static const G4double inch = 2.54*cm; 

	// *************** All default dimensions should be placed here **************
	// *** Any derived dimnesions will be calcualted in a function below *********

	// Mirror dimensions from meeting website on 30-Jan-2014
	// Placement from detectorDrawing.pdf
	mirror.inner_radius = 0.0*inch;       // Its solid - duh
	mirror.outer_radius = 1.2*inch;       // 30-Jan-2014 meeting

	// 0.010" +- 0.001" = 0.254mm is the dimension speced by manufacturer
	//  mirror.length = 0.254 * mm;           // 30-Jan-2014 meeting
	// Acutal measurements show that true value is:
	mirror.length = 0.275*mm;     // ELOG from 1-Dec-2015 0.275(6) mm

	// Tweaked
	//  mirror.length = 0.300 * mm;

	mirror.rotation_angle = -9.5*deg;
	// 30-Jan-2014 meeting, MJA swapped signs to -9.5*deg, 9.6.2015.
	// Mirror mount dimensions from collimator_back.pdf & collimator_front.pdf
	mirror_mount.inner_radius = 0.0*inch; // Solid to start
	mirror_mount.outer_radius = (2.913/2.0) * inch; // collimator_{front,back}.pdf
	// Take the fat side of one slice and add to it the thin side of the other slice
	mirror_length_bias = 0.00*mm;
	be_window_bias     = 0.00*mm;
	bb1_bias           = 0.00*mm;

	G4double lip = (0.519*inch) - fabs((2.0*mirror_mount.outer_radius)*tan(mirror.rotation_angle));
	mirror_mount.length             = (0.519*inch) + lip;
	mirror_mount.cutout_side_length = 1.220 * inch;
	mirror_mount.cutout_radius      = 2.441/2.0 * inch;
	mirror_mount.cutout_depth       = 0.040 * inch;

	// ***********************************************************
	// The dimensions on the drawing are given in diameter. 
	simpleChamberTub.inner_radius = (11.75/2.0)*inch;
	simpleChamberTub.outer_radius = (12.5/2.0)*inch;
	simpleChamberTub.length       = 22.0*inch;

	simpleChamberRentrantFlangeCut.inner_radius = 0.0*inch; //Solid for cut
	simpleChamberRentrantFlangeCut.outer_radius = (4.2/2.0)*inch;
	simpleChamberRentrantFlangeCut.length       = 13.0*inch;

	// ***********************************************************
	// These are the coil dimensions outlined in /Drawings/cros-coil.png
	// I will use the unit cm here because that is what gets injected 
	// in by using inch.
	distanceFromCloseEdgeToPolarization = 7.0*cm;
	distanceFromBottomToChamberCenter   = 6.0*cm;
	tubeOuterDiameter                   = (1.0/8.0)*inch;
	tubeInnerDiameter                   = (1.0/16.0)*inch;
	tubeSpacing                         = (1.0/16.0)*inch;
	G4String nameStart    = "InnerTorus";
	G4String nameStartTub = "OuterTube";
	G4String tempName     = G4String();
	for(int ycount = 0; ycount < 4; ++ycount)
	{
		for(int xcount = 0; xcount < 4; ++xcount)
		{
			tempName = nameStart+"_PZ_"+patch::to_string(ycount)+"_"+patch::to_string(xcount);
			coil_inner_torus_PZ[ycount][xcount].nameBase = tempName;
			coil_inner_torus_PZ[ycount][xcount].inner_radius = 0.0;
			coil_inner_torus_PZ[ycount][xcount].outer_radius = tubeInnerDiameter/2.0;
			coil_inner_torus_PZ[ycount][xcount].sweep_radius =
				((distanceFromCloseEdgeToPolarization + (tubeOuterDiameter/2.0)) + double(xcount)*(tubeOuterDiameter+tubeSpacing));

			coil_inner_torus_PZ[ycount][xcount].start_phi = 0;
			coil_inner_torus_PZ[ycount][xcount].delta_phi = 360*deg;
			coil_inner_torus_PZ[ycount][xcount].center_position =
				G4ThreeVector(0, 0, 
				((distanceFromBottomToChamberCenter + (tubeOuterDiameter/2.0)) + double(ycount)*(tubeOuterDiameter+tubeSpacing)));

			tempName = nameStartTub+"_PZ_"+patch::to_string(ycount)+"_"+patch::to_string(xcount);
			coil_outer_tubs_PZ[ycount][xcount].nameBase = tempName;
			coil_outer_tubs_PZ[ycount][xcount].inner_radius =
				(distanceFromCloseEdgeToPolarization + (double(xcount)*(tubeOuterDiameter+tubeSpacing)));

			coil_outer_tubs_PZ[ycount][xcount].outer_radius =
				((distanceFromCloseEdgeToPolarization + (tubeOuterDiameter)) + (double(xcount)*(tubeOuterDiameter+tubeSpacing)));

			coil_outer_tubs_PZ[ycount][xcount].length = tubeOuterDiameter;

			coil_outer_tubs_PZ[ycount][xcount].start_phi = 0;
			coil_outer_tubs_PZ[ycount][xcount].delta_phi = 360*deg;
			coil_outer_tubs_PZ[ycount][xcount].center_position =
				G4ThreeVector(0, 0, ((distanceFromBottomToChamberCenter + (tubeOuterDiameter/2.0)) + double(ycount)*(tubeOuterDiameter+tubeSpacing)));
			
			//====================================================================
			
			tempName = nameStart+"_MZ_"+patch::to_string(ycount)+"_"+patch::to_string(xcount);
			coil_inner_torus_MZ[ycount][xcount].nameBase = tempName;
			coil_inner_torus_MZ[ycount][xcount].inner_radius = 0.0;
			coil_inner_torus_MZ[ycount][xcount].outer_radius = tubeInnerDiameter/2.0;
			coil_inner_torus_MZ[ycount][xcount].sweep_radius =
				((distanceFromCloseEdgeToPolarization + (tubeOuterDiameter/2.0)) + double(xcount)*(tubeOuterDiameter+tubeSpacing));

			coil_inner_torus_MZ[ycount][xcount].start_phi = 0;
			coil_inner_torus_MZ[ycount][xcount].delta_phi = 360*deg;
			coil_inner_torus_MZ[ycount][xcount].center_position =
				G4ThreeVector(0, 0, 
				-((distanceFromBottomToChamberCenter + (tubeOuterDiameter/2.0)) + double(ycount)*(tubeOuterDiameter+tubeSpacing)));
			
			tempName = nameStartTub+"_MZ_"+patch::to_string(ycount)+"_"+patch::to_string(xcount);
			coil_outer_tubs_MZ[ycount][xcount].nameBase = tempName;
			coil_outer_tubs_MZ[ycount][xcount].inner_radius =
				(distanceFromCloseEdgeToPolarization+(double(xcount)*(tubeOuterDiameter+tubeSpacing)));
			
			coil_outer_tubs_MZ[ycount][xcount].outer_radius =
				((distanceFromCloseEdgeToPolarization + (tubeOuterDiameter))+(double(xcount)*(tubeOuterDiameter+tubeSpacing)));
			
			coil_outer_tubs_MZ[ycount][xcount].length = tubeOuterDiameter;
			
			coil_outer_tubs_MZ[ycount][xcount].start_phi = 0;
			coil_outer_tubs_MZ[ycount][xcount].delta_phi = 360*deg;
			coil_outer_tubs_MZ[ycount][xcount].center_position =
			G4ThreeVector(0, 0, 
				-((distanceFromBottomToChamberCenter+(tubeOuterDiameter/2.0))+double(ycount)*(tubeOuterDiameter+tubeSpacing)));
		} // end loop over x.
	} // end loop over y.

	// ***********************************************************
	// The reentrant flange manufactured by Brush-Wellman
	// Described in reentrant_flange.pdf
	// Data from the drawings
	reentrant_flange_front_face.inner_radius = (58.00/2.0)*mm;
	reentrant_flange_front_face.outer_radius = (94.59/2.0)*mm;
	beryllium_window.inner_radius = 0.0*mm;
	beryllium_window.outer_radius = (58.00/2.0) * mm;
	beryllium_window.length = 0.229 * mm;
	// tweaked
	//  beryllium_window.length = 0.252*mm;
	reentrant_flange_descender.inner_radius2 = (98.56*mm)/2.0;
	reentrant_flange_descender.outer_radius2 = (101.60*mm)/2.0;
	reentrant_flange_descender.length = 10.00*mm;
	reentrant_flange_pipe.inner_radius = (98.56*mm)/2.0;
	reentrant_flange_pipe.outer_radius = (101.60*mm)/2.0;
	reentrant_flange_pipe.length = 130.0*mm;
	reentrant_flange_front_face.length = 10.00 * mm;

	// The SD frame is defined in the file BB1.pdf (the 3.0 mm thickness
	// is measured by hand I think)
	sd_frame.length = 55.0*mm;            // ???
	sd_frame.width  = 55.0*mm;            // ???
	sd_frame.depth  = 3.0*mm;
	sd_frame.cutout_side_length = 44.4*mm; // Enough for the entire chip
	sd_frame.cutout_depth = 1.05*sd_frame.depth; // All the way through
	//  sd_frame.center_position = G4ThreeVector(0.0, 0.0, 98.5*mm);

	strip_detector.length = 40.0*mm;
	strip_detector.width  = 40.0*mm;
	// The nominal value
	//  strip_detector.depth  = 0.3*mm;
	// Data sheets have 290, 297 um, so this spans them:
	strip_detector.depth = 0.295*mm;

	// There is non-active area of silicon that should be included in
	// the simulation.  See BB1.pdf
	sd_inactive.length = 44.4*mm;
	sd_inactive.width  = 44.4*mm;
	// The nominal value
	//  sd_inactive.depth  = 0.3*mm;
	// Data sheets have 290, 297 um, so this spans them:
	sd_inactive.depth = 0.295*mm;

	// The SD is mounted to the flange by 4x 2-56 screws.  The socket
	// cap heads stick up out ouf the SD frame and the scintillator
	// rests against these screws.  This creates a small gap between the
	// silicon and scintillator.   See BB1.pdf for palcement
	// 0.082" = 2-56 socket head cap screw height
	// 0.020" = #2 flat washer height
	sd_mounting_screw_head.inner_radius = 0.0*mm;
	sd_mounting_screw_head.outer_radius = (0.137/2.0) * inch;
	sd_mounting_screw_head.length = (0.082 + 0.020) * inch;
	// ***********************************************************

	// Teflon front face and scintillator
	// The teflon front face covers the entire front face of the
	// scintillator. The covering is assumed to be completely uniform
	// and flat mostly because modeling it as anything else would be
	// impossible.  The thickness (90 um) comes from a perusing of the
	// internet and could be off in reality by 10s of microns.  
	scintillator.inner_radius = 0.0;
	scintillator.outer_radius = (90.0/2.0) * mm;

	teflon_front_face.length = 90.0*um;
	// teflon_front_face.length = 5.0*mm;

	scintillator.length = 3.5 * cm;
	// ***********************************************************

	this-> DefineMaterials();
	detectorMessenger = new K37DetectorMessenger(this);
	changeZtoX = new CLHEP::HepRotation();
	changeZtoX -> rotateX(90.*deg);
	
	// ******SET CONSTRUCTION FLAGS HERE**************************************
	// why not just initialize these at the start??  
	// these don't become relevant until G4VPhysicalVolume* K37DetectorConstruction::ConstructK37Experiment().
	makeScintillators     = true;    // Must be true!
	makeStripDetectors    = true;    // Must be true!
	makeChamber           = false;
	makeSimpleChamber     = true;
	makeMirrors           = true;
	makeHoops             = true;
	makeElectronMCP       = true;
	makeCoils             = true;
	makeRecoilMCP         = true;
	make_sd_holders_      = true;
	make_beryllium_foils_ = true;
	//
	record_strip_detector_data_ = true;  // from K37RunAction
} // end of 

void K37DetectorConstruction::SetupVisAttributes()
{
	// ConstructK37Experiment_shared() calls ConstructSimpleChamber() or ConstructChamber().
	// ConstructK37Experiment_shared() calls ConstructAir().
	// ConstructK37Experiment_shared() calls ConstructBerylliumFoils(), ConstructMirrors(), ConstructHoops().
	// ConstructK37Experiment_shared() calls ConstructCoils(), ConstructScintillators(), ConstructStripDetectors().
	// ConstructK37Experiment_shared() calls ConstructElectronMCP(), ConstructRecoilMCP().
	/*
	G4VisAttributes * world_logVisAttributes;      // formerly within ConstructK37Experiment_shared().
	G4VisAttributes * air_vis;                     // formerly within ConstructAir().
	G4VisAttributes * chamber_logVisAttributes;    // formerly within ConstructSimpleChamber() and ConstructChamber().
	G4VisAttributes * OPRF_logVisAttributes;       // formerly within ConstructSimpleChamber() and ConstructChamber().
	G4VisAttributes * RFD_logVisAttributes;        // formerly within ConstructSimpleChamber() and ConstructChamber().
	G4VisAttributes * beryllium_logVisAttributes;  // formerly within ConstructBerylliumFoils() 
	G4VisAttributes * FFRF_logVisAttributes;       // formerly within ConstructBerylliumFoils() 
	G4VisAttributes * mirror_logVisAttributes[2];  // formerly within ConstructMirrors().
	G4VisAttributes * MM_logVisAttributes;         // formerly within ConstructMirrors().
	G4VisAttributes * cut_ESP_logVisAttributes;    // formerly within ConstructHoops().
	G4VisAttributes * hoop7_logVisAttributes;      // formerly within ConstructHoops().
	G4VisAttributes * coils_logVisAttributes;      // within ConstructCoils().
	G4VisAttributes * coilLiquid_logVisAttributes; // within ConstructCoils().
	G4VisAttributes * scintillator_vis;            // formerly within ConstructScintillators().
	G4VisAttributes * teflon_vis;                  // formerly within ConstructScintillators().
	G4VisAttributes * strip_detector_vis;          // formerly within ConstructStripDetectors(). no extra requirements.
	G4VisAttributes * dedxFrame_logVisAttributes;  // formerly within ConstructStripDetectors(), requires if (make_sd_holders_) 
	G4VisAttributes * sd_inactive_vis;             // formerly within ConstructStripDetectors(), requires if (make_sd_holders_) 
	G4VisAttributes * mounting_screw_head_vis;     // formerly within ConstructStripDetectors(), requires if (make_sd_holders_) 
	G4VisAttributes * SOED_logVisAttributes;       // formerly within ConstructElectronMCP().
	G4VisAttributes * rmcp_logVisAttributes_;      // formerly within ConstructRecoilMCP().
	*/
	
	// G4VisAttributes(false) means invisible. It is better than using the
	// convience function G4VisAttributes::Invisble becuse it produces a
	// non const pointer that can later be delete avoiding a memory leak.
	world_logVisAttributes = new G4VisAttributes(false);
	world_log_ -> SetVisAttributes(world_logVisAttributes);
	//	air_vis = new G4VisAttributes(false); // invisible
	air_vis = new G4VisAttributes(G4Colour(0.53, 0.80, 0.98, 0.1)); // sky blue
	air_vis -> SetForceSolid(true);
	air_log_plus_ -> SetVisAttributes(air_vis);
	air_log_mins_ -> SetVisAttributes(air_vis);
	//
	if(makeChamber)
	{
		// G4VisAttributes(false) means invisible. It is better than using the
		// convience function G4VisAttributes::Invisble becuse it produces
		// a non const pointer that can later be delete avoiding a memory
		// leak.
		//chamber_logVisAttributes = new G4VisAttributes(false);
		chamber_logVisAttributes = new G4VisAttributes(G4Colour(255/255., 170/255., 0, 0.1));
		chamber_logVisAttributes -> SetForceSolid(true);
		
		chamber_log_ -> SetVisAttributes(chamber_logVisAttributes);
		//
		OPRF_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
		OPRF_logVisAttributes -> SetForceSolid(true);
		OPRF_log -> SetVisAttributes(OPRF_logVisAttributes);
		//
		RFD_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
		RFD_logVisAttributes-> SetForceSolid(true);
		RFD_log -> SetVisAttributes(RFD_logVisAttributes);
	}
	if(makeSimpleChamber)
	{
		chamber_logVisAttributes = new G4VisAttributes( G4Colour(0.96, 0.96, 0.96, 0.1) );
		chamber_logVisAttributes -> SetForceSolid(true);
		temp_log     -> SetVisAttributes(chamber_logVisAttributes);
		chamber_log_ -> SetVisAttributes(chamber_logVisAttributes);
		//
		OPRF_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
		OPRF_logVisAttributes -> SetForceSolid(true);
		OPRF_log -> SetVisAttributes(OPRF_logVisAttributes);
		//
		RFD_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
		RFD_logVisAttributes -> SetForceSolid(true);
		RFD_log -> SetVisAttributes(RFD_logVisAttributes);
	}
	//
	if (make_beryllium_foils_)
	{
		// previously, beryllium visual attributes were defined and set before the relevant G4PVPlacement.  
		// Not anymore.  Hopefully it doesn't matter.
		// same for air_log_plus_, air_log_mins_, and world_log_.  
		// also mirror_log[i], ...
		// upper_scintillator_log_ and lower_scintillator_log are different though!!  Phew.
		beryllium_logVisAttributes = new G4VisAttributes(G4Colour(0.80, 0.36, 0.36, 0.8)); // pale red
		beryllium_logVisAttributes -> SetForceSolid(true);
		for (int i = 0; i < 2; i++) 
		{
			beryllium_log[i] -> SetVisAttributes(beryllium_logVisAttributes);
		}
		//
		FFRF_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
		FFRF_logVisAttributes-> SetForceSolid(true);
		FFRF_log -> SetVisAttributes(FFRF_logVisAttributes);
	}
	if (makeMirrors)
	{
		// do we really need two separate mirror_logVisAttributes ? 
		for (int i = 0; i < 2; i++) 
		{
			mirror_logVisAttributes[i] = new G4VisAttributes(G4Colour(0.9, 0.1, 0.1, 0.9));
			mirror_logVisAttributes[i] -> SetForceSolid(true);
			mirror_log[i] -> SetVisAttributes(mirror_logVisAttributes[i]);
		}
		
	//	G4LogicalVolume *mirror_mount_log = new G4LogicalVolume(mirror_mount_final, MirrorMountMaterial, "MM_log", 0, 0, 0);
		MM_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
		MM_logVisAttributes-> SetForceWireframe(true);
		mirror_mount_log -> SetVisAttributes(MM_logVisAttributes);
	}
	if (makeHoops)
	{
		// this will never work:
		cut_ESP_logVisAttributes = new G4VisAttributes(G4Colour(0.0, 0.0, 0.0, 1.0));
		cut_ESP_logVisAttributes -> SetForceSolid(true);
		hoop_3through6_box_log -> SetVisAttributes(cut_ESP_logVisAttributes);  // 3-6
		hoop_1_log             -> SetVisAttributes(cut_ESP_logVisAttributes);  // 1
		//
		hoop7_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
		hoop7_logVisAttributes -> SetForceSolid(true);
		hoop_7_log -> SetVisAttributes(hoop7_logVisAttributes);    // 7
		hoop_2_log -> SetVisAttributes(hoop7_logVisAttributes);    // 2
	}
	if (makeCoils)
	{
		// This part necessitates a weird thing in the geometry construction function.  
		// Because there are a bunch of coils, there's now a 2D array of pointers to the
		// logical volumes.  Hopefully it's fine, but also, I might be introducing a bug.
		coils_logVisAttributes = new G4VisAttributes(G4Colour(0.6, 0.35, 0.0, 0.25));
		coils_logVisAttributes -> SetForceSolid(true);
		
		coilLiquid_logVisAttributes = new G4VisAttributes(G4Colour(0.0, 0.0, 1.0, 0.8));
		coilLiquid_logVisAttributes -> SetForceSolid(false);
		
		for(int ycount = 0; ycount < 4; ++ycount)
		{
			for(int xcount = 0; xcount < 4; ++xcount)
			{
				coil_log_t[ycount][xcount]      -> SetVisAttributes(coilLiquid_logVisAttributes);
				coilMetal_log_t[ycount][xcount] -> SetVisAttributes(coils_logVisAttributes);
				
				coil_log_b[ycount][xcount]      -> SetVisAttributes(coilLiquid_logVisAttributes);
				coilMetal_log_b[ycount][xcount] -> SetVisAttributes(coils_logVisAttributes);
				
			}
		}
		
	}
	if (makeScintillators)
	{
		// Use same vis attributes for both detectors :-)
		scintillator_vis = new G4VisAttributes(G4Colour(0.11, 0.11, 0.44));
		scintillator_vis -> SetForceSolid(true);
		upper_scintillator_log_ -> SetVisAttributes(scintillator_vis);
		lower_scintillator_log_ -> SetVisAttributes(scintillator_vis);
		
		// white
		teflon_vis = new G4VisAttributes(G4Colour(1.0, 1.0, 1.0));
		teflon_vis -> SetForceSolid(true);
		teflon_log -> SetVisAttributes(teflon_vis);
		
	}
//	G4cout << "Doing the DSSSDs." << G4endl;
	if (makeStripDetectors)
	{
		// Same vis for both detectors :-)
		strip_detector_vis = new G4VisAttributes(G4Colour(0.96, 0.96, 0.0, 0.5)); // yellow
		strip_detector_vis -> SetForceSolid(true);
		upper_strip_detector_log_ -> SetVisAttributes(strip_detector_vis);
		lower_strip_detector_log_ -> SetVisAttributes(strip_detector_vis);
		//
		if (make_sd_holders_)
		{
			dedxFrame_logVisAttributes = new G4VisAttributes(G4Colour(0.85, 0.65, 0.13));
			dedxFrame_logVisAttributes-> SetForceSolid(true);
			dedxFrame_log -> SetVisAttributes(dedxFrame_logVisAttributes);
			//
			sd_inactive_vis = new G4VisAttributes(G4Colour(0.5, 0.5, 0.0));
			sd_inactive_vis -> SetForceSolid(true);
			sd_inactive_log -> SetVisAttributes(sd_inactive_vis);
			//
			mounting_screw_head_vis = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
			mounting_screw_head_vis -> SetForceSolid(true);
			mounting_screw_head_log -> SetVisAttributes(mounting_screw_head_vis);
		}
	}
//	G4cout << "Done with the DSSSDs." << G4endl;
	if (makeElectronMCP)
	{
		SOED_logVisAttributes = new G4VisAttributes(G4Colour(0.3, 0.3, 0.3));
		SOED_logVisAttributes    -> SetForceSolid(true);
		electron_mcp_log_        -> SetVisAttributes(SOED_logVisAttributes);
		electron_mcp_log_noSense -> SetVisAttributes(SOED_logVisAttributes);
		/*
		// these ones aren't saved in the class yet:
		G4VisAttributes *spacer_vis = new G4VisAttributes(G4Colour(0.6, 0.35, 0.0, 0.25));
		spacer_vis -> SetForceSolid(true);
		G4VisAttributes *ceramic_vis = new G4VisAttributes(G4Colour(0.75, 0.75, 0.75));
		ceramic_vis -> SetForceSolid(true);
		//
		// note:  spacer_log is defined locally within several different functions, 
		// not just the one that makes the eMCP.
		// ceramic_log too.
		G4LogicalVolume *spacer_log = new G4LogicalVolume(spacer_tub, spacer_mat,
		                                                  "eMCP_spacer_log", 0, 0, 0,
		                                                  check_all_for_overlaps_);
		G4LogicalVolume *ceramic_log = new G4LogicalVolume(ceramic_tub, ceramic_mat,
		                                                  "eMCP_ceramic", 0, 0, 0,
		                                                  check_all_for_overlaps_);
		//
		spacer_log -> SetVisAttributes(spacer_vis);
		ceramic_log -> SetVisAttributes(ceramic_vis);
		*/
	}
	if (makeRecoilMCP)
	{
		rmcp_logVisAttributes_ = new G4VisAttributes(G4Colour(0.3, 0.3, 0.3));
		rmcp_logVisAttributes_ -> SetForceSolid(true);
		recoil_mcp_log_    -> SetVisAttributes(rmcp_logVisAttributes_);
		mcp_plate_rear_log -> SetVisAttributes(rmcp_logVisAttributes_);
		//
		/*
		// not saved yet:
		G4VisAttributes *ceramic_vis = new G4VisAttributes(G4Colour(1.0, 1.0, 1.0));  // not the same.
		ceramic_vis -> SetForceSolid(true);
		
		G4VisAttributes *spacer_vis = new G4VisAttributes(G4Colour(0.6, 0.35, 0.0, 0.25));  // same.
		spacer_vis -> SetForceSolid(true);
		
		G4LogicalVolume *ceramic_log = new G4LogicalVolume(ceramic_tub, ceramic_mat,
		                                                  "rMCP_ceramic", 0, 0, 0,
		                                                  check_all_for_overlaps_);
		// Note:  it's eMCP_spacer_log even for the rMCP.  
		G4LogicalVolume *spacer_log = new G4LogicalVolume(spacer_tub, spacer_mat,
		                                                  "eMCP_spacer_log", 0, 0, 0,
		                                                  check_all_for_overlaps_);
		//
		ceramic_log -> SetVisAttributes(ceramic_vis);
		spacer_log -> SetVisAttributes(spacer_vis);
		*/
	}
}

K37DetectorConstruction::~K37DetectorConstruction() 
{
//	G4cout << "K37DetectorConstruction is being deleted ..." << G4endl;
	if (detectorMessenger)               delete detectorMessenger;
	if (changeZtoX)                      delete changeZtoX;
	if (changeZto45)                     delete changeZto45;
	if (changeZtoNeg45)                  delete changeZtoNeg45;
	if (changeZto35)                     delete changeZto35;
	if (changeZtoNeg35)                  delete changeZtoNeg35;
	if (changeZtoNeg62)                  delete changeZtoNeg62;
	if (changeZtoNeg118)                 delete changeZtoNeg118;
	if (changeYtoBeamAxis)               delete changeYtoBeamAxis;
	if (changeYtoBeamAxisForLPP1)        delete changeYtoBeamAxisForLPP1;
	if (changeYtoBeamAxisForLPP2)        delete changeYtoBeamAxisForLPP2;
	if (changeYtoBeamAxisForLPP3)        delete changeYtoBeamAxisForLPP3;
	if (changeYtoBeamAxisForLPP4)        delete changeYtoBeamAxisForLPP4;
	if (rotationForOpticalPumpingBeams1) delete rotationForOpticalPumpingBeams1;
	if (rotationForOpticalPumpingBeams2) delete rotationForOpticalPumpingBeams2;
	if (RFDRotation)                     delete RFDRotation;
	if (FFRFRotation)                    delete FFRFRotation;
	if (MirrorCutRotation)               delete MirrorCutRotation;
	if (hoopRotation)                    delete hoopRotation;
	if (zRot) delete zRot;
	if (y180) delete y180;
	if (mirror_rotation)             delete mirror_rotation;
	if (MMRotation)                  delete MMRotation;
	if (world_log_)                  delete world_log_;
	if (world_phys_)                 delete world_phys_;
	if (scintillator_tubs_)          delete scintillator_tubs_;
	if (upper_scintillator_log_)     delete upper_scintillator_log_;
	if (lower_scintillator_log_)     delete lower_scintillator_log_;
	if (strip_detector_box_[0])      delete strip_detector_box_[0];
	if (strip_detector_box_[1])      delete strip_detector_box_[1];
	if (upper_strip_detector_log_)   delete upper_strip_detector_log_;
	if (lower_strip_detector_log_)   delete lower_strip_detector_log_;
	if (chamber_box_)                delete chamber_box_;
	if (chamber_log_)                delete chamber_log_;
	if (electron_mcp_tub_)           delete electron_mcp_tub_;
	if (electron_mcp_log_)           delete electron_mcp_log_;
	if (electron_mcp_log_noSense)    delete electron_mcp_log_noSense;
	if (recoil_mcp_tub_)             delete recoil_mcp_tub_;
	if (recoil_mcp_log_)             delete recoil_mcp_log_;
	if (dedxFrame_logVisAttributes)  delete dedxFrame_logVisAttributes;
	if (chamber_logVisAttributes)    delete chamber_logVisAttributes;
	if (OPRF_logVisAttributes)       delete OPRF_logVisAttributes;
	if (RFD_logVisAttributes)        delete RFD_logVisAttributes;
	if (beryllium_logVisAttributes)  delete beryllium_logVisAttributes;
	if (FFRF_logVisAttributes)       delete FFRF_logVisAttributes;
	if (mirror_logVisAttributes[0])  delete mirror_logVisAttributes[0];
	if (mirror_logVisAttributes[1])  delete mirror_logVisAttributes[1];
	if (MM_logVisAttributes)         delete MM_logVisAttributes;
	if (cut_ESP_logVisAttributes)    delete cut_ESP_logVisAttributes;
	if (hoop7_logVisAttributes)      delete hoop7_logVisAttributes;
	if (SOED_logVisAttributes)       delete SOED_logVisAttributes;
	if (coils_logVisAttributes)      delete coils_logVisAttributes;
	if (coilLiquid_logVisAttributes) delete coilLiquid_logVisAttributes;
	if (world_logVisAttributes)      delete world_logVisAttributes;
	if (air_vis)                     delete air_vis;
	if (scintillator_vis)            delete scintillator_vis;
	if (teflon_vis)                  delete teflon_vis;
	if (strip_detector_vis)          delete strip_detector_vis;
	if (sd_inactive_vis)             delete sd_inactive_vis;
	if (mounting_screw_head_vis)     delete mounting_screw_head_vis;
	if (rmcp_logVisAttributes_)      delete rmcp_logVisAttributes_;
	//
	if (mirror_log[0])               delete mirror_log[0];
	if (mirror_log[1])               delete mirror_log[1];
	if (beryllium_log[0])            delete beryllium_log[0];
	if (beryllium_log[1])            delete beryllium_log[1];
	
	// G4Cache items cannot be deleted.  So the SDs will remain.  
	// Is it really OK to just delete the geometry elements without deleting the associated detectors????
	
//	G4cout << "The K37DetectorConstruction is gone!" << G4endl;
}

//G4ThreadLocal K37ElectricFieldSetup* K37DetectorConstruction::the_Efield = 0;

// --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- //

void K37DetectorConstruction::ConstructK37Experiment_local() // K37DetectorConstruction::ConstructSDandField()
{
//	G4cout << "Called K37DetectorConstruction::ConstructK37Experiment_local()." << G4endl;
	
	// all the things that can't be shared for multi-threading will go here.
	// ConstructK37Experiment_shared() has already been called.  
	// the geometry and various volumes are already set up.
	
//	G4SDManager::GetSDMpointer() -> SetVerboseLevel(5);  // temporary...
	
	if (makeScintillators)     { ConstructScintillatorsSD();  }  // checks if detectors exist.
	if (makeStripDetectors)    { ConstructStripDetectorsSD(); }  // checks if detectors exist.
	if (makeMirrors)           { ConstructMirrorsSD();        }  // doesn't check.
	if (makeElectronMCP)       { ConstructElectronMCPSD();    }  // checks if detector exists.
	if (makeRecoilMCP)         { ConstructRecoilMCPSD();      }  // checks if detector exists.
	if (make_beryllium_foils_) { ConstructBerylliumFoilsSD(); }  // doesn't check.
	
	// ok, now I guess construct the field??
//	K37ElectricFieldSetup * field = new K37ElectricFieldSetup();  // this is before run/initialize ...
	if( !the_Efield.Get() )
	{
	//	G4cout << "We don't have an electric field yet!  Creating one now..." << G4endl;
		K37ElectricFieldSetup * anewfield = new K37ElectricFieldSetup();
		G4AutoDelete::Register(anewfield); //Kernel will delete the messenger
	    the_Efield.Put(anewfield);
		
//	//	the_Efield = new K37ElectricFieldSetup();
//    F02ElectricFieldSetup* fieldSetup = new F02ElectricFieldSetup();
//    G4AutoDelete::Register(fieldSetup); //Kernel will delete the messenger
//    fEmFieldSetup.Put(fieldSetup);
	}
	
	return;
}


// ConstructK37Experiment() is the *only* thing called from UpdateGeometry(), 
// and calling UpdateGeometry() will make K37 crash later on during event 
// generation.  Unless running in GDB, ofc.
// G4VPhysicalVolume* K37DetectorConstruction::ConstructK37Experiment() 
// 
G4VPhysicalVolume* K37DetectorConstruction::ConstructK37Experiment_shared() // K37DetectorConstruction::Construct()
{
	// This function will *only* construct the items that can be shared between threads.  
	// Basically, anything that uses SDman will have to be moved elsewhere.
	
	// G4SDManager::GetSDMpointer() -> SetVerboseLevel(5);  // temporary...
	
	G4GeometryManager::GetInstance()->OpenGeometry();
	
	// ------------------------------------------ Sensitive Detector / filters
	/*------------------------------------------------------------------------
	  The Sensitivity of each volume will be set at the time the volume is
	  created. This will lead to what I believe to be a better organization of
	  the code because every aspect of a volume will no be localized so that
	  volumes can be added and subtracted more easily.
	  ------------------------------------------------------------------------
	  This is no longer a good idea (and maybe not workable at all), with 
	  the advent of multithreading.  Instead, the geometry and the sensitive
	  detectors will be set up separately now.  Hopefully the (thread-local) 
	  SDs will just look at the (global) geometry definitions...
	  ------------------------------------------------------------------------*/
//	G4SDManager* SDman = G4SDManager::GetSDMpointer();

	world_box_  = new G4Box("world_box", world_size_/2.0, world_size_/2.0, world_size_/2.0);
	world_log_  = new G4LogicalVolume(world_box_, world_material_, "world_log", 0, 0, 0);
	world_phys_ = new G4PVPlacement(0, G4ThreeVector(), world_log_, "world_phys", 0, false, 0);

	/*
	// G4VisAttributes(false) means invisible. It is better than using the
	// convience function G4VisAttributes::Invisble becuse it produces a
	// non const pointer that can later be delete avoiding a memory leak.
	world_logVisAttributes = new G4VisAttributes(false);
	world_log_ -> SetVisAttributes(world_logVisAttributes);
	*/

	CalculateDimensions();
	ConstructAir();
	
	if (makeScintillators)     { ConstructScintillators();  }
	if (makeStripDetectors)    { ConstructStripDetectors(); }
	// 
	if (makeChamber)           { ConstructChamber();        } 
	if (makeSimpleChamber)     { ConstructSimpleChamber();  } 
	// 
	if (makeMirrors)           { ConstructMirrors();        }  
	if (makeHoops)             { ConstructHoops();          }
	if (makeElectronMCP)       { ConstructElectronMCP();    }
	if (makeCoils)             { ConstructCoils();          }
	if (makeRecoilMCP)         { ConstructRecoilMCP();      }
	if (make_beryllium_foils_) { ConstructBerylliumFoils(); }
	//
	ConstructSupportRods();
	if(do_the_visualizing)     { SetupVisAttributes();      }
	
	return world_phys_;
}

// --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- //
// --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- //

void K37DetectorConstruction::CalculateDimensions() 
{
	bool verbose = false;
	// The FRONT of the mirror is 85 mm away so the center is slightly
	// beyond that Mirror length dimensions drives many things. However,
	// when tweaking it differently at +/-z, it really shouldn't drive
	// things.  Therefore, have the nominal dimension drive everything
	// and the tweaks only affect the mirror itself
  
	G4double z = (85.0*mm) + ((mirror.length/2.0)/cos(mirror.rotation_angle));
	mirror.center_position = G4ThreeVector(0.0, 0.0, z);
	mirror_mount.center_position = mirror.center_position;
	// Derived data
	reentrant_flange_front_face.inner_radius2 =
	    reentrant_flange_front_face.inner_radius;

	G4double outer_radius_change = reentrant_flange_pipe.outer_radius -
	    reentrant_flange_front_face.outer_radius;
	reentrant_flange_front_face.outer_radius2 =
	    reentrant_flange_front_face.outer_radius + (0.5*outer_radius_change);
	G4double zpos = mirror_mount.center_position.z() +
	    (0.5*mirror_mount.length) + (0.5*reentrant_flange_front_face.length);
	reentrant_flange_front_face.center_position = G4ThreeVector(0.0, 0.0, zpos);

	// ***********************************************************
	// The beryllium window is defined in reentrant_flange.pdf
	//  beryllium_window.center_position = G4ThreeVector(0.0, 0.0, 96.89*mm);
	zpos = reentrant_flange_front_face.center_position.z() +
	    (0.5*reentrant_flange_front_face.length) - (0.5*beryllium_window.length);
	beryllium_window.center_position = G4ThreeVector(0.0, 0.0, zpos);

	// Reentrant flange descender
	reentrant_flange_descender.inner_radius =
	    reentrant_flange_front_face.outer_radius;
	reentrant_flange_descender.outer_radius =
	    reentrant_flange_front_face.outer_radius2;
	zpos = reentrant_flange_front_face.center_position.z() +
	    (0.5*reentrant_flange_front_face.length) +
	    (0.5*reentrant_flange_descender.length);
	reentrant_flange_descender.center_position = G4ThreeVector(0.0, 0.0, zpos);

	// Reentrant flange pipe
	zpos = reentrant_flange_descender.center_position.z() +
	    (0.5*reentrant_flange_descender.length) + (0.5*reentrant_flange_pipe.length);
	reentrant_flange_pipe.center_position = G4ThreeVector(0, 0, zpos);

	sd_frame.center_position =
	    G4ThreeVector(0.0, 0.0, reentrant_flange_front_face.center_position.z() +
	                  (0.5*reentrant_flange_front_face.length) +
	                  (0.5*sd_frame.depth));

	strip_detector.center_position = sd_frame.center_position;
	sd_inactive.center_position = sd_frame.center_position;

	// There are 4 of them with different x,y coordinates but the same
	// z-coordinate.  Define just that z-coordinate now
	zpos = sd_frame.center_position.z();
	zpos += (sd_frame.depth/2.0);
	zpos += (sd_mounting_screw_head.length/2.0);
	G4double off_center = 25.0*mm;        // x,y dimensions away from
	                                      // the axis
	sd_mounting_screw_head.center_position =
	    G4ThreeVector(off_center, off_center, zpos);
	teflon_front_face.inner_radius = scintillator.inner_radius;
	teflon_front_face.outer_radius = scintillator.outer_radius;

	zpos = sd_frame.center_position.z();
	zpos += (sd_frame.depth/2.0);
	zpos += sd_mounting_screw_head.length;
	zpos += (teflon_front_face.length/2.0);
	teflon_front_face.center_position = G4ThreeVector(0.0, 0.0, zpos);


	zpos = teflon_front_face.center_position.z();
	zpos += (0.5*teflon_front_face.length);
	zpos += (0.5*scintillator.length);
	scintillator.center_position = G4ThreeVector(0.0, 0.0, zpos);
	
	if(verbose)
	{
		G4cout << "*******************************************************" << G4endl;
		G4cout << "**************** Geometry Definitions: ****************" << G4endl;
		G4cout << "   Mirror thickness (perpendicular to face): "
		       << G4BestUnit(mirror.length, "Length") << G4endl;
		G4cout << "   Mirror center to chamber center: "
		       << G4BestUnit(mirror.center_position.z(), "Length") << G4endl;
		G4cout << "   Mirror mount / collimator length: "
		       << G4BestUnit(mirror_mount.length, "Length") << G4endl;
		G4cout << "   Be window thickness: "
		       << G4BestUnit(beryllium_window.length, "Length") << G4endl;
		G4cout << "   Strip detector thickness: "
		       << G4BestUnit(strip_detector.depth, "Length") << G4endl;
		G4cout << "   Strip detector front to chamber center: "
		       << G4BestUnit(strip_detector.center_position.z() -
		                     (0.5*strip_detector.depth), "Length") << G4endl;
		// G4cout << "   Teflon front to chamber center: "
		//        << G4BestUnit(teflon_front_face.center_position.z() -
		//                      (0.5*teflon_front_face.length), "Length")
		//        << G4endl;
		G4cout << "   Scintillator front to chamber center: "
		       << G4BestUnit(scintillator.center_position.z() -
		                     (0.5*scintillator.length), "Length") << G4endl;
		G4cout << "*******************************************************" << G4endl;
	}
}

void K37DetectorConstruction::ConstructScintillators() 
{
//	G4SDManager* SDman = G4SDManager::GetSDMpointer();
	
	// Use same solid for both detectors :-)
	scintillator_tubs_ = new G4Tubs("scint_sol", scintillator.inner_radius,
	                                scintillator.outer_radius,
	                                scintillator.length/2.0, 0.0, 2.0*M_PI);
	/*
	// Use same vis attributes for both detectors :-)
	scintillator_vis = new G4VisAttributes(G4Colour(0.11, 0.11, 0.44));
	scintillator_vis -> SetForceSolid(true);
	*/
	G4ThreeVector pos = -1.0*(scintillator.center_position - reentrant_flange_pipe.center_position);

	upper_scintillator_log_ =  new G4LogicalVolume(scintillator_tubs_,
	                                               FullEnergyDetectorMaterial,
	                                               "scint_plusZ_log", 0, 0, 0);

	upper_scintillator_phys_ = new G4PVPlacement(0, pos,
	                      upper_scintillator_log_, "scint_plusZ_phys",
	                      air_log_plus_, false, 0, check_all_for_overlaps_);
	/*
	upper_scintillator_log_ -> SetVisAttributes(scintillator_vis);
	*/
	lower_scintillator_log_ = new G4LogicalVolume(scintillator_tubs_,
	                                              FullEnergyDetectorMaterial,
	                                              "scint_minusZ_log", 0, 0, 0);
	lower_scintillator_phys_ = new G4PVPlacement(0, pos,
	                      lower_scintillator_log_, "scint_minusZ_phys",
	                      air_log_mins_, false, 0, check_all_for_overlaps_);
	/*
	lower_scintillator_log_ -> SetVisAttributes(scintillator_vis);
	*/
	/*
	// Set up sensitive detectors
	G4String fullenergy1SDname = "/mydet/scintillatorPlusZ";
	if (!upper_scintillator_sens_) 
	{
		upper_scintillator_sens_ = new K37ScintillatorSD(fullenergy1SDname);
		SDman->AddNewDetector(upper_scintillator_sens_);
	}
	upper_scintillator_log_ -> SetSensitiveDetector(upper_scintillator_sens_);
	G4String fullenergy2SDname = "/mydet/scintillatorMinusZ";
	if (!lower_scintillator_sens_) 
	{
		lower_scintillator_sens_ = new K37ScintillatorSD(fullenergy2SDname);
		SDman->AddNewDetector(lower_scintillator_sens_);
	}
	lower_scintillator_log_ -> SetSensitiveDetector(lower_scintillator_sens_);
	*/
	
	//  Teflon front face
	G4Tubs *teflon_tubs = new G4Tubs("teflon_tubs",
	                                 teflon_front_face.inner_radius,
	                                 teflon_front_face.outer_radius,
	                                 teflon_front_face.length/2.0,
	                                 0.0, 2*M_PI*rad);

	teflon_log = new G4LogicalVolume(teflon_tubs,
									 trinat_materials_.find("G4_TEFLON")->second,
									"teflon_front_face", 0, 0, 0);
//	G4LogicalVolume *teflon_log = new G4LogicalVolume(teflon_tubs,
//									trinat_materials_.find("G4_TEFLON") -> second,
//									"teflon_front_face", 0, 0, 0);
	/*
	// white
	teflon_vis = new G4VisAttributes(G4Colour(1.0, 1.0, 1.0));
	teflon_vis -> SetForceSolid(true);
	teflon_log -> SetVisAttributes(teflon_vis);
	*/
	pos = -1.0*(teflon_front_face.center_position - reentrant_flange_pipe.center_position);

	new G4PVPlacement(0, pos, teflon_log, "teflon_plus_z", air_log_plus_, false, 0, check_all_for_overlaps_);
	new G4PVPlacement(0, pos, teflon_log, "teflon_mins_z", air_log_mins_, false, 0, check_all_for_overlaps_);
}

void K37DetectorConstruction::ConstructStripDetectors() 
{
//	G4SDManager* SDman = G4SDManager::GetSDMpointer();
	
	// Same solid for both detectors :-)
	// Not anymore! :-))
	strip_detector_box_[0] = new G4Box("dedx_sol_0", strip_detector.length/2.0,
	                                   strip_detector.width/2.0,
	                                   (strip_detector.depth-bb1_bias)/2.0);
	strip_detector_box_[1] = new G4Box("dedx_sol_1", strip_detector.length/2.0,
	                                   strip_detector.width/2.0,
	                                   (strip_detector.depth+bb1_bias)/2.0);
	/*
	// Same vis for both detectors :-)
	strip_detector_vis = new G4VisAttributes(G4Colour(0.96, 0.96, 0.0, 0.5)); // yellow
	strip_detector_vis -> SetForceSolid(true);
	*/
	upper_strip_detector_log_ = new G4LogicalVolume(strip_detector_box_[0],
	                                                DeDxDetectorMaterial,
	                                                "dedx_plusZ_log", 0, 0, 0);
	/*
	upper_strip_detector_log_ -> SetVisAttributes(strip_detector_vis);
	*/
	G4ThreeVector pos = reentrant_flange_pipe.center_position - strip_detector.center_position;
	upper_strip_detector_phys_ = new G4PVPlacement(0,
	                                               pos,
	                                               upper_strip_detector_log_,
	                                               "dedx_plusZ_phys",
	                                               air_log_plus_, false, 0,
	                                               check_all_for_overlaps_);

	lower_strip_detector_log_ = new G4LogicalVolume(strip_detector_box_[1],
	                                                DeDxDetectorMaterial,
	                                                "dedx_MinusZ_log", 0,
	                                                0, 0);
	/*
	lower_strip_detector_log_ -> SetVisAttributes(strip_detector_vis);
	*/
	lower_strip_detector_phys_ = new G4PVPlacement(0,
	                                               pos,
	                                               lower_strip_detector_log_,
	                                               "dedx_minusZ_phys", air_log_mins_,
	                                               false, 0, check_all_for_overlaps_);
	/*
	G4String dedx1SDname = "/mydet/dsssdPlusZ";
	if (!upper_strip_detector_sens_) 
	{
		upper_strip_detector_sens_ = new K37StripDetectorSD(dedx1SDname);
		//                                                    pos_of_center, numStrips, stripWidth
		upper_strip_detector_sens_ -> SetupParameters(strip_detector.center_position, 40, 1.0*mm);
		SDman->AddNewDetector(upper_strip_detector_sens_);
	}
	upper_strip_detector_log_->SetSensitiveDetector(upper_strip_detector_sens_);
	G4String dedx2SDname = "/mydet/dsssdMinusZ";
	if (!lower_strip_detector_sens_) 
	{
		lower_strip_detector_sens_ = new K37StripDetectorSD(dedx2SDname);
		//                                                        pos_of_center, numStrips, stripWidth
		lower_strip_detector_sens_ -> SetupParameters(-1.0*strip_detector.center_position, 40, 1.0*mm);
		SDman->AddNewDetector(lower_strip_detector_sens_);
	}
	lower_strip_detector_log_->SetSensitiveDetector(lower_strip_detector_sens_);
	*/
	
	// ------------------------------ dedx mount
	if (make_sd_holders_) 
	{
		G4VSolid * dedx_holder_sol = new G4Box("dedx_holder_sol",
		                                       sd_frame.length/2.0,
		                                       sd_frame.width/2.0,
		                                       sd_frame.depth/2.0);
		G4VSolid * dedx_holder_cut_sol = new G4Box("dedx_holder_cut_sol",
		                                           sd_frame.cutout_side_length/2.0,
		                                           sd_frame.cutout_side_length/2.0,
		                                           sd_frame.cutout_depth/2.0);
		G4SubtractionSolid* dedxFrame_sol =
		    new G4SubtractionSolid("dedxFrame_sol", dedx_holder_sol, dedx_holder_cut_sol);
			
	//	G4LogicalVolume* dedxFrame_log = new G4LogicalVolume(dedxFrame_sol, SiliconDetectorFrameMaterial, "dedxFrame_log", 0, 0, 0);
		dedxFrame_log = new G4LogicalVolume(dedxFrame_sol, SiliconDetectorFrameMaterial, "dedxFrame_log", 0, 0, 0);
		/*
		dedxFrame_logVisAttributes = new G4VisAttributes(G4Colour(0.85, 0.65, 0.13));
		dedxFrame_logVisAttributes-> SetForceSolid(true);
		dedxFrame_log -> SetVisAttributes(dedxFrame_logVisAttributes);
		*/
		pos = reentrant_flange_pipe.center_position - sd_frame.center_position;
		      // + G4ThreeVector(0., 0., fabs(be_window_bias));

		new G4PVPlacement(0, pos, dedxFrame_log,
		                  "dedxFrame_plusZ_phys", air_log_plus_, false, 0,
		                  check_all_for_overlaps_);
		new G4PVPlacement(0, pos, dedxFrame_log,
		                  "dedxFrame_minusZ_phys", air_log_mins_, false, 0,
		                  check_all_for_overlaps_);
		// Add the inactive Si inside the frame
		G4Box *sd_inactive_box = new G4Box("sd_inactive_box",
		                                   sd_inactive.length/2.0,
		                                   sd_inactive.width/2.0,
		                                   sd_inactive.depth/2.0);
		G4SubtractionSolid *sd_inactive_sub = new G4SubtractionSolid("sd_inactive_sub",
																	  sd_inactive_box,
																	  strip_detector_box_[0]);
		sd_inactive_log = new G4LogicalVolume(sd_inactive_sub,
											  DeDxDetectorMaterial,
											  "sd_inactive_log");
	//	G4LogicalVolume *sd_inactive_log = new G4LogicalVolume(sd_inactive_sub,
	//	                                                       DeDxDetectorMaterial,
	//	                                                       "sd_inactive_log");
															   /*
		sd_inactive_vis = new G4VisAttributes(G4Colour(0.5, 0.5, 0.0));
		sd_inactive_vis -> SetForceSolid(true);
		sd_inactive_log -> SetVisAttributes(sd_inactive_vis);
		*/
		pos = reentrant_flange_pipe.center_position - sd_inactive.center_position;
		new G4PVPlacement(0, pos, sd_inactive_log,
		                  "sd_inactive_plusZphys", air_log_plus_, false, 0,
		                  check_all_for_overlaps_);
		new G4PVPlacement(0, pos, sd_inactive_log,
		                  "sd_inactive_minsZphys", air_log_mins_, false, 0,
		                  check_all_for_overlaps_);
		// Add the heads of the mounting screws that the scintillator
		// rests against
		G4Tubs *mounting_screw_head = new G4Tubs("mounting_screw_head_tub",
		                                         sd_mounting_screw_head.inner_radius,
		                                         sd_mounting_screw_head.outer_radius,
		                                         sd_mounting_screw_head.length/2.0, 0, 2*M_PI);
		
		mounting_screw_head_log = new G4LogicalVolume(mounting_screw_head, ChamberMaterial, "mounting_screw_head_log");
	//	G4LogicalVolume *mounting_screw_head_log = new G4LogicalVolume(mounting_screw_head, ChamberMaterial, "mounting_screw_head_log");
		/*
		mounting_screw_head_vis = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
		mounting_screw_head_vis -> SetForceSolid(true);
		mounting_screw_head_log -> SetVisAttributes(mounting_screw_head_vis);
		*/
		// Place them!
		G4double xpos, ypos, zpos;
		xpos = sd_mounting_screw_head.center_position.x();
		ypos = sd_mounting_screw_head.center_position.y();
		zpos = reentrant_flange_pipe.center_position.z() -
		    sd_mounting_screw_head.center_position.z();
		new G4PVPlacement(0, G4ThreeVector(xpos, ypos, zpos),
		                  mounting_screw_head_log, "screwHeadP1",
		                  air_log_plus_, false, 0, check_all_for_overlaps_);
		new G4PVPlacement(0, G4ThreeVector(xpos, ypos, zpos),
		                  mounting_screw_head_log, "screwHeadM1",
		                  air_log_mins_, false, 0, check_all_for_overlaps_);
		xpos *= -1.0;
		new G4PVPlacement(0, G4ThreeVector(xpos, ypos, zpos),
		                  mounting_screw_head_log, "screwHeadP2",
		                  air_log_plus_, false, 0, check_all_for_overlaps_);
		new G4PVPlacement(0, G4ThreeVector(xpos, ypos, zpos),
		                  mounting_screw_head_log, "screwHeadM2",
		                  air_log_mins_, false, 0, check_all_for_overlaps_);
		ypos *= -1.0;
		new G4PVPlacement(0, G4ThreeVector(xpos, ypos, zpos),
		                  mounting_screw_head_log, "screwHeadP3",
		                  air_log_plus_, false, 0, check_all_for_overlaps_);
		new G4PVPlacement(0, G4ThreeVector(xpos, ypos, zpos),
		                  mounting_screw_head_log, "screwHeadM3",
		                  air_log_mins_, false, 0, check_all_for_overlaps_);
		
		xpos *= -1.0;
		new G4PVPlacement(0, G4ThreeVector(xpos, ypos, zpos),
		                  mounting_screw_head_log, "screwHeadP4",
		                  air_log_plus_, false, 0, check_all_for_overlaps_);
		new G4PVPlacement(0, G4ThreeVector(xpos, ypos, zpos),
		                  mounting_screw_head_log, "screwHeadM4",
		                  air_log_mins_, false, 0, check_all_for_overlaps_);
	}
}  // End construct strip detectors

void K37DetectorConstruction::ConstructAir() 
{
	G4NistManager* nist_manager = G4NistManager::Instance();
	// Air is needed to fill the space between the strip detector and
	// scintillator.  I will be constructed first to fill the entire
	G4Tubs *reentrant_pipe = new G4Tubs("air_in_pipe",
	                                    0.0,
	                                    reentrant_flange_pipe.inner_radius,
	                                    reentrant_flange_pipe.length/2.0,
	                                    0.0, 2*M_PI*rad);

	G4Cons *reentrant_des = new G4Cons("air_in_descender",
	                                   0.0,
	                                   reentrant_flange_descender.inner_radius2,
	                                   0.0,
	                                   reentrant_flange_descender.inner_radius,
	                                   (reentrant_flange_descender.length)/2.0,
	                                   0.0, 2.0*M_PI*rad);
	G4UnionSolid *reentrant_air =
	    new G4UnionSolid("air_in_reentrant", reentrant_pipe, reentrant_des, 0,
	                     G4ThreeVector(0.0, 0.0,
	                                   0.5*(reentrant_flange_pipe.length +
	                                        reentrant_flange_descender.length)));
	air_log_plus_ =
	    new G4LogicalVolume(reentrant_air,
	                        nist_manager -> FindOrBuildMaterial("G4_AIR"),
	                        "air_in_reentrant_plus", 0, 0, 0);
	air_log_mins_ =
	    new G4LogicalVolume(reentrant_air,
	                        nist_manager -> FindOrBuildMaterial("G4_AIR"),
	                        "air_in_reentrant_mins", 0, 0, 0);
	
	/*
	//	air_vis = new G4VisAttributes(false); // invisible
	air_vis = new G4VisAttributes(G4Colour(0.53, 0.80, 0.98, 0.1)); // sky blue
	air_vis -> SetForceSolid(true);
	air_log_plus_ -> SetVisAttributes(air_vis);
	air_log_mins_ -> SetVisAttributes(air_vis);
	*/
	G4ThreeVector pos = reentrant_flange_pipe.center_position;


	zRot = new CLHEP::HepRotation;
	zRot -> rotateY(180.0*deg);

	new G4PVPlacement(zRot, pos, air_log_plus_, "air_plus_z", world_log_,
	                  false, 0, check_all_for_overlaps_);

	new G4PVPlacement(0, -1.0*pos, air_log_mins_, "air_mins_z", world_log_,
	                  false, 0, check_all_for_overlaps_);
}


// What's the difference between K37DetectorConstruction::ConstructSimpleChamber()
// and K37DetectorConstruction::ConstructChamber() ??
// Simple Chamber: 
//    G4Tubs* Chamber_sol, RentrantCut_sol, CFOPB_sol, OPRF_sol, RFD_sol, 
//    G4SubtractionSolid* ChamberCut0_sol, ChamberCut1_sol, ChamberCut2_sol, 
//    also, logical volumes to go with the cuts???
// Chamber:
//    G4Box* chamberBlock_sol
//    G4Tubs* CFTVDP_sol, CFTMCPS_sol, CFTMOT1_sol, CFTMOT2_sol, CFTSDP1_sol, CFTSDP2_sol, CFTLDP1_sol, CFTLDP2_sol
//    G4SubtractionSolid* ChamberCut1_sol, ChamberCut2_sol, ChamberCut3_sol, ChamberCut4_sol, ChamberCut5_sol, ChamberCut6_sol, ChamberCut7_sol
//    ....
//    G4Tubs* CFPFFT_sol, CFLPP_sol, 
//    G4SubtractionSolid* ChamberCut9_sol, ChamberCut10_sol
//    .... and more!

void K37DetectorConstruction::ConstructSimpleChamber()
{
	G4Tubs* Chamber_sol = new G4Tubs("Chamber_sol",
	      simpleChamberTub.inner_radius,
	      simpleChamberTub.outer_radius,
	      simpleChamberTub.length/2.0,
	      0.0*deg,
	      360.0*deg);

	G4Tubs* RentrantCut_sol = new G4Tubs("RentrantCut_sol",
	      simpleChamberRentrantFlangeCut.inner_radius,
	      simpleChamberRentrantFlangeCut.outer_radius,
	      simpleChamberRentrantFlangeCut.length/2.0,
	      0.0*deg,
	      360.0*deg);

	// ------------------------------ cut for optical pumping beams (CFOPB)
	G4Tubs* CFOPB_sol = new G4Tubs("CFOPB_sol", 0.0, (38.10/2.0)*mm,
	                               (simpleChamberTub.outer_radius -
	                                simpleChamberTub.inner_radius)*4, 0.0*deg,
	                               360.0*deg);
	rotationForOpticalPumpingBeams1 = new CLHEP::HepRotation();
	rotationForOpticalPumpingBeams1->rotateX(-19*deg);


//	G4LogicalVolume *temp_log = new G4LogicalVolume(CFOPB_sol, 
//													ChamberMaterial,
//													"temp_log", 0, 0, 0);
//												   
	temp_log = new G4LogicalVolume(CFOPB_sol, ChamberMaterial, "temp_log", 0, 0, 0);

	// X=0 because we are in the YZ plane Z=170 is arbitrary
	// Y=(tan19)*Z + 29.2655 where 29.2655 is the constant to make it cross
	// Z at Z=-85mm the mirror position
  
	rotationForOpticalPumpingBeams2 = new CLHEP::HepRotation();
	rotationForOpticalPumpingBeams2->rotateX(19*deg);
	G4ThreeVector positionOfOpticalPumpingBeams2(0.0*mm,
	                                             ((0.3443*-170.0)-29.2655)*mm,
	                                             -170.0*mm);
	// The formula is almost the same as above but the sign of the constant
	// is flipped so that it crosses the Zaxis at Z=85mm the other mirror.
	// ------------------------------------------------------------------------
	G4ThreeVector noChange(0, 0, 0);

	G4SubtractionSolid* ChamberCut0_sol = new G4SubtractionSolid("ChamberCut0_sol",
																  Chamber_sol, RentrantCut_sol, changeZtoX, noChange);

	CLHEP::HepRotation* temp_rot = new CLHEP::HepRotation();
	double nudge = 1.2;
	G4ThreeVector positionOfOpticalPumpingBeams1(0.0*mm, -nudge*(((0.3443*170.0)+29.2655)*mm), nudge*simpleChamberTub.inner_radius);
	G4ThreeVector temp_pos(0, -positionOfOpticalPumpingBeams1.z()/nudge, positionOfOpticalPumpingBeams1.y()/nudge );
	temp_rot -> rotateX(90*deg);
	temp_rot -> rotateX(-19*deg);

	G4SubtractionSolid* ChamberCut1_sol =
		new G4SubtractionSolid("ChamberCut1_sol",
								ChamberCut0_sol, CFOPB_sol,
								temp_rot,
								temp_pos);

	temp_pos.set(0, positionOfOpticalPumpingBeams1.z()/nudge, -positionOfOpticalPumpingBeams1.y()/nudge);

	G4SubtractionSolid* ChamberCut2_sol =
	    new G4SubtractionSolid("ChamberCut2_sol", ChamberCut1_sol, CFOPB_sol, temp_rot, temp_pos);

	chamber_log_ = new G4LogicalVolume(ChamberCut2_sol, ChamberMaterial, "chamber_log", 0, 0, 0);

	chamber_phys_ = new G4PVPlacement(changeZtoX,
									  noChange,
									  chamber_log_,
									  "chamber_phys",
									  world_log_,
									  false,
									  0,
									  check_all_for_overlaps_);
	
	/*
	chamber_logVisAttributes = new G4VisAttributes( G4Colour(0.96, 0.96, 0.96, 0.1) );
	temp_log -> SetVisAttributes(chamber_logVisAttributes);
	//chamber_logVisAttributes-> SetForceWireframe(true);
	chamber_logVisAttributes-> SetForceSolid(true);
	chamber_log_ -> SetVisAttributes(chamber_logVisAttributes);
	*/
	
	// ------------------------------ Reentrant Flanges for beta-telescope (z+ and z-) (OPRF)
	G4VSolid * OPRF_sol = new G4Tubs("OPRF_sol",
	                                 reentrant_flange_pipe.inner_radius,
	                                 reentrant_flange_pipe.outer_radius,
	                                 reentrant_flange_pipe.length/2.0,
	                                 0.0*deg, 360.0*deg);

//	G4LogicalVolume * OPRF_log = new G4LogicalVolume(OPRF_sol,
//	                                                 ChamberMaterial,
//	                                                 "OPRF_log", 0, 0, 0);

	OPRF_log = new G4LogicalVolume(OPRF_sol,
								   ChamberMaterial,
								   "OPRF_log", 0, 0, 0);
	//
	new G4PVPlacement(0, reentrant_flange_pipe.center_position, OPRF_log,
	                  "OPRF_plusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
	new G4PVPlacement(0, -1.0*reentrant_flange_pipe.center_position, OPRF_log,
	                  "OPRF_minusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
	/*
	OPRF_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
	OPRF_logVisAttributes-> SetForceSolid(true);
	OPRF_log -> SetVisAttributes(OPRF_logVisAttributes);
	*/
	// ------------------------------ Reentrant Flange Descender (RFD)
	G4VSolid * RFD_sol = new G4Cons("RFD_sol",
	                                reentrant_flange_descender.inner_radius,
	                                reentrant_flange_descender.outer_radius,
	                                reentrant_flange_descender.inner_radius2,
	                                reentrant_flange_descender.outer_radius2,
	                                reentrant_flange_descender.length/2.0,
	                                0.0*deg, 360.0*deg);
									
	RFD_log = new G4LogicalVolume(RFD_sol, ChamberMaterial, "RFD_log", 0, 0, 0);
	/*
	G4LogicalVolume* RFD_log = new G4LogicalVolume(RFD_sol, ChamberMaterial, "RFD_log", 0, 0, 0);
	RFD_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
	RFD_logVisAttributes-> SetForceSolid(true);
	RFD_log -> SetVisAttributes(RFD_logVisAttributes);
	*/
	new G4PVPlacement(0, reentrant_flange_descender.center_position,
	                  RFD_log, "RFD_plusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);

	RFDRotation = new CLHEP::HepRotation();
	RFDRotation->rotateX(180.*deg);

	new G4PVPlacement(RFDRotation,
	                  -1.0*reentrant_flange_descender.center_position,
	                  RFD_log, "RFD_minusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
}

void K37DetectorConstruction::ConstructChamber() 
{
	G4double chamberBlock_x = 558.8*mm;
	G4double chamberBlock_y = 457.2*mm;
	G4double chamberBlock_z = 508.0*mm;

	// Trap center will be at (0,0,0) positive Z is along the direction of
	// the beta detectors going up.
	// Y is going into the pumping port and X is coming out toward the
	// computers in the TRINAT lab.

	G4Box* chamberBlock_sol = new G4Box("chamberBlock_sol", (chamberBlock_x/2.),
	                                    (chamberBlock_y/2.), (chamberBlock_z/2.));

	// ------------------------------ Cut for the vertical Detectror Ports (CFTVDP)
	G4Tubs* CFTVDP_sol = new G4Tubs("CFTVDP_sol", 0.0, (106.68/2.0)*mm,
	                                (510.0/2.)*mm, 0.0*deg, 360.0*deg);

	G4SubtractionSolid* ChamberCut1_sol =
		new G4SubtractionSolid("ChamberCut1_sol", chamberBlock_sol, CFTVDP_sol);

	// ------------------------------ Cut for the MCPs (CFTMCPS)
	G4Tubs* CFTMCPS_sol = new G4Tubs("CFTMCPS_sol", 0.0, (298.45/2.0)*mm,
	                                 (560.0/2.)*mm, 0.0*deg, 360.0*deg);
	G4ThreeVector noChange(0, 0, 0);
	G4SubtractionSolid* ChamberCut2_sol =
		new G4SubtractionSolid("ChamberCut2_sol", ChamberCut1_sol, CFTMCPS_sol,
	                            changeZtoX, noChange);

	// ------------------------------ Cut for the 45 deg MOT pipes (CFTMOT)
	G4Tubs* CFTMOT1_sol = new G4Tubs("CFTMOT1_sol", 0.0, (38.10/2.0)*mm,
	                                 (850.0/2.)*mm, 0.0*deg, 360.0*deg);

	changeZto45 = new CLHEP::HepRotation();
	changeZto45->rotateY(90.0*deg);
	changeZto45->rotateX(45.*deg);

	G4SubtractionSolid* ChamberCut3_sol =
		new G4SubtractionSolid("ChamberCut3_sol", ChamberCut2_sol, CFTMOT1_sol,
	                            changeZto45, noChange);

	G4Tubs* CFTMOT2_sol = new G4Tubs("CFTMOT2_sol", 0.0, (38.10/2.0)*mm,
	                                 (850.0/2.)*mm, 0.0*deg, 360.0*deg);

	changeZtoNeg45 = new CLHEP::HepRotation();
	changeZtoNeg45->rotateY(90.0*deg);
	changeZtoNeg45->rotateX(-45.0*deg);

	G4SubtractionSolid* ChamberCut4_sol =
		new G4SubtractionSolid("ChamberCut4_sol", ChamberCut3_sol, CFTMOT2_sol,
	                            changeZtoNeg45, noChange);

	// ------------------------------ Cut for the small diagnostic ports (CFTSDP)
	G4Tubs* CFTSDP1_sol = new G4Tubs("CFTSDP1_sol", 0.0, (38.10/2.0)*mm,
	                                 (850.0/2.)*mm, 0.0*deg, 360.0*deg);

	changeZto35 = new CLHEP::HepRotation();
	changeZto35->rotateY(35.0*deg);

	G4SubtractionSolid* ChamberCut5_sol =
		new G4SubtractionSolid("ChamberCut5_sol", ChamberCut4_sol, CFTSDP1_sol,
	                            changeZto35, noChange);

	G4Tubs* CFTSDP2_sol = new G4Tubs("CFTSDP2_sol", 0.0, (38.10/2.0)*mm,
	                                 (850.0/2.)*mm, 0.0*deg, 360.0*deg);

	changeZtoNeg35 = new CLHEP::HepRotation();
	changeZtoNeg35->rotateY(-35.0*deg);

	G4SubtractionSolid* ChamberCut6_sol =
		new G4SubtractionSolid("ChamberCut6_sol", ChamberCut5_sol, CFTSDP2_sol,
	                            changeZtoNeg35, noChange);

	// ------------------------------ Cut for the large diagnostic ports (CFTLDP)
	G4Tubs* CFTLDP1_sol = new G4Tubs("CFTLDP1_sol", 0.0, (76.20/2.0)*mm,
	                                 (450.0/2.)*mm, 0.0*deg, 360.0*deg);

	changeZtoNeg62 = new CLHEP::HepRotation();
	changeZtoNeg62->rotateY(-62.0*deg);

	G4ThreeVector cutFromTrapTowardPositveY62(100*mm, 0, (.531709*100)*mm);

	G4SubtractionSolid* ChamberCut7_sol =
		new G4SubtractionSolid("ChamberCut7_sol", ChamberCut6_sol, CFTLDP1_sol,
	                            changeZtoNeg62, cutFromTrapTowardPositveY62);

	G4Tubs* CFTLDP2_sol = new G4Tubs("CFTLDP2_sol", 0.0, (76.20/2.0)*mm,
	                                 (450.0/2.)*mm, 0.0*deg, 360.0*deg);

	changeZtoNeg118 = new CLHEP::HepRotation();
	changeZtoNeg118->rotateY(-118.0*deg);

	G4ThreeVector cutFromTrapTowardPositveY118(100*mm, 0, (-.531709*100)*mm);

	G4SubtractionSolid* ChamberCut8_sol =
		new G4SubtractionSolid("ChamberCut8_sol", ChamberCut7_sol, CFTLDP2_sol,
	                            changeZtoNeg118, cutFromTrapTowardPositveY118);

	// ------------------------------ Cut for pipe from first trap (CFPFFT)
	G4Tubs* CFPFFT_sol = new G4Tubs("CFPFFT_sol", 0.0, 9.53*mm,
	                                (560/2.)*mm, 0.0*deg, 360.0*deg);

	changeYtoBeamAxis = new CLHEP::HepRotation();
	changeYtoBeamAxis->rotateY(90*deg);

	G4SubtractionSolid* ChamberCut9_sol =
		new G4SubtractionSolid("ChamberCut9_sol", ChamberCut8_sol, CFPFFT_sol,
	                            changeYtoBeamAxis, noChange);

	// ------------------------------ Cut for large pumping port (CFLPP)
	G4Tubs* CFLPP_sol = new G4Tubs("CFLPP_sol", 0.0, 25.4*mm , (285/2.)*mm,
	                               0.0*deg, 360.0*deg);

	changeYtoBeamAxisForLPP1 = new CLHEP::HepRotation();
	changeYtoBeamAxisForLPP1 -> rotateY(90*deg);

	G4ThreeVector positionForPumpingPortChamfer1(-160.0*mm, (152.4/2.0)*mm, (152.4/2.0)*mm);

	G4SubtractionSolid* ChamberCut10_sol =
		new G4SubtractionSolid("ChamberCut10_sol", ChamberCut9_sol, CFLPP_sol,
								changeYtoBeamAxisForLPP1,
								positionForPumpingPortChamfer1);

	changeYtoBeamAxisForLPP2 = new CLHEP::HepRotation();
	changeYtoBeamAxisForLPP2 -> rotateY(90*deg);

	G4ThreeVector positionForPumpingPortChamfer2(-160.0*mm, (-152.4/2.0)*mm, (152.4/2.0)*mm);

	G4SubtractionSolid* ChamberCut11_sol =
		new G4SubtractionSolid("ChamberCut11_sol", ChamberCut10_sol, CFLPP_sol,
								changeYtoBeamAxisForLPP2,
								positionForPumpingPortChamfer2);

	changeYtoBeamAxisForLPP3 = new CLHEP::HepRotation();
	changeYtoBeamAxisForLPP3 -> rotateY(90*deg);

	G4ThreeVector positionForPumpingPortChamfer3(-160.0*mm, (-152.4/2.0)*mm, (-152.4/2.0)*mm);

	G4SubtractionSolid* ChamberCut12_sol =
		new G4SubtractionSolid("ChamberCut12_sol", ChamberCut11_sol,
								CFLPP_sol, changeYtoBeamAxisForLPP3,
								positionForPumpingPortChamfer3);

	changeYtoBeamAxisForLPP4 = new CLHEP::HepRotation();
	changeYtoBeamAxisForLPP4 -> rotateY(90*deg);

	G4ThreeVector positionForPumpingPortChamfer4(-160.0*mm, (152.4/2.0)*mm, (-152.4/2.0)*mm);

	G4SubtractionSolid* ChamberCut13_sol =
		new G4SubtractionSolid("ChamberCut13_sol", ChamberCut12_sol, CFLPP_sol,
								changeYtoBeamAxisForLPP4,
								positionForPumpingPortChamfer4);

	// ------------------------------ central box cuts for large pumping port
	G4Box* CFLPPBoxCut1_sol = new G4Box("CFLPPBoxCut1_sol", (285/2.)*mm, (152.4/2.)*mm, (203.2/2.)*mm);

	G4ThreeVector positionForPumpingPortChamfer5(-160.0*mm, 0.0 , 0.0);

	G4SubtractionSolid* ChamberCut14_sol =
		new G4SubtractionSolid("ChamberCut14_sol", ChamberCut13_sol,
	                            CFLPPBoxCut1_sol, 0,
	                            positionForPumpingPortChamfer5);

	G4Box* CFLPPBoxCut2_sol = new G4Box("CFLPPBoxCut2_sol", (285/2.)*mm, (203.2/2.)*mm, (152.4/2.)*mm);

	G4SubtractionSolid* ChamberCut15_sol =
		new G4SubtractionSolid("ChamberCut15_sol", ChamberCut14_sol,
	                            CFLPPBoxCut2_sol, 0, positionForPumpingPortChamfer5);

	// ------------------------------ cut for optical pumping beams (CFOPB)
	G4Tubs* CFOPB_sol = new G4Tubs("CFOPB_sol", 0.0, (38.10/2.0)*mm,
	                               (300/2.)*mm, 0.0*deg, 360.0*deg);
	rotationForOpticalPumpingBeams1 = new CLHEP::HepRotation();
	rotationForOpticalPumpingBeams1->rotateX(19*deg);
	G4ThreeVector positionOfOpticalPumpingBeams1(0.0*mm,
	                                             ((0.3443*170.0)+29.2655)*mm,
	                                             170.0*mm);
	// X=0 because we are in the YZ plane Z=170 is arbitrary
	// Y=(tan19)*Z + 29.2655 where 29.2655 is the constant to make it cross
	// Z at Z=-85mm the mirror position

	G4SubtractionSolid* ChamberCut16_sol = new G4SubtractionSolid("ChamberCut16_sol", 
																   ChamberCut15_sol, 
																   CFOPB_sol,
																   rotationForOpticalPumpingBeams1,
																   positionOfOpticalPumpingBeams1);

	rotationForOpticalPumpingBeams2 = new CLHEP::HepRotation();
	rotationForOpticalPumpingBeams2->rotateX(19*deg);

	G4ThreeVector positionOfOpticalPumpingBeams2(0.0*mm,
	                                             ((0.3443*-170.0)-29.2655)*mm,
	                                             -170.0*mm);
	// The formula is almost the same as above but the sign of the constant
	// is flipped so that it crosses the Zaxis at Z=85mm the other mirror.

	G4SubtractionSolid* ChamberCut17_sol = new G4SubtractionSolid("ChamberCut17_sol", 
																   ChamberCut16_sol, 
																   CFOPB_sol,
																   rotationForOpticalPumpingBeams2,
																   positionOfOpticalPumpingBeams2);

	chamber_box_ = ChamberCut17_sol;      // Store the final product
	
	// ------------------------------ Final Product after all of the cuts.
	chamber_log_ = new G4LogicalVolume(chamber_box_, ChamberMaterial,
	                                   "chamber_log_", 0, 0, 0);
	y180 = new G4RotationMatrix( G4ThreeVector(0.0, 1.0, 0.0), 180.*deg );
	chamber_phys_ = new G4PVPlacement(y180, G4ThreeVector(0.0, 0.0, 0.0),
	                                  chamber_log_, "chamber_phys", world_log_,
	                                  false, 0, check_all_for_overlaps_);
	/*
	// G4VisAttributes(false) means invisible. It is better than using the
	// convience function G4VisAttributes::Invisble becuse it produces
	// a non const pointer that can later be delete avoiding a memory
	// leak.
	//chamber_logVisAttributes = new G4VisAttributes(false);
	chamber_logVisAttributes = new G4VisAttributes(G4Colour(255/255., 170/255., 0, 0.1));

	chamber_logVisAttributes-> SetForceSolid(true);
	chamber_log_ -> SetVisAttributes(chamber_logVisAttributes);
	*/
	
	// ------------------------------ Reentrant Flanges for beta-telescope
	// (z+ and z-) (OPRF)
	G4VSolid * OPRF_sol = new G4Tubs("OPRF_sol",
	                                 reentrant_flange_pipe.inner_radius,
	                                 reentrant_flange_pipe.outer_radius,
	                                 reentrant_flange_pipe.length/2.0,
	                                 0.0*deg, 360.0*deg);

//	G4LogicalVolume * OPRF_log = new G4LogicalVolume(OPRF_sol,
//	                                                 ChamberMaterial,
//	                                                 "OPRF_log", 0, 0, 0);
	OPRF_log = new G4LogicalVolume(OPRF_sol,
								   ChamberMaterial,
								   "OPRF_log", 0, 0, 0);

	new G4PVPlacement(0, reentrant_flange_pipe.center_position, OPRF_log,
	                  "OPRF_plusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
	new G4PVPlacement(0, -1.0*reentrant_flange_pipe.center_position, OPRF_log,
	                  "OPRF_minusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
	/*
	OPRF_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
	OPRF_logVisAttributes-> SetForceSolid(true);
	OPRF_log -> SetVisAttributes(OPRF_logVisAttributes);
	*/

	// ------------------------------ Reentrant Flange Descender (RFD)
	G4VSolid * RFD_sol = new G4Cons("RFD_sol",
	                                reentrant_flange_descender.inner_radius,
	                                reentrant_flange_descender.outer_radius,
	                                reentrant_flange_descender.inner_radius2,
	                                reentrant_flange_descender.outer_radius2,
	                                reentrant_flange_descender.length/2.0,
	                                0.0*deg, 360.0*deg);
	//
	RFD_log = new G4LogicalVolume(RFD_sol, ChamberMaterial, "RFD_log", 0, 0, 0);
	/*
	G4LogicalVolume* RFD_log = new G4LogicalVolume(RFD_sol, ChamberMaterial, "RFD_log", 0, 0, 0);
	RFD_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
	RFD_logVisAttributes-> SetForceSolid(true);
	RFD_log -> SetVisAttributes(RFD_logVisAttributes);
	*/
	new G4PVPlacement(0, reentrant_flange_descender.center_position,
	                  RFD_log, "RFD_plusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);

	RFDRotation = new CLHEP::HepRotation();
	RFDRotation->rotateX(180.*deg);

	new G4PVPlacement(RFDRotation,
	                  -1.0*reentrant_flange_descender.center_position,
	                  RFD_log, "RFD_minusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
}  // End construct chamber

void K37DetectorConstruction::ConstructBerylliumFoils() 
{
//	G4SDManager* SDman = G4SDManager::GetSDMpointer();
	
	// ------------------------------ beryllium declared here so that a socket could be cut out for it
	/*
	beryllium_logVisAttributes = new G4VisAttributes(G4Colour(0.80, 0.36, 0.36, 0.8)); // pale red
	beryllium_logVisAttributes -> SetForceSolid(true);
	*/
	
	G4VSolid *beryllium_sol[2];
	for (int i = 0; i < 2; i++) 
	{
		G4double doublei = static_cast<double>(i);
		G4double thislength = beryllium_window.length + ((2*doublei)-1.0)*be_window_bias;
		G4String name = "beryllium_sol_" + to_string(i);
		beryllium_sol[i] = new G4Tubs(name,
		                              beryllium_window.inner_radius,
		                              beryllium_window.outer_radius,
		                              thislength/2.0,
		                              0.0*rad, 2*M_PI*rad);
		name = "beryllium_log_" + to_string(i);
		beryllium_log[i] = new G4LogicalVolume(beryllium_sol[i],
		                                       FoilMaterial,
		                                       name, 0,0,0 );
		/*
		beryllium_log[i] -> SetVisAttributes(beryllium_logVisAttributes);
		*/
		/*
		// Set up primitive scorer for beryllium
		if (!be_scorer_) 
		{
			be_scorer_ = new G4MultiFunctionalDetector("BeWindowEnergyScorer");
			G4VPrimitiveScorer *edep_sens = new G4PSEnergyDeposit("Edep");
			be_scorer_ -> RegisterPrimitive(edep_sens);
			SDman -> AddNewDetector(be_scorer_);
		}
		beryllium_log -> SetSensitiveDetector(be_scorer_);
		*/
		if (i == 0) 
		{
			name = "beryllium_plusZ_phys";
		} 
		else 
		{
			name = "beryllium_minusZ_phys";
		}
		new G4PVPlacement(0, ((-2.*i) + 1.)*beryllium_window.center_position,
		                  beryllium_log[i], name, world_log_, false,
		                  0, check_all_for_overlaps_);
	}

	// ------------------------------ Front Face Reentrant Flange (FFRF)
	G4VSolid * FFRF_beforeCut_sol =
		new G4Cons("FFRF_beforeCut_sol",
	               reentrant_flange_front_face.inner_radius,
	               reentrant_flange_front_face.outer_radius,
	               reentrant_flange_front_face.inner_radius2,
	               reentrant_flange_front_face.outer_radius2,
	               reentrant_flange_front_face.length/2.0,
	               0.0*deg, 360.0*deg);

	G4ThreeVector cutForBeryllium(0.0, 0.0,
	      0.5*(reentrant_flange_front_face.length - beryllium_window.length));

	G4SubtractionSolid* FFRF_sol =
	  new G4SubtractionSolid("FFRF_sol", FFRF_beforeCut_sol, beryllium_sol[0], 0,
	                         cutForBeryllium);

//	G4LogicalVolume* FFRF_log = new G4LogicalVolume(FFRF_sol, ChamberMaterial, "FFRF_log", 0, 0, 0);
	FFRF_log = new G4LogicalVolume(FFRF_sol, ChamberMaterial, "FFRF_log", 0, 0, 0);
	/*
	FFRF_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
	FFRF_logVisAttributes-> SetForceSolid(true);
	FFRF_log -> SetVisAttributes(FFRF_logVisAttributes);
	*/
	new G4PVPlacement(0,
	                  reentrant_flange_front_face.center_position,
	                  FFRF_log, "FFRF_plusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);

	FFRFRotation = new CLHEP::HepRotation();
	FFRFRotation -> rotateX(180.*deg);

	new G4PVPlacement(FFRFRotation,
	                  -1.0*reentrant_flange_front_face.center_position,
	                  FFRF_log, "FFRF_minusZ_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
}
void K37DetectorConstruction::ConstructMirrors() 
{
	bool verbose = false;
	
	// SiC mirrors to reflect OP light
	mirror_rotation = new CLHEP::HepRotation();
	mirror_rotation -> rotateX(mirror.rotation_angle);

	G4Tubs *mirror_sol[2];
	
	// Set up primitive scorer for mirror
//	G4MultiFunctionalDetector *mirror_scorer = new G4MultiFunctionalDetector("MirrorEnergyScorer");
//	G4VPrimitiveScorer *edep_sens = new G4PSEnergyDeposit("Edep");
//	mirror_scorer -> RegisterPrimitive(edep_sens);

	for (int i = 0; i < 2; i++) 
	{
		G4double amount_to_bias = (static_cast<double>(i)*2.0)-1.0;
		G4double this_mirror_length = mirror.length + (amount_to_bias*mirror_length_bias);
		if(verbose)
		{
			G4cout << "mirror.length: " << mirror.length << G4endl;\
			G4cout << "(2i-1)*mirror_length_bias: "
				   << (amount_to_bias*mirror_length_bias) << G4endl;
		}
		G4String tubname = "mirror_" + to_string(i);
		mirror_sol[i] = new G4Tubs(tubname, mirror.inner_radius,
		                           mirror.outer_radius, this_mirror_length/2.0, 0.0*deg, 360.0*deg);
		
		if(verbose)
		{
			G4cout << "MIRROR SOL" << i << " has length "
				   << G4BestUnit(mirror_sol[i] -> GetZHalfLength()*2, "Length") << G4endl;
		}
		G4String logname = "mirror_log_" + to_string(i);
		mirror_log[i] = new G4LogicalVolume(mirror_sol[i], MirrorMaterial, logname, 0, 0, 0);
		mirror_logVisAttributes[i] = new G4VisAttributes(G4Colour(0.9, 0.1, 0.1, 0.9));
		mirror_logVisAttributes[i] -> SetForceSolid(true);
		mirror_log[i] -> SetVisAttributes(mirror_logVisAttributes[i]);
		
	//	mirror_log[i] -> SetSensitiveDetector(mirror_scorer);
	//	G4SDManager::GetSDMpointer() -> AddNewDetector(mirror_scorer);
		
		G4String placename = "";
		if (i == 0) 
		{
			placename = "mirror_plusZ_phys";
		} 
		else 
		{
			placename = "mirror_minusZ_phys";
		}
		new G4PVPlacement(mirror_rotation, ((-2*i)+1)*mirror.center_position,
		                  mirror_log[i], placename, world_log_, false, i,
		                  check_all_for_overlaps_);
	}
	
	// ------------------------------ Mirror Mount (MM)
	G4Tubs *mirror_mount_tub =
		new G4Tubs("mirror_mount_tub", mirror_mount.inner_radius,
	                mirror_mount.outer_radius, mirror_mount.length/2.0, 0.0,
	                2.*M_PI);
	// make the box slightly bigger in the z-direction for safety
	G4Box *collimator_box = new G4Box("collimator_box",
	                                  mirror_mount.cutout_side_length/2.0,
	                                  mirror_mount.cutout_side_length/2.0,
	                                  1.05*mirror_mount.length/2.0);

	G4Tubs *mount_tubs = new G4Tubs("collimator_tubs",
	                                0.0, mirror_mount.cutout_radius,
	                                mirror_mount.cutout_depth/2.0, 0.0, 2*M_PI);

	G4SubtractionSolid* mirror_mount_sub =
		new G4SubtractionSolid("MM_sol", mirror_mount_tub, collimator_box);

	G4SubtractionSolid* mirror_mount_final =
		new G4SubtractionSolid("MM_final", mirror_mount_sub, mount_tubs,
	                           mirror_rotation, G4ThreeVector(0, 0, 0));

	mirror_mount_log = new G4LogicalVolume(mirror_mount_final, MirrorMountMaterial, "MM_log", 0, 0, 0);
	/*
	G4LogicalVolume *mirror_mount_log =
		new G4LogicalVolume(mirror_mount_final, MirrorMountMaterial, "MM_log", 0, 0, 0);

	MM_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5, 0.5));
	MM_logVisAttributes-> SetForceWireframe(true);
	mirror_mount_log -> SetVisAttributes(MM_logVisAttributes);
	*/
	new G4PVPlacement(0, mirror_mount.center_position,
	               mirror_mount_log, "MM_plusZ_phys", world_log_, false, 0,
	               check_all_for_overlaps_);

	MMRotation = new CLHEP::HepRotation();
	MMRotation->rotateX(180.*deg);
	new G4PVPlacement(MMRotation, -1.0*mirror_mount.center_position,
	                  mirror_mount_log, "MM_minusZ_phys", world_log_, false, 1,
	                  check_all_for_overlaps_);
	//
//	G4cout << "End ConstructMirrors()." << G4endl;
}  // End ConstructMirrors

void K37DetectorConstruction::ConstructHoops() 
{
	hoopRotation = new G4RotationMatrix();
	hoopRotation->rotateX(90.*deg);
	
	/*
	cut_ESP_logVisAttributes = new G4VisAttributes(G4Colour(0.0, 0.0, 0.0, 1.0));
	cut_ESP_logVisAttributes-> SetForceSolid(true);
	*/
	// ------------------------------  3 - 6  The definitions for these came
	// from Alexandre and a drawing for them is in /Drawings/hoops.pdf

	G4double hoop_3through6_x = (180./2.)*mm;
	G4double hoop_3through6_y = (90./2.)*mm;
	G4double hoop_3through6_z = (1./2.)*mm;

	G4Box * hoop_3through6_box_sol = new G4Box("hoop_3through6_box_sol",
	                                           hoop_3through6_x,
	                                           hoop_3through6_y,
	                                           hoop_3through6_z);

	G4double hoop_3through6innerCut_x = (165.8/2.)*mm;
	G4double hoop_3through6innerCut_y = (75.8/2.)*mm;
	G4double hoop_3through6innerCut_z = (1.5/2.)*mm;

	G4Box * hoop_3through6_boxInnerCut_sol =
		new G4Box("hoop_3through6_boxInnerCut_sol", hoop_3through6innerCut_x,
	               hoop_3through6innerCut_y, hoop_3through6innerCut_z);
	G4SubtractionSolid* cut_hoop_3through6_box_sol =
		new G4SubtractionSolid("cut_hoop_3through6_box_sol",
	                            hoop_3through6_box_sol,
	                            hoop_3through6_boxInnerCut_sol);
	G4double mountingHole_rmax = (4.2/2.)*mm;
	G4double mountingHole_rmin = 0.      *mm;
	G4double mountingHole_dz   = (4.0/2.)*mm;
	G4double mountingHole_Sphi = 0.      *deg;
	G4double mountingHole_Dphi = 360.    *deg;

	G4Tubs * mountingHole_sol = new G4Tubs("mountingHole_sol",
	                                       mountingHole_rmin,
	                                       mountingHole_rmax,
	                                       mountingHole_dz,
	                                       mountingHole_Sphi,
	                                       mountingHole_Dphi);

	G4SubtractionSolid* cut_hoop_3through6_withHole1_sol =
		new G4SubtractionSolid("cut_hoop_3through6_withHole1_sol",
	                            cut_hoop_3through6_box_sol, mountingHole_sol,
	                            0, G4ThreeVector(85.0*mm, 40.0*mm, 0));
	G4SubtractionSolid* cut_hoop_3through6_withHole2_sol =
		new G4SubtractionSolid("cut_hoop_3through6_withHole2_sol",
	                            cut_hoop_3through6_withHole1_sol,
	                            mountingHole_sol, 0,
	                            G4ThreeVector(85.0*mm, -40.0*mm, 0));
	G4SubtractionSolid* cut_hoop_3through6_withHole3_sol =
		new G4SubtractionSolid("cut_hoop_3through6_withHole3_sol",
	                            cut_hoop_3through6_withHole2_sol,
	                            mountingHole_sol, 0,
	                            G4ThreeVector(-85.0*mm, 40.0*mm, 0));
	G4SubtractionSolid* cut_hoop_3through6_withHoles_sol =
		new G4SubtractionSolid("cut_hoop_3through6_withHoles_sol",
	                            cut_hoop_3through6_withHole3_sol,
	                            mountingHole_sol, 0,
	                            G4ThreeVector(-85.0*mm, -40.0*mm, 0));
								
//	G4LogicalVolume *hoop_3through6_box_log =
//		new G4LogicalVolume(cut_hoop_3through6_withHoles_sol, HoopMaterial,
//	                       "hoop_3through6_box_log", 0, 0, 0);
	hoop_3through6_box_log = new G4LogicalVolume(cut_hoop_3through6_withHoles_sol, HoopMaterial,
												"hoop_3through6_box_log", 0, 0, 0);
	// ------------------------------  2
	// Before 5-12-11 hoop 2 and 7 where identical. That is why many of the
	// variable names here are for hoop 7.
	// Hoop 7 now has its own implementation that appears below. It only
	// differes by the middle openiing. See pictures in the drawings
	// directory.

	G4double hoop_7_x = (180./2.)*mm;
	G4double hoop_7_y = (90./2.)*mm;
	G4double hoop_7_z = (3./2.)*mm;
	//  G4double hoop_2_z = /
	G4Box * hoop_7_box_sol = new G4Box("hoop_7_box_sol", hoop_7_x , hoop_7_y, hoop_7_z);

	G4double hoop_7innerCut_z = hoop_7_z * 1.2;

	G4double hoop_7beamCut_x = (35.0/2.)*mm;
	G4double hoop_7beamCut_y = (25.0/2.)*mm;
	G4double hoop_7beamCut_z = hoop_7innerCut_z;

	G4EllipticalTube * hoop_7beamCut =
		new G4EllipticalTube("hoop_7beamCut", hoop_7beamCut_x ,
	                          hoop_7beamCut_y, hoop_7beamCut_z);


	G4double hoop_7InnerHole_rmax = (75.8/2.)*mm;
	G4double hoop_7InnerHole_rmin = 0.      *mm;
	G4double hoop_7InnerHole_dz   = hoop_7innerCut_z;
	G4double hoop_7InnerHole_Sphi = 0.  *deg;
	G4double hoop_7InnerHole_Dphi = 360.*deg;
	G4Tubs * hoop_7InnerHole_sol  = new G4Tubs("hoop_7InnerHole_sol",
	                                            hoop_7InnerHole_rmin,
	                                            hoop_7InnerHole_rmax,
	                                            hoop_7InnerHole_dz,
	                                            hoop_7InnerHole_Sphi,
	                                            hoop_7InnerHole_Dphi);

	G4SubtractionSolid* cutTube_hoop_7_box_sol =
		new G4SubtractionSolid("cutTube_hoop_7_box_sol", hoop_7_box_sol,
	                            hoop_7InnerHole_sol);
	G4SubtractionSolid* cutTube_hoop_7_withHole1_sol =
		new G4SubtractionSolid("cutTube_hoop_7_withHole1_sol",
	                            cutTube_hoop_7_box_sol, mountingHole_sol, 0,
	                            G4ThreeVector(85.0*mm, 40.0*mm, 0));
	G4SubtractionSolid* cutTube_hoop_7_withHole2_sol =
		new G4SubtractionSolid("cutTube_hoop_7_withHole2_sol",
	                            cutTube_hoop_7_withHole1_sol, mountingHole_sol,
	                            0, G4ThreeVector(85.0*mm, -40.0*mm, 0));
	G4SubtractionSolid* cutTube_hoop_7_withHole3_sol =
		new G4SubtractionSolid("cutTube_hoop_7_withHole3_sol",
	                            cutTube_hoop_7_withHole2_sol, mountingHole_sol,
	                            0, G4ThreeVector(-85.0*mm, 40.0*mm, 0));
	G4SubtractionSolid* cutTube_hoop_7_withHoles_sol =
		new G4SubtractionSolid("cutTube_hoop_7_withHoles_sol",
	                            cutTube_hoop_7_withHole3_sol, mountingHole_sol,
	                            0, G4ThreeVector(-85.0*mm, -40.0*mm, 0));

	G4SubtractionSolid* cutTube_hoop_7_withHolesAndBeams1_sol =
		new G4SubtractionSolid("cutTube_hoop_7_withHolesAndBeams1_sol",
	                            cutTube_hoop_7_withHoles_sol,
	                            hoop_7beamCut, 0,
	                            G4ThreeVector(75.0*mm, 0, 0));
	G4SubtractionSolid* cutTube_hoop_7_withHolesAndBeams_sol =
		new G4SubtractionSolid("cutTube_hoop_7_withHolesAndBeams_sol",
	                            cutTube_hoop_7_withHolesAndBeams1_sol,
	                            hoop_7beamCut, 0,
	                            G4ThreeVector(-75.0*mm, 0, 0));

	G4NistManager* nist_manager = G4NistManager::Instance();
	G4Material *hoop7_mat = nist_manager -> FindOrBuildMaterial("G4_Ti");

//	G4LogicalVolume *hoop_7_log =
//		new G4LogicalVolume(cutTube_hoop_7_withHolesAndBeams_sol, hoop7_mat, "hoop_7_log", 0, 0, 0);
	hoop_7_log = new G4LogicalVolume(cutTube_hoop_7_withHolesAndBeams_sol, hoop7_mat, "hoop_7_log", 0, 0, 0);

	G4double hoop_2_dz = (1.0/2.0)*mm;
	G4Box * hoop_2_box_sol = new G4Box("hoop_7_box_sol",
	                                   hoop_7_x , hoop_7_y, hoop_2_dz);
	G4IntersectionSolid *hoop_2_sol =
		new G4IntersectionSolid("hoop_2_sol",
	                            cutTube_hoop_7_withHolesAndBeams_sol,
	                            hoop_2_box_sol);

	G4Material *hoop2_mat = nist_manager ->
		FindOrBuildMaterial("titanium_grade5", true, false);
//	G4LogicalVolume *hoop_2_log =
//		new G4LogicalVolume(hoop_2_sol, hoop2_mat, "hoop_2_log", 0, 0, 0);
	hoop_2_log = new G4LogicalVolume(hoop_2_sol, hoop2_mat, "hoop_2_log", 0, 0, 0);

	// ------------------------------  1
	G4double hoop_1_x = (180./2.)*mm;
	G4double hoop_1_y = (100./2.)*mm;
	G4double hoop_1_z = (1./2.)*mm;

	G4Box * hoop_1_box_sol = new G4Box("hoop_1_box_sol",
	                                   hoop_1_x , hoop_1_y, hoop_1_z);

	G4double hoop_1_centralHole_rmax = (90.0/2.)*mm;
	G4double hoop_1_centralHole_rmin = 0.       *mm;
	G4double hoop_1_centralHole_dz   = (1./2.) *mm;
	G4double hoop_1_centralHole_Sphi = 0.   *deg;
	G4double hoop_1_centralHole_Dphi = 360. *deg;

	G4Tubs * hoop_1_centralHole_sol =
		new G4Tubs("hoop_1_centralHole_sol", hoop_1_centralHole_rmin,
					hoop_1_centralHole_rmax, hoop_1_centralHole_dz,
					hoop_1_centralHole_Sphi, hoop_1_centralHole_Dphi);

	G4SubtractionSolid* cut_hoop_1_box_sol =
		new G4SubtractionSolid("cut_hoop_1_box_sol", hoop_1_box_sol,
								hoop_1_centralHole_sol);

	G4SubtractionSolid* cut_hoop_1_withHole1_sol =
		new G4SubtractionSolid("cut_hoop_1_withHole1_sol", cut_hoop_1_box_sol,
								mountingHole_sol, 0,
								G4ThreeVector(85.0*mm, 40.0*mm, 0));
	G4SubtractionSolid* cut_hoop_1_withHole2_sol =
		new G4SubtractionSolid("cut_hoop_1_withHole2_sol",
								cut_hoop_1_withHole1_sol, mountingHole_sol, 0,
								G4ThreeVector(85.0*mm, -40.0*mm, 0));
	G4SubtractionSolid* cut_hoop_1_withHole3_sol =
		new G4SubtractionSolid("cut_hoop_1_withHole3_sol",
								cut_hoop_1_withHole2_sol, mountingHole_sol,
								0, G4ThreeVector(-85.0*mm, 40.0*mm, 0));
	G4SubtractionSolid* cut_hoop_1_withHoles_sol =
		new G4SubtractionSolid("cut_hoop_1_withHoles_sol",
								cut_hoop_1_withHole3_sol, mountingHole_sol, 0,
								G4ThreeVector(-85.0*mm, -40.0*mm, 0));

	G4SubtractionSolid* cut_hoop_1_withHolesAndBeams1_sol =
		new G4SubtractionSolid("cut_hoop_1_withHolesAndBeams1_sol",
								cut_hoop_1_withHoles_sol, hoop_7beamCut, 0,
								G4ThreeVector(97.0*mm, 0, 0));
	G4SubtractionSolid* cut_hoop_1_withHolesAndBeams_sol =
		new G4SubtractionSolid("cut_hoop_1_withHolesAndBeams_sol",
								cut_hoop_1_withHolesAndBeams1_sol,
								hoop_7beamCut, 0,
								G4ThreeVector(-97.0*mm, 0, 0));
//	G4LogicalVolume *hoop_1_log =
//		new G4LogicalVolume(cut_hoop_1_withHolesAndBeams_sol, HoopMaterial,
//							"hoop_1_log", 0, 0, 0);
	hoop_1_log = new G4LogicalVolume(cut_hoop_1_withHolesAndBeams_sol, HoopMaterial,
									"hoop_1_log", 0, 0, 0);
	// ------------------------------  vis and placment of hoops
	/*
	cut_ESP_logVisAttributes-> SetForceSolid(true);
	hoop7_logVisAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
	hoop7_logVisAttributes-> SetForceSolid(true);

	hoop_3through6_box_log -> SetVisAttributes(cut_ESP_logVisAttributes);  // 3-6
	hoop_7_log -> SetVisAttributes(hoop7_logVisAttributes);    // 7
	hoop_2_log -> SetVisAttributes(hoop7_logVisAttributes);    // 2
	hoop_1_log -> SetVisAttributes(cut_ESP_logVisAttributes);  // 1
	*/
	
	new G4PVPlacement(hoopRotation, G4ThreeVector(0., 57.5*mm, 0.),
	                  hoop_3through6_box_log, "hoop_3through6_phys_3",
	                  world_log_, false, 0, check_all_for_overlaps_);
	new G4PVPlacement(hoopRotation, G4ThreeVector(0., 29.5*mm, 0.),
	                  hoop_3through6_box_log, "hoop_3through6_phys_4",
	                  world_log_, false, 0, check_all_for_overlaps_);
	new G4PVPlacement(hoopRotation, G4ThreeVector(0., -29.5*mm, 0.),
	                  hoop_3through6_box_log, "hoop_3through6_phys_5",
	                  world_log_, false, 0, check_all_for_overlaps_);
	new G4PVPlacement(hoopRotation, G4ThreeVector(0., -57.5*mm, 0.),
	                  hoop_3through6_box_log, "hoop_3through6_phys_6",
	                  world_log_, false, 0, check_all_for_overlaps_);
	new G4PVPlacement(hoopRotation, G4ThreeVector(0., -75.5*mm, 0.),
	                  hoop_7_log, "hoop_7_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
	new G4PVPlacement(hoopRotation, G4ThreeVector(0., 75.5*mm, 0.),
	                  hoop_2_log, "hoop_2_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
	new G4PVPlacement(hoopRotation, G4ThreeVector(0., 97.5*mm, 0.),
	                  hoop_1_log, "hoop_1_phys", world_log_, false, 0,
	                  check_all_for_overlaps_);
}  // End construct hoops

void K37DetectorConstruction::ConstructElectronMCP() 
{
//	G4SDManager* sd_man = G4SDManager::GetSDMpointer();
	
	bool verbose = false;
	G4NistManager* nist_manager = G4NistManager::Instance();

	G4double mcp_plate_OD = 86.6*mm;
	G4double mcp_plate_thickness = 1.0*mm;
	G4double mcp_front_to_chamber_center = 100.0*mm;
	G4double mcp_oar = 0.70; // open area ratio
	G4double mcp_density_eff = nist_manager ->
	FindOrBuildMaterial("G4_GLASS_LEAD") -> GetDensity();
	if(verbose)
	{
		G4cout << "Old density  = " << mcp_density_eff/(g/cm3) << G4endl;
	}
	mcp_density_eff *= (1.0 - mcp_oar);
	if(verbose)
	{
		G4cout << "New density  = " << mcp_density_eff/(g/cm3) << G4endl;
	}
	G4Material *emcp_material = nist_manager -> FindOrBuildMaterial("emcp_mat");
	if (!emcp_material) 
	{
		emcp_material = nist_manager -> BuildMaterialWithNewDensity("emcp_mat", "G4_GLASS_LEAD", mcp_density_eff);
	}

	G4double mcp_spacer_thickness = 0.15*mm;
	G4double spacer_ID = 83.0*mm;
	G4double spacer_OD = 87.0*mm;
	G4Material *spacer_mat = nist_manager -> FindOrBuildMaterial("G4_Cu");

	G4double ceramic_thickness = 2.0*mm;
	G4double ceramic_ID = 83.0*mm;
	G4double ceramic_OD = 105.0*mm;
	G4Material *ceramic_mat = nist_manager -> FindOrBuildMaterial("G4_ALUMINUM_OXIDE");

	G4double z_pos = -1.0*(mcp_front_to_chamber_center) + (0.5*ceramic_thickness);
	G4Tubs *ceramic_tub = new G4Tubs("ceramic_tub", ceramic_ID/2.0, ceramic_OD/2.0,
	                                 ceramic_thickness/2.0 - (0.001*mm),
	                                 0.0, 360.0*deg);
	G4LogicalVolume *ceramic_log = new G4LogicalVolume(ceramic_tub, ceramic_mat,
	                                                  "eMCP_ceramic", 0, 0, 0,
	                                                  check_all_for_overlaps_);
	new G4PVPlacement(changeZtoX, G4ThreeVector(0., z_pos, 0), ceramic_log,
	                  "eMCP_ceramic_front", world_log_, false, 0, check_all_for_overlaps_);

	z_pos = -1.0*(mcp_front_to_chamber_center) - (0.5*mcp_plate_thickness);  
	G4cout << "Electron MCP OD = " << G4BestUnit(mcp_plate_OD, "Length") << G4endl;
	electron_mcp_tub_ = new G4Tubs("mcp_plate_sol", 0.0, mcp_plate_OD/2.0,
	                               mcp_plate_thickness/2.0, 0.0, 360.*deg);
	
	electron_mcp_log_        = new G4LogicalVolume(electron_mcp_tub_, emcp_material,
	                                              "mcp_plate_log", 0, 0, 0, check_all_for_overlaps_);
	electron_mcp_log_noSense = new G4LogicalVolume(electron_mcp_tub_, emcp_material,
	                                              "mcp_plate_log", 0, 0, 0, check_all_for_overlaps_);
	
	electron_mcp_phys_ = new G4PVPlacement(changeZtoX,
	                                       G4ThreeVector(0., z_pos, 0),
	                                       electron_mcp_log_,
	                                       "eMCP_front_plate_phys",
	                                       world_log_, false, 0, check_all_for_overlaps_);

	G4Tubs *spacer_tub = new G4Tubs("spacer_sol", spacer_ID/2.0, spacer_OD/2.0,
	                                mcp_spacer_thickness/2.0 - (0.001*mm), 0.0, 360.0*deg);
	G4LogicalVolume *spacer_log = new G4LogicalVolume(spacer_tub, spacer_mat,
	                                                  "eMCP_spacer_log", 0, 0, 0,
	                                                  check_all_for_overlaps_);
	for (int i = 0; i < 2; i++) 
	{
		z_pos = z_pos - (0.5*mcp_plate_thickness) - (0.5*mcp_spacer_thickness);
		
		new G4PVPlacement(changeZtoX, G4ThreeVector(0., z_pos, 0),
						  spacer_log, "eMCP_spacer_phys", world_log_,
						  false, 0, check_all_for_overlaps_);
						  
		z_pos = z_pos - (0.5*mcp_plate_thickness) - (0.5*mcp_spacer_thickness);    
		                  
		new G4PVPlacement(changeZtoX, G4ThreeVector(0., z_pos, 0),
						  electron_mcp_log_noSense, "eMCP_mid_plate_phys",
						  world_log_, false, 0, check_all_for_overlaps_);
	}

	z_pos = z_pos - (0.5*mcp_plate_thickness) - (0.5*ceramic_thickness);
	
	new G4PVPlacement(changeZtoX, G4ThreeVector(0., z_pos, 0), ceramic_log,
					 "eMCP_ceramic_back", world_log_, false, 0, check_all_for_overlaps_);

	G4VisAttributes *spacer_vis = new G4VisAttributes(G4Colour(0.6, 0.35, 0.0, 0.25));
	spacer_vis -> SetForceSolid(true);

	G4VisAttributes *ceramic_vis = new G4VisAttributes(G4Colour(0.75, 0.75, 0.75));
	ceramic_vis -> SetForceSolid(true);
	
	/*
	SOED_logVisAttributes = new G4VisAttributes(G4Colour(0.3, 0.3, 0.3));
	SOED_logVisAttributes-> SetForceSolid(true);
	electron_mcp_log_        -> SetVisAttributes(SOED_logVisAttributes);
	electron_mcp_log_noSense -> SetVisAttributes(SOED_logVisAttributes);
	*/
	spacer_log -> SetVisAttributes(spacer_vis);
	ceramic_log -> SetVisAttributes(ceramic_vis);
	
	/*
	// // Set up sensitive detector
	if (!electron_mcp_sens_) 
	{
		electron_mcp_sens_ = new K37ElectronMCPSD("/mydet/electron_mcp");
		sd_man -> AddNewDetector(electron_mcp_sens_);
	}
		electron_mcp_log_ -> SetSensitiveDetector(electron_mcp_sens_);
	*/
	
}  // End construct EMCP

void K37DetectorConstruction::ConstructRecoilMCP() 
{
//	G4SDManager* sd_man = G4SDManager::GetSDMpointer();
	G4NistManager* nist_manager = G4NistManager::Instance();

	G4double mcp_plate_OD                = 86.6*mm;
	G4double mcp_plate_thickness_front   = 0.6*mm;
	G4double mcp_oar_front               = 0.55; // open area ratio

	G4double mcp_plate_thickness_rear    = 1.5*mm;
	G4double mcp_oar_rear                = 0.56;  // open area ratio

	G4double mcp_front_to_chamber_center = 104.1*mm;

	G4double lg_density = nist_manager -> FindOrBuildMaterial("G4_GLASS_LEAD") -> GetDensity();
	G4double mcp_density_eff_front = lg_density * (1.0 - mcp_oar_front);
	G4double mcp_density_eff_rear = lg_density * (1.0 - mcp_oar_rear);

	G4Material *rmcp_material_front = nist_manager ->
		FindOrBuildMaterial("rmcp_mat_front");
	if (!rmcp_material_front) 
	{
		rmcp_material_front = nist_manager ->
			BuildMaterialWithNewDensity("rmcp_mat_front", "G4_GLASS_LEAD", mcp_density_eff_front);
	}
	G4Material *rmcp_material_rear = nist_manager -> FindOrBuildMaterial("rmcp_mat_rear");
	if (!rmcp_material_rear) 
	{
		rmcp_material_rear = nist_manager ->
			BuildMaterialWithNewDensity("rmcp_mat_rear", "G4_GLASS_LEAD", mcp_density_eff_rear);
	}

	G4double mcp_spacer_thickness = 0.20*mm;
	G4double spacer_ID = 83.0*mm;
	G4double spacer_OD = 87.0*mm;
	G4Material *spacer_mat = nist_manager -> FindOrBuildMaterial("G4_Cu");

	G4double ceramic_thickness = 2.0*mm;
	G4double ceramic_ID = 83.0*mm;
	G4double ceramic_OD = 105.0*mm;
	G4Material *ceramic_mat = nist_manager -> FindOrBuildMaterial("G4_ALUMINUM_OXIDE");

	G4double z_pos = 1.0*(mcp_front_to_chamber_center) - (0.5*ceramic_thickness);
	G4Tubs *ceramic_tub = new G4Tubs("ceramic_tub", ceramic_ID/2.0, ceramic_OD/2.0,
	                                 ceramic_thickness/2.0 - (0.001*mm), 0.0, 360.0*deg);
	G4LogicalVolume *ceramic_log = new G4LogicalVolume(ceramic_tub, ceramic_mat,
	                                                  "rMCP_ceramic", 0, 0, 0,
	                                                  check_all_for_overlaps_);
	new G4PVPlacement(changeZtoX, G4ThreeVector(0., z_pos, 0), ceramic_log,
	                  "rMCP_ceramic_front", world_log_, false, 0, check_all_for_overlaps_);

	z_pos = mcp_front_to_chamber_center + (0.5*mcp_plate_thickness_front);
	
	G4cout << "Recoil MCP OD = " << G4BestUnit(mcp_plate_OD, "Length") << G4endl;
	recoil_mcp_tub_ = new G4Tubs("rmcp_sol", 0.0, mcp_plate_OD/2.0,
	                              mcp_plate_thickness_front/2.0, 0.0, 360.0*deg);

	recoil_mcp_log_ = new G4LogicalVolume(recoil_mcp_tub_, rmcp_material_front, "rmcp_log", 0, 0, 0);
	recoil_mcp_phys_ = new G4PVPlacement(changeZtoX,
	                                     G4ThreeVector(0., z_pos, 0),
	                                     recoil_mcp_log_, "rmcp_phys", world_log_,
	                                     false, 0, check_all_for_overlaps_);

	z_pos = z_pos + (0.5*mcp_plate_thickness_front) + (0.5*mcp_spacer_thickness);
  
	G4Tubs *spacer_tub = new G4Tubs("spacer_sol", spacer_ID/2.0, spacer_OD/2.0,
	                                mcp_spacer_thickness/2.0 - (0.001*mm), 0.0, 360.0*deg);
	G4LogicalVolume *spacer_log = new G4LogicalVolume(spacer_tub, spacer_mat,
	                                                  "eMCP_spacer_log", 0, 0, 0,
	                                                  check_all_for_overlaps_);
	new G4PVPlacement(changeZtoX, G4ThreeVector(0., z_pos, 0),
	                  spacer_log, "rMCP_spacer_phys", world_log_,
	                  false, 0, check_all_for_overlaps_);

	z_pos = z_pos + (0.5*mcp_spacer_thickness) + (0.5*mcp_plate_thickness_rear);
	G4Tubs *mcp_plate_rear_tub          = new G4Tubs("rmcp_plate_rear", 0.0, mcp_plate_OD/2.0, mcp_plate_thickness_rear/2.0, 0.0, 360.0*deg);
	
//	G4LogicalVolume * mcp_plate_rear_log = new G4LogicalVolume(mcp_plate_rear_tub, rmcp_material_rear, "rmcp_plate_rear_log", 0, 0, 0);
	mcp_plate_rear_log = new G4LogicalVolume(mcp_plate_rear_tub, rmcp_material_rear, "rmcp_plate_rear_log", 0, 0, 0);
	
	new G4PVPlacement(changeZtoX, G4ThreeVector(0., z_pos, 0.),
	                  mcp_plate_rear_log, "rMCP_plate_rear_phys", world_log_,
	                  false, 0, check_all_for_overlaps_);

	z_pos = z_pos + (0.5*mcp_plate_thickness_rear) + (0.5*ceramic_thickness);
	new G4PVPlacement(changeZtoX, G4ThreeVector(0., z_pos, 0.),
	                  ceramic_log, "rMCP_ceramic_rear", world_log_,
	                  false, 0, check_all_for_overlaps_);
	/*
	rmcp_logVisAttributes_ = new G4VisAttributes(G4Colour(0.3, 0.3, 0.3));
	rmcp_logVisAttributes_ -> SetForceSolid(true);
	recoil_mcp_log_ -> SetVisAttributes(rmcp_logVisAttributes_);
	*/
	
//	// Set up sensitive detector
//	if (!recoil_mcp_sens_) 
//	{
//		recoil_mcp_sens_ = new K37RecoilMCPSD("/mydet/recoil_mcp");
//		sd_man -> AddNewDetector(recoil_mcp_sens_);
//	}

	G4VisAttributes *ceramic_vis = new G4VisAttributes(G4Colour(1.0, 1.0, 1.0));
	ceramic_vis -> SetForceSolid(true);

	G4VisAttributes *spacer_vis = new G4VisAttributes(G4Colour(0.6, 0.35, 0.0, 0.25));
	spacer_vis -> SetForceSolid(true);

//	recoil_mcp_log_ -> SetSensitiveDetector(recoil_mcp_sens_);
	ceramic_log -> SetVisAttributes(ceramic_vis);
	spacer_log -> SetVisAttributes(spacer_vis);
	/*
	mcp_plate_rear_log -> SetVisAttributes(rmcp_logVisAttributes_);
	*/
	
}
void K37DetectorConstruction::PlaceSpacerWithBounds(G4double ymin, G4double ymax) 
{
	G4double spacer_ID = 4.001*mm;  
	G4double spacer_OD = 10.0*mm; 
	G4double spacer_xoffset = 85.0*mm;
	G4double spacer_zoffset = 40.0*mm;

	G4double spacer_length = fabs(ymax - ymin) - 0.001*mm;
	G4double spacer_center = (ymax + ymin) / 2.0;
	G4Material *spacer_mat = HoopMaterial;

	G4Tubs *spacer_tub = new G4Tubs("spacer_tub", spacer_ID/2.0, spacer_OD/2.0,
	                                spacer_length/2.0, 0.0, 360.0*deg);

	G4LogicalVolume *spacer_log = new G4LogicalVolume(spacer_tub, spacer_mat, "spacer_log", 0, 0, 0);
	for (int i = 0; i < 2; i++) 
	{
		double sx = pow(-1.0, i);
		for (int j = 0; j < 2; j++) 
		{
			double sz = pow(-1.0, j);
			new G4PVPlacement(changeZtoX, G4ThreeVector(sx*spacer_xoffset,
			                                            spacer_center,
			                                            sz*spacer_zoffset),
			                  spacer_log, "spacer_phys", world_log_,
			                  false, 0, check_all_for_overlaps_);
		}
	}
	G4VisAttributes *spacer_vis = new G4VisAttributes(G4Colour(0.0, 0.0, 0.0, 0.1));
	spacer_vis -> SetForceSolid(true);
	spacer_log -> SetVisAttributes(spacer_vis);
}

void K37DetectorConstruction::ConstructSupportRods() 
{
	G4NistManager* nist_manager = G4NistManager::Instance();

	G4double ceramic_rods_OD      =   4.0*mm;
	G4double ceramic_rods_length  = 268.5*mm;
	G4double ceramic_rods_center  =  58.75*mm;
	G4double ceramic_rods_xoffset =  85.0*mm;
	G4double ceramic_rods_zoffset =  40.0*mm;

	G4double hoop_center[8]    = {193.0, 97.5*mm, 75.5*mm, 57.5*mm, 29.5*mm, -29.5*mm, -57.5*mm, -75.5*mm};                                                         
	G4double hoop_thickness[8] = {  0.0,  1.0*mm,  1.0*mm,  1.0*mm,  1.0*mm,   1.0*mm,   1.0*mm,   3.0*mm};
  
	G4Material *ceramic_rods_mat = nist_manager -> FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
	G4VisAttributes *ceramic_rods_vis = new G4VisAttributes(G4Colour(1., 1., 1., 0.33));
	ceramic_rods_vis -> SetForceSolid(true);

	G4Tubs *ceramic_rods_tub = new G4Tubs("ceramic_rod_log", 0.0, ceramic_rods_OD/2.0,
	                                      ceramic_rods_length/2.0, 0.0, 360.0*deg);
	G4LogicalVolume *ceramic_rods_log =
	  new G4LogicalVolume(ceramic_rods_tub, ceramic_rods_mat, "ceramic_rod_log", 0, 0, 0);

	for (int i = 0; i < 2; i++) 
	{
		double sx = pow(-1.0, i);
		for (int j = 0; j < 2; j++) 
		{
			double sz = pow(-1.0, j);
			new G4PVPlacement(changeZtoX, 
					G4ThreeVector(sx*ceramic_rods_xoffset, 
					              ceramic_rods_center, 
					              sz*ceramic_rods_zoffset),
					        ceramic_rods_log, "ceramic_rod_phys", world_log_,
					        false, 0, check_all_for_overlaps_);
		}
	}
	ceramic_rods_log -> SetVisAttributes(ceramic_rods_vis);

	for (int i = 0; i < 8-1; i++) 
	{ // ymin > ymax .
		G4double ymin = hoop_center[i] - (hoop_thickness[i]/2.0);
		G4double ymax = hoop_center[i+1] + hoop_thickness[i+1]/2.0;
		PlaceSpacerWithBounds(ymin, ymax);
	}
}

void K37DetectorConstruction::ConstructCoils() 
{
	G4Torus *coil_sol;
//	G4LogicalVolume *coil_log;
	G4Tubs *coilMetal_sol;
//	G4LogicalVolume *coilMetal_log;
	
	G4SubtractionSolid *coilMetalSubtracted_sol;
	
	G4String solNameTorus         = G4String();
	G4String logNameTorus         = G4String();
	G4String physNameTorus        = G4String();
	
	G4String solNameTubSubtracted = G4String();
	G4String solNameTub           = G4String();
	G4String logNameTub           = G4String();
	G4String physNameTub          = G4String();
	/*
	coils_logVisAttributes = new G4VisAttributes(G4Colour(0.6, 0.35, 0.0, 0.25));
	coils_logVisAttributes -> SetForceSolid(true);
	
	coilLiquid_logVisAttributes = new G4VisAttributes(G4Colour(0.0, 0.0, 1.0, 0.8));
	coilLiquid_logVisAttributes -> SetForceSolid(false);
	*/
	
	for(int ycount = 0; ycount < 4; ++ycount)
	{
		for(int xcount = 0; xcount < 4; ++xcount)
		{
			solNameTorus = coil_inner_torus_PZ[ycount][xcount].nameBase + "_sol";
			logNameTorus = coil_inner_torus_PZ[ycount][xcount].nameBase + "_log";
			physNameTorus= coil_inner_torus_PZ[ycount][xcount].nameBase + "_phys";
			
			solNameTubSubtracted = coil_outer_tubs_PZ[ycount][xcount].nameBase + "_sub_sol";
			
			solNameTub = coil_outer_tubs_PZ[ycount][xcount].nameBase + "_sol";
			logNameTub = coil_outer_tubs_PZ[ycount][xcount].nameBase + "_log";
			physNameTub= coil_outer_tubs_PZ[ycount][xcount].nameBase + "_phys";
			
			coil_sol = new G4Torus(solNameTorus, 
				coil_inner_torus_PZ[ycount][xcount].inner_radius,
				coil_inner_torus_PZ[ycount][xcount].outer_radius,
				coil_inner_torus_PZ[ycount][xcount].sweep_radius,
				coil_inner_torus_PZ[ycount][xcount].start_phi,
				coil_inner_torus_PZ[ycount][xcount].delta_phi );
			
			coilMetal_sol = new G4Tubs(solNameTub, 
				coil_outer_tubs_PZ[ycount][xcount].inner_radius,
				coil_outer_tubs_PZ[ycount][xcount].outer_radius,
				coil_outer_tubs_PZ[ycount][xcount].length/2.0,
				coil_outer_tubs_PZ[ycount][xcount].start_phi,
				coil_outer_tubs_PZ[ycount][xcount].delta_phi );
			
			coilMetalSubtracted_sol = new G4SubtractionSolid( solNameTubSubtracted, coilMetal_sol, coil_sol);
			/*
		//	coil_log      = new G4LogicalVolume( coil_sol, CoilsLiquid, logNameTorus, 0,0,0 );
		//	coilMetal_log = new G4LogicalVolume( coilMetalSubtracted_sol, CoilsMetal, logNameTub, 0,0,0 );
		//	coil_log      -> SetVisAttributes(coilLiquid_logVisAttributes);
		//	coilMetal_log -> SetVisAttributes(coils_logVisAttributes);
		//	
		//	new G4PVPlacement(0, coil_inner_torus_PZ[ycount][xcount].center_position,
		//		coil_log, physNameTorus, world_log_,
		//		false, 0, check_all_for_overlaps_ );
		//	new G4PVPlacement(0, coil_outer_tubs_PZ[ycount][xcount].center_position,
		//		coilMetal_log, physNameTub, world_log_,
		//		false, 0, check_all_for_overlaps_ );
			*/
			
			coil_log_t[ycount][xcount]      = new G4LogicalVolume( coil_sol, CoilsLiquid, logNameTorus, 0,0,0 );
			coilMetal_log_t[ycount][xcount] = new G4LogicalVolume( coilMetalSubtracted_sol, CoilsMetal, logNameTub, 0,0,0 );
			
			new G4PVPlacement(0, coil_inner_torus_PZ[ycount][xcount].center_position,
				coil_log_t[ycount][xcount], physNameTorus, world_log_,
				false, 0, check_all_for_overlaps_ );
			new G4PVPlacement(0, coil_outer_tubs_PZ[ycount][xcount].center_position,
				coilMetal_log_t[ycount][xcount], physNameTub, world_log_,
				false, 0, check_all_for_overlaps_ );
		//	coil_log_t[ycount][xcount]      -> SetVisAttributes(coilLiquid_logVisAttributes);
		//	coilMetal_log_t[ycount][xcount] -> SetVisAttributes(coils_logVisAttributes);
			
			//Minus Z coils =====================================================
			solNameTorus = coil_inner_torus_MZ[ycount][xcount].nameBase + "_sol";
			logNameTorus = coil_inner_torus_MZ[ycount][xcount].nameBase + "_log";
			physNameTorus= coil_inner_torus_MZ[ycount][xcount].nameBase + "_phys";
			
			solNameTubSubtracted = coil_outer_tubs_MZ[ycount][xcount].nameBase + "_sub_sol";

			solNameTub = coil_outer_tubs_MZ[ycount][xcount].nameBase + "_sol";
			logNameTub = coil_outer_tubs_MZ[ycount][xcount].nameBase + "_log";
			physNameTub= coil_outer_tubs_MZ[ycount][xcount].nameBase + "_phys";

			coil_sol = new G4Torus(solNameTorus, 
				coil_inner_torus_MZ[ycount][xcount].inner_radius,
				coil_inner_torus_MZ[ycount][xcount].outer_radius,
				coil_inner_torus_MZ[ycount][xcount].sweep_radius,
				coil_inner_torus_MZ[ycount][xcount].start_phi,
				coil_inner_torus_MZ[ycount][xcount].delta_phi );

			coilMetal_sol = new G4Tubs(solNameTub, 
				coil_outer_tubs_MZ[ycount][xcount].inner_radius,
				coil_outer_tubs_MZ[ycount][xcount].outer_radius,
				coil_outer_tubs_MZ[ycount][xcount].length/2.0,
				coil_outer_tubs_MZ[ycount][xcount].start_phi,
				coil_outer_tubs_MZ[ycount][xcount].delta_phi );
			
			coilMetalSubtracted_sol = new G4SubtractionSolid( solNameTubSubtracted, coilMetal_sol, coil_sol);
			/*
		//	coil_log      = new G4LogicalVolume( coil_sol, CoilsLiquid, logNameTorus, 0,0,0 );
		//	coilMetal_log = new G4LogicalVolume( coilMetalSubtracted_sol, CoilsMetal, logNameTub, 0,0,0 );
		//	coil_log      -> SetVisAttributes(coilLiquid_logVisAttributes);
		//	coilMetal_log -> SetVisAttributes(coils_logVisAttributes);
		//	
		//	new G4PVPlacement(0, coil_inner_torus_MZ[ycount][xcount].center_position,
		//		coil_log, physNameTorus, world_log_,
		//		false, 0, check_all_for_overlaps_ );
		//	new G4PVPlacement(0, coil_outer_tubs_MZ[ycount][xcount].center_position,
		//		coilMetal_log, physNameTub, world_log_,
		//		false, 0, check_all_for_overlaps_ );
			*/
			coil_log_b[ycount][xcount]      = new G4LogicalVolume( coil_sol, CoilsLiquid, logNameTorus, 0,0,0 );
			coilMetal_log_b[ycount][xcount] = new G4LogicalVolume( coilMetalSubtracted_sol, CoilsMetal, logNameTub, 0,0,0 );
			
			new G4PVPlacement(0, coil_inner_torus_MZ[ycount][xcount].center_position,
				coil_log_b[ycount][xcount], physNameTorus, world_log_,
				false, 0, check_all_for_overlaps_ );
			new G4PVPlacement(0, coil_outer_tubs_MZ[ycount][xcount].center_position,
				coilMetal_log_b[ycount][xcount], physNameTub, world_log_,
				false, 0, check_all_for_overlaps_ );
		//	coil_log_b[ycount][xcount]      -> SetVisAttributes(coilLiquid_logVisAttributes);
		//	coilMetal_log_b[ycount][xcount] -> SetVisAttributes(coils_logVisAttributes);
			
		} // end loop over x
	} //end loop over y
}  // End construct coils

void K37DetectorConstruction::DefineMaterials() 
{
	G4NistManager* nist_manager = G4NistManager::Instance();
	// ------------------------------------------------------ materials
	// atomic mass, atomic number, density, fractional mass
	G4double a, z, density, fractionmass;
	G4int nel, natoms,  ncomponents;  // number of elements, number of atoms
	
	G4Element* N  = new G4Element("Nitrogen"  , "N",  z = 7.,  a = 14.01    *g/mole);
	G4Element* O  = new G4Element("Oxygen"    , "O",  z = 8.,  a = 16.00    *g/mole);
	G4Element* F  = new G4Element("Fluorine"  , "F",  z = 9.,  a = 18.9984  *g/mole);
	G4Element* Mg = new G4Element("Magnesium" , "Mg", z = 12., a = 24.3050  *g/mole);
	G4Element* K  = new G4Element("Potassium" , "K",  z = 19., a = 39.0983  *g/mole);
	G4Element* B  = new G4Element("Boron"     , "B",  z = 5.,  a = 10.811   *g/mole);
	G4Element* C  = new G4Element("Carbon"    , "C",  z = 6.,  a = 12.0107  *g/mole);
	G4Element* Si = new G4Element("Silicon"   , "Si", z = 14., a = 28.086   *g/mole);
	G4Element* Mn = new G4Element("Manganese" , "Mn", z = 25., a = 54.94    *g/mole);
	G4Element* Cr = new G4Element("Chromium"  , "Cr", z = 24., a = 52.00    *g/mole);
	G4Element* Ni = new G4Element("Nickel"    , "Ni", z = 28., a = 58.70    *g/mole);
	G4Element* Fe = new G4Element("Iron"      , "Fe", z = 26., a = 55.85    *g/mole);
	G4Element* Al = new G4Element("Aluminum"  , "Al", z = 13., a = 26.9815  *g/mole);
	G4Element* H  = new G4Element("Hydrogen"  , "H",  z = 1.,  a= 1.01      *g/mole);
	G4Element* S  = new G4Element("Sulfur"    , "S",  z = 16., a = 32.065   *g/mole);
	G4Element* P  = new G4Element("Phosphorus", "P",  z = 15., a = 30.973762*g/mole);
	
	G4Material* vacuum = new G4Material("Vac ", z = 1., a = 1.01*g/mole,
	                                 density = universe_mean_density, kStateGas,
	                                 2.73*kelvin, 3.e-18*pascal);

	G4Material* SiliconCarbide = new G4Material("SiliconCarbide",
	                                            density = 3.21*g/cm3, nel = 2);  //nominal
	                                            //density = 2.889*g/cm3, nel = 2); //+10%
	                                            //density = 3.531*g/cm3, nel = 2); //-10%
	SiliconCarbide->AddElement(Si, natoms = 1);
	SiliconCarbide->AddElement(C, natoms = 1);

	G4Material* SiliconDiOxide = new G4Material("SiliconDiOxide",
	                                            density = 2.648*g/cm3, nel = 2);
	SiliconDiOxide->AddElement(Si, natoms = 1);
	SiliconDiOxide->AddElement(O , natoms = 2);


	G4Material* MagnesiumOxide = new G4Material("MagnesiumOxide",
	                                            density = 3.58*g/cm3, nel = 2);
	MagnesiumOxide->AddElement(Mg, natoms = 1);
	MagnesiumOxide->AddElement(O , natoms = 1);

	G4Material* AluminumOxide = new G4Material("AluminumOxide",
	                                           density = 3.95*g/cm3, nel = 2);
	AluminumOxide->AddElement(Al, natoms = 2);
	AluminumOxide->AddElement(O , natoms = 3);

	G4Material* PotassiumOxide = new G4Material("PotassiumOxide",
	                                            density = 2.35*g/cm3, nel = 2);
	PotassiumOxide->AddElement(K, natoms = 2);
	PotassiumOxide->AddElement(O , natoms = 1);


	G4Material* BoronOxide = new G4Material("BoronOxide", density = 2.55*g/cm3,
	                                        nel = 2);
	BoronOxide->AddElement(B, natoms = 2);
	BoronOxide->AddElement(O , natoms = 3);


	G4Material* bc408 = new G4Material("BC408",
	                                   density = 1.032*g/cm3, nel = 2);
	bc408->AddElement(H, 30);
	bc408->AddElement(C, 27);

	G4Material* Scintillator = bc408;

	G4Material* StainlessSteel = new G4Material("StainlessSteel",
	                                            density = 8.03*g/cm3, nel = 9);
	StainlessSteel -> AddElement(Fe, fractionmass = 0.70995);
	StainlessSteel -> AddElement(Cr, fractionmass = 0.18);
	StainlessSteel -> AddElement(Ni, fractionmass = 0.08);
	StainlessSteel -> AddElement(Mn, fractionmass = 0.02);
	StainlessSteel -> AddElement(N , fractionmass = 0.001);
	StainlessSteel -> AddElement(S , fractionmass = 0.0003);
	StainlessSteel -> AddElement(C , fractionmass = 0.0008);
	StainlessSteel -> AddElement(Si, fractionmass = 0.0075);
	StainlessSteel -> AddElement(P , fractionmass = 0.00045);

	G4Material* GlassyCarbon = new G4Material("GlassyCarbon", z = 6.,
	                                          a = 12.0107*g/mole,
	                                          density = 1.5*g/cm3);
	G4Material* Beryllium = new G4Material("Beryllium", z = 4.,
	                                       a = 9.012182*g/mole,
	                                       density = 1.848*g/cm3); //nominal
	                                       //density = 2.0328*g/cm3); //+10%
	                                       //density = 1.6632*g/cm3); //-10%
	G4Material* SiliconMat = new G4Material("SiliconMat", z = 14.,
	                                        a = 28.086*g/mole,
	                                        density= 2.329*g/cm3);


	G4Material* Sapphire = new G4Material("Sapphire", density = 3.98*g/cm3,
	                                      nel = 2);
	Sapphire -> AddElement(Al, natoms = 2);
	Sapphire -> AddElement(O, natoms = 1);

	G4Material* Kapton = new G4Material("Kapton", density = 1.42*mg/cm3,
	                                    nel = 4);
	Kapton -> AddElement(H, fractionmass = 0.026362);
	Kapton -> AddElement(C, fractionmass = 0.691133);
	Kapton -> AddElement(N, fractionmass = 0.073270);
	Kapton -> AddElement(O, fractionmass = 0.209235);

	G4Material* Macor = new G4Material("Macor", density = 2.52*g/cm3,
	                                   ncomponents = 6);
	Macor -> AddMaterial(SiliconDiOxide, fractionmass = 46*perCent);
	Macor -> AddMaterial(MagnesiumOxide, fractionmass = 17*perCent);
	Macor -> AddMaterial(AluminumOxide , fractionmass = 16*perCent);
	Macor -> AddMaterial(PotassiumOxide, fractionmass = 10*perCent);
	Macor -> AddMaterial(BoronOxide    , fractionmass = 7*perCent);
	Macor -> AddElement(F, fractionmass = 4*perCent);

	vector<G4String> elem   = {"Ti", "Al", "V"};
	vector<double>   weight = {0.90, 0.06, 0.04};
	nist_manager -> ConstructNewMaterial("titanium_grade5", elem, weight,
	                                     4.43*g/cm3);

	G4Material *teflon = nist_manager -> FindOrBuildMaterial("G4_TEFLON");
	G4Material *water = nist_manager -> FindOrBuildMaterial("G4_WATER");
	G4Material *copper = nist_manager -> FindOrBuildMaterial("G4_Cu");
	trinat_materials_.insert(std::pair<G4String, G4Material*>(teflon -> GetName(),
                                                            teflon));
/*   Pre-defined material:
     density = 6.22 g/cm3
     mean excitation energy = 526.4 eV
     radiation length = 1.266 cm
     nuclear interaction length = 25.755 cm
     Composition by mass:
                   Lead     (Z = 82) - 0.751938
                   Oxygen   (Z = 8)  - 0.156453
                   Silicon  (Z = 14) - 0.080866
                   titanium_grade2 (Z = 22) - 0.008092
                   Arsenic  (Z = 33) - 0.002651
   */
  
	// Set default materials
	MirrorMaterial = SiliconCarbide;
	world_material_ = vacuum;
	FullEnergyDetectorMaterial= Scintillator;
	DeDxDetectorMaterial = SiliconMat;
	SiliconDetectorFrameMaterial = Kapton;
	ChamberMaterial = StainlessSteel;
	FoilMaterial = Beryllium;
	HoopMaterial = GlassyCarbon;
	MirrorMountMaterial= StainlessSteel;
	CoilsMetal = copper;
	CoilsLiquid = water;
}

void K37DetectorConstruction::SetMirrorMaterial(G4String materialChoice) // does this even still work??
{
	// get the pointer to the material table
	const G4MaterialTable* theMaterialTable = G4Material::GetMaterialTable();
	
	// search the material by its name
	G4Material*materialSelectedFromTable;
	for (size_t J = 0; J < theMaterialTable->size(); J++) 
	{
		materialSelectedFromTable = (*theMaterialTable)[J];
		if (materialSelectedFromTable ->GetName() == materialChoice) 
		{
			MirrorMaterial =materialSelectedFromTable;
			for (int i = 0; i < 2; i++) 
			{
				mirror_log[i] ->SetMaterial(materialSelectedFromTable);
			}
		}
	}
}

/*
// Do I **really** want the UpdateGeometry() function to be a thing??
void K37DetectorConstruction::UpdateGeometry() 
{
	G4RunManager::GetRunManager() -> DefineWorldVolume( ConstructK37Experiment() );
}
*/
void K37DetectorConstruction::PrintMaterialList() 
{
	const G4MaterialTable* theMaterialTable = G4Material::GetMaterialTable();
	G4cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
		   << G4endl;
	G4cout << "----------------- Materials available --------------------"
		   << G4endl;

	for (size_t J = 0; J < theMaterialTable->size(); J++) 
	{
		G4Material*  materialSelectedFromTable = (*theMaterialTable)[J];
		G4cout << materialSelectedFromTable ->GetName() << G4endl;
	}
	G4cout << "----------------- Materials available --------------------"
		   << G4endl;
	G4cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
		   << G4endl;
}

// --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- //
// --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- //
// Sensitive Detectors!
// --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- // --- o --- //

void K37DetectorConstruction::ConstructStripDetectorsSD()  // do I still even want SDman->AddNewDetector(...) ???  nah, this is done via G4VUserDetectorConstruction::SetSensitiveDetector(...) now.
{
	/*
//	G4SDManager* SDman = G4SDManager::GetSDMpointer();
	
	G4String dedx1SDname = "/mydet/dsssdPlusZ";
	if (!upper_strip_detector_sens_) 
	{
		upper_strip_detector_sens_ = new K37StripDetectorSD(dedx1SDname);
		upper_strip_detector_sens_ -> SetupParameters(strip_detector.center_position, 40, 1.0*mm);
		//                                                    pos_of_center, numStrips, stripWidth
	//	SDman->AddNewDetector(upper_strip_detector_sens_);
	}
//	upper_strip_detector_log_->SetSensitiveDetector(upper_strip_detector_sens_);
	G4VUserDetectorConstruction::SetSensitiveDetector(upper_strip_detector_log_, upper_strip_detector_sens_);

	G4String dedx2SDname = "/mydet/dsssdMinusZ";
	if (!lower_strip_detector_sens_) 
	{
		lower_strip_detector_sens_ = new K37StripDetectorSD(dedx2SDname);
		lower_strip_detector_sens_ -> SetupParameters(-1.0*strip_detector.center_position, 40, 1.0*mm);
		//                                                        pos_of_center, numStrips, stripWidth
	//	SDman->AddNewDetector(lower_strip_detector_sens_);
	}
//	lower_strip_detector_log_->SetSensitiveDetector(lower_strip_detector_sens_);
	G4VUserDetectorConstruction::SetSensitiveDetector(lower_strip_detector_log_, lower_strip_detector_sens_);
	*/
	
	G4String dedx1SDname = "/mydet/dsssdPlusZ";
//	K37StripDetectorSD * upper_strip_detector_sens_ = new K37StripDetectorSD(dedx1SDname);
//	upper_strip_detector_sens_ -> SetupParameters(strip_detector.center_position, 40, 1.0*mm);
//	G4SDManager::GetSDMpointer() -> AddNewDetector(upper_strip_detector_sens_);
//	G4VUserDetectorConstruction::SetSensitiveDetector(upper_strip_detector_log_, upper_strip_detector_sens_);

	the_detectors.Get().upper_strip_detector_sens_ = new K37StripDetectorSD(dedx1SDname);
	the_detectors.Get().upper_strip_detector_sens_ -> SetupParameters(strip_detector.center_position, 40, 1.0*mm);
	G4SDManager::GetSDMpointer() -> AddNewDetector(the_detectors.Get().upper_strip_detector_sens_);
	G4VUserDetectorConstruction::SetSensitiveDetector(upper_strip_detector_log_, the_detectors.Get().upper_strip_detector_sens_);

	G4String dedx2SDname = "/mydet/dsssdMinusZ";
//	K37StripDetectorSD * lower_strip_detector_sens_ = new K37StripDetectorSD(dedx2SDname);
//	lower_strip_detector_sens_ -> SetupParameters(-1.0*strip_detector.center_position, 40, 1.0*mm);
//	G4SDManager::GetSDMpointer() -> AddNewDetector(lower_strip_detector_sens_);
//	G4VUserDetectorConstruction::SetSensitiveDetector(lower_strip_detector_log_, lower_strip_detector_sens_);
	
	the_detectors.Get().lower_strip_detector_sens_ = new K37StripDetectorSD(dedx2SDname);
	the_detectors.Get().lower_strip_detector_sens_ -> SetupParameters(-1.0*strip_detector.center_position, 40, 1.0*mm);
	G4SDManager::GetSDMpointer() -> AddNewDetector(the_detectors.Get().lower_strip_detector_sens_);
	G4VUserDetectorConstruction::SetSensitiveDetector(lower_strip_detector_log_, the_detectors.Get().lower_strip_detector_sens_);
	
	return;	
}
void K37DetectorConstruction::ConstructScintillatorsSD()
{
//	G4cout << "Called K37DetectorConstruction::ConstructScintillatorsSD()." << G4endl;
//	
//	in Ex.B3b, we also make a new primative scorer and register it..
//  G4VPrimitiveScorer* primitiv1 = new G4PSEnergyDeposit("edep");
//  cryst->RegisterPrimitive(primitiv1);
	
	// Set up sensitive detectors
	G4String fullenergy1SDname = "/mydet/scintillatorPlusZ";
//	K37ScintillatorSD * upper_scintillator_sens_ = new K37ScintillatorSD(fullenergy1SDname);
//	G4SDManager::GetSDMpointer() -> AddNewDetector(upper_scintillator_sens_);
//	G4VUserDetectorConstruction::SetSensitiveDetector(upper_scintillator_log_, upper_scintillator_sens_);
	
	the_detectors.Get().upper_scintillator_sens_ = new K37ScintillatorSD(fullenergy1SDname);
	G4SDManager::GetSDMpointer() -> AddNewDetector( the_detectors.Get().upper_scintillator_sens_ );
	G4VUserDetectorConstruction::SetSensitiveDetector(upper_scintillator_log_, the_detectors.Get().upper_scintillator_sens_ );

	G4String fullenergy2SDname = "/mydet/scintillatorMinusZ";
//	K37ScintillatorSD * lower_scintillator_sens_ = new K37ScintillatorSD(fullenergy2SDname);
//	G4SDManager::GetSDMpointer() -> AddNewDetector(lower_scintillator_sens_);
//	G4VUserDetectorConstruction::SetSensitiveDetector(lower_scintillator_log_, lower_scintillator_sens_);
	
	the_detectors.Get().lower_scintillator_sens_ = new K37ScintillatorSD(fullenergy2SDname);
	G4SDManager::GetSDMpointer() -> AddNewDetector(the_detectors.Get().lower_scintillator_sens_ );
	G4VUserDetectorConstruction::SetSensitiveDetector(lower_scintillator_log_, the_detectors.Get().lower_scintillator_sens_ );
	
	return;	
}
void K37DetectorConstruction::ConstructBerylliumFoilsSD() 
{
//	G4cout << "Called K37DetectorConstruction::ConstructBerylliumFoilsSD()." << G4endl;
	/*
//	G4SDManager* SDman = G4SDManager::GetSDMpointer();
	for (int i = 0; i < 2; i++) 
	{
		if (!be_scorer_) 
		{
			be_scorer_ = new G4MultiFunctionalDetector("BeWindowEnergyScorer");
			G4VPrimitiveScorer *edep_sens = new G4PSEnergyDeposit("Edep");
			be_scorer_ -> RegisterPrimitive(edep_sens);
			SDman -> AddNewDetector(be_scorer_);
		}
		beryllium_log[i] -> SetSensitiveDetector(be_scorer_);
	}
//	G4VUserDetectorConstruction::SetSensitiveDetector(beryllium_log[i], be_scorer_);
	*/

	// MJA 30.July.2019:  This functionality *might have* been working incorrectly in the past, s.t.
	// only one of the two beryllium foils would have its energy deposition tracked.  I'm not certain.
	
//	G4MultiFunctionalDetector * be_scorer_t = new G4MultiFunctionalDetector("BeWindowEnergyScorerT");  // this one's the multifunctional detector.
//	G4MultiFunctionalDetector * be_scorer_b = new G4MultiFunctionalDetector("BeWindowEnergyScorerB");
//	G4VPrimitiveScorer * Be_edep_sens_t = new G4PSEnergyDeposit("BeEdepT");  // this one's the scorer.
//	G4VPrimitiveScorer * Be_edep_sens_b = new G4PSEnergyDeposit("BeEdepB");
//	be_scorer_t -> RegisterPrimitive(Be_edep_sens_t); // attach the scorer to the multifunctional detector.
//	be_scorer_b -> RegisterPrimitive(Be_edep_sens_b); // 
//	G4SDManager::GetSDMpointer()->AddNewDetector(be_scorer_t);
//	G4SDManager::GetSDMpointer()->AddNewDetector(be_scorer_b);
//	
//	G4VUserDetectorConstruction::SetSensitiveDetector(beryllium_log[0], be_scorer_t);
//	G4VUserDetectorConstruction::SetSensitiveDetector(beryllium_log[1], be_scorer_b);

	the_detectors.Get().be_scorer_t = new G4MultiFunctionalDetector("BeWindowEnergyScorerT");  // this one's the multifunctional detector.
	the_detectors.Get().be_scorer_b = new G4MultiFunctionalDetector("BeWindowEnergyScorerB");
	the_detectors.Get().Be_edep_sens_t = new G4PSEnergyDeposit("BeEdepT");  // this one's the scorer.
	the_detectors.Get().Be_edep_sens_b = new G4PSEnergyDeposit("BeEdepB");
	the_detectors.Get().be_scorer_t -> RegisterPrimitive(the_detectors.Get().Be_edep_sens_t); // attach the scorer to the multifunctional detector.
	the_detectors.Get().be_scorer_b -> RegisterPrimitive(the_detectors.Get().Be_edep_sens_b); // 
	G4SDManager::GetSDMpointer()->AddNewDetector(the_detectors.Get().be_scorer_t);
	G4SDManager::GetSDMpointer()->AddNewDetector(the_detectors.Get().be_scorer_b);
	
	G4VUserDetectorConstruction::SetSensitiveDetector(beryllium_log[0], the_detectors.Get().be_scorer_t);
	G4VUserDetectorConstruction::SetSensitiveDetector(beryllium_log[1], the_detectors.Get().be_scorer_b);
	
	return;
}
void K37DetectorConstruction::ConstructMirrorsSD() 
{
//	G4cout << "Called K37DetectorConstruction::ConstructMirrorsSD() " << G4endl;
	/*
	mirror_log[0] -> SetSensitiveDetector(mirror_scorer_t);
	mirror_log[1] -> SetSensitiveDetector(mirror_scorer_b);
	G4SDManager::GetSDMpointer() -> AddNewDetector(mirror_scorer_t);
	G4SDManager::GetSDMpointer() -> AddNewDetector(mirror_scorer_b);
	*/
//	for (int i = 0; i < 2; i++) 
//	{
//		mirror_log[i] -> SetSensitiveDetector(mirror_scorer);
//		G4SDManager::GetSDMpointer() -> AddNewDetector(mirror_scorer);
//	}
	
	// MJA 29.July.2019:  This functionality was done *wrong* in the past -- probably wrong for Ben's PRL.
	// It isn't *necessarily* an important feature to use, and it probably wouldn't change the result.  
	// More likely, it would only change evaluation of mirror scattering as a systematic by a factor of 2.
	// Or possibly do nothing at all, if it wasn't used.
	
	// Set up primitive scorer for mirror
//	G4MultiFunctionalDetector * mirror_scorer_t = new G4MultiFunctionalDetector("MirrorEnergyScorerT");
//	G4MultiFunctionalDetector * mirror_scorer_b = new G4MultiFunctionalDetector("MirrorEnergyScorerB");
//	G4VPrimitiveScorer * mirror_edep_sens_t = new G4PSEnergyDeposit("MirrorEdepT");
//	G4VPrimitiveScorer * mirror_edep_sens_b = new G4PSEnergyDeposit("MirrorEdepB");
//	mirror_scorer_t -> RegisterPrimitive(mirror_edep_sens_t);
//	mirror_scorer_b -> RegisterPrimitive(mirror_edep_sens_b);
//	G4SDManager::GetSDMpointer() -> AddNewDetector(mirror_scorer_t);
//	G4SDManager::GetSDMpointer() -> AddNewDetector(mirror_scorer_b);
//	
//	G4VUserDetectorConstruction::SetSensitiveDetector(mirror_log[0], mirror_scorer_t);
//	G4VUserDetectorConstruction::SetSensitiveDetector(mirror_log[1], mirror_scorer_b);

	the_detectors.Get().mirror_scorer_t = new G4MultiFunctionalDetector("MirrorEnergyScorerT");
	the_detectors.Get().mirror_scorer_b = new G4MultiFunctionalDetector("MirrorEnergyScorerB");
	the_detectors.Get().mirror_edep_sens_t = new G4PSEnergyDeposit("MirrorEdepT");
	the_detectors.Get().mirror_edep_sens_b = new G4PSEnergyDeposit("MirrorEdepB");
	the_detectors.Get().mirror_scorer_t -> RegisterPrimitive(the_detectors.Get().mirror_edep_sens_t);
	the_detectors.Get().mirror_scorer_b -> RegisterPrimitive(the_detectors.Get().mirror_edep_sens_b);
	G4SDManager::GetSDMpointer() -> AddNewDetector(the_detectors.Get().mirror_scorer_t);
	G4SDManager::GetSDMpointer() -> AddNewDetector(the_detectors.Get().mirror_scorer_b);

	G4VUserDetectorConstruction::SetSensitiveDetector(mirror_log[0], the_detectors.Get().mirror_scorer_t);
	G4VUserDetectorConstruction::SetSensitiveDetector(mirror_log[1], the_detectors.Get().mirror_scorer_b);

	return;
}
void K37DetectorConstruction::ConstructElectronMCPSD() // checks.
{
//	G4cout << "Called K37DetectorConstruction::ConstructElectronMCPSD()." << G4endl;
	/*
	if (!electron_mcp_sens_) 
	{
		electron_mcp_sens_ = new K37ElectronMCPSD("/mydet/electron_mcp");
	}
	G4VUserDetectorConstruction::SetSensitiveDetector(electron_mcp_log_, electron_mcp_sens_);
	*/
//	K37ElectronMCPSD * electron_mcp_sens_ = new K37ElectronMCPSD("/mydet/electron_mcp");
//	G4SDManager::GetSDMpointer() -> AddNewDetector(electron_mcp_sens_);
//	G4VUserDetectorConstruction::SetSensitiveDetector(electron_mcp_log_, electron_mcp_sens_);
	
	the_detectors.Get().electron_mcp_sens_ = new K37ElectronMCPSD("/mydet/electron_mcp");
	G4SDManager::GetSDMpointer() -> AddNewDetector(the_detectors.Get().electron_mcp_sens_);
	G4VUserDetectorConstruction::SetSensitiveDetector(electron_mcp_log_, the_detectors.Get().electron_mcp_sens_);
	return;
}
void K37DetectorConstruction::ConstructRecoilMCPSD() // checks.
{
//	G4cout << "Called K37DetectorConstruction::ConstructRecoilMCPSD()." << G4endl;
	/*
	if (!recoil_mcp_sens_) 
	{
		recoil_mcp_sens_ = new K37RecoilMCPSD("/mydet/recoil_mcp");
	}
	//
	G4VUserDetectorConstruction::SetSensitiveDetector(recoil_mcp_log_, recoil_mcp_sens_);
	*/
//	K37RecoilMCPSD * recoil_mcp_sens_ = new K37RecoilMCPSD("/mydet/recoil_mcp");
//	G4SDManager::GetSDMpointer() -> AddNewDetector(recoil_mcp_sens_);
//	G4VUserDetectorConstruction::SetSensitiveDetector(recoil_mcp_log_, recoil_mcp_sens_);
	
	the_detectors.Get().recoil_mcp_sens_ = new K37RecoilMCPSD("/mydet/recoil_mcp");
	G4SDManager::GetSDMpointer() -> AddNewDetector(the_detectors.Get().recoil_mcp_sens_);
	G4VUserDetectorConstruction::SetSensitiveDetector(recoil_mcp_log_, the_detectors.Get().recoil_mcp_sens_);
	return;
}
