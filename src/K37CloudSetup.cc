// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm 2013

#include <fstream>
#include <sstream>

#include "K37CloudSetup.hh"

#include "globals.hh"
//#include "Randomize.hh"
#include "G4SystemOfUnits.hh"  // it's in K37CloudSetup.hh now.
#include "G4UnitsTable.hh"

using std::ifstream;

K37CloudSetup::K37CloudSetup()
  : decay_time_(-10),
	cloud_center_(       G4ThreeVector(0., 0., 0.) ),
	initial_cloud_size_( G4ThreeVector(0., 0., 0.) ),
	temperature_(        G4ThreeVector(0., 0., 0.) ),
	sail_velocity_(      G4ThreeVector(0., 0., 0.) ),
	polarization_(1.0),  // 1.0 = fully polarized up.
	alignment_(1.0),     // 1.0 = fully aligned, either up or down.
	cycleTime(1.9067*ms),
	//  expansion_before_polarized_(332*microsecond), 
	//  expansion_before_polarized_(400*microsecond), 
	expansion_before_polarized_(300*microsecond), 
	MatchedRunsetLetter( G4String("0") ), 
	initialize_complete_(false) 
{
	G4cout << "Called K37CloudSetup::K37CloudSetup() [with parameters!]" << G4endl;
	CloudMessenger = new K37CloudMessenger(this);
	//
	G4cout << "Initialized cloud with center: " << cloud_center_ << G4endl;
	G4cout << "                  temperature: " << temperature_ << G4endl;
	G4cout << "                     position: " << initial_cloud_size_ << G4endl;
	G4cout << "                sail velocity: " << sail_velocity_ << G4endl;
	//
	G4cout << "K37CloudSetup::K37CloudSetup(...) [with parameters!] is finished, and so far, we think its matched runset letter is:  " << MatchedRunsetLetter << G4endl;
}

/*
K37CloudSetup::K37CloudSetup(G4ThreeVector cloud_center,
                             G4ThreeVector cloud_temperature,
                             G4ThreeVector cloud_size_start,
                             G4ThreeVector sail_velocity)
  : decay_time_(-10),
	cloud_center_(cloud_center),
	initial_cloud_size_(cloud_size_start),
	temperature_(cloud_temperature),
	sail_velocity_(sail_velocity),
	cycleTime(1.9067*ms),
	//  expansion_before_polarized_(332*microsecond), 
	//  expansion_before_polarized_(400*microsecond), 
	expansion_before_polarized_(300*microsecond), 
	MatchedRunsetLetter( G4String("0") ), 
	initialize_complete_(false) 
{
	G4cout << "Called K37CloudSetup::K37CloudSetup() [with parameters!]" << G4endl;
	CloudMessenger = new K37CloudMessenger(this);
	//
	G4cout << "Initialized cloud with center: " << cloud_center << G4endl;
	G4cout << "                  temperature: " << cloud_temperature << G4endl;
	G4cout << "                     position: " << cloud_size_start << G4endl;
	G4cout << "                sail velocity: " << sail_velocity << G4endl;
	//
	G4cout << "K37CloudSetup::K37CloudSetup(...) [with parameters!] is finished, and so far, we think its matched runset letter is:  " << MatchedRunsetLetter << G4endl;
}
*/
/*
K37CloudSetup::K37CloudSetup()
{
	G4cout << "Called K37CloudSetup::K37CloudSetup() [no parameters]" << G4endl;
	
	//
	K37CloudSetup( G4ThreeVector(0., 0., 0.),   // cloud_center
	               G4ThreeVector(0., 0., 0.),   // cloud_temperature
	               G4ThreeVector(0., 0., 0.),   // cloud_size_start
	               G4ThreeVector(0., 0., 0.) ); // sail_velocity
	//
	
	G4cout << "Done with Called K37CloudSetup::K37CloudSetup() [no parameters]." << G4endl;
}
*/

K37CloudSetup::~K37CloudSetup() 
{ 
	G4cout << "Deleting the cloud setup. " << G4endl;
	// delete the cloud messenger?  Or maybe not?
}

/*
void K37CloudSetup::makeEvent() // replaced by G4ThreeVector K37PrimaryGeneratorAction::GetEventPositionFromCloud(K37CloudSetup * the_cloud)
{
	G4cout << "We're in K37CloudSetup::makeEvent().  We should probably not use this class to make or store non-shared things." << G4endl;
	
	// Position of the decaying particle at the instant the cloud starts expanding
	if (!initialize_complete_) Initialize();

	G4ThreeVector initial_position =
	    G4ThreeVector(G4RandGauss::shoot(cloud_center_.x(), initial_cloud_size_.x()),
	                  G4RandGauss::shoot(cloud_center_.y(), initial_cloud_size_.y()),
	                  G4RandGauss::shoot(cloud_center_.z(), initial_cloud_size_.z()));

	// The trapping laser is off 200 us before the OP laser comes on and it takes
	// ~ 100 us for the atoms to polarize.  Therefore the atoms will have been
	// expanidng for at least 300 us (expansion_before_polarized_) before they
	// will be classified as "polarized" and counted.
	decay_time_ = -10;
	while (decay_time_ < expansion_before_polarized_) 
	{ // pick a new thing for "decay_time_" until you get something longer than "expansion_before_polarized_".
		decay_time_ = cycleTime*G4UniformRand();  // G4UniformRand() ranges from 0 to 1.
	}

	// Thermal velocity of individual atom (random)
	final_velocity_ = G4ThreeVector(G4RandGauss::shoot(0, velocity_sigma_.x()),
									G4RandGauss::shoot(0, velocity_sigma_.y()),
									G4RandGauss::shoot(0, velocity_sigma_.z()) );
	// Overall (bulk) movement of the cloud

	final_velocity_ += sail_velocity_;
	final_position_ = initial_position + final_velocity_*decay_time_;
}
*/

void K37CloudSetup::SetupVelocitySigma(G4ThreeVector temperature) 
{
	velocity_sigma_.setX(CalcSigma(temperature.x()));
	velocity_sigma_.setY(CalcSigma(temperature.y()));
	velocity_sigma_.setZ(CalcSigma(temperature.z()));
}

G4double K37CloudSetup::CalcSigma(G4double temperature) 
{
	// MB velocity distribution for each dimension is gaussian (normal) centered
	// at zero with standard deviation sigma = sqrt(kT/m)
	// G4 keeps masses in terms of energy so the c*c gives it actual velocity
	// units
	/*
	G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
	// Mass of Ar37...
	// MJA:  Since we're using this to calculate cloud expansion, we want 37K, not 37Ar.
	G4double mass = particleTable -> GetIon(19, 37, 0) -> GetPDGMass();
	// G4ParticleTable::GetIon() is obsolete and has been removed.  Use G4IonTable::GetIon() instead.
	*/
	
	G4IonTable * ionTable = G4IonTable::GetIonTable();
	G4double mass = ionTable -> GetIon(19, 37, 0) -> GetPDGMass();
	
	G4double mean_speed = sqrt(temperature * k_Boltzmann * c_squared / mass);
	return mean_speed;
}

void K37CloudSetup::SetMatchedRunsetLetter(G4String newRunsetLetter)
{
	MatchedRunsetLetter = newRunsetLetter;
}

G4String K37CloudSetup::GetMatchedRunsetLetter()
{
	return MatchedRunsetLetter;
}


void K37CloudSetup::SetCloudCenter(G4ThreeVector center) 
{
	bool verbose = false;
	cloud_center_ = center;
	if(verbose)
	{
		G4cout << "Updating cloud center to (" << G4BestUnit(center.x(), "Length")
		       << ", " << G4BestUnit(center.y(), "Length") << ", "
		       << G4BestUnit(center.z(), "Length") << ")" << G4endl;
	}
}

void K37CloudSetup::SetTemperature(G4ThreeVector temp) 
{
	bool verbose=false;
	temperature_ = temp;
	if (temp.x() < 0 || temp.y() < 0 || temp.z() < 0) 
	{
		G4cout << "ERROR! Cannot set negative temperature." << G4endl;
	} 
	else
	{
		if(verbose)
		{
			G4cout << "Updating cloud temp to (" << G4BestUnit(temp.x(), "Temperature")
			       << ", " << G4BestUnit(temp.y(), "Temperature") << ", "
			       << G4BestUnit(temp.z(), "Temperature") << ")" << G4endl;
		}
		SetupVelocitySigma(temp);
	}
}

void K37CloudSetup::SetTemperature(G4double temp) 
{
	SetTemperature(G4ThreeVector(temp, temp, temp));
}

void K37CloudSetup::SetInitialCloudSize(G4ThreeVector size) 
{
	bool verbose = false;
	if (size.x() < 0 || size.y() < 0 || size.z() < 0) 
	{
		G4cout << "ERROR! Cannot set negative cloud size" << G4endl;
	} 
	else 
	{
		if(verbose)
		{
			G4cout << "Updating cloud size to (" << G4BestUnit(size.x(), "Length")
			       << ", " << G4BestUnit(size.y(), "Length") << ", "
			       << G4BestUnit(size.z(), "Length") << ")" << G4endl;
		}
		initial_cloud_size_ = size;
	}
}

void K37CloudSetup::SetInitialCloudSize(G4double size) 
{
	SetInitialCloudSize(G4ThreeVector(size, size, size));
}

void K37CloudSetup::Initialize() 
{
	SetupVelocitySigma(temperature_);
	initialize_complete_ = true;
}

void K37CloudSetup::SetSailVelocity(G4ThreeVector vel) 
{
	bool verbose = false;
	if(verbose)
	{
		G4cout << "Updating sail velocity to (" << vel.x()/(mm/microsecond) << ", "
		       << vel.y()/(mm/microsecond) << ", " << vel.z()/(mm/microsecond) << ")"
		       << " mm / us " << G4endl;
	}
	sail_velocity_ = vel;
}

void K37CloudSetup::SetFreeExpansionTime(G4double this_time)
{
	bool verbose = false;
	expansion_before_polarized_ = this_time;
	if(verbose)
	{
		G4cout << "Updating expansion time before polarized to:  ";
		G4cout << G4BestUnit(expansion_before_polarized_, "Time") << G4endl;
	}
}
void K37CloudSetup::SetOP_CycleTime(G4double this_time)
{
	bool verbose = false;
	cycleTime = this_time;
	if(verbose)
	{
		G4cout << "Updating total OP cycle time to:  " << G4BestUnit(cycleTime, "Time") << G4endl;
	}
}

void K37CloudSetup::SetPolarization(double pol) 
{
	if (fabs(pol) <= 1.0) 
	{
		polarization_ = pol;
		G4cout << "Polarization set to " << pol << G4endl;
	} 
	else 
	{
		G4cout << "WARNING: Polarization " << pol
			   << " not in allowed range.  No changes made." << G4endl;
	}
}
void K37CloudSetup::SetAlignment(double ali) 
{
	if (ali <= 1.0 && ali >= 0.0) 
	{
		alignment_ = ali;
		G4cout << "Alignment set to " << ali << G4endl;
	} 
	else if (ali < 0.0 && ali >= -1.0) 
	{
		ali = fabs(ali);
		G4cout << "WARNING: Alignment must be positive (for both polarization states)";
		G4cout << G4endl;
		G4cout << "Note that this sign convention is opposite that in JTW 1957 and that";
		G4cout << G4endl;
		G4cout << "this change is fully documented in the \"JTWEvent.cc\" source file";
		G4cout << G4endl;
		G4cout << "*********************************************************" << G4endl;
		G4cout << "   Setting alignment to T = +" << ali << G4endl;
		G4cout << "*********************************************************" << G4endl;
		alignment_ = ali;
	} 
	else 
	{
		G4cout << "WARNING: Alignment " << ali
			   << " not in allowed range.  No changes made." << G4endl;
	}
}
