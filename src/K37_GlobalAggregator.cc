// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013

// C / C++
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>

//
#include "G4Threading.hh"
#include "G4UIcommand.hh"
#include "G4DigiManager.hh"

// Project Specific
#include "K37_GlobalAggregator.hh"
#include "K37RunManagerMT.hh"
#include "K37PrimaryGeneratorAction.hh"
#include "K37EventGenerator.hh"
#include "K37EventAction.hh"
#include "K37RunAction.hh"
// #include "K37ScintillatorDigitizer.hh"  // 

#include "hadd.hh"

using std::abs;
//using std::cerr;
using std::cout;
using std::endl;
//using std::left;
//using std::setw;
//using std::sqrt;
using std::ifstream;


bool fexists(const std::string& filename) 
{
	ifstream ifile(filename.c_str());
	bool good = ifile.is_open();
	ifile.close();
	return good;
}

// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 
GlobalAggregator::GlobalAggregator():
	autoincrement(true),
	setsave_metadata(true),
	metadata_filename(G4String("Output/MetaData.txt")),
	config_filename("SetupScripts/IOconfiguration.txt")  // bake it in here.
{
	G4cout << "Creating a new GlobalAggregator::GlobalAggregator()." << G4endl;
	theNTuple = 0;
	rootFile = 0;
	
	SetupIO();
	G4cout << "Done creating the GlobalAggregator." << G4endl;
}

void GlobalAggregator::SetupIO()
{
	bool v = false;
	if(v) { G4cout << "Called GlobalAggregator::SetupIO()" << G4endl; }
	
	//	
	FILE *io_file;
	io_file = fopen(config_filename.c_str(), "r");
	if(io_file==NULL)
	{
		G4cout << "Could not open " << config_filename << G4endl;
		return;
	}
	else
	{
		if(v) { G4cout << "Opened " << config_filename << G4endl; }
	}
	
	char output_directory[400];
	char thirdparam[400];         // thirdparam probably says "output".  it's not even third anymore either.
	int  tmp_filenumber = 1;
	
	fscanf(io_file, "%s", output_directory);
	fscanf(io_file, "%s", thirdparam);
	fscanf(io_file, "%i", &tmp_filenumber);
	
	filenumber = G4int(tmp_filenumber);   // this is the file number recorded in IOConfiguration.txt.  If we just compiled, it's probably "1".  We'll check later.
	namestub   = G4String(thirdparam);      
	out_dir    = G4String(output_directory);
	
	if(autoincrement)
	{
		// New:  out_dir will come with a trailing slash.
		G4String fn      = out_dir + namestub + G4String("_1") + G4String(".root");  
		G4String fn_tmp  = out_dir + namestub + G4String("_1") + G4String("_tmp0") + G4String(".root");  
		
		if(v)
		{
			if( fexists(fn) )     { G4cout << "file exists:         " << fn     << G4endl; }
			else                  { G4cout << "file does not exist: " << fn     << G4endl; }
			if( fexists(fn_tmp) ) { G4cout << "file exists:         " << fn_tmp << G4endl; }
			else                  { G4cout << "file does not exist: " << fn_tmp << G4endl; }
		}

		while( fexists(fn) || fexists(fn_tmp) )
		{
			filenumber++;
			fn     = out_dir+namestub + G4String("_") + G4String(patch::to_string(filenumber)) + G4String(".root");  
			fn_tmp = out_dir+namestub + G4String("_") + G4String(patch::to_string(filenumber)) + G4String("_tmp0") + G4String(".root");  
			// only check for tmp file from worker thread 0.
			
			if(v)
			{
				if( fexists(fn) )     { G4cout << "file exists:         " << fn     << G4endl; }
				else                  { G4cout << "file does not exist: " << fn     << G4endl; }
				if( fexists(fn_tmp) ) { G4cout << "file exists:         " << fn_tmp << G4endl; }
				else                  { G4cout << "file does not exist: " << fn_tmp << G4endl; }
			}
		}
		if(v) 
		{ 
			G4cout << "filenumber = " << filenumber << G4endl; 
			G4cout << "global mini-name will be:  " << GetGlobalMiniName()  << G4endl;
		}
		
		// put the filenumber results back in the file...
		fprintf(io_file, "%s %s %i", out_dir.c_str(), namestub.c_str(), filenumber);
	}
	// close the file when we're done.
	fclose(io_file);   // 
	
	G4cout << "Set global output file:  " << GetGlobalFullName()  << G4endl;
	if(v)
	{
		G4cout << "Done with GlobalAggregator::SetupIO()."  << G4endl;
	}
}

GlobalAggregator::~GlobalAggregator() 
{
	G4cout << "Deleting the GlobalAggregator ..." << G4endl;
	
	// do I even want to delete these things??
	if(rootFile) delete rootFile;
	if(theNTuple) delete theNTuple;
}


// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 
void GlobalAggregator::EndRun() // Does this thing actually never get called???
{
	G4cout << "Called GlobalAggregator::EndRun()" << G4endl;
	
	G4RunManager * RunMan        = G4RunManager::GetRunManager();  
	G4RunManager * MasterRunMan  = G4MTRunManager::GetMasterRunManager();  
	
	G4cout << "From within Aggregator::EndRun():   MasterRunMan->GetUserRunAction()->IsMaster() = " << MasterRunMan->GetUserRunAction()->IsMaster() << G4endl;
	G4cout << "From within Aggregator::EndRun():   RunMan->GetUserRunAction()->IsMaster()       = " << RunMan->GetUserRunAction()->IsMaster()       << G4endl;
	G4cout << "From within Aggregator::EndRun():   G4Threading::G4GetThreadId()                 = " << G4Threading::G4GetThreadId()                 << G4endl;
	G4cout << "From within Aggregator::EndRun():   G4Threading::IsWorkerThread()                = " << G4Threading::IsWorkerThread()                << G4endl;
	
	
	// Merge TTrees !
	// Total accepted doesn't come out right!
	G4cout << "OK, we're going to try merging TTrees and putting them into a new root file... maybe." << G4endl;
	
	
	G4cout << "We've finished GlobalAggregator::EndRun()." << G4endl;
}

void GlobalAggregator::MergeRootFiles()
{
	vector<string> the_filenames;
	K37RunManagerMT * MasterRunMan  = K37RunManagerMT::GetMasterRunManager();
	
	// How many worker threads?
	int N_workers = K37RunManagerMT::GetMasterRunManager()->GetNumberOfThreads();
	G4cout << "N_workers = " << N_workers << ";\tGetGlobalNameStub() = " << GetGlobalNameStub() << G4endl;
	
	// Which root files will we hadd?
	for (int i=0; i<N_workers; i++)
	{
		the_filenames.push_back( Make_LocalFileName(i) );
	}
	
	bool delete_when_hadded = false;
	hadd2(GetGlobalFullName(), the_filenames, delete_when_hadded);
}

void GlobalAggregator::SaveMetaData()
{
//	G4cout << "Called GlobalAggregator::SaveMetaData()." << G4endl;
	if( !setsave_metadata )              { return; } 
	if( G4Threading::IsWorkerThread() )  { return; } 
	
	
	FILE *io_file;
	io_file = fopen(metadata_filename, "a+");  // create the file if it doesn't exist.
	if(io_file==NULL)
	{
		G4cout << "ERROR:  " << metadata_filename << " could not be opened." << G4endl;
		return;
	}
	// otherwise, do stuff.
	
	K37ElectricFieldSetup     * Efield        = K37RunManagerMT::GetMasterRunManager()->GetUserDetectorConstruction()->GetField();  // hopefully this doesn't kill it...
	K37PhysicsList            * physlist      = K37RunManagerMT::GetMasterRunManager()->GetPhysicsList();
	K37RunAction              * the_runaction =  (K37RunAction*)(G4RunManager::GetRunManager()->GetUserRunAction());
//	G4RunManager              * runMan        = G4RunManager::GetRunManager();
	K37RunManagerMT           * MasterRunMan  = K37RunManagerMT::GetMasterRunManager();
	K37CloudSetup             * cloud         = (MasterRunMan->GetUserActionInitialization())->GetCloud();
	
	if( !cloud ) { G4cout << "no cloud.  :("  << G4endl; }
	G4cout << "cloud->GetPolarization() = " << cloud->GetPolarization() << G4endl;
	G4cout << "cloud->GetAlignment()    = " << cloud->GetAlignment() << G4endl;
	
//	//  K37PrimaryGeneratorAction is only created for the worker threads.  the master thread doesn't have one.  
//	// I need pga directly, to get event acceptance types, and recoil charge.
//	// and indirectly to get the event generator to get mincostheta, and to get the isotope, to get rho.
//	K37PrimaryGeneratorAction * pga      = (K37PrimaryGeneratorAction*)G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction();  // why no pga??
//	if(pga) { G4cout << "pga exists!" << G4endl; }
//	else { G4cout << "no pga.  :(" << G4endl; }
//	K37EventGenerator         * eGen     = pga -> GetEventGenerator();  // this breaks on the master thread, but it's fine on the worker threads.
//	Isotope                   * isotope  = eGen -> GetSMphysics();
	
//	G4cout << "* beeteedubs, the matched runset letter is:  " << cloud->GetMatchedRunsetLetter() << G4endl;
//	G4cout << "Efield->GetConstantFieldValue() = " << Efield->GetConstantFieldValue() << G4endl;
	
	fprintf(io_file, "%i\t",   filenumber);
	fprintf(io_file, "%s\t",   GetGlobalMiniName().c_str() );
	fprintf(io_file, "%i\t",   0);  // "has_been_summed/I:"
	fprintf(io_file, "%i\t",   0);  //    "is_a_sum/I:"
	
	fprintf(io_file, "%s\t",   (the_runaction->Get_AcceptanceTypesString()).c_str() ); 
	fprintf(io_file, "%s\t",   (cloud->GetMatchedRunsetLetter()).c_str() );     // 
	
	fprintf(io_file, "%i\t",   the_runaction->Get_Nevents_total() );      // number of events generated.
	fprintf(io_file, "%i\t",   the_runaction->Get_Naccepted_total() );    // number of events accepted.
	
//	fprintf(io_file, "%f\t",   eGen->GetMinCosTheta() );
	fprintf(io_file, "%f\t",   -10.0 );  // MinCosTheta placeholder.
	
	fprintf(io_file, "%f\t",   (Efield->GetConstantFieldValue())/(volt/cm) );
	//
	fprintf(io_file, "%f\t",   -10.0 ); // "DipoleMoment/D:" placeholder.
	fprintf(io_file, "%f\t",   -10.0 ); // "QuadrupoleMoment/D:" placeholder.
	fprintf(io_file, "%f\t",   -10.0 ); // "OctopoleMoment/D:" placeholder.
	
	
	fprintf(io_file, "%i\t",   Efield->GetStepperTypeIndex() );
	fprintf(io_file, "%s\t",   (Efield->GetStepperName()).c_str() );   //
	fprintf(io_file, "%f\t",   (Efield->GetMinStep())/mm );
	
//	G4cout << "About to look for the EmName from PhysicsList." << G4endl;
	fprintf(io_file, "%s\t",   (physlist->GetEmName()).c_str());  // this one breaks it.  Or not?

	// true cloud parameters.
	fprintf(io_file, "%f\t",   cloud->GetCloudCenter().x()/mm); 
	fprintf(io_file, "%f\t",   cloud->GetCloudCenter().y()/mm);  
	fprintf(io_file, "%f\t",   cloud->GetCloudCenter().z()/mm);  
	fprintf(io_file, "%f\t",   cloud->GetInitialCloudSize().x()/mm);
	fprintf(io_file, "%f\t",   cloud->GetInitialCloudSize().y()/mm);
	fprintf(io_file, "%f\t",   cloud->GetInitialCloudSize().z()/mm);
	fprintf(io_file, "%f\t",   cloud->GetTemperature().x()/kelvin*1000.0 );  // .  
	fprintf(io_file, "%f\t",   cloud->GetTemperature().y()/kelvin*1000.0 );  // .  
	fprintf(io_file, "%f\t",   cloud->GetTemperature().z()/kelvin*1000.0 );  // .  
	fprintf(io_file, "%f\t",   cloud->GetSailVelocity().x()/(mm/ms) );
	fprintf(io_file, "%f\t",   cloud->GetSailVelocity().y()/(mm/ms) );
	fprintf(io_file, "%f\t",   cloud->GetSailVelocity().z()/(mm/ms) );
	//
	fprintf(io_file, "%f\t",   cloud->GetFreeExpansionTime()/ms);
	fprintf(io_file, "%f\t",   cloud->GetOP_CycleTime()/ms);
	
//	fprintf(io_file, "%i\t",   pga->GetRecoilCharge() );     // 
//	fprintf(io_file, "%i\t",   1 );     //  recoil charge placeholder.
	
	// Notation that will probably become obsolete (or maybe we'll just treat them as derived quantities?)
	
//	fprintf(io_file, "%f\t",   isotope -> FindValue("RHO") );   //  Does it work yet?
	fprintf(io_file, "%f\t",   -10.0 );   //  rho placeholder
	
	fprintf(io_file, "%f\t",   cloud->GetPolarization() );  // this doesn't seem to work...
	fprintf(io_file, "%f\t",   cloud->GetAlignment() );

	
	fprintf(io_file, "\n");
	//
	fclose(io_file);
	G4cout << "Done with GlobalAggregator::SaveMetaData()." << G4endl;
}

void GlobalAggregator::PrintMetaDataHeader()
{
	G4cout << "Called GlobalAggregator::PrintMetaDataHeader()." << G4endl;
	
	FILE *io_file;
	io_file = fopen(metadata_filename, "a+");
	if(io_file == NULL)
	{
		G4cout << "Couldn't open the MetaData file, for some reason..." << G4endl;
		G4cout << "metadata_filename:  " << metadata_filename << G4endl;
		return;
	}
	
	fprintf(io_file, "%s", "Run/I:");
	fprintf(io_file, "%s", "Filename/C:");
	fprintf(io_file, "%s", "has_been_summed/I:");  // formerly "BadFlag"
	fprintf(io_file, "%s", "is_a_sum/I:");  // formerly "is_summed"
	
	fprintf(io_file, "%s", "SaveEventTypes/C:");
	fprintf(io_file, "%s", "matches_runset/C:");
	
	fprintf(io_file, "%s", "EventsGenerated/I:");
	fprintf(io_file, "%s", "EventsSaved/I:");
	fprintf(io_file, "%s", "MinCosTheta/D:");

	fprintf(io_file, "%s", "ChargeState/I:");
	fprintf(io_file, "%s", "Efield/D:");
	
	// Polarization (new notation)
	fprintf(io_file, "%s", "DipoleMoment/D:");
	fprintf(io_file, "%s", "QuadrupoleMoment/D:");
	fprintf(io_file, "%s", "OctopoleMoment/D:");
	
	fprintf(io_file, "%s", "StepperType/I:");
	fprintf(io_file, "%s", "StepperName/C:");
	fprintf(io_file, "%s", "StepMax_mm/D:"); // is this ... actually the minimum step?
	fprintf(io_file, "%s", "PhysicsListName/C:");
	
	// Cloud parameters:
	fprintf(io_file, "%s", "Trap_x_mm/D:");
	fprintf(io_file, "%s", "Trap_y_mm/D:");
	fprintf(io_file, "%s", "Trap_z_mm/D:");
	fprintf(io_file, "%s", "Trap_sigma_x/D:");
	fprintf(io_file, "%s", "Trap_sigma_y/D:");
	fprintf(io_file, "%s", "Trap_sigma_z/D:");
	fprintf(io_file, "%s", "Temperature_x_mK/D:");  // 
	fprintf(io_file, "%s", "Temperature_y_mK/D:");  // 
	fprintf(io_file, "%s", "Temperature_z_mK/D:");  // 
	fprintf(io_file, "%s", "SailVelocity_x_mm_per_ms/D:");
	fprintf(io_file, "%s", "SailVelocity_y_mm_per_ms/D:");
	fprintf(io_file, "%s", "SailVelocity_z_mm_per_ms/D:");
	
	fprintf(io_file, "%s", "ExpandBeforePolarized_ms/D:");  // 
	fprintf(io_file, "%s", "OP_CycleTime_ms/D:");  // 
//	fprintf(io_file, "%i\t",   1 );     //  recoil charge goes here.
	
	// Things in old notation that will probably become obsolete soon:
	fprintf(io_file, "%s", "Rho/D:");
	fprintf(io_file, "%s", "Polarization/D:");
	fprintf(io_file, "%s", "Alignment/D");


	fprintf(io_file, "\n");
	fclose(io_file);
}

