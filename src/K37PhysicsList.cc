// PhysicsList class:  
// \file electromagnetic/TestEm7/include/PhysicsList.hh
// Code originally from Geant4 9.x, 
// Pasted here by Ben Fenker and modified slightly.
// Replaced by the equivalent code in Geant 4.10.5, 
// Modified by Melissa Anholm to match Ben Fenker's previous modifications.
// Further modified by Melissa Anholm to incorporate a whole 'nother related
//   class, which was previously coded by some combination of 
//   Spencer/Ben/Melissa.  
// 
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm - 2013

// #include "globals.hh"  // in K37PhysicsList.hh already

#include "G4ProcessManager.hh"
#include "G4ParticleTypes.hh"        // needed?
#include "G4Decay.hh"
#include "G4ComptonScattering.hh"    // needed?
#include "G4GammaConversion.hh"      // needed?
#include "G4PhotoElectricEffect.hh"  // needed?
#include "G4eMultipleScattering.hh"  // needed?
#include "G4eIonisation.hh"          // needed?
#include "G4eBremsstrahlung.hh"      // needed?
#include "G4eplusAnnihilation.hh"    // needed?
#include "G4hMultipleScattering.hh"  // needed?
#include "G4IonConstructor.hh"       // needed?
#include "G4ionIonisation.hh"        // needed?
#include "G4IonParametrisedLossModel.hh"
#include "G4StepLimiter.hh"   // G4StepLimiter, not StepMax ...
#include "G4UserSpecialCuts.hh"
#include "G4DecayTable.hh"           // needed?

#include "K37PhysicsList.hh"
#include "K37PhysicsListMessenger.hh"  // probably included by K37PhysicsList.hh.  Or maybe not.

#include "PhysListEmStandard.hh"
#include "PhysListEmStandardNR.hh"
#include "G4EmStandardPhysics.hh"
#include "G4EmStandardPhysics_option1.hh"
#include "G4EmStandardPhysics_option2.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmStandardPhysics_option4.hh"
#include "G4EmStandardPhysicsWVI.hh"
#include "G4EmStandardPhysicsGS.hh"
#include "G4EmStandardPhysicsSS.hh"

#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4EmLowEPPhysics.hh"

#include "G4DecayPhysics.hh"

#include "G4HadronElasticPhysics.hh"
#include "G4HadronDElasticPhysics.hh"
#include "G4HadronHElasticPhysics.hh"
#include "G4HadronInelasticQBBC.hh"
#include "G4IonPhysics.hh"

#include "G4LossTableManager.hh"
#include "G4EmConfigurator.hh"
#include "G4UnitsTable.hh"

#include "StepMax.hh"
#include "G4IonFluctuations.hh"
#include "G4UniversalFluctuation.hh"
#include "G4BraggIonGasModel.hh"
#include "G4BetheBlochIonGasModel.hh"
#include "G4PhysicalConstants.hh"

//
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//PhysicsList::PhysicsList() : G4VModularPhysicsList(),
//K37PhysicsList::K37PhysicsList() : K37PhysicsList_(),  // bf, copied by mja.
K37PhysicsList::K37PhysicsList() : 
	G4VModularPhysicsList(), 
	fStepMaxProcess(nullptr)//, 
//	theDecayProcess(0)
{
	//   G4LossTableManager::Instance();  // do I need this??

	defaultCutValue = 1.*mm;  // defined in G4VUserPhysicsList, apparently.  And G4VModularPhysicsList inherits from G4VUserPhysicsList.
	cutForGamma     = defaultCutValue;
	cutForElectron  = defaultCutValue;
	cutForPositron  = defaultCutValue;

	fHelIsRegisted  = false;
	fBicIsRegisted  = false;
	fBiciIsRegisted = false;

	// protected member of the base class
	verboseLevel = 1;
//	SetVerboseLevel(1);  // uh....  this is from the original K37PhysicsList class...
	SetVerboseLevel(0);  

	fMessenger = new K37PhysicsListMessenger(this);

	// EM physics
	fEmName = G4String("emstandard_opt0");  
	fEmPhysicsList = new G4EmStandardPhysics(verboseLevel);

	// Deacy physics and all particles
	fDecPhysicsList = new G4DecayPhysics(verboseLevel);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
K37PhysicsList::~K37PhysicsList()
{
	delete fMessenger;
	delete fEmPhysicsList;
	delete fDecPhysicsList;
	for(size_t i=0; i<fHadronPhys.size(); i++) {delete fHadronPhys[i];}
//	delete theDecayProcess;  // from original K37PhysicsList.  Is this deleted in the correct order???
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void K37PhysicsList::ConstructParticle()
{
	fDecPhysicsList->ConstructParticle();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void K37PhysicsList::ConstructProcess()
{
	// transportation
	AddTransportation();

	// electromagnetic physics list
	fEmPhysicsList->ConstructProcess();

	// decay physics list
	fDecPhysicsList->ConstructProcess();

	// hadronic physics lists
	for(size_t i=0; i<fHadronPhys.size(); i++) 
	{
		fHadronPhys[i]->ConstructProcess();
	}

	// step limitation (as a full process)
	AddStepMax();  
	G4cout << "Finished with K37PhysicsList::ConstructProcess()." << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void K37PhysicsList::AddPhysicsList(const G4String& name)
{
	if (verboseLevel>1) 
	{
		G4cout << "PhysicsList::AddPhysicsList: <" << name << ">" << G4endl;
	}

	if (name == fEmName) { return; }

	if (name == "local") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new PhysListEmStandard(name);
	} 
	else if (name == "emstandard_opt0") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmStandardPhysics(verboseLevel);
	} 
	else if (name == "emstandard_opt1") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmStandardPhysics_option1(verboseLevel);
	} 
	else if (name == "emstandard_opt2") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmStandardPhysics_option2(verboseLevel);
	} 
	else if (name == "emstandard_opt3") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmStandardPhysics_option3(verboseLevel);
	} 
	else if (name == "emstandard_opt4") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmStandardPhysics_option4(verboseLevel);
	} 
	else if (name == "ionGasModels") 
	{
		AddPhysicsList("emstandard_opt0");
		fEmName = name;
		AddIonGasModels();
	} 
	else if (name == "standardNR") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new PhysListEmStandardNR(name);
	} 
	else if (name == "emlivermore") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmLivermorePhysics(verboseLevel);
	} 
	else if (name == "empenelope") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmPenelopePhysics(verboseLevel);
	} 
	else if (name == "emlowenergy") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmLowEPPhysics(verboseLevel);
	} 
	else if (name == "emstandardSS") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmStandardPhysicsSS(verboseLevel);
	} 
	else if (name == "emstandardWVI") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmStandardPhysicsWVI(verboseLevel);
	} 
	else if (name == "emstandardGS") 
	{
		fEmName = name;
		delete fEmPhysicsList;
		fEmPhysicsList = new G4EmStandardPhysicsGS(verboseLevel);
	} 
	else if (name == "elastic" && !fHelIsRegisted) 
	{
		fHadronPhys.push_back( new G4HadronElasticPhysics(verboseLevel));
		fHelIsRegisted = true;
	} 
	else if (name == "DElastic" && !fHelIsRegisted) 
	{
		fHadronPhys.push_back( new G4HadronDElasticPhysics(verboseLevel));
		fHelIsRegisted = true;
	} 
	else if (name == "HElastic" && !fHelIsRegisted) 
	{
		fHadronPhys.push_back( new G4HadronHElasticPhysics(verboseLevel));
		fHelIsRegisted = true;
	} 
	else if (name == "binary" && !fBicIsRegisted) 
	{
		fHadronPhys.push_back(new G4HadronInelasticQBBC(verboseLevel));
		fBicIsRegisted = true;
	} 
	else if (name == "binary_ion" && !fBiciIsRegisted) 
	{
		fHadronPhys.push_back(new G4IonPhysics(verboseLevel));
		fBiciIsRegisted = true;
	} 
	else 
	{
		G4cout << "PhysicsList::AddPhysicsList: <" << name << ">"
		       << " is not defined"
		       << G4endl;
	}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*
void K37PhysicsList_::AddStepMax() 
{
	G4cout << "*** Called K37PhysicsList_::AddStepMax()" << G4endl;

	// Step limitation seen as a process
	// G4StepLimiter* stepLimiter = new G4StepLimiter();
	// G4UserSpecialCuts* userCuts = new G4UserSpecialCuts();
	
	// theParticleIterator is obsolete.  We have to make a new local particle iterator.
	//theParticleIterator->reset();
	G4ParticleTable::G4PTblDicIterator* a_new_particle_iterator = this->GetParticleIterator();
	a_new_particle_iterator -> reset();
//	while ((*theParticleIterator)()) 
	while ((*a_new_particle_iterator)()) 
	{
		G4ParticleDefinition* particle = a_new_particle_iterator->value();
		G4ProcessManager* pmanager = particle->GetProcessManager();
		
		if (particle->GetPDGCharge() != 0.0) 
		{
			pmanager ->AddDiscreteProcess(stepLimiter);
			// pmanager ->AddDiscreteProcess(userCuts);
		}
	}
}
*/

void K37PhysicsList::AddStepMax()
{
	G4cout << "*** Called PhysicsList::AddStepMax()" << G4endl;
	
	// Step limitation seen as a process
	fStepMaxProcess = new StepMax();

	// theParticleIterator is obsolete.  We have to make a new local particle iterator.
	// below is one way you can do it.  but what's actually used is another (better??) way to do it.
	// G4ParticleTable::G4PTblDicIterator* a_new_particle_iterator = this->GetParticleIterator();

	auto particleIterator=GetParticleIterator();
	particleIterator->reset();
//	int i=0;
	while ((*particleIterator)())
	{
	//	G4cout << "* Adding processes for particle " << i << ":  " << G4endl;
	//	i++;
		//
		G4ParticleDefinition* particle = particleIterator->value();
		G4ProcessManager* pmanager = particle->GetProcessManager();

		//  IsApplicable(...)  returns:   (particle.GetPDGCharge() != 0. && !particle.IsShortLived())
		if (fStepMaxProcess->IsApplicable(*particle) && pmanager)
		{
			pmanager ->AddDiscreteProcess(fStepMaxProcess);
		}
	}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void K37PhysicsList::AddIonGasModels()
{
	G4EmConfigurator* em_config = G4LossTableManager::Instance()->EmConfigurator();
	auto particleIterator=GetParticleIterator();
	particleIterator->reset();
	while ((*particleIterator)())
	{
		G4ParticleDefinition* particle = particleIterator->value();
		G4String partname = particle->GetParticleName();
		if(partname == "alpha" || partname == "He3" || partname == "GenericIon") 
		{
			G4BraggIonGasModel* mod1 = new G4BraggIonGasModel();
			G4BetheBlochIonGasModel* mod2 = new G4BetheBlochIonGasModel();
			G4double eth = 2.*MeV*particle->GetPDGMass()/proton_mass_c2;
			em_config->SetExtraEmModel(partname,"ionIoni",mod1,"",0.0,eth,     new G4IonFluctuations());
			em_config->SetExtraEmModel(partname,"ionIoni",mod2,"",eth,100*TeV, new G4UniversalFluctuation());
		}
	}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// below:  stuff re-added by mja, taken from the old version.
void K37PhysicsList::SetCuts() 
{
	if (verboseLevel >0) 
	{
		G4cout << "PhysicsList::SetCuts:";
		G4cout << "CutLength : " << G4BestUnit(defaultCutValue, "Length") << G4endl;
	}
	SetCutsWithDefault();  // set *all* the cuts...  does this include anything other than gammas and e+/-'s?
	
	// set cut values for gamma at first and for e- second and next for e+,
	// because some processes for e+/e- need cut values for gamma
	SetCutValue(cutForGamma, "gamma");
	SetCutValue(cutForElectron, "e-");
	SetCutValue(cutForPositron, "e+");

	if (verboseLevel>0) DumpCutValuesTable();   // MELISSA LOOK HERE!!  this is the thing that outputs that giant table and won't stfu.
}

// ...oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
void K37PhysicsList::SetCutForGamma(G4double cut) 
{
	cutForGamma = cut;
	SetParticleCuts(cutForGamma, G4Gamma::Gamma());
}

// ...oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
void K37PhysicsList::SetCutForElectron(G4double cut) 
{
	cutForElectron = cut;
	SetParticleCuts(cutForElectron, G4Electron::Electron());
}

// ...oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
void K37PhysicsList::SetCutForPositron(G4double cut) 
{
	cutForPositron = cut;
	SetParticleCuts(cutForPositron, G4Positron::Positron());
}

// ...oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// ...oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
