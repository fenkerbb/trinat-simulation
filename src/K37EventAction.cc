// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm 2014

#include <math.h>
#include <algorithm>
#include <iomanip>
#include <cmath>
#include <map>

//#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"
#include "G4UnitsTable.hh"
#include "G4DigiManager.hh"
#include "G4THitsMap.hh" // ??

#include "K37Config.hh"
//#include "K37ContainerForStripInformation.hh"
//#include "K37ElectronMCPHit.hh"  // Not needed here??
//#include "K37RecoilMCPHit.hh"  // Not needed here??
//#include "K37ScintillatorHit.hh"  // Not needed here??
#include "K37PrimaryGeneratorAction.hh"

#include "K37ScintillatorDigitizer.hh"
#include "K37StripDetectorDigitizer.hh"

//#include "K37_LocalAggregator.hh"
#include "K37EventGenerator.hh"
#include "K37EventAction.hh"

#include "K37RunManagerMT.hh"
#include "K37RunAction.hh"
#include "K37Run.hh"

using std::map;

using std::min;
using std::pow;
using std::ofstream;
using std::setw;
using std::left;

using K37_ABC::K37_Data;  // where is this included?

K37EventAction::K37EventAction()
    :
//	v1190_factor_ns(0.09765625),  // in K37Run -- do we still need it here?
//	event_type(0),
    stripHandler(new K37ContainerForStripInformation())//,
	/*
    energyUpperScint_Total(0),
    energyUpperScint_AllElse(0),
    energyUpperScint_Positron(0),
    energyUpperScint_Electron(0),
    energyUpperScint_Gamma(0),
    energyLowerScint_Total(0),
    energyLowerScint_AllElse(0),
    energyLowerScint_Positron(0),
    energyLowerScint_Electron(0),
    energyLowerScint_Gamma(0),
    energyUpperSilicon_Total(0),
    energyUpperSilicon_AllElse(0),
    energyUpperSilicon_Positron(0),
    energyUpperSilicon_Electron(0),
    energyUpperSilicon_Gamma(0),
    energyLowerSilicon_Total(0),
    energyLowerSilicon_AllElse(0),
    energyLowerSilicon_Positron(0),
    energyLowerSilicon_Electron(0),
    energyLowerSilicon_Gamma(0)
	*/
{
//	G4cout << "* The K37EventAction is created !" << G4endl;
	if( !G4Threading::IsWorkerThread() ) 
	{
		G4cout << "Creating a new K37EventAction on the *master* thread!!  ...weird." << G4endl;
	}
	
	fullenergy1CollID = -1;
	fullenergy2CollID = -1;
	dedx1CollID = -1;
	dedx2CollID = -1;
	recoil_mcp_collection_id = -1;
	electron_mcp_collection_id = -1;

	theElectron = G4Electron::ElectronDefinition();
	emass = theElectron -> GetPDGMass();

//	accepted = 0;    // K37EventAction::accepted

	// Default values
	electron_mcp_threshold_ = 2.0 * keV;  // do I still need this??
	
	// Declare digitizers here (following example advanced/ChargeExchangeMC)
	G4DigiManager *digiManager(G4DigiManager::GetDMpointer());

	digiManager -> AddNewModule(new K37ScintillatorDigitizer("scintillatorPlusZ"));
	digiManager -> AddNewModule(new K37ScintillatorDigitizer("scintillatorMinusZ"));

	digiManager -> AddNewModule(new K37StripDetectorDigitizer("dsssdPlusZ",
	                          ( G4String(CONFIGURATION_DIRECTORY) + G4String("/upper_strip_detector_x.res") ).c_str(),
	                          ( G4String(CONFIGURATION_DIRECTORY) + G4String("/upper_strip_detector_y.res") ).c_str() ));

	digiManager -> AddNewModule(new K37StripDetectorDigitizer("dsssdMinusZ",
	                          ( G4String(CONFIGURATION_DIRECTORY) + G4String("/lower_strip_detector_x.res") ).c_str(),
	                          ( G4String(CONFIGURATION_DIRECTORY) + G4String("/lower_strip_detector_y.res") ).c_str() ));

//	digiManager -> List();  // what's this do?  ... prints out the list of detectors and numbers to go with them, apparently.

	event_messenger_ = new K37EventMessenger(this);
}

K37EventAction::~K37EventAction() 
{
	G4cout << "Deleting the K37EventAction." << G4endl;
	delete stripHandler;
	delete event_messenger_;
}

void K37EventAction::BeginOfEventAction(const G4Event* ev) 
{
//	if (ev -> GetEventID() == 0) G4cout << G4endl;
	if (ev -> GetEventID() % 10000 == 0) // MELISSA LOOK HERE!  MAKE THIS NUMBER EDITABLE VIA INTERFACE COMMANDS! 
//	if (ev -> GetEventID() % 1 == 0) // all the events?
//	if (ev -> GetEventID() % 10 == 0) // 
	{
	//	G4cout << "\r" << "Event " << ev -> GetEventID() << G4endl;
	//	G4cout << G4endl;
		G4cout << "Event " << ev -> GetEventID() << G4endl;
	}
	G4SDManager * SDman = G4SDManager::GetSDMpointer();
	
	if (fullenergy1CollID <0 || dedx1CollID < 0) // basically, this is here so you only do this once per event. ... no, once per run.
	{
	//	G4cout << "K37EventAction::BeginOfEventAction(...) is naming some channels, or something." << G4endl;
		
		G4String colNam;
		fullenergy1CollID          = SDman -> GetCollectionID(colNam="scintillatorPlusZHC");
		fullenergy2CollID          = SDman -> GetCollectionID(colNam="scintillatorMinusZHC");
		dedx1CollID                = SDman -> GetCollectionID(colNam="dsssdPlusZHC");
		dedx2CollID                = SDman -> GetCollectionID(colNam="dsssdMinusZHC");
		recoil_mcp_collection_id   = SDman -> GetCollectionID(colNam="rMCP_HC");
		electron_mcp_collection_id = SDman -> GetCollectionID(colNam="eMCP_HC");
		
	//	G4cout << "... and here are the \'names\':  " << G4endl;
	//	G4cout << "\tfullenergy1CollID       =" << fullenergy1CollID        << ";\tfullenergy2CollID         =" << fullenergy2CollID << G4endl;
	//	G4cout << "\tdedx1CollID             =" << dedx1CollID              << ";\tdedx2CollID               =" << dedx2CollID << G4endl;
	//	G4cout << "\trecoil_mcp_collection_id=" << recoil_mcp_collection_id << ";\telectron_mcp_collection_id=" << electron_mcp_collection_id << G4endl;
	//	
	//	G4cout << "... Done!" << G4endl;
	}
	stripHandler->BeginingOfEventClear();  // this seems to crash it sometimes?
//	G4cout << "Done with K37EventAction::BeginOfEventAction(...)." << G4endl;
}

//void K37Run::RecordEvent(const G4Event* evt) 
void K37EventAction::EndOfEventAction(const G4Event* evt) 
{
//	G4cout << "K37EventAction::EndOfEventAction(...) is called.  ...But why?!?!" << G4endl;
	if (fullenergy1CollID < 0 || dedx1CollID < 0) 
	{ 
		G4cout << "Possibly our detectors are undefined/not initialized?  idk, but it's bad." << G4endl;
		return; 
	}
	
	// ***************************************************************************
	// Determine what type of event this is.
	// ***************************************************************************
//	G4double event_type = 0.0;
//	event_type = EventTypeSorter(evt);
	
//	...
	
//	G4cout << "We've finished K37EventAction::EndOfEventAction(...)." << G4endl;
}  // End of event action

