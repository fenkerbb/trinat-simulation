// Authors: Spencer Behling, Benjamin Fenker, Melissa Anholm 2013

#include "K37PrimaryGeneratorMessenger.hh"
#include "K37Config.hh"

// ----------------------------------

K37PrimaryGeneratorMessenger::K37PrimaryGeneratorMessenger(K37PrimaryGeneratorAction* Gun)
  :action_(Gun) 
{
	gunDir = new G4UIdirectory("/K37/gun/");
	gunDir->SetGuidance("PrimaryGenerator control");

	// Commands that go here:
	/*
	set_event_acceptance_types_ = new G4UIcmdWithAString("/K37/gun/SetEventTypes", this);
	set_event_acceptance_types_ -> SetParameterName("EventTypeString", true);
	set_event_acceptance_types_ -> SetDefaultValue("CIA");  //
	set_event_acceptance_types_ -> AvailableForStates(G4State_PreInit, G4State_Idle);

	set_event_acceptance_types_ -> SetGuidance("Allowed event types:  ABCDEFGHIJ (...)");
	set_event_acceptance_types_ -> SetGuidance("Event types are set to match the codes used in acquisition.");
	set_event_acceptance_types_ -> SetGuidance(" A = 1:    (scint_t || scint_b)");
	set_event_acceptance_types_ -> SetGuidance(" B = 2:     [not implemented] -- (scint && LED)");
	set_event_acceptance_types_ -> SetGuidance(" C = 4:    (eMCP && (scint_t || scint_b))");
	set_event_acceptance_types_ -> SetGuidance(" D = 8:     [not implemented] -- (eMCP && UV)");
	set_event_acceptance_types_ -> SetGuidance(" E = 16:    [not implemented] -- (rMCP && UV)");
	set_event_acceptance_types_ -> SetGuidance(" F = 32:   (eMCP && rMCP)");
	set_event_acceptance_types_ -> SetGuidance(" G = 64:   (eMCP)");
	set_event_acceptance_types_ -> SetGuidance(" H = 128:  (rMCP)");
	set_event_acceptance_types_ -> SetGuidance(" I = 256:  (rMCP && (scint_t || scint_b))");
	set_event_acceptance_types_ -> SetGuidance(" J = 512:  (rMCP && (scint_t || scint_b) && eMCP)");
	set_event_acceptance_types_ -> SetGuidance(" K = 1024: (naive hit to either beta detector)\n");
	
	get_event_acceptance_types_ =  new G4UIcmdWithABool("/K37/gun/GetEventTypes", this);
	get_event_acceptance_types_ -> SetGuidance("Printing accepted event types...\n");
	get_event_acceptance_types_ -> SetGuidance("Allowed event types:  ABCDEFGHIJ (...)");
	get_event_acceptance_types_ -> SetGuidance("Event types are set to match the codes used in acquisition.");
	get_event_acceptance_types_ -> SetGuidance(" A = 1:    (scint_t || scint_b)");
	get_event_acceptance_types_ -> SetGuidance(" B = 2:     [not implemented] -- (scint && LED)");
	get_event_acceptance_types_ -> SetGuidance(" C = 4:    (eMCP && (scint_t || scint_b))");
	get_event_acceptance_types_ -> SetGuidance(" D = 8:     [not implemented] -- (eMCP && UV)");
	get_event_acceptance_types_ -> SetGuidance(" E = 16:    [not implemented] -- (rMCP && UV)");
	get_event_acceptance_types_ -> SetGuidance(" F = 32:   (eMCP && rMCP)");
	get_event_acceptance_types_ -> SetGuidance(" G = 64:   (eMCP)");
	get_event_acceptance_types_ -> SetGuidance(" H = 128:  (rMCP)");
	get_event_acceptance_types_ -> SetGuidance(" I = 256:  (rMCP && (scint_t || scint_b))");
	get_event_acceptance_types_ -> SetGuidance(" J = 512:  (rMCP && (scint_t || scint_b) && eMCP)");
	get_event_acceptance_types_ -> SetGuidance(" K = 1024: (naive hit to either beta detector)\n");
	get_event_acceptance_types_ -> SetParameterName("dummy", true);
	*/
	set_recoil_charge_cmd_ = new G4UIcmdWithAnInteger("/K37/gun/setRecoilCharge", this);
	set_recoil_charge_cmd_ -> SetGuidance("Enter charge state of recoil Ar.");
	set_recoil_charge_cmd_ -> SetGuidance("Special values:");
	set_recoil_charge_cmd_ -> SetGuidance("-2:  Randomize distribution based on file.");
	set_recoil_charge_cmd_ -> SetGuidance("-3:  Photoions.  (not implemented?)");
	set_recoil_charge_cmd_ -> SetParameterName("Charge state", true);
	set_recoil_charge_cmd_ -> SetDefaultValue(1);

	get_chargestate_ = new G4UIcmdWithABool("/K37/gun/getRecoilCharge", this);
	get_chargestate_ -> SetGuidance("Print out current charge state info.");
	get_chargestate_ -> SetParameterName("dummy", true);

	set_chargestate_file_ = new G4UIcmdWithAString("/K37/gun/SetChargeStatesFile", this);
	set_chargestate_file_ -> SetGuidance("Enter the name of the file");
	set_chargestate_file_ -> SetGuidance("to describe the charge state distribution of the recoils.");
	set_chargestate_file_ -> SetGuidance("Search for file within directory: ");
	set_chargestate_file_ -> SetGuidance(CONFIGURATION_DIRECTORY);
	set_chargestate_file_ -> SetDefaultValue("charge_state_dist.res");
	set_chargestate_file_ -> SetParameterName("ChargeFile", true);

	set_make_beta_ = new G4UIcmdWithABool("/K37/gun/setMakeBeta", this);
	set_make_beta_ -> SetGuidance("Enter bool to generate primary betas or not");
	set_make_beta_ -> SetParameterName("Make Recoils", true);
	set_make_beta_ -> SetDefaultValue(true);

	set_make_recoil_ = new G4UIcmdWithABool("/K37/gun/setMakeRecoil", this);
	set_make_recoil_ -> SetGuidance("Enter bool to generate primary recoils or not");
	set_make_recoil_ -> SetParameterName("Make Recoils", true);
	set_make_recoil_ -> SetDefaultValue(true);

	set_make_shakeoff_electrons_ = new G4UIcmdWithABool("/K37/gun/setMakeShakeoffElectrons", this);
	set_make_shakeoff_electrons_ -> SetGuidance("Enter bool to generate primary SOEs or not");
	set_make_shakeoff_electrons_ -> SetParameterName("Make SOEs", true);
	set_make_shakeoff_electrons_ -> SetDefaultValue(true);
	
	// maps
	set_make_uniform_energy_ = new G4UIcmdWithABool("/K37/gun/setMakeUniformEnergy", this);
	set_make_uniform_energy_ -> SetGuidance("Just generate a uniform beta spectrum?");
	set_make_uniform_energy_ -> SetGuidance("(used for creating maps)");
	set_make_uniform_energy_ -> SetParameterName("Make Uniform", true); // what does this even do?!
	set_make_uniform_energy_ -> SetDefaultValue(false);
	
	set_make_monoenergetic_ = new G4UIcmdWithABool("/K37/gun/setMakeMonoenergetic", this);
	set_make_monoenergetic_ -> SetGuidance("Generate monoenergetic betas?");
	set_make_monoenergetic_ -> SetParameterName("Make Monoenergetic", true); // what does this even do?!
	set_make_monoenergetic_ -> SetDefaultValue(false);
	
	set_monoenergetic_energy_ = new G4UIcmdWithADoubleAndUnit("/K37/gun/setMonoenergeticE", this);
	set_monoenergetic_energy_ -> SetGuidance("For monoenergetic beta generation, what energy should be generated?");
	set_monoenergetic_energy_ -> SetParameterName("E_mono", true); // what does this even do?!
	set_monoenergetic_energy_ -> SetDefaultValue(2.5*MeV);
	//
	
	
	set_minimum_cos_theta_cmd_ = new G4UIcmdWithADouble("/K37/gun/setMinCosTheta", this);
	set_minimum_cos_theta_cmd_ -> SetGuidance("Enter cos(theta) for gun's cone");
	set_minimum_cos_theta_cmd_ -> SetParameterName("cosTheta", true);
	// 98.5 DSSSD distance and (40 x 40) DSSSD dimensions subten
	// cos(theta) = 0.85 and 0.75 is a safety factor.
	set_minimum_cos_theta_cmd_ -> SetDefaultValue(0.75);

	set_cone_half_angle_cmd_ = new G4UIcmdWithADoubleAndUnit("/K37/gun/setConeHalfAngle", this);
	set_cone_half_angle_cmd_ -> SetGuidance("Enter half-angle for emitted betas");
	set_cone_half_angle_cmd_ -> SetParameterName("theta/2", true);
	set_cone_half_angle_cmd_ -> SetDefaultValue(acos(0.75)*rad);
	set_cone_half_angle_cmd_ -> SetDefaultUnit("radian");

//	set_BigA = new G4UIcmdWithADouble( "/K37/gun/setBigA", this);
//	set_BigA -> SetGuidance( "Enter A_{beta}. The default value is the current SM prediction for 37K.");
//	set_BigA -> SetGuidance( "OBSOLETE");
//	set_BigA -> SetParameterName("A_{beta}", true);
//	set_BigA -> SetDefaultValue(-0.573938);

	toggle_gps_ = new G4UIcmdWithoutParameter("/K37/gun/toggleGPS", this);
	toggle_gps_ -> SetGuidance("Turn on/off the G4GeneralParticleSource");

	set_gps_ = new G4UIcmdWithABool("/K37/gun/useGPS", this);
	set_gps_ -> SetGuidance("Turn on/off the G4GeneralParticleSource");
	set_gps_ -> SetDefaultValue(false);
	
//	set_rho =
//		new G4UIcmdWithADouble("/K37/gun/setRho", this);
//	set_rho -> SetGuidance("Enter value of rho");
//	set_rho -> SetGuidance("All JTW coefficients will be updated according to this value");
//	set_rho -> SetGuidance("Value read from input file will be overwritten");
//	set_rho -> SetParameterName("rho", false);
}

// ----------------------------------

K37PrimaryGeneratorMessenger::~K37PrimaryGeneratorMessenger() 
{
	delete gunDir;
	delete set_recoil_charge_cmd_;
	delete set_make_beta_;
	delete set_make_recoil_;
	delete set_make_shakeoff_electrons_;
	delete set_make_uniform_energy_;
	delete set_make_monoenergetic_;
	delete set_monoenergetic_energy_;
	
	delete set_minimum_cos_theta_cmd_;
	delete set_cone_half_angle_cmd_;
//	delete set_BigA;
	delete toggle_gps_;
	delete set_gps_;  // MJA:  pretty sure this should go here...
	delete set_chargestate_file_;
	delete get_chargestate_;
//	delete set_event_acceptance_types_;
//	delete get_event_acceptance_types_;
}

// ----------------------------------

void K37PrimaryGeneratorMessenger::SetNewValue(G4UIcommand* command, G4String newValue) 
{
	if (command == set_recoil_charge_cmd_) 
	{
		action_ -> SetRecoilCharge(set_recoil_charge_cmd_ -> GetNewIntValue(newValue));
	}
	if (command == set_make_beta_) 
	{
		action_ -> SetMakeBeta(set_make_beta_ -> GetNewBoolValue(newValue));
	}
	if (command == set_make_recoil_) 
	{
		action_ -> SetMakeRecoil(set_make_recoil_ -> GetNewBoolValue(newValue));
	}
	if (command == set_make_shakeoff_electrons_) 
	{
		action_ -> SetMakeShakeoffElectrons(set_make_shakeoff_electrons_ -> GetNewBoolValue(newValue));
	}
	//
	if (command == set_make_uniform_energy_) 
	{
		action_ -> SetMakeUniformEnergy(set_make_uniform_energy_ -> GetNewBoolValue(newValue));
	}
	if (command == set_make_monoenergetic_) 
	{
		G4cout << "set_make_monoenergetic_ isn't implemented yet." << G4endl;
	//	action_ -> SetMakeMonoenergetic(set_make_monoenergetic_ -> GetNewBoolValue(newValue));
	}
	if (command == set_monoenergetic_energy_) 
	{
		G4cout << "set_monoenergetic_energy_ isn't implemented yet." << G4endl;
		// action_ -> GetEventGenerator() -> SetConeHalfAngle(set_cone_half_angle_cmd_ -> GetNewDoubleValue(newValue));
	}
	//
	if (command == set_minimum_cos_theta_cmd_) 
	{
		G4cout << "Setting Minimum Cos(theta):  " << newValue << G4endl;
		action_ -> GetEventGenerator() -> SetMinCosTheta(set_minimum_cos_theta_cmd_ -> GetNewDoubleValue(newValue));
	}
	if (command == set_cone_half_angle_cmd_) 
	{
	//	G4cout << "Called set_cone_half_angle_cmd_" << G4endl;
		G4cout << "Setting Minimum Cone Half-angle:  " << newValue << G4endl;
	//	G4cout << "Setting Minimum Cone Half-angle:  " << newValue/degree << " degrees" << G4endl;  // yeah, this doesn't work.
		action_ -> GetEventGenerator() -> SetConeHalfAngle(set_cone_half_angle_cmd_ -> GetNewDoubleValue(newValue));
	//	G4cout << "End of set_cone_half_angle_cmd_" << G4endl;
	}
//	if (command == set_BigA) 
//	{
//		action_ -> GetEventGenerator() -> SetBigA(set_BigA -> GetNewDoubleValue(newValue));
//	}
	if (command == toggle_gps_) 
	{
		action_ -> ToggleUseGPS();
	}
	if (command == set_gps_) 
	{
		action_ -> SetUseGPS(set_gps_ -> GetNewBoolValue(newValue));
	}
	if (command == set_chargestate_file_) 
	{
		action_ -> SetChargeStatesFile(newValue);
		action_ -> GetChargeStates();
	}
	if (command == get_chargestate_) 
	{
		G4cout << "Charge state:  " << action_->GetRecoilCharge() << G4endl;
		if(action_->GetRecoilCharge() == -2)
		{
			G4cout << "Charge state distribution has been read from file: " << G4endl;
			G4cout << action_->GetChargeStatesFile() << G4endl;
			action_ -> GetChargeStates();
		}
		else if(action_->GetRecoilCharge() == -3)
		{
			G4cout << "Photoions?" << G4endl;
		}
	}
	/*
	if (command == set_event_acceptance_types_)
	{
		action_ -> SetEventTypes(newValue);
	}
	if (command == get_event_acceptance_types_)
	{
		G4cout << "Allowed event types:  ABCDEFGHIJ (...)" << G4endl;
		G4cout << "Event types are set to match the codes used in acquisition." << G4endl;
		G4cout << " A = 1:    (scint_t || scint_b)" << G4endl;
		G4cout << " B = 2:     [not implemented] -- (scint && LED)" << G4endl;
		G4cout << " C = 4:    (eMCP && (scint_t || scint_b))" << G4endl;
		G4cout << " D = 8:     [not implemented] -- (eMCP && UV)" << G4endl;
		G4cout << " E = 16:    [not implemented] -- (rMCP && UV)" << G4endl;
		G4cout << " F = 32:   (eMCP && rMCP)" << G4endl;
		G4cout << " G = 64:   (eMCP)" << G4endl;
		G4cout << " H = 128:  (rMCP)" << G4endl;
		G4cout << " I = 256:  (rMCP && (scint_t || scint_b))" << G4endl;
		G4cout << " J = 512:  (rMCP && (scint_t || scint_b) && eMCP)" << G4endl;
		G4cout << " K = 1024: (naive hit to either beta detector)\n" << G4endl;
	//	G4cout << G4endl;
		action_ -> GetEventTypes();
	}
	*/
	//  if (command == set_rho) {
	//G4cout << "This command might be obsolete??" << G4endl;
	//G4cout << "Use:  /K37/isotope/SetRho ...?" << G4endl;
	////    double rho = set_rho -> GetNewDoubleValue(newValue);
	////    action_ -> GetEventGenerator() -> SetRho( set_rho->GetNewDoubleValue(newValue) );
	//  }
  
}

// ----------------------------------

