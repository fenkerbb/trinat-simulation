#include "globals.hh"  // G4cout.

#include "K37_OutputChannels.hh"

// ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // ----- // 
// Out of sight, out of mind.

set_of_channels::set_of_channels()
{
	initialize_the_channels();
}

void set_of_channels::initialize_the_channels()
{
	// Detectors
	qdc_upper_pmt    = new Generic_Channel("QDC_UpperPMT"   , -1, "/D");
	qdc_lower_pmt    = new Generic_Channel("QDC_LowerPMT"   , -1, "/D");
	
	qdc_upper_pmt_ae = new Generic_Channel("QDC_UpperPMT_AE", -1, "/D");
	qdc_lower_pmt_ae = new Generic_Channel("QDC_LowerPMT_AE", -1, "/D");
	qdc_upper_pmt_p  = new Generic_Channel("QDC_UpperPMT_P" , -1, "/D");
	qdc_lower_pmt_p  = new Generic_Channel("QDC_LowerPMT_P" , -1, "/D");
	qdc_upper_pmt_e  = new Generic_Channel("QDC_UpperPMT_E" , -1, "/D");
	qdc_lower_pmt_e  = new Generic_Channel("QDC_LowerPMT_E" , -1, "/D");
	qdc_upper_pmt_g  = new Generic_Channel("QDC_UpperPMT_G" , -1, "/D");
	qdc_lower_pmt_g  = new Generic_Channel("QDC_LowerPMT_G" , -1, "/D");

	qdc_upper_dssd     = new Generic_Channel("QDC_UpperSilicon"   , -1, "/D");
	qdc_lower_dssd     = new Generic_Channel("QDC_LowerSilicon"   , -1, "/D");
	qdc_upper_dssd_ae  = new Generic_Channel("QDC_UpperSilicon_AE", -1, "/D");
	qdc_lower_dssd_ae  = new Generic_Channel("QDC_LowerSilicon_AE", -1, "/D");
	qdc_upper_dssd_p   = new Generic_Channel("QDC_UpperSilicon_P" , -1, "/D");
	qdc_lower_dssd_p   = new Generic_Channel("QDC_LowerSilicon_P" , -1, "/D");
	qdc_upper_dssd_e   = new Generic_Channel("QDC_UpperSilicon_E" , -1, "/D");
	qdc_lower_dssd_e   = new Generic_Channel("QDC_LowerSilicon_E" , -1, "/D");
	qdc_upper_dssd_g   = new Generic_Channel("QDC_UpperSilicon_G" , -1, "/D");
	qdc_lower_dssd_g   = new Generic_Channel("QDC_LowerSilicon_G" , -1, "/D");
	qdc_upper_mirror   = new Generic_Channel("Upper_Mirror_Edep",   -1, "/D");
	qdc_lower_mirror   = new Generic_Channel("Lower_Mirror_Edep",   -1, "/D");
	qdc_upper_BeWindow = new Generic_Channel("Upper_BeWindow_Edep",-1, "/D");
	qdc_lower_BeWindow = new Generic_Channel("Lower_BeWindow_Edep",-1, "/D");

	dl_x_pos      = new TDC_Channel("DL_X_Pos"     , 3, "/v", false);
	dl_z_pos      = new TDC_Channel("DL_Z_Pos"     , 4, "/v", false);
	hex_x_pos     = new TDC_Channel("HEX75_XPos"   , 5, "/v", false);
	hex_z_pos     = new TDC_Channel("HEX75_ZPos"   , 6, "/v", false);

	// TDCs
	tdc_scint_top_     = new TDC_Channel("TDC_SCINT_TOP"    , 201, "/v", false);
	tdc_scint_bottom_  = new TDC_Channel("TDC_SCINT_BOTTOM" , 202, "/v", false);
	tdc_ion_mcp_       = new TDC_Channel("TDC_ION_MCP"      , 203, "/v", false);
	tdc_electron_mcp_  = new TDC_Channel("TDC_ELECTRON_MCP" , 203, "/v", false);  // really?
	tdc_photo_diode_   = new TDC_Channel("TDC_PHOTO_DIODE"  , 204, "/v", false);
//	volumeList         = new TDC_Channel("VOLUME_LIST"      , 208, "/vs",false);  // does anything bad happen if I just disable it?  ...have to disable it everywhere.

	strip_detector_upper_x_ = new TDC_Channel("BB1_AMPLITUDE_UX", 209, "/v", false);
	strip_detector_upper_y_ = new TDC_Channel("BB1_AMPLITUDE_UY", 210, "/v", false);
	strip_detector_lower_x_ = new TDC_Channel("BB1_AMPLITUDE_LX", 211, "/v", false);
	strip_detector_lower_y_ = new TDC_Channel("BB1_AMPLITUDE_LY", 212, "/v", false);

	// strip_detector_upper_x_T = new TDC_Channel("BB1_UX_PEAKTIME", 209, "/v", false);
	// strip_detector_upper_y_T = new TDC_Channel("BB1_UY_PEAKTIME", 210, "/v", false);
	// strip_detector_lower_x_T = new TDC_Channel("BB1_LX_PEAKTIME", 211, "/v", false);
	// strip_detector_lower_y_T = new TDC_Channel("BB1_LY_PEAKTIME", 212, "/v", false);

	// Event generator and other Geant-only data
	electron_kinetic_energy_generated_ = new Generic_Channel("T_GEN_ELE",     301, "/D");
	electron_mu_generated_ = new Generic_Channel("MU_GEN_ELE",                302, "/D");
	recoil_mu_generated_   = new Generic_Channel("MU_GEN_RECOIL",             302, "/D");
	recoil_mcp_particle_   = new Generic_Channel("ION_MCP_PARTICLE_PDG",      303, "/D");
	recoil_charge_state_   = new Generic_Channel("ION_CHARGE",                304, "/D");
	two_percent_branch_    = new Generic_Channel("TWO_PERCENT_BRANCH",         -1, "/D");  
	upper_pmt_particle_    = new Generic_Channel("UPPER_SCINTILLATOR_PDG",    305, "/D");
	lower_pmt_particle_    = new Generic_Channel("LOWER_SCINTILLATOR_PDG",    305, "/D");  // really?

	num_hits_r_mcp_        = new Generic_Channel("RECOIL_MCP_N_HITS",         306, "/D");
	num_hits_e_mcp_        = new Generic_Channel("ELECTRON_MCP_N_HITS",       307, "/D");

	electron_mcp_particle_ = new Generic_Channel("ELECTRON_MCP_PARTICLE_PDG", 303, "/D");
	
	// Information to match analyzer (not really simulated)
//	run_action_       = new Generic_Channel("Run_Number"      , 401, "/D");
	ttlbit_op_beam_   = new Generic_Channel("TTLBit_OPBeam"   , 404, "/D");
	vnim_event_type_  = new Generic_Channel("VNIM_event_type" , 405, "/D");
	
//	ttlbit_sigmaplus_ = new Generic_Channel("TTLBit_SigmaPlus", 406, "/D");  // Can I make this an "I" ?
	
	gen_Ebeta_   = new Generic_Channel("gen_Ebeta",                 501, "/D");
	gen_Tbeta_   = new Generic_Channel("gen_Tbeta",                 502, "/D");
	gen_costheta_= new Generic_Channel("gen_costheta",              503, "/D");
	gen_xhit_t_  = new Generic_Channel("gen_xhit_t",                504, "/D");  // can't actually make a vector of hits....
	gen_yhit_t_  = new Generic_Channel("gen_yhit_t",                505, "/D");
	gen_rhit_t_  = new Generic_Channel("gen_rhit_t",                506, "/D");
	gen_xhit_b_  = new Generic_Channel("gen_xhit_b",                507, "/D");
	gen_yhit_b_  = new Generic_Channel("gen_yhit_b",                508, "/D");
	gen_rhit_b_  = new Generic_Channel("gen_rhit_b",                509, "/D");
	
	naive_hit_t_ = new Generic_Channel("naive_hit_t",               510, "/D");  // can't make an integer work.
	naive_hit_b_ = new Generic_Channel("naive_hit_b",               511, "/D");  //
	
	
//	// this breaks it:
//	RegisterChannel(channelset->num_hits_r_mcp_);  // tghis one fucks it up??
	
}

set_of_channels::~set_of_channels()
{
//	delete upper_scint_E;
//	delete lower_scint_E;
	
	delete qdc_upper_pmt;
	delete qdc_lower_pmt;
	delete qdc_upper_pmt_ae;
	delete qdc_lower_pmt_ae;
	delete qdc_upper_pmt_p;
	delete qdc_lower_pmt_p;
	delete qdc_upper_pmt_e;
	delete qdc_lower_pmt_e;
	delete qdc_upper_pmt_g;
	delete qdc_lower_pmt_g;

	delete qdc_upper_dssd;
	delete qdc_lower_dssd;
	delete qdc_upper_dssd_ae;
	delete qdc_lower_dssd_ae;
	delete qdc_upper_dssd_p;
	delete qdc_lower_dssd_p;
	delete qdc_upper_dssd_e;
	delete qdc_lower_dssd_e;
	delete qdc_upper_dssd_g;
	delete qdc_lower_dssd_g;
	
	delete dl_x_pos;
	delete dl_z_pos;
	delete hex_x_pos;
	delete hex_z_pos;
	delete strip_detector_upper_x_;
	delete strip_detector_upper_y_;
	delete strip_detector_lower_x_;
	delete strip_detector_lower_y_;
	// delete strip_detector_upper_x_T;
	// delete strip_detector_upper_y_T;
	// delete strip_detector_lower_x_T;
	// delete strip_detector_lower_y_T;

	delete tdc_scint_top_;
	delete tdc_scint_bottom_;
	delete tdc_ion_mcp_;
	delete tdc_electron_mcp_;
	delete tdc_photo_diode_;
//	delete volumeList;
	delete electron_kinetic_energy_generated_;
	delete electron_mu_generated_;
	delete recoil_mu_generated_;
//	delete run_action_;
	delete recoil_mcp_particle_;
	delete recoil_charge_state_;
	delete two_percent_branch_;
	delete ttlbit_op_beam_;
//	delete ttlbit_sigmaplus_;
	
	delete vnim_event_type_;
	delete upper_pmt_particle_;
	delete lower_pmt_particle_;
	delete num_hits_r_mcp_;
	delete num_hits_e_mcp_;
	delete electron_mcp_particle_;
	delete qdc_upper_mirror;
	delete qdc_lower_mirror;
	delete qdc_upper_BeWindow;
	delete qdc_lower_BeWindow;
	
	
	delete gen_Ebeta_;
	delete gen_Tbeta_;
	delete gen_costheta_;
	delete naive_hit_t_;
	delete gen_xhit_t_;
	delete gen_yhit_t_;
	delete gen_rhit_t_;
	delete naive_hit_b_;
	delete gen_xhit_b_; 
	delete gen_yhit_b_; 
	delete gen_rhit_b_;
}	
	