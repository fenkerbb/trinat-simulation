// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 
// 2013 -

#include "globals.hh"
#include "K37ElectricFieldMessenger.hh"

//------------------------------------------------

K37ElectricFieldMessenger::K37ElectricFieldMessenger(K37ElectricFieldSetup* pEMfield) /// what ... is even happening here??
  :fEFieldSetup(pEMfield) 

//K37ElectricFieldMessenger::K37ElectricFieldMessenger(K37ElectricField* ef) /// what ... is even happening here??
//: the_field(ef)
{
	fieldDir = new G4UIdirectory("/K37/field/");
	fieldDir->SetGuidance("F02 field tracking control.");
	
	StepperCmd = new G4UIcmdWithAnInteger("/K37/field/setStepperType", this);
	StepperCmd -> SetGuidance("Select stepper type for electric field");
	StepperCmd -> SetGuidance("0:  G4ExplicitEuler");
	StepperCmd -> SetGuidance("1:  G4ImplicitEuler");
	StepperCmd -> SetGuidance("2:  G4SimpleRunge");
	StepperCmd -> SetGuidance("3:  G4SimpleHeum");
	StepperCmd -> SetGuidance("4:  G4ClassicalRK4 (default)");
	StepperCmd -> SetGuidance("5:  G4CashKarpRKF45");
	StepperCmd -> SetGuidance("6:  G4RKG3_Stepper       (not currently working)");
	StepperCmd -> SetGuidance("7:  G4HelixExplicitEuler (not valid)");
	StepperCmd -> SetGuidance("8:  G4HelixImplicitEuler (not valid)");
	StepperCmd -> SetGuidance("9:  G4HelixSimpleRunge   (not valid)");
	StepperCmd -> SetParameterName("choice", true);
	StepperCmd -> SetDefaultValue(4);
	StepperCmd -> AvailableForStates(G4State_PreInit, G4State_Idle);  // is it because this is called after initialization??  idle should be fine too...
	
	SetEfield_cmd = new G4UIcmdWithADoubleAndUnit("/K37/field/SetEField", this);
	SetEfield_cmd->SetGuidance("Define uniform Electric field.");
	SetEfield_cmd->SetGuidance("Electric field will be in 'Y' direction.");
	SetEfield_cmd->SetParameterName("E_y", false, false);
	SetEfield_cmd->SetDefaultUnit("kV/cm");
	SetEfield_cmd->AvailableForStates(G4State_Idle);

	MinStepCmd = new G4UIcmdWithADoubleAndUnit("/K37/field/setMinStep", this);
	MinStepCmd->SetGuidance("Define minimal step");
	MinStepCmd->SetParameterName("min step", false, false);
	MinStepCmd->SetDefaultUnit("mm"); // not a unit yet?
	MinStepCmd->AvailableForStates(G4State_Idle);

	print_field_cmd = new G4UIcmdWithoutParameter("/K37/field/print", this);
	print_field_cmd -> SetGuidance("Print the current electric field");
	print_field_cmd -> AvailableForStates(G4State_Idle);
}

//------------------------------------------------
K37ElectricFieldMessenger::~K37ElectricFieldMessenger() 
{
	// what order do I delete these in?!
	if (fieldDir)        { delete fieldDir;        }
	if (StepperCmd)      { delete StepperCmd;      }
	if (SetEfield_cmd)   { delete SetEfield_cmd;   }
	if (MinStepCmd)      { delete MinStepCmd;      }
	if (print_field_cmd) { delete print_field_cmd; }
}

//------------------------------------------------

void K37ElectricFieldMessenger::SetNewValue(G4UIcommand* command, G4String newValue) 
{
	
	if (command == StepperCmd) // it crashes sometimes when this is called?? but before it gets to the first print statement.  What???
	{ 
		fEFieldSetup->SetStepperType( StepperCmd->GetNewIntValue(newValue) );
	}
	else if (command == SetEfield_cmd) 
	{
		fEFieldSetup->SetFieldValue(SetEfield_cmd->GetNewDoubleValue(newValue)); 
	//	G4cout << "Set new Efield value:  " << G4BestUnit(fEFieldSetup->GetConstantFieldValue(), "Electric field") << G4endl;
	}
	else if (command == MinStepCmd) 
	{ 
		fEFieldSetup->SetMinStep( MinStepCmd->GetNewDoubleValue(newValue) ); 
	}
	else if (command == print_field_cmd) 
	{
		G4double field = fEFieldSetup -> GetConstantFieldValue();
		G4cout << "Electric Field Magnitude:  " << G4BestUnit(field, "Electric field") << G4endl;
	}
	else
	{
		G4cout << "Command not recognized." << G4endl;
	}
	
//	G4cout << "Command not recognized.  Command is never recognized." << G4endl;
	return;
}

//------------------------------------------------
