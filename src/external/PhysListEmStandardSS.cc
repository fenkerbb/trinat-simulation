//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file medical/electronScattering/src/PhysListEmStandardSS.cc
/// \brief Implementation of the PhysListEmStandardSS class
//
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// 
// Some modifications to the default values by Melissa Anholm, May 2019.


#include "PhysListEmStandardSS.hh"

#include "G4BuilderType.hh"
#include "G4ParticleDefinition.hh"
#include "G4ProcessManager.hh"

#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"

#include "G4CoulombScattering.hh"

#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"

#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

#include "G4hIonisation.hh"
#include "G4ionIonisation.hh"

#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PhysListEmStandardSS::PhysListEmStandardSS(const G4String& name)
   :  G4VPhysicsConstructor(name)
{
    SetPhysicsType(bElectromagnetic);

    G4EmParameters* param = G4EmParameters::Instance();
    param->SetDefaults();
	// MJA:  Notes from upgrade to G4.10.5.
	// param::SetDefaults() sets many things.  But does it set them like we had before??
	// minKinEnergy = 100*eV, as before.  I think.
	// maxKinEnergy = 10*TeV, while it was 100*TeV before.  Probably.  This is actually fine.
	// it called SetPolarAngleLimit(0.0), which is *probably* equivalent to SetMscThetaLimit(0.0)
	// It sets spline to true, which we'd for some reason set to false, unlike what was in the original file.
	param->SetSpline(false);  // this is new!!!!
	//  SetLambdaBinning(...) has been partially replaced by SetNumberOfBinsPerDecade(...).  The other part of the functionality now happens for free when you set the min. and max. KE.  You have to call SetNumberOfBinsPerDecade(...) before you set the max. and min. KE though I think.  Or, really, you just need to call one of those afterwards, I think.  So I'll set those again, even though they're set in param->SetDefaults().
	// We had SetLambdaBinning(12*20) for 12 decades of energy range before, so we had nbinsPerDecade=20.
	param->SetNumberOfBinsPerDecade(20);  
	param->SetMinEnergy(100*eV);
	param->SetMaxEnergy(10*TeV);
	// Other things we used to set are:
	//   SetSubCutoff(false);
	//   SetDEDXBinning(12*20);	//default=12*7
	// ...I don't know what those things are.  
	// 
	// G4EmProcessOptions::SetSubCutoff(false).  There is no replacement for this in the new thingy, but the old thingy might mess everything up if I'm not careful.  Also, I don't know what it does.  Apparently it should be called in both the 'master' and 'worker' threads.  
	//
	// SetDEDXBinning(...) seems to have been removed completely without any replacement.  
	
    param->SetMscThetaLimit(0.0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PhysListEmStandardSS::~PhysListEmStandardSS()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PhysListEmStandardSS::ConstructProcess()
{
  // Add standard EM Processes

  auto particleIterator=GetParticleIterator();
  particleIterator->reset();
  while( (*particleIterator)() ){
    G4ParticleDefinition* particle = particleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
     
    if (particleName == "gamma") {
      // gamma
      pmanager->AddDiscreteProcess(new G4PhotoElectricEffect);
      pmanager->AddDiscreteProcess(new G4ComptonScattering);
      pmanager->AddDiscreteProcess(new G4GammaConversion);
      
    } else if (particleName == "e-") {
      //electron
      pmanager->AddProcess(new G4eIonisation,        -1, 1, 1);
      pmanager->AddProcess(new G4eBremsstrahlung,    -1, 2, 2);
      pmanager->AddDiscreteProcess(new G4CoulombScattering);            
            
    } else if (particleName == "e+") {
      //positron
      pmanager->AddProcess(new G4eIonisation,        -1, 1, 1);
      pmanager->AddProcess(new G4eBremsstrahlung,    -1, 2, 2);
      pmanager->AddProcess(new G4eplusAnnihilation,   0,-1, 3);
      pmanager->AddDiscreteProcess(new G4CoulombScattering);            
            
    } else if (particleName == "mu+" || 
               particleName == "mu-"    ) {
      //muon
      pmanager->AddProcess(new G4MuIonisation,       -1, 1, 1);
      pmanager->AddProcess(new G4MuBremsstrahlung,   -1, 2, 2);
      pmanager->AddProcess(new G4MuPairProduction,   -1, 3, 3);
      pmanager->AddDiscreteProcess(new G4CoulombScattering);              
             
    } else if (particleName == "alpha" || particleName == "He3") {
      G4CoulombScattering* cs = new G4CoulombScattering();
      cs->SetBuildTableFlag(false);
      pmanager->AddProcess(new G4ionIonisation,      -1, 1, 1);      
      pmanager->AddDiscreteProcess(cs);

    } else if (particleName == "GenericIon" ) { 
      G4CoulombScattering* cs = new G4CoulombScattering();
      cs->SetBuildTableFlag(false);
      pmanager->AddProcess(new G4ionIonisation,      -1, 1, 1);      
      pmanager->AddDiscreteProcess(cs);
     
    } else if ((!particle->IsShortLived()) &&
               (particle->GetPDGCharge() != 0.0) && 
               (particle->GetParticleName() != "chargedgeantino")) {
      //all others charged particles except geantino
      pmanager->AddDiscreteProcess(new G4CoulombScattering);            
      pmanager->AddProcess(new G4hIonisation,        -1, 1, 1);
    }
  }
  
  /*
  // MJA:  old settings here.  I don't think I can set the settings this way anyore though.
  // 
  // Em options
  //
  // Main options and setting parameters are shown here.
  // Several of them have default values.
  //
  G4EmProcessOptions emOptions;

  //physics tables
  //
  emOptions.SetMinEnergy(100*eV);	//default    
  emOptions.SetMaxEnergy(100*TeV);	//default  
  emOptions.SetDEDXBinning(12*20);	//default=12*7  
  emOptions.SetLambdaBinning(12*20);	//default=12*7
  //  emOptions.SetSplineFlag(true);	//default
    
  //energy loss
  //
  //emOptions.SetStepFunction(0.2, 100*um);	//default=(0.2, 1*mm)      
  //emOptions.SetLinearLossLimit(1.e-2);		//default
 
  //ionization
  //
  emOptions.SetSubCutoff(false);	//default  

  // scattering
  emOptions.SetPolarAngleLimit(0.0);
  */
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

