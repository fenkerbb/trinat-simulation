// Authors: Spencer Behling, Benjamin Fenker, and Melissa Anholm 
// 2019 - 
// 
// The K37ActionInitialization class is specifically for use with multithreading in Geant4.10.x.
// 
// The master thread calls BuildForMaster() but not Build().  
// Then the worker threads call Build().
// Then the RunManager is initialized.  

#include "G4Threading.hh"

#include "K37ActionInitialization.hh"

#include "K37SteppingAction.hh"
#include "K37StackingAction.hh"

#include "K37EventGeneratorNoRecoilOrderEffects.hh"
#include "K37PrimaryGeneratorAction.hh"
#include "K37EventGenerator.hh"

#include "K37RunManagerMT.hh"
#include "K37EventAction.hh"

#include "K37_Data.hh"
#include "K37RunAction.hh"

#include "Randomize.hh"

// ----- *  ----- *  ----- *  ----- *  ----- *  ----- *  ----- *  ----- *  ----- *  ----- //

K37ActionInitialization::K37ActionInitialization()
	:G4VUserActionInitialization(), 
	the_cloud(0), the_aggregator(0)
{
	G4cout << "Called K37ActionInitialization::K37ActionInitialization()." << G4endl;
	G4cout << "K37ActionInitialization:  Seed:  " << G4Random::getTheSeed() << G4endl;
	G4cout << "K37ActionInitialization:  First random number:  \t" << G4UniformRand() << G4endl;
	
	the_cloud      = new K37CloudSetup();  // doesn't like this?
//	G4cout << "the_cloud is created, or at least that's what the K37ActionInitialization thinks." << G4endl;
	
	the_aggregator = new GlobalAggregator();
//	G4cout << "the_aggregator is created, or at least that's what the K37ActionInitialization thinks." << G4endl;
	
//	G4cout << "We're at the end of K37ActionInitialization::K37ActionInitialization()." << G4endl;
}

K37ActionInitialization::~K37ActionInitialization()
{
	G4cout << "Deleting the K37ActionInitialization, which will also delete the cloud and an aggregator." << G4endl;
	delete the_cloud;
	delete the_aggregator;
}

void K37ActionInitialization::Build() const
{
	G4cout << "Called K37ActionInitialization::Build() -- Thread ID:  " << G4Threading::G4GetThreadId() << G4endl;
	G4cout << "K37ActionInitialization::Build():  Seed:  " << G4Random::getTheSeed() << G4endl;
	G4cout << "K37ActionInitialization::Build():  First random number:  \t" << G4UniformRand() << G4endl;
	
	K37PrimaryGeneratorAction * pga = new K37PrimaryGeneratorAction( new K37EventGenerator(), new K37EventGeneratorNoRecoilOrderEffects() );
	SetUserAction( pga );
//	G4cout << "Build():  Created/set the (local?) K37PrimaryGeneratorAction." << G4endl;

	K37RunAction* ra = new K37RunAction();
	SetUserAction(ra);
//	G4cout << "Build():  Created/set the (local?) K37RunAction." << G4endl;
	
	K37EventAction * ea = new K37EventAction();
	SetUserAction(ea);
//	G4cout << "Build():  Created/set the (local?) K37EventAction." << G4endl;
	
// Owned by local G4WorkerRunManager:  K37RunAction, G4EventManager
// Owned by local G4EventManager:  K37PGA, K37EventAction, K37StackingAction, G4TrackingManager.
// Owned by local G4TrackingManager:  K37TrackingAction, G4SteppingManager.
// Owned by local G4SteppingManager:  K37SteppingAction.
	
	SetUserAction( new K37SteppingAction() );
	SetUserAction( new K37StackingAction() );  // this one, at least, is done.
}

void K37ActionInitialization::BuildForMaster() const
{
	G4cout << "Called K37ActionInitialization::BuildForMaster() -- Thread ID:  " << G4Threading::G4GetThreadId() << G4endl;
	G4cout << "K37ActionInitialization::BuildForMaster():  Seed:  " << G4Random::getTheSeed() << G4endl;
	G4cout << "K37ActionInitialization::BuildForMaster():  First random number:  \t" << G4UniformRand() << G4endl;

	SetUserAction( new K37RunAction() );
//	G4cout << "Created and set the (global!) K37RunAction." << G4endl;
}
