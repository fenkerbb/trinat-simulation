// Author: Benjamin Fenker 2015
#include <algorithm>    // std::find
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <unordered_set>
#include <vector>

#include <TApplication.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TMath.h>
#include <TMinuit.h>
#include <TString.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TTimer.h>

#include "fit.h"
#include "units.h"

#include <boost/filesystem.hpp>

using std::cin;
using std::cout;
using std::endl;
using std::fabs;
using std::map;
using std::max;
using std::min;
using std::to_string;
using std::string;
using std::unordered_set;
using std::vector;
using boost::filesystem::exists;
using boost::filesystem::remove;
//#define printf(...)
/*
Compile with
make
*/

TMinuit *myMinuit;
std::ofstream logstream;
TH1F *gData, *gMCdata, *currentMC, *residuals, *gNoise, *currentNoise,
  *currentGaus, *partialMC;
TCanvas *gCan;
double bmin = -1;
double bmax = 200.;
int nfunc = 0;
int npoints = 0;
bool gBatch = false;
TTimer *timer;

TApplication *app;

Int_t AdjustMINUIT(TMinuit *myMinuit);
int FitSpectrum(TH1F *data, TH1F *mc_raw, string outfname, char u, char x,
                int s, int delta_bmin);
TH1F* GetHistogramForStrip(string fname, char uORL, char xORy, int n);
int GetInteriorNumber(int snumber);

int GetInteriorNumber(int snumber) 
{
  if (snumber < 0 || snumber > 39) return 0;

  return min(snumber, 39-snumber);
                                     
}

void DrawFCN(TMinuit *minuit) 
{
  gPad -> Clear();
  gData -> Draw("E1X0");
  // Plot at best-fit values
  double slp, slp_err, off, off_err;
  double res, res_err, bkg, bkg_err;
  double cen, cen_err, sig, sig_err;
  double amp, amp_err, lin_slp, lin_slp_err, lin_amp, lin_amp_err;
  double noise_cen, noise_cen_err, noise_sig, noise_sig_err;
  double noise_amp, noise_amp_err;
  minuit -> GetParameter(0, slp, slp_err);
  minuit -> GetParameter(1, off, off_err);
  minuit -> GetParameter(2, res, res_err);
  minuit -> GetParameter(3, bkg, bkg_err);
  minuit -> GetParameter(4, cen, cen_err);
  minuit -> GetParameter(5, sig, sig_err);
  minuit -> GetParameter(6, amp, amp_err);
  minuit -> GetParameter(7, lin_slp, lin_slp_err);
  minuit -> GetParameter(8, lin_amp, lin_amp_err);
  minuit -> GetParameter(9, noise_cen, noise_cen_err);
  minuit -> GetParameter(10, noise_sig, noise_sig_err);
  minuit -> GetParameter(11, noise_amp, noise_amp_err);
  double pars[12] = {slp, off, res, bkg, cen, sig, amp, lin_slp, lin_amp,
                     noise_cen, noise_sig, noise_amp};
  Double_t grad, func;
  minuit -> Eval(12, &grad, func, pars, 3);
  if (currentMC) {
    gData -> Draw("E1X0P");
    cout <<    currentMC -> GetName() << endl;
    currentMC -> SetLineColor(kRed);
    currentMC -> GetXaxis() -> SetRange(bmin, bmax);
    currentMC -> Draw("SAME");
  } else {
    cout << "couldn't draw" << endl;
  }
}

Int_t AdjustMINUIT(TMinuit *minuit) 
{
  int pnum = 0;
  double pval, pstep;
  Int_t ierflg;
  Int_t mynpars = minuit -> GetNumPars();
  mynpars = 12;
  cout << "mynpars " << mynpars << endl;
  Double_t amin = 0.0;
  do {
    DrawFCN(minuit);
    minuit -> mnprin(1, amin);
    cout << "Enter -1 to accept and begin fit" << endl;
    cout << "Otherwise enter par number, value, step size" << endl;
    cout << "Step size = 0 means fix parameter" << endl;
    if (gBatch) {
      pnum = -1;
    } else {
      timer -> TurnOn();
      timer -> Reset();
      cin >> pnum;
      timer -> TurnOff();
    }
    if (pnum > 0) 
    {
      cin >> pval >> pstep;
      // MINUIT is fortran and counts from one, I'm counting from zero
      pnum--;
      minuit -> Release(pnum);
      TString pname;
      Double_t var, err, xlolim, xuplim;
      Int_t iuint;      
      minuit -> mnpout(pnum, pname, var, err, xlolim, xuplim, iuint);
      if (pnum >= 0 && pnum < mynpars) 
      {
        if (pstep < 1E-6) 
        {
          minuit -> mnparm(pnum, pname, pval, 1E-5, xlolim, xuplim, ierflg);
          minuit -> FixParameter(pnum);
        } 
        else 
        {
          minuit -> mnparm(pnum, pname, pval, pstep, xlolim, xuplim, ierflg);          
        }
      }
    }
  } while (pnum >= 0);
  return ierflg;
}

int SaveFit(string ofname, TMinuit *tm, TH1F *data, TH1F *raw_mc, TH1F *fit_mc,
             TH1F *resid, double min, double max, double chi2, int npar,
            int statu, TH1F *cnoise, TH1F *cgaus) 
{
  cout << "Saving output to file " << ofname << endl;
  TFile *f = new TFile(ofname.c_str(), "RECREATE");
  tm -> Write("fit_result");
  data -> Write("exp_data");
  raw_mc -> Write("raw_mc");
  fit_mc -> Write("fit_mc");
  resid -> Write("residuals");
  cnoise -> Write("sim_noise");
  cgaus -> Write("gaussian");
  partialMC -> Write("partial_mc");
  TArrayD d(3);
  d[0] = min;
  d[1] = max;
  d[2] = chi2;
  TArrayI i(1);
  i[0] = statu;
  f -> WriteObject(&d, "limits");
  f -> WriteObject(&i, "return_status");
  f -> Close();
  delete f;
  return 0;
}

bool StringVectorHas(vector<string> v, string s) 
{
  int n = find(v.begin(), v.end(), s) - v.begin();
  return (n < v.size());
}

void GetChi2(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag) 
{
  for (int p = 0; p < 12; p++) 
  {
    TString cn = "";
    Double_t val, err, xlo, xup;
    Int_t iint;
    myMinuit -> mnpout(p, cn, val, err, xlo, xup, iint);
    if (err > 0.0) 
    {
      logstream << std::setprecision(9) << std::fixed << val << "\t";
    }
  }
  double slope = par[0];
  double intercept = par[1];
  double resolution = par[2];
  double background = par[3];
  double gauss_center = par[4];
  double gauss_sigma = par[5];
  double gauss_amp_rel = par[6];
  double lin_slope = par[7];
  double lin_rel_amp = par[8];
  double noise_center = par[9];
  double noise_sigma = par[10];
  double noise_amp_rel = par[11];

  //  cout << slope << endl;
  double chi2 = 0.0;

  TH1F *mc_clone = new TH1F(*gMCdata);
  TH1F *noise_clone = new TH1F(*gNoise);


  Fit::AddXaxisJitter(mc_clone, resolution, false, 0, 0);
  TH1F *mc_cal = Fit::CalibrateAndMatchBinning(mc_clone, slope,
                                               intercept, gData);

  mc_cal -> SetNameTitle("G4_MC", "G4_MC");

  TH1F *noise_cal = Fit::CalibrateAndMatchBinning(noise_clone, 1.0,
                                                  noise_center, gData);
  Fit::AddXaxisJitter(noise_cal, noise_sigma, false, 0, 0);

  noise_cal -> SetNameTitle("noise_this", "noise_this");
  gPad -> Update();
  double mc_area = mc_cal -> Integral(mc_cal -> FindBin(bmin),
                                      mc_cal -> FindBin(bmax));
  double gauss_amp = mc_area * gauss_amp_rel;
  double noise_area = noise_cal -> Integral(noise_cal -> FindBin(bmin),
                                            noise_cal -> FindBin(bmax));
  // cout << "MC area: " << mc_area << "\t Noise_area = " << noise_area << endl;
  // cout << "Scaling by: " << noise_amp_rel*(mc_area / noise_area) << endl;
  noise_cal -> Scale(noise_amp_rel*(mc_area / noise_area));

  // vogon
  if (partialMC) delete partialMC;
  partialMC = new TH1F(*mc_cal);
  partialMC -> SetDirectory(0);

  if (noise_amp_rel > 0.) 
    mc_cal -> Add(noise_cal);
  double line_amp = mc_area * lin_rel_amp;
  Fit::AddLinearBackground(mc_cal, line_amp, lin_slope);
  
  TH1F *ghist = Fit::AddGaussToHist(mc_cal, gauss_amp, gauss_center,
                                    gauss_sigma);
  // r = 0 on success
  double final_scale = Fit::ScaleSimulationIncludingBackground(gData, mc_cal,
                                                               background, bmin,
                                                               bmax);
  if (final_scale <= 0) 
  {
    chi2 = 10000;
  } 
  else 
  {
    chi2 = Fit::CompareHists(gData, mc_cal, "logl", bmin, bmax);
    mc_cal -> SetLineColor(kRed);
    mc_cal -> Draw("SAME HIST");
    gPad -> SetLogy(1);
    gPad -> Update();
  }

  if (currentMC) delete currentMC;
  currentMC = new TH1F(*mc_cal);
  currentMC -> SetDirectory(0);

  if (currentNoise) delete currentNoise;
  currentNoise = new TH1F(*noise_cal);
  currentNoise -> Scale(final_scale);
  currentNoise -> SetDirectory(0);

  if (currentGaus) delete currentGaus;
  currentGaus = new TH1F(*ghist);
  currentGaus -> Scale(final_scale);
  currentGaus -> SetDirectory(0);

  partialMC -> Scale(final_scale);
  cout << nfunc << "\t" << chi2 << "\r";
  cout.flush();
  if (nfunc % 50 == 0) {
    cout << nfunc << "\t" << chi2 << endl;
  }
  nfunc++;
  logstream << std::setprecision(9) << std::fixed << chi2 << endl;
  delete mc_cal;
  delete noise_cal;
  delete mc_clone;
  delete noise_clone;
  delete ghist;

  f = chi2;

  if (iflag == 3) 
  {
    if (!gData) 
    {
      cout << "where'd the data go?" << endl;
    }
    npoints = (double)(bmax - bmin) / (gData -> GetBinWidth(2));
    residuals = Fit::GetResidualHistogram(gData, currentMC, bmin, bmax);
  }
}

#if defined(__MAKECINT__) || defined(__CINT__)
int bb1_fit(bool batch = false, int delta_bmin = 0) {

  gBatch = batch;
  // if (argc > 1) {
  //   for (int i = 0; i < argc; i++) {
  //     if (strcmp(argv[i], "-batch") == 0) {
  //       cout << "Running in BATCH mode" << endl;
  //       gBatch = true;
  //     }
  //   }
  // }
#else
int main(int argc, char *argv[]) 
{
  int delta_bmin = 0;
  gBatch = false;
  app = new TApplication("bb1fit", &argc, argv);
#endif
  
  timer = new TTimer("gSystem->ProcessEvents();", 50, kFALSE);

  string datadir = "~/Documents/Analysis/BB1/Spectra";
  string mcdir = "~/Documents/Analysis/Calibrations/SimulatedSpectra";

  vector<char> ulstring = {'U', 'L'};
  vector<char> xystring = {'X', 'Y'};
  // vector<string> spectail = {"_513_t25-40.root",
  //                            "_513_t25-40_500keVScint.root"};
  vector<string> dset = {"A", "B", "C", "D", "E", "EA", "EB", "EC", "ED",
                         "rmcp", "emcp", "all", "all-cal"};

  vector<string> mctail = {"polarized_pointCloud_566M_interior_bestGeom.root"};
  //vector<string> mctail = {"polarized_pointCloud_395M_interior_550keV.root"};
  //  vector<string> mctail = {"temp.root"};


  int specchoice = -1;
  while (specchoice < 0) {
    cout << "Select data set to process" << endl;
    for (unsigned int i = 0; i < dset.size(); i++) {
      cout << i << "\t" << dset[i] << endl;
    }
    cout << "  Alternatively, enter run number for single run" << endl;
    cin >> specchoice;
  }

  
  int ul = -1;
  while (ul != 0 && ul != 1) {
    cout << "Select detector to process" << endl;
    cout << "0\tU" << endl;
    cout << "1\tL" << endl;
    cin >> ul;
  }

  int xy = -1;
  while (xy != 0 && xy != 1) {
    cout << "Select plane to process" << endl;
    cout << "0\tX" << endl;
    cout << "1\tY" << endl;
    cin >> xy;
  }

  string fname = "";
  if (specchoice < dset.size()) {
    fname = datadir + "/Set" + dset[specchoice];
  } else {
    fname = datadir + "/Run" + to_string(specchoice);
  }
  fname =  fname + "_t25-40_500keVScint.root";
  cout << "File name: " << fname << endl;

  int sn[2] = {-1, 39};
  while (sn[0] < 0 || sn[0] > 39 || sn[1] < 0 || sn[1] > 39 || sn[1] < sn[0]) {
    cout << "Enter minimum and maximum strip number (0-39): " << endl;
    cin >> sn[0] >> sn[1];
  }

  string outdir = "FitResults/";
  if (specchoice < dset.size()) {
    outdir = outdir + "Set" + dset[specchoice] + "Results/";
  } else {
    outdir = outdir + "Run" + to_string(specchoice) + "Results/";
  }

  string noisefile = "~/Documents/Analysis/BB1/noise.root";
  TFile *nf = new TFile(noisefile.c_str());
  gNoise = (TH1F*)nf -> Get("noise");
  //  gNoise -> Rebin(10);
  for (int s = sn[0]; s <= sn[1]; s++) {
    string outfname = "";
    TH1F *data;
    if (dset[specchoice] == "all-cal") {
      outfname = "~/Documents/Analysis/BB1/Results/linearity_all_"
        + string(1,ulstring[ul]) + string(1,xystring[xy]) + "_correctedYX.root";
      string hname = string(1, ulstring[ul]) + string(1, xystring[xy]) + "_DetectorE";
      TFile *f = new TFile(outfname.c_str());
      data = (TH1F*)f->Get(hname.c_str());
    } else {
      outfname = outdir + "result_" + ulstring[ul] + xystring[xy] + "_" +
        to_string(s);
      if (gBatch && delta_bmin > 0) 
        outfname = outfname + "_bminP" + to_string(delta_bmin);
      if (gBatch && delta_bmin < 0)
        outfname = outfname + "_bminM" + to_string(delta_bmin);
      outfname = outfname + ".root";
      data = GetHistogramForStrip(fname, ulstring[ul], xystring[xy], s);
      data -> Rebin(5);  

    }
    if (!data) {
      cout << "Failed" << endl;
      return 0;
    }
    
    int mcchoice = 0;
    while (mcchoice < 0 || mcchoice >= mctail.size()) {
      cout << "Select MC file to read simulation from" << endl;
      for (int i = 0; i < mctail.size(); i++) {
        cout << i << "\t" << mctail[i] << endl;
      }
      //      cin >> mcchoice;
      mcchoice = 0;
    } 

    string mcfilename = mcdir + "/" + mctail[mcchoice];
    char mcname[1024];
    int interior = GetInteriorNumber(s);
    sprintf(mcname, "Interior_%02d", interior);
    cout << "SPECCHOICE: " << endl << dset[specchoice] << endl;
    if (dset[specchoice] == "all-cal") {
      cout << "Special case of already calibrated and corrected spectra"
           << endl;
      mcfilename = "~/Documents/Analysis/BB1/geant_linearity_LX.root";
      sprintf(mcname, "LX_DetectorE");
    }
    TFile *mcfile = new TFile(mcfilename.c_str());

    cout << mcname << endl;
    TH1F *mc = (TH1F*)mcfile -> Get(mcname);
    // TH1F *mc = (TH1F*)mcfile -> Get("UpperSilicon_500keVScint");
    // TH1F *mcl = (TH1F*)mcfile -> Get("LowerSilicon_500keVScint");
    // mc -> Add(mcl);
    if (dset[specchoice] != "all-cal")
      mc -> Rebin(25);
    //    mc -> Smooth(5);

    if ((ul == 0 && xy == 1 && s == 0)  || // UY-00 has no pulses
        (ul == 0 && xy == 1 && s == 8)  || // UY-08 has no spectrum
        // (ul == 0 && xy == 1 && s == 10) || // UY-10 has no spectrum
        // (ul == 0 && xy == 1 && s == 34) || // UY-34 has no spectrum
        // (ul == 0 && xy == 1 && s == 36) || // UY-36 has no spectrum
        // (ul == 0 && xy == 1 && s == 38) || // UY-38 has no spectrum
        (ul == 1 && xy == 1 && s == 32) || // LY-32 has no pulses
        (ul == 1 && xy == 1 && s == 38)) { // LY-38 has no puless)
      cout << "Skipping dead strip" << endl;
    }  else {
      FitSpectrum(data, mc, outfname, ulstring[ul], xystring[xy], s, delta_bmin);
    }
    mcfile -> Close();
    delete mcfile;
    delete data;
  }
  
  //  app -> Run();

  nf -> Close();
  delete nf;
  //  delete app;

  return 0;
}

TH1F* GetHistogramForStrip(string fname, char uORl, char xORy, int n) 
{
  TFile *datfile = new TFile(fname.c_str());
  if (datfile -> IsZombie()) {
    cout << "File " << fname << " is a ZOMBIE. This cannot work" << endl;
    return 0;
  }
  cout << "Got file " << endl;
  char hname[1024] = "";
  sprintf(hname, "BB1_AMPLITUDE_%c%c_%02d", uORl, xORy, n);
  cout << hname << endl;
  TH1F *h;
  if (datfile -> GetListOfKeys() -> Contains(hname)) {
    h = new TH1F(*(TH1F*)(datfile -> Get(hname)));
    h -> SetDirectory(0);
  } else {
    cout << hname << " not found in file" << endl;
    return 0;
  }

  // datfile -> Close();
  // delete datfile;
  return h;;
}

int FitSpectrum(TH1F *data, TH1F *mc_raw, string outfname, char ulstring,
                char xystring, int sn, int delta_bmin) 
{
  nfunc = 0;
  gData = data;
  gMCdata = mc_raw;

  gCan = new TCanvas();
  data -> SetMarkerStyle(kFullDotMedium);
  data -> Draw("E1X0P");
  //  int totalpars  = par_name.size();
  //  cout << "Total parameter #: " << totalpars << endl;
  myMinuit = new TMinuit(12);
  myMinuit -> SetName("current_fit");
  myMinuit -> SetFCN(GetChi2);

  Double_t arglist[10];
  Int_t ierflg = 0;

  myMinuit -> SetPrintLevel(0);
  // arglist[0] = 1.0;    // my logl fcn calculats -2ln(lambda) 
  // myMinuit -> mnexcm("SET ERR", arglist, 1, ierflg);
  
  double max_slope = 1.0 / (mc_raw -> GetBinWidth(2));
  cout << "Derived max_slope = " << max_slope << endl;

  myMinuit -> mnparm(0, "Slope [chan/keV]", 0.4, 0.1 ,  0.01, max_slope,
                     ierflg);
  myMinuit -> mnparm(1, "Offset [chan]",     0., 0.1,  -0.2, bmax/2., ierflg);
  myMinuit -> mnparm(2, "Resolution [keV]", 20.,  0.1, 0.0, 100.0, ierflg);
  myMinuit -> mnparm(3, "Background [1/chan]", 0.2, 0.1, 0.0, 10000.0, ierflg);
  myMinuit -> mnparm(4, "Gauss center [chan]", 12.9, 0.01, 0.0, 30.0, ierflg);
  myMinuit -> mnparm(5, "Gauss sigma [chan]", 4.13, 0.01, 0.0, 100., ierflg);
  myMinuit -> mnparm(6, "Gauss rel. amplitude", 0.05, 0.01, 0.0, 100., ierflg);
  myMinuit -> mnparm(7, "Lin. Bkg. Slope Rel.", 0.1, 0.01, -10000.0, 0.0,
                     ierflg);
  myMinuit -> mnparm(8, "Lin. Bkg. Rel. Amplitude", 0.1, 0.01, 0.0, 1000,
                     ierflg);
  myMinuit -> mnparm(9, "Noise offset [chan]", 2.2, 0.5, 0.0, 100.0, ierflg);
  myMinuit -> mnparm(10, "Noise width", 2.5, 0.5, 1, 10*max_slope,
                     ierflg);
  myMinuit -> mnparm(11, "Noise rel. amplitude", 7., 0.1, 0.0, 100.0, ierflg);
  
  //   This block should just fix some parameters that are usually fixed
   vector<int> params_to_fix = {1, 3, 4, 5, 7, 8};

  for (unsigned int j = 0; j < params_to_fix.size(); j++) {
    myMinuit -> FixParameter(params_to_fix[j]);
  }

  vector<Double_t> gin(12, 0.0);

  // Look for files to copy fit from
  bool found_fit = false;
  cout << outfname << endl;
  vector<string> path_to_search = {outfname, "OrigResults", "SetDResults",
                                   "Run510Results", "Run513Results/OrigSort"};
  vector<string> fext = {"", "_all_", "_D_", "_", ""};
  for (int i = 0; i < path_to_search.size() && !found_fit; i++) 
  {
    if (i == 0 && delta_bmin != 0) continue;
    string fname = path_to_search[0];
    if (i > 0) 
    {
      fname = "FitResults/";
      fname = fname + path_to_search[i] + "/result" + "_" +
          string(1, ulstring) + string(1, xystring) + "_" + to_string(sn) +
          ".root";
    }
    cout << "Searching for " << fname << endl;
    TFile *f = new TFile(fname.c_str());
    if (!f) continue;
    if (f -> IsOpen()) 
    {
      cout << "File found. Searching for fit_result" << endl;
      if (f -> GetListOfKeys() -> Contains("fit_result")) 
      {
        cout << "Got it!" << endl;
        Fit::CopyParameterValues(fname, myMinuit);
        found_fit = true;
      }
      if (f -> GetListOfKeys() -> Contains("limits")) 
      {
        TArrayD *td = (TArrayD*)f->Get("limits");
        bmin = td -> At(0);
        bmax = td -> At(1);
        if (gBatch) 
        {
          bmin = bmin + delta_bmin;
        }
      }
      f -> Close();
    }
    delete f;
  }

  DrawFCN(myMinuit);
  bool goodIn = false;
  bmin = 0;
  bmax = 300;
  while (!goodIn) 
  {
    cout << "Enter bmin and bmax" << endl;
    timer -> TurnOn();
    timer -> Reset();
    cout << "Current bmin, bmax = " << bmin << "\t" << bmax << endl;
    cin >> bmin >> bmax;
    if (bmin > -1 && bmax > bmin) goodIn = true;
    timer -> TurnOff();
  }

  if (!gBatch) 
  {
    AdjustMINUIT(myMinuit);
  }

  int doScan = -1;
  cout << "Skip MIGRAD and simply scan parameter?" << endl;
  cin >> doScan;
  if (doScan == 1) 
  {
    cout << "Enter parameter to scan..." << endl;
    cin >> doScan;
    double v, e;
    myMinuit -> GetParameter(doScan-1, v, e);
    Double_t from = v - (2.*e);
    Double_t to   = v + (2.*e);
    char scancmd[1024];
    sprintf(scancmd, "SCAN %d 40 %f %f", doScan, from, to);
    cout << scancmd << endl;
    cout << "Delta = " << fabs(to-from)/40. << endl;
    myMinuit -> Command(scancmd);
    TGraph *gr = (TGraph*)myMinuit->GetPlot(); 

    TCanvas *tcan = new TCanvas();
    tcan -> cd();
    gr -> Draw("A*");
    app -> Run();
    return 0;
  }

  logstream.open(".log.txt", std::ios::out);    
  for (int p = 0; p < myMinuit -> GetNumPars(); p++) 
  {
    TString cn = "";
    Double_t val, err, xlo, xup;
    Int_t iint;
    myMinuit -> mnpout(p, cn, val, err, xlo, xup, iint);

    if (err > 0.0) {
      logstream << cn << "\t";
    }
  }
  logstream << "chi2" << endl;

  cout << endl << "See log file .log.txt for progress" << endl;
  cout << "Iteration \t chi2" << endl;

  //  Adjust machine precision with which MINUIT operates
  arglist[0] = 5*pow(10, -6);
  myMinuit -> mnexcm("SET EPS", arglist, 1, ierflg);
  

  /* Manually call function for debugging purposes */
  // double pars[3] = {0.4, 0.1, 20.};
  // Double_t grad, func;
  // myMinuit -> Eval(3, &grad, func, pars, 0);
  // cout << "Eavlue f = " << func << endl;


  arglist[0] = 500;                    // Force stop after ~500 calls
  arglist[1] = 100.;                  // Stop when EDM < 0.1
  cout << "BMIN: " << bmin << endl;
  cout << "BMAX: " << bmax << endl;
  myMinuit -> SetPrintLevel(1);
  myMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

  //  gPad -> Update();

  if (ierflg == 4) {                    // failed
    cout << "MINIMIZATION FAILED!!! " << endl;
    cout << "Attempting MC minimization" << endl;
    arglist[0] = 100;
    // myMinuit -> mnexcm("SEEK", arglist, 1, ierflg);
    // cout << "SEEK returned " << ierflg << endl;


    arglist[0] = 500;                    // Force stop after ~500 calls
    arglist[1] = 100.;                  // Stop when EDM < 0.1
    cout << "BMIN: " << bmin << endl;
    cout << "BMAX: " << bmax << endl;

    myMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

    if (ierflg == 4) {
      arglist[0] = pow(10, -5);
      myMinuit -> mnexcm("SET EPS", arglist, 1, ierflg);
      arglist[0] = 500;                    // Force stop after ~500 calls
      arglist[1] = 100.;                  // Stop when EDM < 0.1
      myMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);
    }
  
  }
  int keep_going = 1;

  if (ierflg != 4 && !gBatch) {
    DrawFCN(myMinuit);
    cout << "Accept this fit or change something? (1 = accept, anything else rejects)" << endl;
    timer -> TurnOn();
    timer -> Reset();
    cin >> keep_going;
    if (keep_going != 1) {
      ierflg = 4;
      keep_going = 1;
    }
    timer -> TurnOff();
  }

  keep_going = 0;
  while (ierflg == 4 && keep_going == 1 && !gBatch) {

    int choice;
    cout << "Keep trying for new minimum? (1 = yes, otherwise no)" << endl;
    cin >> keep_going;
    if (keep_going != 1) continue;

    myMinuit -> mnmatu(1);
    AdjustMINUIT(myMinuit);

    // arglist[0] = 2;
    // myMinuit -> mnexcm("SET STR", arglist, 1, ierflg);

    cout << "EXECUTE SEEK? (1 = yes, otherwise no)" << endl;
    cin >> choice;
    if (choice == 1) {
      cout << "Attempting MC minimization" << endl;
      arglist[0] = 100;
      myMinuit -> mnexcm("SEEK", arglist, 1, ierflg);
    }

    cout << "Change fitting range? (1 = yes, otherwise no)" << endl;
    cin >> choice;
    if (choice == 1) {
      cout << "Enter min and max channels: " << endl;
      cin >> bmin >> bmax;
    }

    cout << "Change EPS (floating point precision)? (1 = yes, otherwise no)"
         << endl;
    cin >> choice;
    if (choice == 1) {
      double eps = 1.0;
      while (eps >= 0) {
        cout << "Enter log of machine precision (<0)" << endl;
        cin >> eps;
      }
      arglist[0] = pow(10, eps);
      myMinuit -> mnexcm("SET EPS", arglist, 1, ierflg);
    }

    cout << "Change goal EDM? (1 = yes, otherwise no)" << endl;
    cin >> choice;
    if (choice == 1) {
      double edm = 10;
      while (edm >= 1.0) {
        cout << "Enter new goal EDM" << endl;
        cin >> edm;
      }
      arglist[1] = edm*1000.0; 
    }

    arglist[0] = 500.0;         // Force stop after ~500 calls
    cout << "BMIN: " << bmin << endl;
    cout << "BMAX: " << bmax << endl;
    myMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

    if (ierflg != 4 && !gBatch) {
      DrawFCN(myMinuit);
      cout << "Accept this fit or change something? (1 = accept, anything else rejects)" << endl;
      timer -> TurnOn();
      timer -> Reset();
      cin >> keep_going;
      if (keep_going != 1)
        ierflg = 4;
      timer -> TurnOff();
    }
  }
  int final_status = ierflg;
  /* PERFORM MINOS ERROR ANALYSI
     arglist[0] = 100;
     myMinuit->mnexcm("MINOS", arglist, 0, ierflg);
  */

  /* Make 2D chi2 map once minimum has been found
     myMinuit -> Release(1);
     TGraph *g = (TGraph*)myMinuit -> Contour();
     g -> Draw("A*");
  */

  /* Scan 1D over a parameter to "explore the space" */
     // myMinuit -> FixParameter(0);
     // myMinuit -> FixParameter(1);
     // myMinuit -> Command("SCAN 3 50 15 25.");
     // TGraph *gr = (TGraph*)myMinuit->GetPlot(); 
     // gr -> Draw("A*");
     // gPad -> SetLogy(0);
     

  myMinuit->mnmatu(1);                                  // error matrix
  logstream.close();

  DrawFCN(myMinuit);

  //  cout << "Npoints = " << npoints << endl;
  // cout << "Final exit status " << final_status << endl;
  // for (int i = 1; i <= mc_cal -> GetNbinsX(); i++) {
  //   double bc = mc_cal -> GetBinCenter(i);
  //   if (bc < bmin || bc > bmax) mc_cal -> SetBinContent(i, 0.0);
  // }
  //  gPad -> SetLogy(1);

  int choice = 0;
  if (!gBatch) {
    
    cout << "Save result to " << outfname << " (1 = yes, otherwise no)" << endl;
    cin >> choice;
  }
  if (choice != 1 && !gBatch) {
    cout << "Enter new filename? (1 = yes, otherwise no)"  << endl;
    cin >> choice;
    if (choice == 1) {
      cout << "Enter new filename" << endl;
      cin >> outfname;
    }
  }
  int rval = 0;
  if (choice == 1 || gBatch) {
    rval = SaveFit(outfname, myMinuit, gData, gMCdata, currentMC,
                       residuals, bmin, bmax, myMinuit -> fAmin, npoints,
                   final_status, currentNoise, currentGaus);
  }
  delete gCan;
  delete myMinuit;
  return rval;
}
