// Author: Benjamin Fenker
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <exception>
#include <fstream>
#include <iostream>
#include <math.h>

// ROOT includes
#include <TFile.h>
#include <TH1F.h>
#include <TMinuit.h>

// boost includes
//#include <boost/algorithm/string.hpp>


//#include "units.h"
#include "fit.h"

enum 
{
  success = 0,
  bad_file = 1,
  read_error = 2,
  no_counts_in_range = 3
};

double Fit::ApplyCalibration_EtoChan(double E, double m, double b) 
{
  return b + (m*E);
}

TH1F* Fit::CalibrateAndMatchBinning(TH1 *hist_to_cal, double slope,
                                    double offset, TH1 *hmatch) 
{
  int verbose = 0;
  TH1F *newHist = new TH1F("calibrated hist", "calibrated hist",
                           hmatch -> GetNbinsX(), hmatch -> GetBinLowEdge(1),
                           hmatch -> GetBinLowEdge(hmatch -> GetNbinsX()) +
                           hmatch -> GetBinWidth(hmatch -> GetNbinsX()));
  std::vector<int> times_filled(newHist -> GetNbinsX() + 2, 0);
  for (int i = 1; i <= hist_to_cal -> GetNbinsX(); i++) {
    double E = hist_to_cal -> GetBinCenter(i);
    double Q = ApplyCalibration_EtoChan(E, slope, offset);
    
    int bin = newHist -> FindBin(Q);
    if (bin == newHist -> GetNbinsX() + 1) {
      break;
    }
    newHist -> SetBinContent(bin, newHist->GetBinContent(bin) +
                                  hist_to_cal -> GetBinContent(i));

    times_filled[bin]++;
    if (verbose > 0) {
     std::cout << "Energy " << E << " keV --> channel " << Q << " , bin "
               << bin << ", times filled: " << times_filled[bin] << std::endl;
    }
  }
  for (int i = 1; i <= newHist -> GetNbinsX(); i++) {
    if (times_filled[i] > 0) {
      newHist ->
          SetBinContent(i, newHist -> GetBinContent(i) / times_filled[i]);
    } else {
      newHist -> SetBinContent(i, 0.0);
    }
    newHist -> SetBinError(i, 0.0);
  }
  return newHist;
}


int Fit::FillHistFromFile(TH1F *hist_to_fill, std::string fname,
                                 double tscale, std::string option,
                                 double toffset) 
{
  // boost::algorithm::to_lower(option);

  // std::ifstream ifs(fname, std::ifstream::in);
  // if (!ifs.is_open()) {
  //   return bad_file;
  // }

  
  // std::string line;
  // std::vector<std::string> word;
  // std::vector<int> times_filled(hist_to_fill -> GetNbinsX() + 1, 0);
  // for (int i = 1; i <= hist_to_fill -> GetNbinsX(); i++) 
  //   hist_to_fill -> SetBinContent(i, 0.0);

  // while (std::getline(ifs, line)) {
  //   while (line.c_str()[0] == ' ') line.erase(0, 1);
  //   //    std::cout << line << std::endl;
  //   boost::split(word, line, boost::is_any_of(" \t"));
  //   //    std::cout << word[0] << "\t" << word[1] << std::endl;
  //   double time, fluor;
  //   try {
  //     time = tscale * stod(word[0]);
  //     //      fluor = stod(word[word.size()-1]);
  //     fluor = stod(word[1]);
  //   }
  //   catch (...) {
  //     std::cout << "Error converting to double in file " << fname << std::endl;
  //     return read_error;
  //   }
  //   // Adjust time
  //   time = time + toffset;
  //   //        std::cout << time << "\t" << fluor << std::endl;
  //   int binn = hist_to_fill -> FindBin(time);
  //   hist_to_fill ->
  //       SetBinContent(binn, hist_to_fill->GetBinContent(binn) + fluor);
  //   times_filled[binn]++;
  // }
  // ifs.close();
  // for (int i = 1; i <= hist_to_fill -> GetNbinsX(); i++) {
  //   if (times_filled[i] > 0) {
  //     hist_to_fill ->
  //         SetBinContent(i, hist_to_fill -> GetBinContent(i) / times_filled[i]);
  //   } else {
  //     hist_to_fill -> SetBinContent(i, 0.0);
  //   }
  //   //    hist_to_fill -> SetBinContent(i, hist_to_fill -> GetBinContent(i));
  //   hist_to_fill -> SetBinError(i, 0.0);
  //   if (option == "rate") {
  //     hist_to_fill -> SetBinContent(i, hist_to_fill -> GetBinContent(i) / 
  //                                   hist_to_fill -> GetBinWidth(i));
  //     // hist_to_fill -> SetBinError(i, hist_to_fill -> GetBinError(i) /
  //     //                             hist_to_fill -> GetBinWidth(i));
  //   }
  // }
  
  return success;
}

TH1F* Fit::GetResidualHistogram(TH1F *data, TH1F *model,
                                  double min, double max) 
{
  if (!data) {
    std::cout << "Data histogram must be initialized" << std::endl;;
    return NULL;
  }
  if (!model) {
    std::cout << "Model histogrma must be initialized" << std::endl;;
    return NULL;
  }
  if (max <= min) {
    std::cout << "Max must be greater than min" << std::endl;;
    max = data -> GetBinLowEdge(data -> GetNbinsX()) +
        data -> GetBinWidth(data -> GetNbinsX());
    min = data -> GetBinLowEdge(0);
  }
  if (!HistsHaveSameBinning(data, model)) {
    std::cout << "ERROR: CANNOT COMPARE HISTS " << data -> GetName() << " and ";
    std::cout << model -> GetName() << " MUST RETURN 0.0 CHI2" << std::endl;
    return NULL;
  }
  //  std::cout << "Creating residuals " << std::endl;
  TH1F *residuals = new TH1F("residuals", "residuals", data -> GetNbinsX(),
                             data -> GetBinLowEdge(1),
                             data -> GetBinLowEdge(data -> GetNbinsX()) +
                             data -> GetBinWidth(data -> GetNbinsX()));
  for (int i = 0; i <= data -> GetNbinsX(); i++) {
    if (data -> GetBinLowEdge(i) < min) continue;
    if (data -> GetBinLowEdge(i) + data -> GetBinWidth(i) > max) continue;
      double sim = model -> GetBinContent(i);
      double exp = data -> GetBinContent(i);
      //      double err = data -> GetBinError(i);
      double r = 0.0;
      if (sim > 0) {
        r = (exp - sim) / sqrt(sim);
      }
      residuals -> SetBinContent(i, r);
      residuals -> SetBinError(i, 0.);
  }
  return residuals;
}

double Fit::CompareHists(TH1F *data, TH1F *model, std::string option,
                                double min, double max) //gets us a chi2.
{
  int verbose = 0;
  if (verbose > 0) 
  {
    std::cout << "Center\tSimulation\tData\tUncert.\tchi2" << std::endl;
  }
  std::transform(option.begin(), option.end(), option.begin(), ::tolower);
  if (!HistsHaveSameBinning(data, model)) 
  {
    std::cout << "ERROR: CANNOT COMPARE HISTS " << data -> GetName() << " and ";
    std::cout << model -> GetName() << " MUST RETURN 0.0 CHI2" << std::endl;
    return 0.0;
  }
  if (option == "logl") 
  {
    double logl = 0.0;
    double t = 0.0;
    for (int i = 0; i <= data -> GetNbinsX(); i++) {
      if (data -> GetBinLowEdge(i) < min) continue;
      if (data -> GetBinLowEdge(i) + data -> GetBinWidth(i) > max) continue;
      double sim = model -> GetBinContent(i);
      double exp = data -> GetBinContent(i);

      if (sim > 0) {
        if (exp > 0) {
          t = (sim - exp + exp*log(exp / sim));
        } else {                        // exp <= 0
          t = sim;
        }
      } else {                          //  sim < 0
        t = 0.0;
      }
      if (verbose > 0) {
        std::cout << data -> GetBinCenter(i) << "\t" << sim << "\t"
                  << exp << "\t" << t << std::endl;
      }

      // Multiply by two to get error definitions right
      t = t * 2.0;

      logl = logl + t;
    }
    int j;
    if (verbose > 0) {
      std::cout << "Enter any number to continue...";
      std::cin >> j;
    }
    return logl;
  } 
  else // chi^2
  {
    double chi2 = 0.0;
    double t = 0.0;
    for (int i = 0; i <= data -> GetNbinsX(); i++) 
    {
      if (data -> GetBinLowEdge(i) < min) continue;
      if (data -> GetBinLowEdge(i) + data -> GetBinWidth(i) > max) continue;
      double sim = model -> GetBinContent(i);
      double exp = data -> GetBinContent(i);
      double err = data -> GetBinError(i);

      t = exp - sim;
      if (err > 0.0) 
      {
        t = t / (err);
      }
      t = t * t;
      if (verbose > 0) 
      {
        std::cout << data -> GetBinCenter(i) << "\t" << sim << "\t"
                  << exp << "\t" << err << "\t" << t << std::endl;
      }
      chi2 = chi2 + t;
    }
    return chi2;
  }
}

bool Fit::HistsHaveSameBinning(TH1F *a, TH1F *b) 
{
  bool same = true;
  double eps = 1.E-3;
  if (!a || !b) {
    std::cout << "ERROR: Histogram doesn't exist" << std::endl;
    return false;
  }
  if (a -> GetNbinsX() != b -> GetNbinsX()) return false;

  if (same) {
    for (int i = 1; i <= a -> GetNbinsX(); i++) {
      if (fabs(a->GetBinCenter(i) - b->GetBinCenter(i)) > eps) same = false;
    }
  }

  return same;
}

double Fit::ScaleSimulationIncludingBackground(TH1F *data, TH1F *model,
                                               double bkg_per_bin, double start,
                                               double stop) 
{
  double scale_num = (data -> Integral(data -> FindBin(start),
                                       data -> FindBin(stop)));
  int nbins = data -> FindBin(stop) - data -> FindBin(start);

  scale_num = scale_num - (bkg_per_bin * nbins);
  double scale_den = model -> Integral(model -> FindBin(start),
                                       model -> FindBin(stop));
  if (scale_den <= 0.0) {
    std::cout << "ERROR: UNSCALED MODEL HAS NO COUNTS IN RANGE" << std::endl;
    return no_counts_in_range;
  }
  double scale = scale_num / scale_den;
  //  scale = 2633.;
  model -> Scale(scale);
  for (int i = 1; i < model -> GetNbinsX(); i++) {
    model -> SetBinContent(i, model -> GetBinContent(i) + bkg_per_bin);
  }
  return scale;;
}

void Fit::ScaleSimulationIncludingSNratio(TH1F *data, TH1F *model,
                                          double sn_ratio, double start,
                                          double stop) 
{
  int nbins = data -> FindBin(stop) - data -> FindBin(start);
  double ntot = (data -> Integral(data -> FindBin(start),
                                  data -> FindBin(stop)));
  double bkg_per_bin = (ntot / nbins) / (sn_ratio + 1.0);
  // std::cout << "Ntot: " << ntot << " nbins: " << nbins << " sn_ratio: "
  //           << sn_ratio << " bkg_per_bin: " << bkg_per_bin << std::endl;

  ScaleSimulationIncludingBackground(data, model, bkg_per_bin, start, stop);
}

int Fit::AddXaxisJitter(TH1F *model, double sigma, bool preserve_area,
                           double from_here, double to_here) 
{
  if (sigma < 1E-6) return 0;
  int debug = 0;

  double initial_area = model -> Integral(model -> FindBin(from_here),
                                          model -> FindBin(to_here));
  TH1F *theclone = new TH1F(*model);

  TF1 *gaussfcn = new TF1("gauss", "gaus", model -> GetXaxis() -> GetXmin(),
                          model -> GetXaxis() -> GetXmax());
  gaussfcn -> SetParameter(2, sigma);   // parameter 2 is sigma

  for (int i = 1; i < model -> GetNbinsX(); i++) {
    double content = 0.0;
    if (debug > 1) {
      std::cout << "Considering bin " << i << " at " << model -> GetBinCenter(i)
                << std::endl;
    }

    // Restrict to only bins within 5-sigma of target
    int center = model -> GetXaxis() -> GetBinCenter(i);
    int bmin = model -> FindBin(center - 5.0*sigma);
    int bmax = model -> FindBin(center + 5.0*sigma);
    bmin = std::max(0, bmin);
    bmax = std::min(model -> GetNbinsX(), bmax);
    int nfills = 0;
    for (int b = bmin; b < bmax; b++) {
      // parameter 0 is constant
      gaussfcn -> SetParameter(0, theclone -> GetBinContent(b));
      // parameter 1 is mean (middle)
      gaussfcn -> SetParameter(1, theclone -> GetXaxis() -> GetBinCenter(b));
      
      double contrib = gaussfcn -> Eval(model -> GetXaxis() -> GetBinCenter(i));
      if (debug > 1) {
        std::cout << "Contribution from bin " << b << " (center = "
                  << gaussfcn -> GetParameter(1) << ", content = "
                  << gaussfcn -> GetParameter(0) << ") is " << contrib << std::endl;
      }
      content = content + contrib;
      nfills++;
    }
    if (debug > 0) {
      std::cout << "Total bin content is " << content << std::endl;
      int j;
      std::cin >> j;
    }
    content = content / nfills;
    model -> SetBinContent(i, content);
    }
    
  double final_area = model -> Integral(model -> FindBin(from_here),
                                        model -> FindBin(to_here));
  if (preserve_area) {
    model -> Scale(initial_area/final_area);
  }

  delete theclone;
  delete gaussfcn;
  return 0;
}

TH1F* Fit::AddGaussToHist(TH1F *hist, double total_integral, double mean,
                        double sigma) 
{
  TH1F *newHist = new TH1F("gauss hist", "gauss hist",
                           hist -> GetNbinsX(), hist -> GetBinLowEdge(1),
                           hist -> GetBinLowEdge(hist -> GetNbinsX()) +
                           hist -> GetBinWidth(hist -> GetNbinsX()));
  
  if (fabs(sigma) < 1E-6 || total_integral < 1E-6) return newHist;
  

  TF1 *g = new TF1("graussfcn", "gaus(0)", hist -> GetBinLowEdge(1),
                   hist -> GetBinLowEdge(hist -> GetNbinsX()) +
                   hist -> GetBinWidth(hist -> GetNbinsX()));
  g -> SetParameters(1.0, mean, sigma);

  for (int i = 1; i <= newHist -> GetNbinsX(); i++) {
    double c = newHist -> GetBinCenter(i);
    double e = g -> Eval(c);
    newHist -> SetBinContent(i, e);
  }

  if (newHist -> Integral() < 1E-6) return newHist;
  
  newHist -> Scale(total_integral/newHist -> Integral());
  hist -> Add(newHist);
  //  delete newHist;
  delete g;
  return newHist;
}

Double_t Fit::LineWithArea(Double_t *xp, Double_t *par) 
{
  Double_t x = xp[0];
  Double_t m = par[0];
  Double_t N = par[1];

  if (m >= 0.0 || N <= 0.0) {
    return 0.0;
  }
  double cutoff = sqrt(-2.0*N/m);
  double b = sqrt(-2.0*m*N);
  //  std::cout << "b = " << b << std::endl;
  if (x <= cutoff) {
    return (m*x) + b; 
  } else {
    return 0.0;
  }
}

int Fit::AddLinearBackground(TH1F *hist, double total_integral, double slope) 
{
  if (slope >= 0.0 || total_integral <= 0.0) return 0;
  std::cout << "Adding background..." << std::endl;
  std::cout << "Rel. amplitude: " << hist -> Integral() / total_integral << std::endl;
  TH1F *newHist = new TH1F("lin hist", "lin hist",
                           hist -> GetNbinsX(), hist -> GetBinLowEdge(1),
                           hist -> GetBinLowEdge(hist -> GetNbinsX()) +
                           hist -> GetBinWidth(hist -> GetNbinsX()));
  TF1 *g = new TF1("linfcn", Fit::LineWithArea, 0, 1000, 2);
  g -> SetParameters(slope, total_integral);
  for (int i = 1; i <= newHist -> GetNbinsX(); i++) {
    double c = newHist -> GetBinCenter(i);
    double e = g -> Eval(c);
    newHist -> SetBinContent(i, e);
  }
  hist -> Add(newHist);
  delete newHist;
  delete g;
  return 0;
}

void Fit::CopyParameterValues(std::string fname, TMinuit *tm) 
{
  std::cout << "Copying paramter values from " << fname << std::endl;

  // Get tm's parameter names
  TString cn = "";
  Double_t val, err, xlo, xup;
  Int_t iint;

  std::vector<TString> par_name(tm -> GetNumPars(), "");
  std::vector<double> par_llim(tm -> GetNumPars(), 0.);
  std::vector<double> par_ulim(tm -> GetNumPars(), 0.);

  for (int p = 0; p < tm -> GetNumPars(); p++) {
    tm -> mnpout(p, cn, val, err, xlo, xup, iint);
    par_name[p] = cn;
    par_llim[p] = xlo;
    par_ulim[p] = xup;
  }

  TFile *f = new TFile(fname.c_str());
  TMinuit *oldFit = (TMinuit*)f->Get("fit_result");
  std::cout << oldFit << std::endl;
  std::cout << tm << std::endl;
  Int_t ierflg = 0;
  for (int p = 0; p < oldFit -> GetNumPars()+2; p++) {
    oldFit -> mnpout(p, cn, val, err, xlo, xup, iint);
    std::cout << "Parameter " << cn << " = " << val << "\t+/-"
              << err << " ---> ";
    unsigned int thisn = find(par_name.begin(), par_name.end(), cn) -
        par_name.begin();
    if (thisn >= par_name.size()) {     // not found
      std::cout << "Not found" << std::endl;
      continue;
    }
    std::cout << " Local number " << thisn+1 << std::endl;
    if (err < 1E-6) {
      tm -> mnparm(thisn, cn, val, 1E-5,
                        par_llim[thisn], par_ulim[thisn], ierflg);
      tm -> FixParameter(thisn);
      
    } else {
      tm -> mnparm(thisn, cn, val, err,
                        par_llim[thisn], par_ulim[thisn], ierflg);
    }
  }
  f -> Close();
  delete f;
  delete oldFit;
}
