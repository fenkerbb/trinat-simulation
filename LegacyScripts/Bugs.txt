Bug Report!

* Chamber geometry change stuff -- you can adjust it if you put it into the vis.mac file, but it doesn't seem to update (or maybe the drawing just doesn't update??) if you type something like "/K37/geometry/setMakeHoops false" into the command line within K37 itself.

* Electric field change stuff -- this one has the opposite issue:  it can be changed from the command line within K37, but the program will die horribly if I try to change it from the vis.mac file, using something like "/K37/field/setFieldY 39500" -- it's fine, just has to go after "/run/initialize".

* charge state distribution stuff -- they're all +1's by default.  /K37/gun/setRecoilCharge ; Available charge states: +1 -> +3.  No neutrals.  Need higher charge states too.  ...that's not really true.  "0" is ok.

* Event acceptance -- I can't tell from looking at individual events what criteria it uses to accept/reject events.  


* Settings script stuff -- Sometimes editing vis.mac makes everything crash horribly.  Can't narrow down the cause yet, because it's all stuff that I don't think should produce that effect.  Maybe it cares about order?  Also, doesn't like comments to start mid-line.  Or a space right after the "#".  Seems to be okay (afer all!) with a vis.mac that's created *before* K37. ...or maybe it does!  it's ok if it's modified afterwards. Sometimes it doesn't like commented lines where everything seems fine.

Here's the output:

/Users/spiffyzha/Desktop/Trinat_Geant/build$ ./K37

*************************************************************
 Geant4 version Name: geant4-09-06-patch-04    (30-January-2015)
                      Copyright : Geant4 Collaboration
                      Reference : NIM A 506 (2003), 250-303
                            WWW : http://cern.ch/geant4
*************************************************************

For electric field tracking...G4CashKarpRKF45 is called
rho = 0.576468
/Users/spiffyzha/Desktop/Trinat_Geant/build/configurationFiles/
   0 : scintillatorPlusZ
   1 : scintillatorMinusZ
   2 : dsssdPlusZ
   3 : dsssdMinusZ
Available UI session types: [ GAG, tcsh, csh ]
error: xp_attach_gl_context returned: 2
X Error of failed request:  0
  Major opcode of failed request:  149 (GLX)
  Minor opcode of failed request:  26 (X_GLXMakeContextCurrent)
  Serial number of failed request:  34
  Current serial number in output stream:  34
/Users/spiffyzha/Desktop/Trinat_Geant/build$ 
